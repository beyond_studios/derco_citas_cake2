delimiter $$

-- drop table chevrolets$$

CREATE TABLE `chevrolets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vim` varchar(255) DEFAULT null,
  `placa` varchar(255) DEFAULT null,
  `fecha_base` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8$$

delimiter ;
