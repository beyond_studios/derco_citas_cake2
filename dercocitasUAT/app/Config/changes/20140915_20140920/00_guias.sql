## PASOS HA SEGUIR 
 * Correr script en el siguientes Orden
	1.- 01_change_db.sql
	2.- 02_insert_chevrolets.sql
	

## NUEVOS LINK
http://localhost/dercosap_20140912/Chevrolets/index


#########################################################
## ESTA PARTE SOLO SIRVE DE REFERENCIA NO TOCAR
## generando insert 
delimiter $$

drop table chevrolets_tmp$$
CREATE TABLE `chevrolets_tmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vim` varchar(255) DEFAULT NULL,
  `placa` varchar(255) DEFAULT NULL,
  `fecha_base` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8$$

delimiter ;

LOAD DATA INFILE '/AVENTURA/BASEV2.csv'
INTO TABLE chevrolets_tmp
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
(vim, placa, fecha_base);

SET @numero=19980;
update chevrolets_tmp set id = @numero:=@numero+1 WHERE 1=1;

SELECT
	CONCAT('insert into chevrolets (id, vim,placa,fecha_base) values (',id,",'",vim,"','",placa,"','", 
  SUBSTRING(fecha_base,7,4),'-',SUBSTRING(fecha_base,4,2),'-',SUBSTRING(fecha_base,1,2), "');")
FROM
	chevrolets_tmp
WHERE
1=1
-- Aqui viene la exportación
INTO OUTFILE
'C:/AVENTURA/BASEV2_1.csv'
FIELDS TERMINATED BY ';'
OPTIONALLY ENCLOSED BY '\"'
LINES TERMINATED BY '\r\n';

