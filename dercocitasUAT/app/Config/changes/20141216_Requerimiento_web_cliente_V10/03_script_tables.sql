delimiter $$
begin $$

-- CREAMOS LA TABLA MODELO
DROP TABLE IF EXISTS modelos $$
DROP TABLE IF EXISTS servicios $$
DROP TABLE IF EXISTS campanias $$
DROP TABLE IF EXISTS agedetallecitaslosts $$

delimiter $$
DROP TABLE IF EXISTS modelos $$
CREATE TABLE `modelos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) CHARACTER SET latin1 NOT NULL,
  `material` varchar(100) CHARACTER SET latin1 NOT NULL,
  `marca_codigo` varchar(20) CHARACTER SET latin1 NOT NULL references marcas(codigo),
  `status` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT 'AC',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 $$

CREATE TABLE `servicios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(50) DEFAULT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `binario` longblob DEFAULT NULL,
  `anchura` smallint(6) NOT NULL default 200,
  `altura` smallint(6) NOT NULL default 80,
  `agemotivoservicio_id` int(11) NOT NULL REFERENCES `agemotivoservicios` (`id`),
  `description` varchar(250) CHARACTER SET latin1 NOT NULL,
  `status` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT 'AC',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 $$
  
CREATE TABLE `campanias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(50) DEFAULT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `binario` longblob DEFAULT NULL,
  `anchura` smallint(6) NOT NULL default 200,
  `altura` smallint(6) NOT NULL default 80,
  `url` varchar(250),
  `description` varchar(250) CHARACTER SET latin1 NOT NULL,
  `status` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT 'AC',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 $$

CREATE TABLE `agedetallecitaslosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id` int(11) NOT NULL REFERENCES `clientes` (`id`),
  `ageclientesVehiculo_id` int(11) NOT NULL REFERENCES `ageclientes_vehiculos` (`id`),
  `placa` varchar(255) CHARACTER SET latin1 NOT NULL,
  `marca` varchar(255) CHARACTER SET latin1 NOT NULL,
  `modelo` varchar(255) CHARACTER SET latin1 NOT NULL,
  `agetiposervicio_id` int(11) NOT NULL REFERENCES `agetiposervicios` (`id`),
  `otrosServicios` varchar(255)  DEFAULT NULL,
  `motivonocita_id` int(11) NOT NULL,
  `tipoMantenimiento` varchar(45)  DEFAULT NULL,
  `createdsecperson_id` int(11) DEFAULT NULL,
  `fechaRegistro` date NOT NULL,
  `horaRegistro` time DEFAULT '00:00:00',
  `agemotivoservicio_id` int(11) DEFAULT NULL,
  `secproject_id` int(11) DEFAULT NULL,
  `agetipomantenimiento_id` int(11) DEFAULT NULL,
  `apellidoPaterno` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `documento_tipo` varchar(20) CHARACTER SET latin1 NOT NULL,
  `documento_numero` varchar(20) CHARACTER SET latin1 NOT NULL,
  `cliente_tipo` int(11) DEFAULT NULL,
  `str_cliente_tipo` varchar(60) CHARACTER SET latin1 NOT NULL,
  `distrito` varchar(60) CHARACTER SET latin1 NOT NULL,
  `email` varchar(250) CHARACTER SET latin1 NOT NULL,
  `telefono` varchar(20) CHARACTER SET latin1 NOT NULL,
  `celular` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
$$

commit $$
delimiter ;
