CREATE OR REPLACE VIEW `viewccplavadoseguimientos` AS 
select `ot`.`nrocars` AS `OT`,
	`ot`.`descripcion` AS `DESCRIPCION`,
	`ot`.`codasesor` AS `USUARIO_OT`,
	 '' AS `NOMBRE_ASESOR`,   -- `ot`.`nombreasesor`
	`ot`.`ordenfecha` AS `FECHA`,
	`ot`.`ordenhora` AS `HORA_RECIBIDA`,
	`ot`.`horaprometida` AS `HORA_PROMETIDA`,
	`ot`.`tipoorden` AS `TIPO_OT`,
	`ot`.`DocPreOtGe` AS `PRESUP_GEN`,
	`ot`.`nrocono` AS `NRO_CONO`,
	`ot`.`fechaprometida` AS `FECHA_PROM`,
	`ot`.`horaprometida` AS `HORA_PROM`,
	`ot`.`propietario` AS `CLIENTE`,
	`ot`.`chassis` AS `NRO_SERIE`,
	`ot`.`placa` AS `PLACA`,
	`ot`.`marca` AS `MARCA`,
	`ot`.`modelo` AS `MODELO`,
	'' AS `VERSION`,    -- `ot`.`version`
	`ot`.`codasesor` AS `USUARIO_PRE`,
	`ot`.`DocAseFchL` AS `FECHA_PRES`,
	`ot`.`DocAseFchL` AS `FECHA_PRESUP`,
	`ot`.`DocAseFch` AS `FECHA_APROB_ASEG`,
	`ot`.`empresacars` AS `COMPANIA`,
	`ot`.`codlocal` AS `TALLER`,
	`ot`.`nrocars` AS `PRESUPUESTO`,
	`ot`.`estado` AS `OT_ESTADO`,
	(case when (`ot`.`DocAseFch` is not null) then _utf8'4' when (`ot`.`DocAseFchL` is not null) then _utf8'3' else _utf8'1' end) AS `OT_PRE_ESTADO`,
	`ot`.`color` AS `COLOR`,
    `ot`.`ClasePedido` AS `CLASE_PEDIDO`,
	`ccplavadoseguimiento`.`id` AS `seg_id`,
	`ccplavadoseguimiento`.`secperson_lavador_id` AS `secperson_lavador_id`,
	`ccplavadoseguimiento`.`secperson_coordinador_id` AS `secperson_coordinador_id`,
	`ccplavadoseguimiento`.`secperson_jefetaller_id` AS `secperson_jefetaller_id`,
	`ccplavadoseguimiento`.`fecha_lavador` AS `fecha_lavador`,
	`ccplavadoseguimiento`.`comentario_lavado` AS `seg_comentario_lavado`,
	`ccplavadoseguimiento`.`comentario_coordinador` AS `seg_comentario_coordinador`,
	`ccplavadoseguimiento`.`comentario_jefetaller` AS `seg_comentario_jefetaller`,
	`ccplavadoseguimiento`.`nro_seguimiento` AS `nro_seguimiento`,
	`ccplavadoseguimientosestado`.`id` AS `segestado_id`,
	`ccplavadoseguimientosestado`.`descripcion` AS `segestado_descripcion`,
	`ccplavadoseguimientomotivo`.`id` AS `segmotivo_id`,
	`ccplavadoseguimientomotivo`.`descripcion` AS `segmotivo_descripcion`
from (((`talots` `ot` left join `ccplavadoseguimientos` `ccplavadoseguimiento` on((`ccplavadoseguimiento`.`talot_id` = `ot`.`id`))) left join `ccplavadoseguimientosestados` `ccplavadoseguimientosestado` on((`ccplavadoseguimientosestado`.`id` = `ccplavadoseguimiento`.`ccplavadoseguimientosestado_id`))) 
left join `ccplavadoseguimientomotivos` `ccplavadoseguimientomotivo` on((`ccplavadoseguimientomotivo`.`id` = `ccplavadoseguimiento`.`ccplavadoseguimientomotivo_id`))) 
where 1=1
-- AND`ot`.`empresacars` in (_utf8'DP',_utf8'DC')
and year(`ot`.`ordenfecha`) >= 2009