CREATE OR REPLACE VIEW `viewccptabhistorialdocumentos` AS 
SELECT MAX(hst.id) as id, MAX(hst.id) as ccptabhistorialdocumento_id, ot.id as talot_id, hst.ot_numero 
FROM ccptabhistorialdocumentos hst
JOIN talots as ot ON ot.nrocars = hst.ot_numero
GROUP BY ot_numero, ot.id