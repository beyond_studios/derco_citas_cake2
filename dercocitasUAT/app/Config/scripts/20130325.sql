begin;
alter table agedetallecitas add column createdsecperson_id integer;

alter table agedetallecitas add column reschedulesecperson_id integer;
alter table agedetallecitas add column rescheduledate datetime;
alter table agedetallecitas add column reschedulecomment text;

alter table agedetallecitas add column deletesecperson_id integer;
alter table agedetallecitas add column deletedate datetime;
alter table agedetallecitas add column deletecomment text;
commit;