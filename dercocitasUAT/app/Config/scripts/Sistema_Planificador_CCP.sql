CREATE OR REPLACE VIEW `sistema_planificador_ccps` AS 
select 
`ot`.`empresacars` AS `COMPANIA`,
`ot`.`codlocal` AS `TALLER`,
`ot`.`nrocars` AS `OT`,
`ot`.`descripcion` AS `DESCRIPCION`,
`ot`.`codasesor` AS `USUARIO_OT`,
`ot`.`codasesor` AS `NOMBRE_ASESOR`,
`ot`.`ordenfecha` AS `FECHA`,
`ot`.`ordenhora` AS `HORA_RECIBIDA`,
`ot`.`horaprometida` AS `HORA_PROMETIDA`,
`ot`.`tipoorden` AS `TIPO_OT`,
`ot`.`DocPreOtGe` AS `PRESUP_GEN`,
`ot`.`nrocono` AS `NRO_CONO`,
`ot`.`fechaprometida` AS `FECHA_PROM`,
`ot`.`horaprometida` AS `HORA_PROM`,
`ot`.`DocAseFchL` AS `FECHA_PRESUP`,
`ot`.`DocAseFch` AS `FECHA_APROB_ASEG`,
`ot`.`propietario` AS `CLIENTE`,
`ot`.`chassis` AS `NRO_SERIE`,
`ot`.`placa` AS `PLACA`,
`ot`.`marca` AS `MARCA`,
`ot`.`modelo` AS `MODELO`,
_latin1'0001100' AS `VERSION`,
`ot`.`estado` AS `OT_ESTADO`,
`ot`.`DocPreOtGe` AS `PRESUPUESTO`,
`ot`.`codasesor` AS `USUARIO_PRE`,
`ot`.`DocAseFchL` AS `FECHA_PRES`,
(case when (`ot`.`DocAseFch` is not null) 
then _utf8'4' when (`ot`.`DocAseFchL` is not null) 
then _utf8'3' else _utf8'1' end) AS `OT_PRE_ESTADO`,
`ot`.`color` AS `COLOR` 
from `talots` `ot` 
where ((1 = 1) and (year(`ot`.`ordenfecha`) >= 2009));