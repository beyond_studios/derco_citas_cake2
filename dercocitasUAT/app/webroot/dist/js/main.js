$(document).ready(function(){

	//colocamos la clase active al primer boton del menu

	$('.nano').nanoScroller({
		preventPageScrolling: true
	});

	$(".nano").nanoScroller();
	/*
	$("#main").find("img").load(function() {
		$(".nano").nanoScroller();
	});
	*/


	var $opc_menu = $('nav ul li a');

	$opc_menu.not('.active').hover(function(){
		$(this).find('img').css('-webkit-filter','brightness(100)')
		$(this).find('img').css('filter','brightness(100)')

	}, function(){
		$(this).find('img').css('-webkit-filter','none')
		$(this).find('img').css('filter','none')		
	})

	$opc_menu.not('.active').click(function(){
		$opc_menu.removeClass();
		$(this).addClass('active');

		
		$opc_menu.find('img').css('-webkit-filter','none')
		$opc_menu.find('img').css('filter','none')

		$(this).find('img').css('-webkit-filter','brightness(100)')
		$(this).find('img').css('filter','brightness(100)')
		
	})

	$('.nuevaClave').click(function(){
		$('.overAll').css('opacity',1);
		$('.overAll').css('visibility','visible');
		return false;
	})

	$('.cerrarModal').click(function(){
		$('.overAll').css('opacity',0);
		$('.overAll').css('visibility','hidden');
		return false;
	})

	/*
	$('.overAll').click(function(){
		$('.overAll').css('opacity',0);
		$('.overAll').css('visibility','hidden');
		return false;
	})
	*/


	/* Table Scroll */
	// Change the selector if needed
	var $table = $('table.scroll'),
	    $bodyCells = $table.find('tbody tr:first').children(),
	    colWidth;

	// Adjust the width of thead cells when window resizes
	$(window).resize(function() {
	    // Get the tbody columns width array
	    colWidth = $bodyCells.map(function() {
	        return $(this).width();
	    }).get();
	    
	    // Set the width of thead columns
	    $table.find('thead tr').children().each(function(i, v) {
	        $(v).width(colWidth[i]);
	    });
	}).resize(); // Trigger resize handler

})