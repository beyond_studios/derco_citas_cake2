/**
 * @author Ronald
 */
var url="";
var mymsn = new messaje();

$(function(){
	url = $('base').attr('href');
	$('#clienteWebForm').submit(function(e){
		return false;
	});
	
	mymsn.init();
	setColorClienteTipo();
	
	$('#actualizarVehiculo').bind('click',function(event){
		event.preventDefault();
		var id = $('#ClientePlaca').val();
		if(id!='' && $('#clienteWebForm').valid()){
			var marca = $('#ClienteMarca').val();
			var modeloId = $('#ClienteModeloId').val();
			$.ajax({
				url:url+'clientes/actualizarVehiculoJson/'+id+"/"+marca+"/"+modeloId+"/1",
				dataType:'json',
				success:function(data){
					if(data.Success==true){

					}
					mymsn.setMsg(data.Mensaje);
					mymsn.open();
				},
				error:function(){
					mymsn.setMsg('Ha ocurrido un error; Contactese con su administrador');
					mymsn.open();
				}
			});			
		}
	});
	
	/** asignamos al change **/
	$('#ClientePlaca').change(function(){
		val = $('#ClientePlaca option:selected').val();
		
		/** Marca y modelo **/
		$.ajax({
			url:url+'AgeclientesVehiculos/getDtJson',
			data:{id:val},
			dataType:'json',
			success:function(response){
				console.log(response);
				console.log(response._empty);
				if(!response._empty){
					/** select Marca **/
					marcaOptions = $('#ClienteMarca').find('option');
					marcaOptions.each(function(i,k){
						if($(k).attr('value') == response.marca_id){
							$(k).attr('selected', 'selected');
						}else{
							$(k).attr('selected', '');
						}
					});
					
					/** find model **/
					$('#ClienteMarca').trigger('change');
					modelos = $('#ClienteModeloId').find('option');
					modelos.each(function(i,k){
						if($(k).attr('value') == response.modelo_id){
							$(k).attr('selected', 'selected');
						}
					});
				}
			},
			error:function(){
				mymsn.setMsg('Ha ocurrido un error; Contactese con su administrador');
				mymsn.open();
			}
		});			
	});
		
	cmbAni_02('ClienteMarca', 'ClienteModeloId', 'modelos/getJsonList');
});