var urlBase = $('#dtBasic');

function isArray(myArray) {
    return myArray.constructor.toString().indexOf("Array") > -1;
}

function getUrl(urlCake){
	return urlBase.attr('href')+urlCake;
}

/**
 * @author Ronald Tineo S.
 */
$(function(){
	validIpDocLoader();
	
	var validator=$('#ClienteAddclientesdatoForm').validate({
	rules:{
		'data[Cliente][apellidoPaterno]':{
			required:true
		},
		'data[Cliente][apellidoMaterno]':{
			required:true
		},
		'data[Cliente][nombres]':{
			required:true
		},
		'data[Cliente][email]':{
			required:true,
			email:true
		},
		'data[Cliente][telefono]':{
			required:true
		},
		'data[Cliente][celular]':{
			required:true
		},
		'data[Cliente][marca]':{
			required:true
		},
		'data[Cliente][modelo_id]':{
			required:true
		},
		'data[Cliente][placa]':{
			required:true
		},
	},
	errorPlacement: function(label, element)
	{  label.insertAfter(element);}
	});
	
	$('#ClienteAddclientesdatoForm').submit(function(e){
		if ($('#ClienteAddclientesdatoForm').valid()) {
			$('#imgLoader').show();
			$('#btnGuardar').hide();
			$('#btnReset').hide();
			return true;			
		}else{
			return false;
		}

	});
	
	setColorClienteTipo();
	cmbAni_02('ClienteMarcaId', 'ClienteModeloId', 'modelos/getJsonList');
});


function validIpDoc(){
	var option = $('#ClienteDocumentoTipo').find('option:selected');
	var value = option.val();
	
	$('#ClienteDocumentoNumero').attr('value', "");
	
	if(value == "RU"){
		$('#ClienteNombres').removeClass('onlyPlaque');
	}else{
		$('#ClienteNombres').addClass('onlyPlaque');
	}
}
function validIpDocLoader(){
	var option = $('#ClienteDocumentoTipo').find('option:selected');
	var value = option.val();
	
	if(value == "RU"){
		$('#ClienteNombres').removeClass('onlyPlaque');
	}else{
		$('#ClienteNombres').addClass('onlyPlaque');
	}
}

function cmbAni_02(padreId, hijoArrayId, miUrl){
	$('#'+padreId).change(function(){
		val = $('#'+padreId+' option:selected').val();
		
		$.ajax({
			url: getUrl(miUrl),
			data:{id:val},
			dataType:'json',
			beforeSend: function(){
				if(isArray(hijoArrayId)){
					hijoId = hijoArrayId[0];
					for	(index = 1; index < hijoArrayId.length; index++) {
					    OtroHijoId = $('#'+hijoArrayId[index]);
						OtroHijoId.find('option').remove();
					}	
				}else{
					hijoId = hijoArrayId;
				}
				img = $('#'+hijoId).next();
				img.removeClass('hide');
				hijo = $('#'+hijoId);
				hijo.find('option').remove();
				
				if(val == null || val == '' || val == undefined){
					img.addClass('hide');
					return false;
				}
			},
			success:function(response){
				if(!response._empty){
					$.each(response.rows,function(key, value){
					    hijo.append('<option value=' + value.key + '>' + value.value + '</option>');
					});
				}	
				img.addClass('hide');
			},
			error:function(){
				img.addClass('hide');
				return false;
			}
		});	
	});
} 