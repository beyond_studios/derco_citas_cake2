$(document).ready(function(){
	iniFechas();
});

function detCitaExel(miThis){
	var action_exel = miThis.getAttribute('url_exel');
	var accion = $('#f_bsc').attr('action');
	var target = $('#f_bsc').attr('target');
	
	//abro la ventana de impresion
	 var w = window.open('', 'exel','scrollbars=yes,resizable=yes,width=100,height=20,top=150,left=250,status=no,location=no,toolbar=no,menubar=no');
			
	$('#f_bsc').attr('action',action_exel);
	$('#f_bsc').attr('target','exel');
	$('#f_bsc').submit();
	$('#f_bsc').attr('action',accion);
	$('#f_bsc').attr('target',"_self");
}

function newCitaExel(miThis){
	var action_exel = miThis.getAttribute('url_exel');
	var accion = $('#f_bsc').attr('action');
	var target = $('#f_bsc').attr('target');
	
	//abro la ventana de impresion
	 var w = window.open('', 'exel','scrollbars=yes,resizable=yes,width=100,height=20,top=150,left=250,status=no,location=no,toolbar=no,menubar=no');
			
	$('#f_bsc').attr('action',action_exel);
	$('#f_bsc').attr('target','exel');
	$('#f_bsc').submit();
	$('#f_bsc').attr('action',accion);
	$('#f_bsc').attr('target',"_self");
}



function iniFechas() {
	var dates = $("#f_ini").datepicker({
		showOn: 'button', 
		buttonImage: getUrl('/app/webroot/img/calendar.gif'), 
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'dd/mm/yy'
	}, $.datepicker.regional['es']);
}



/**
 * @author Ronald
 */
function add(url)
{	
	var w = window.open(url, 'agregar', 
	        'scrollbars=yes,resizable=yes,width=900,height=600,top=20,left=230,status=no,location=no,toolbar=no,menubar=no');
}

function indexbloqueo(url)
{	
	var w = window.open(url, 'bloqueos', 
	        'scrollbars=yes,resizable=yes,width=900,height=600,top=20,left=230,status=no,location=no,toolbar=no,menubar=no');
}

function editar(url)
{	
	var w = window.open(url, 'editar', 
	        'scrollbars=no,resizable=yes,width=450,height=250,top=100,left=200,status=no,location=no,toolbar=no');
}

function mostrar(url)
{	
	var w = window.open(url, 'editar', 
	        'scrollbars=no,resizable=yes,width=800,height=500,top=50,left=300,status=no,location=no,toolbar=no');
}
function modificarCronograma(url)
{	
	var w = window.open(url, 'modificarcronograma', 
	        'scrollbars=yes,resizable=yes,width=580,height=350,top=100,left=200,status=no,location=no,toolbar=no');
}
function buscadorcliente(url)
{	
	var w = window.open(url, 'buscadorcliente', 
	        'scrollbars=yes,resizable=yes,width=580,height=350,top=100,left=200,status=no,location=no,toolbar=no');
}
function contadorCita(url)
{	
	var w = window.open(url, 'contadorCita', 
	        'scrollbars=yes,resizable=yes,width=600,height=400,top=20,left=230,status=no,location=no,toolbar=no,menubar=no');
}
