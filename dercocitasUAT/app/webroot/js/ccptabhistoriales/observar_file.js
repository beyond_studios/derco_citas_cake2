
var url;
$(document).ready(function(){
	url = $('base').attr('href');
	validarformulario();

});

function validarformulario(){
    var validator=$('#Ccptabhistorial').validate({
	rules:{
            'data[Ccptabhistorialdocumento][ccptabestado_anterior]':{
                required:true,
            },
            'data[Ccptabhistorialdocumento][ccptabestado_id]':{
                required:true,
            },
            'data[Ccptabhistorialdocumento][comentario]':{
                required:true,
            }
        },
	errorPlacement: function(label, element){
            label.insertAfter(element);
        }
    });
}

function agregar_observacion(){
    var cont_horarios = $('#ccptabhistorial_table tbody').find('tr').length;
    id_fila = 'fila' + cont_horarios;
    //alert($(imagen).attr('width'));
    var html='';
    html = '<tr id="'+id_fila+'"> <td></td><td class="valor span-9 last">' +
           '<textarea id="CcptabhistorialComentario' + cont_horarios +'" class="required" rows="2" cols="30" name="data[Ccptabhistorialcomentario]['+cont_horarios+'][comentario]"></textarea>'+
           '<a onclick="eliminar_observacion(this)" href="javascript:;">'+
           '<img width="15" border="0" title="" src="'+url+'img/derco/defecto/minus.gif">'+
           '</a>'+
           '<span class="campoObligatorio">*</span>'+
           '<div>'+
           '<label class="error" style="display: none;" generated="true" for="CcptabhistorialComentario'+cont_horarios+'"></label>'+
           '</div>'+
           '</td></tr>';
       $('#ccptabhistorial_table tbody').append(html);
}

function eliminar_observacion(mythis){
    $(mythis).parent().parent().remove();
    $('#ccptabhistorial_table tbody').find('tr').each(function(i,tr){
        $(tr).find('textarea').each(function(j,e){
            var id=$(e).attr('id');
            var name=$(e).attr('name');
            $(e).attr('id', $(e).attr('id').replace(id, 'CcptabhistorialComentario'+i));
            $(e).attr('name', $(e).attr('name').replace(name, 'data[Ccptabhistorialcomentario]['+i+'][comentario]'));
        });
        $(tr).find('label').each(function(j,e){
            $(e).attr('for', $(e).attr('for').replace(/^CcptabhistorialComentario./, 'CcptabhistorialComentario'+i));
        });
    });
}
