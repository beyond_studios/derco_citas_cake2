var TIPOREPUESTO_ID = 1;

$().ready(function(){
	$('#cabecera td').css('border',0);
	$('#cabecera input').css('width','200px');
	$('#cabecera select').css('width','200px');
	
	$('.box').css('height','25px');
	
	validateForm();
});


function validateForm(){
	$('#setEstimate').validate({
		submitHandler:function(form){
			var $list = 'input[type=submit],input[type=button],input[type=reset],button';
			$(form).find($list).attr('disabled','disabled');
			form.submit();
			return false;
		},
		errorPlacement: function(label, element){	
			if(element.hasClass('msg_error_1'))
				label.insertAfter(element.next());
			else if (element.hasClass('msg_error_2'))
				label.insertAfter(element.next().next());
			else
				label.insertAfter(element);
		}   
	});
}

function baseSetInsurance(miThis){
	var insurance = miThis.parent().find('select option:selected').html();
	$('#WorestimateWorinsuranceName').attr('value',insurance);
}

function baseSetCliente(id,documento_tipo,documento_numero,apellidoPaterno,nombres){
	$('#ClienteId').attr('value',id);
	$('#ClienteDocumentoTipo').attr('value',documento_tipo);
	$('#ClienteDocumentoNumero').attr('value',documento_numero);
	$('#ClienteApellidoPaterno').attr('value',apellidoPaterno +', '+nombres);
}

// INTERACTUAN CON EL BUSCADOR DE VEHICULOS ##########################################
function baseAbrirDialogVehicle(){
	// verificamos que exista un cliente seleccionado
	var cliId = $('#BaseWorclientId').val();
	
	if(!(cliId == '')){
		jQuery("#vehicles").trigger('reloadGrid');
		$("#dialog_vehicle").dialog('open');
	}else{
		jAlert('Seleccione un cliente');
	}
	return false;
}

function baseSetVehicle(id,plaque){
	$('#WorestimateWorvehicleId').attr('value',id);
	$('#WorestimatePlaca').attr('value',plaque);
}

function setFieldRead(miThis){
	var tpDetailId = $(miThis.parent().find("select option:selected").get(0)).val();
	var detailImput = miThis.parent().parent().find('input[type=text], input[type=hidden]');
	
	detailImput.attr('readonly', true);
	detailImput.attr('value', '');
	
	if (tpDetailId == 1) { 			//repueso
		$(detailImput.get(3)).attr('readonly',false);
	}else if (tpDetailId == 2) { 	//mano de obra
		$(detailImput.get(1)).attr('readonly',false);
		$(detailImput.get(2)).attr('readonly',false);
		$(detailImput.get(3)).attr('readonly',false);
	}else if (tpDetailId == 3) { 	//otros
		$(detailImput.get(1)).attr('readonly',false);
		$(detailImput.get(2)).attr('readonly',false);
		$(detailImput.get(3)).attr('readonly',false);
	}
}

// DIALOG DETAILS #############################################################
var trDetailSelected = '';
function baseAbrirDialogDetail(miThis){
	//guardamos la referencia del detalle seleccionado
	trDetailSelected = miThis.parent().parent().parent();
	
	// verificamos que tipo de detalle esta buscando
	var tpDetailId = $(miThis.parent().parent().parent().find("select option:selected").get(0)).val();

	switch(parseInt(tpDetailId)){
		case 1:
			$("#dialog_sparepart").dialog('open');
			break;
		case 2:
			$("#dialog_service").dialog('open');
			break;
		case 3:
			$("#dialog_other").dialog('open');
			break;
		default:	
			jAlert('Seleccione un tipo de detalle');
	}

	return false;
}

	
// DIALOG OTHER #############################################################
function baseSetOther(id,code,description){
	var detailImput = trDetailSelected.find('input[type=text], input[type=hidden]');
	
	detailImput.attr('readonly', true);
	detailImput.attr('value', '');
	
	$(detailImput.get(0)).attr('value',code).attr('readonly',true);
	$(detailImput.get(1)).attr('readonly',false).attr('value',description);
	$(detailImput.get(2)).attr('readonly',false);
	$(detailImput.get(3)).attr('readonly',false);
	$(detailImput.get(9)).attr('value',id);
}

// DIALOG SERVICE #############################################################
function baseSetService(id,code,description,hours){
	var detailImput = trDetailSelected.find('input[type=text], input[type=hidden]');
	
	detailImput.attr('readonly', true);
	detailImput.attr('value', '');
	
	$(detailImput.get(0)).attr('readonly',true).attr('value',code);
	$(detailImput.get(1)).attr('readonly',false).attr('value',description);
	$(detailImput.get(2)).attr('readonly',false);
	$(detailImput.get(3)).attr('readonly',false);
	$(detailImput.get(7)).attr('value',id);
}

// DIALOG SPAREPARTS #############################################################
function baseSetSparepart(id,code,description,averagecost,price,stock,quantity){
	var detailImput = trDetailSelected.find('input[type=text], input[type=hidden]');
	var repuesto_repetido = false;
	
	//al ser un repuesto verificamos que no se repita
	$('#t-estimatedetail tbody').find('tr').each(function(i,e){
		var input_select = $(e).find('input[type=hidden], select option:selected');
		
		if(input_select[0].value == TIPOREPUESTO_ID){
			if(input_select[4].value == id){
				jAlert('El repuesto ya fue seleccionado');
				repuesto_repetido = true;
			}
		}
	});
	
	if(repuesto_repetido) return false;
	  
	detailImput.attr('readonly', true);
	detailImput.attr('value', '');
	
	$(detailImput.get(0)).attr('value',code).attr('readonly',true);
	$(detailImput.get(1)).attr('readonly',true).attr('value',description);
	$(detailImput.get(2)).attr('readonly',true).attr('value',price);
	$(detailImput.get(3)).attr('readonly',false);
	$(detailImput.get(8)).attr('value',id);
}

function getParcialPrice(){
	var t_trs = $('#t-estimatedetail').find('tbody tr');
	
	t_trs.each(function(i,e){
		var input = $(e).find('input[type=text]');	
		var prc = $($(e).find('input[type=text]').get(2)).attr('value');
		var cnt = $($(e).find('input[type=text]').get(3)).attr('value');
		
		if(isNaN(prc)) prc = 0.00;
		else prc = number_format(prc,2,'.','');
	
		
		if(isNaN(cnt)) cnt = 0.00;
		else cnt = number_format(cnt,2,'.','');
		
		var cnt_prc = parseFloat(prc)*parseFloat(cnt);
		if(isNaN(cnt_prc)) cnt_prc = 0.00;
		
		$($(e).find('input[type=text]').get(2)).attr('value',prc);
		$($(e).find('input[type=text]').get(3)).attr('value',cnt);
		$($(e).find('input[type=text]').get(4)).attr('value',number_format(cnt_prc, 2, '.',''));
	});
	
	getTotalPrice();
}

function getTotalPrice(){
	var t_trs = $('#t-estimatedetail').find('tbody tr');
	var montoTotal = parseFloat(0.00);
	
	t_trs.each(function(i,e){
		var input = $(e).find('input[type=text]');	
		var monto = $($(e).find('input[type=text]').get(4)).attr('value');
		
		
		if(isNaN(monto)) monto = 0.00;
		else monto = number_format(monto,2,'.','');
	
		montoTotal = parseFloat(montoTotal) + parseFloat(monto);
	});
	
	//recuperamos el descuento
	var dsc = $('#WorestimateDiscount').attr('value');
	if(isNaN(dsc)) dsc = 0.00;
	else dsc = number_format(dsc,2,'.','');
		
	$('#WorestimateTotalprice').attr('value',number_format(montoTotal,2,'.',''));
	$('#WorestimateDiscount').attr('value',dsc);
	$('#WorestimateTax').attr('value',number_format(parseFloat(montoTotal)-parseFloat(dsc),2,'.',''));
	
}
