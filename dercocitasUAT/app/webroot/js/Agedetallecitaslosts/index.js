$(document).ready(function(){
	
});

function iniFechas() {
	var dates = $("#f_ini, #f_fin").datepicker({
		showOn: 'button', 
		buttonImage: getUrl('/app/webroot/img/calendar.gif'), 
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'dd/mm/yy',
		onSelect: function(selectedDate){
			var option = this.id == "f_ini" ? "minDate" : "maxDate",
				instance = $(this).data( "datepicker" ),
				date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings);
			dates.not(this).datepicker("option", option, date);
		}
	}, $.datepicker.regional['es']);
}


function getMotivoServicio(miThis){
	var option = $(miThis).find('option:selected')
	var id = option.val();
	var selectHijo = $('#motivoServicio');
	var marcaId = $('#MarcaId').val();
	$('#buttonSubmit').addClass('hide');
	$('#listaPrincipal').html("");
	
	if((id == "")){
		selectHijo.html('<option value="" selected>Seleccionar</option>').attr('disabled', true);
		$('#tipoMantenimiento').html('<option value="" selected>Select...</option>').attr('disabled', true).removeClass('required');
		return false;
	}
	
	selectHijo.next().removeClass('hide');
	$.ajax({
		url:getUrl('agemotivoservicios/getMotivoServicioJson/'+id),
		async : false,
		dataType:'json',
		success:function(response, postdata){
			selectHijo.next().addClass('hide');
			selectHijo.html('<option value="" selected>Seleccionar</option>');
			if(!response.susses){
				selectHijo.attr('disabled', true);
				alert(response.errors.msg);
				return false;
			}
			
			$.each(response.data, function(i,val){
				selectHijo.append('<option agecitacalendario_id="'+val.agecitacalendario_id+'" value="'+i+'">'+val.name+'</option>');
			});
			
			selectHijo.attr('disabled', false);
		},
		error:function(){
			selectHijo.html('<option selected>Error...</option>');
			selectHijo.attr('disabled', true);
			selectHijo.next().addClass('hide');
		}
	});
	
	//set value to zero
	$('#AgedetallecitaslostServiciomenor').val(0);
	$('#AgedetallecitaslostServiciomayor').val(0);
}

function getTipoMantenimiento(miThis){
	var option = $(miThis).find('option:selected')
	var id = option.val();
	var selectHijo = $('#tipoMantenimiento');
	var selectHijo2 = $('#tipoServicio');
	
	var secproject_id = $('#SecprojectId').val();
	var motivoservicio_id = $('#motivoServicio').val();
	
	$('#buttonSubmit').addClass('hide');
	$('#listaPrincipal').html("");
	
	if((id == "")){
		selectHijo.html('<option value="" selected>Seleccionar</option>').attr('disabled', true);
		selectHijo2.html('<option value="" selected>Seleccionar</option>').attr('disabled', true);
		return false;
	}
	
	selectHijo.next().removeClass('hide');
	$.ajax({
		url:getUrl('agetipomantenimientos/getTipoMantenimientoJson/'+id),
		async : true,
		dataType:'json',
		success:function(response, postdata){
			selectHijo.attr('disabled', true);
			selectHijo.next().addClass('hide');
			selectHijo.html('<option value="" selected>Seleccionar</option>');
			if(!response.susses){
				return false;
			}
			$.each(response.data, function(i,val){
				selectHijo.append('<option value="'+i+'">'+val.name+'</option>');
			});
			
			selectHijo.next().addClass('hide');
		},
		error:function(){
			selectHijo.html('<option selected>Error...</option>');
			selectHijo.attr('disabled', true);
			selectHijo.next().addClass('hide');
		}
	});
	
	selectHijo2.next().removeClass('hide');
	$.ajax({
		url:getUrl('agetiposervicios/getTiposerviciosJson/'+id),
		async : true,
		dataType:'json',
		success:function(response, postdata){
			selectHijo2.next().addClass('hide');
			selectHijo2.html('<option value="" selected>Seleccionar</option>');
			if(!response.susses){
				selectHijo2.attr('disabled', true);
				return false;
			}
			
			$.each(response.data, function(i,val){
				selectHijo2.append('<option mantenimiento="'+val.maintenance+'" value="'+i+'">'+val.name+'</option>');
			});
			selectHijo2.attr('disabled', false);
		},
		error:function(){
			selectHijo2.html('<option selected>Error...</option>');
			selectHijo2.attr('disabled', true);
			selectHijo2.next().addClass('hide');
		}
	});
	
	verTipomantenimiento($('#tipoServicio'));
}

function verTipomantenimiento(miThis){
	var v = $(miThis).find('option:selected').val();
	var m = $(miThis).find('option:selected').attr('mantenimiento');
	var selectHijo2 = $('#tipoMantenimiento');
	
	selectHijo2.attr('value', '');
	selectHijo2.attr('disabled', true);
	
	if (!(v == "" || v == "0")) {
		if(m == true || m == 1 || m == "true"){
			selectHijo2.attr('disabled', false);
		}
	}	
}

/**
 * @author Ronald
 */
function add(url)
{	
	var w = window.open(url, 'agregar', 
	        'scrollbars=yes,resizable=yes,width=750,height=450,top=50,left=250,status=no,location=no,toolbar=no,menubar=no');
}

function editar(url)
{	
	var w = window.open(url, 'editar', 
	        'scrollbars=no,resizable=yes,width=450,height=250,top=100,left=200,status=no,location=no,toolbar=no');
}

function mostrar(url)
{	
	var w = window.open(url, 'editar', 
	        'scrollbars=no,resizable=yes,width=800,height=500,top=50,left=300,status=no,location=no,toolbar=no');
}
function modificarCronograma(url)
{	
	var w = window.open(url, 'modificarcronograma', 
	        'scrollbars=yes,resizable=yes,width=580,height=350,top=100,left=200,status=no,location=no,toolbar=no');
}
function buscadorcliente(url)
{	
	var w = window.open(url, 'buscadorcliente', 
	        'scrollbars=yes,resizable=yes,width=580,height=350,top=100,left=200,status=no,location=no,toolbar=no');
}
function contadorCita(url)
{	
	var w = window.open(url, 'contadorCita', 
	        'scrollbars=yes,resizable=yes,width=600,height=400,top=20,left=230,status=no,location=no,toolbar=no,menubar=no');
}
