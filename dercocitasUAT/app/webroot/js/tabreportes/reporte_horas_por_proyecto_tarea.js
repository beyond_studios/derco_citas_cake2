/**
 * @author chiusac
 */
//search:true, stype:'text', searchoptions:{dataInit:datePick, attr:{title:'Select Date'}}
var url;
$(document).ready(function(){
		url = $('base').attr('href');
		
		jQuery("#reportes").jqGrid({
			url: url+'tabreportes/getots?q='+1,
			treedatatype: "json",
			datatype: "json",
			//mtype: "POST",
		   	colNames:["id","idConsulta","OT  Tarea  Responsable","Tiempo Planificado","Tiempo Ejecutado"],
		   	colModel:[
		   		{name:'Id',index:'Reporte.id', width:10,hidden:true,key:true, editable:true},
		   		{name:'IdConsulta',index:'Reporte.idConsulta', search:false,width:10,hidden:true, editable:true},
				{name:'Tarea',index:'Reporte.description', search:false, width:460, editable:true},
		   		{name:'Tiempo Planificado',index:'Reporte.tiempoplanificado', width:130, editable:true,search:false},
		   		{name:'Tiempo Ejecutado',index:'Reporte.tiempoejecutado', search:false, width:130, align:"left",editable:true}
		   	],
			height:'300',
			pager : "#pagreportes",
			treeGrid: true,
		  	treeGridModel: 'nested',
		  	ExpandColumn : 'Tarea',
			ExpandColClick : true,
			postData: {fechaInicio : $('#TabreporteFechaInicio').val(),fechaFin: $('#TabreporteFechaFin').val(),descripcion:$('#TabreporteDescripcion').val()},
			caption: "Reportes Por Horas en Proyecto"
		});
		//jQuery("#reportes").jqGrid('navGrid',"#pagreportes");
		jQuery("#reportes").jqGrid('navGrid','#pagreportes',{edit:false,add:false,del:false,search:false});
		//jQuery("#reportes").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : true});
		jQuery('#reportes').jqGrid('navButtonAdd','#pagreportes',{id:'pager_excel', caption:'Exportar a Excel',title:'Exportar a Excel',
			onClickButton : function(e){
								try {
									exportExcel();
								} catch (e) {
									window.location= url+'tabreportes/getots?q='+1;
								}
							},
			buttonicon:'ui-icon-newwin'});
		/*jQuery("#reportes").jqGrid('navButtonAdd', "#pagreportes",{id:'Expand_All',
				    caption: "Expandir Todo",
				    buttonicon: "ui-icon-triangle-2-n-s",
				    onClickButton: function(e) {
										rootNode = jQuery("#reportes").jqGrid('getRowData')[0];
						               rootNode._id_ = rootNode.id;
									   jQuery('#reportes').expandNode(rootNode);    
						              jQuery('#reportes').expandRow(rootNode);
									expandir();

				    },
				    position: "last",
				    title: "Expandir Todo",
				    cursor: "pointer"
				});		*/	
	inicializarFechas();
	});

function inicializarFechas(){
		$('#TabreporteFechaInicio').datepicker({
			showOn: 'both',
			buttonImage: url+('/app/webroot/img/calendar.gif'),
			buttonImageOnly: true,
			buttonText: 'Calendario'
		}, $.datepicker.regional['es']);
		$('#TabreporteFechaFin').datepicker({
			showOn: 'both',
			buttonImage: url+('/app/webroot/img/calendar.gif'),
			buttonImageOnly: true,
			buttonText: 'Calendario'
		}, $.datepicker.regional['es']);		
	}	
$(function(){
	var cjaB=$('#TabreporteDescripcion');
	cjaB.keydown(function(e){
		if(e.keyCode==13 )reloadGrid();});
	});
	
function reloadGrid(){
	//jQuery("#reportes").trigger("reloadGrid");
	$('#TabreporteReporteHorasPorProyectoTareaForm').submit()
}
function expandir(){
	 $(".treeclick","#reportes").each(function(){
									 	$(this).hasClass("tree-plus");
									 	$(this).trigger("click");
									 });
}

function exportExcel()
	{
        var mya=new Array();
        mya=$("#reportes").getDataIDs();				// Get All IDs
        var data=$("#reportes").getRowData(mya[0]);		// Get First row to get the labels
        var colNames=new Array();
		var colLength=0; 
		var columnaInicio=0;
		var columnaInsertaEspacions=0;
		var espacio="";
        var ii=0;
        for (var i in data){
			colNames[ii++]=i;							// capture col names
			}
		//colLength=colNames.length;//muestra todas las columnas
		colLength=5; // muestro solo las primeras 4 columnas
		columnaInicio=2; //empieza en el uno para que no muestr el ID
		columnaInsertaEspacions=2; //columna donde inserta los espacios
        var html="";
        for(k=columnaInicio;k<colLength;k++)
          {
			html=html+colNames[k]+"\t";					// output each Column as tab delimited
           }
		html=html+"\n";									// Output header with end of line
        for(i=0;i<mya.length;i++)
            {
            data=$("#reportes").getRowData(mya[i]);		// get each row
            for(j=columnaInicio;j<colLength;j++)
                {
					espacio="";
					if((data[colNames[5]]==1) && (j==columnaInsertaEspacions) ){
						espacio="     ";
					}
					if((data[colNames[5]]==2) && (j==columnaInsertaEspacions)){
						espacio="          ";
					}
					//alert(data[colNames[4]]);
					html=html+espacio+data[colNames[j]]+"\t";	// output each Row as tab delimited
                }
            html=html+"\n";								// output each row with end of line
            }
        html=html+"\n";									// end of line at the end
        
        document.forms[0].TabreporteExcel.value=html;
        document.forms[0].method='POST';
        document.forms[0].action=url+'tabreportes/exportarExcel';	// send it to server which will open this contents in excel file
        document.forms[0].target='_blank';
        document.forms[0].submit();
	}
function utf8_encode (argString) {
    // Encodes an ISO-8859-1 string to UTF-8  
    // 
    // version: 1107.2516
    // discuss at: http://phpjs.org/functions/utf8_encode
    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: sowberry
    // +    tweaked by: Jack
    // +   bugfixed by: Onno Marsman
    // +   improved by: Yves Sucaet
    // +   bugfixed by: Onno Marsman
    // +   bugfixed by: Ulrich
    // +   bugfixed by: Rafal Kukawski
    // *     example 1: utf8_encode('Kevin van Zonneveld');
    // *     returns 1: 'Kevin van Zonneveld'
    if (argString === null || typeof argString === "undefined") {
        return "";
    }
 
    var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
    var utftext = "",
        start, end, stringl = 0;
 
    start = end = 0;
    stringl = string.length;
    for (var n = 0; n < stringl; n++) {
        var c1 = string.charCodeAt(n);
        var enc = null;
 
        if (c1 < 128) {
            end++;
        } else if (c1 > 127 && c1 < 2048) {
            enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
        } else {
            enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
        }
        if (enc !== null) {
            if (end > start) {
                utftext += string.slice(start, end);
            }
            utftext += enc;
            start = end = n + 1;
        }
    }
 
    if (end > start) {
        utftext += string.slice(start, stringl);
    }
 
    return utftext;
}	
function utf8_decode (str_data) {
    // Converts a UTF-8 encoded string to ISO-8859-1  
    // 
    // version: 1107.2516
    // discuss at: http://phpjs.org/functions/utf8_decode
    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // +      input by: Aman Gupta
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Norman "zEh" Fuchs
    // +   bugfixed by: hitwork
    // +   bugfixed by: Onno Marsman
    // +      input by: Brett Zamir (http://brett-zamir.me)
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // *     example 1: utf8_decode('Kevin van Zonneveld');
    // *     returns 1: 'Kevin van Zonneveld'
    var tmp_arr = [],
        i = 0,
        ac = 0,
        c1 = 0,
        c2 = 0,
        c3 = 0;
 
    str_data += '';
 
    while (i < str_data.length) {
        c1 = str_data.charCodeAt(i);
        if (c1 < 128) {
            tmp_arr[ac++] = String.fromCharCode(c1);
            i++;
        } else if (c1 > 191 && c1 < 224) {
            c2 = str_data.charCodeAt(i + 1);
            tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
            i += 2;
        } else {
            c2 = str_data.charCodeAt(i + 1);
            c3 = str_data.charCodeAt(i + 2);
            tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }
    }
 
    return tmp_arr.join('');
}	