/**
 * @author chiusac
 */
//search:true, stype:'text', searchoptions:{dataInit:datePick, attr:{title:'Select Date'}}
var url;
$(document).ready(function(){
	url = $('base').attr('href');
	inicializarFechas();
});

function inicializarFechas(){
		$('#TabreporteFechaInicio').datepicker({
			showOn: 'both',
			buttonImage: url+('/app/webroot/img/calendar.gif'),
			buttonImageOnly: true,
			buttonText: 'Calendario',
			dateFormat: 'dd-mm-yy'
		}, $.datepicker.regional['es']);
		$('#TabreporteFechaFin').datepicker({
			showOn: 'both',
			buttonImage: url+('/app/webroot/img/calendar.gif'),
			buttonImageOnly: true,
			buttonText: 'Calendario',
			dateFormat: 'dd-mm-yy'
		}, $.datepicker.regional['es']);		
	}	


function exportExcel()
	{
//		$("#TabreporteExcel").val( $("<div>").append( $("#reportes").eq(0).clone()).html());
//
//        document.forms[0].method='POST';
//        document.forms[0].action=url+'tabreportes/exportarExcel';	// send it to server which will open this contents in excel file
//        document.forms[0].target='_blank';
//        document.forms[0].submit();
		var fechaInicial = $('#TabreporteFechaInicio').val();
		if(fechaInicial == '') fechaInicial='0';
		var fechaFinal = $('#TabreporteFechaFin').val();
		if(fechaFinal == '') fechaFinal = '0';
		var persona = $('#TabreporteResponsable').val();
		if(persona == '') persona = '0';
		var parametros = persona+ '/'+ fechaInicial + '/' + fechaFinal
		var url1 = url + '/tabreportes/reportePorDiasExcel/'+ parametros;
		
	    var w = window.open(url1 , 'exportalExcel', 
	        'scrollbars=yes,resizable=yes,width=100,height=100,status=no,location=no,toolbar=no');	
					

	}
function utf8_encode (argString) {
    // Encodes an ISO-8859-1 string to UTF-8  
    // 
    // version: 1107.2516
    // discuss at: http://phpjs.org/functions/utf8_encode
    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: sowberry
    // +    tweaked by: Jack
    // +   bugfixed by: Onno Marsman
    // +   improved by: Yves Sucaet
    // +   bugfixed by: Onno Marsman
    // +   bugfixed by: Ulrich
    // +   bugfixed by: Rafal Kukawski
    // *     example 1: utf8_encode('Kevin van Zonneveld');
    // *     returns 1: 'Kevin van Zonneveld'
    if (argString === null || typeof argString === "undefined") {
        return "";
    }
 
    var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
    var utftext = "",
        start, end, stringl = 0;
 
    start = end = 0;
    stringl = string.length;
    for (var n = 0; n < stringl; n++) {
        var c1 = string.charCodeAt(n);
        var enc = null;
 
        if (c1 < 128) {
            end++;
        } else if (c1 > 127 && c1 < 2048) {
            enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
        } else {
            enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
        }
        if (enc !== null) {
            if (end > start) {
                utftext += string.slice(start, end);
            }
            utftext += enc;
            start = end = n + 1;
        }
    }
 
    if (end > start) {
        utftext += string.slice(start, stringl);
    }
 
    return utftext;
}	
function utf8_decode (str_data) {
    // Converts a UTF-8 encoded string to ISO-8859-1  
    // 
    // version: 1107.2516
    // discuss at: http://phpjs.org/functions/utf8_decode
    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // +      input by: Aman Gupta
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Norman "zEh" Fuchs
    // +   bugfixed by: hitwork
    // +   bugfixed by: Onno Marsman
    // +      input by: Brett Zamir (http://brett-zamir.me)
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // *     example 1: utf8_decode('Kevin van Zonneveld');
    // *     returns 1: 'Kevin van Zonneveld'
    var tmp_arr = [],
        i = 0,
        ac = 0,
        c1 = 0,
        c2 = 0,
        c3 = 0;
 
    str_data += '';
 
    while (i < str_data.length) {
        c1 = str_data.charCodeAt(i);
        if (c1 < 128) {
            tmp_arr[ac++] = String.fromCharCode(c1);
            i++;
        } else if (c1 > 191 && c1 < 224) {
            c2 = str_data.charCodeAt(i + 1);
            tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
            i += 2;
        } else {
            c2 = str_data.charCodeAt(i + 1);
            c3 = str_data.charCodeAt(i + 2);
            tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }
    }
 
    return tmp_arr.join('');
}	