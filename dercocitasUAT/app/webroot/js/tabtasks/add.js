$(document).ready(function() {
	InicializarDialogs();
	verificaCodigoMantis();
});

$(function(){
var validator = $("#TabtaskAddForm").validate({  
     rules: {  
         'data[Tabtask][id]':{
		 	required:true
		 },
		'data[Tabtask][description]':{
			required:true
		}, 
		'data[Tabtask][tasktype_id]':{
			required:true
		},
		'data[Tabtask][state]':{
			required:true
		},
		
     }, 
     errorPlacement: function(label, element) {  

             label.insertAfter(element);  
     }    
 });
 });



function mostrarTareasMantis(miThis){
	$('#dialog')
		.data('id',1)
		.dialog('open');
}

function InicializarDialogs(){
	$('#dialog').dialog(
				{
					bgiframe: true,
					resizable: false,
					height: 240,
					width: 410,
					modal: true,
					autoOpen: false,
					overlay: {
						backgroundColor: '#000',
						opacity: 0.5
					},
					open: function() {
						},
					buttons: {
						"Cancelar": function(){
							$(this).dialog("close");
						}
					}
				}
			);//.parent('.ui-dialog').find('.ui-dialog-titlebar-close').hide();//comentar delante del )	
}

function vincularMantis(id,mythis){
	var thisis = $(mythis);
	if(id!=''){
		$('#TabtaskCodigomantis').val(id);
		var descripcion = $('#mantis'+id).val();
		$('#TabtaskDescription').val(id + ': ' +descripcion)
		$('#dialog').dialog('close');
		verificaCodigoMantis();
	}
}

function desvincularMantis(){
	$('#TabtaskCodigomantis').val('');
	$('#TabtaskDescription').val('');
	verificaCodigoMantis();
}

function verificaCodigoMantis(){
	if($('#TabtaskCodigomantis').val() != ''){
		$('#TabtaskDescription').attr('readonly',true);
		$('#TabtaskDescription').addClass('readonly');
		$('#flagTarea').html('<a onclick="desvincularMantis()" href="javascript:;">Desvincular del Mantis</a>');

	}else{
		$('#TabtaskDescription').attr('readonly',false);
		$('#TabtaskDescription').removeClass('readonly');
		$('#flagTarea').html('<a onclick="mostrarTareasMantis()" href="javascript:;">Agregar Una Tarea del Mantis</a>');		
	}
}
