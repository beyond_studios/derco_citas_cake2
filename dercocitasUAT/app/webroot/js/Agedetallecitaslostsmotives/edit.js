/**
 * @author Lourdes
 */
$(function(){
	var validator=$('#AgedetallecitaslostsmotiveEditForm').validate({
	rules:{
		'data[Agedetallecitaslostsmotive][description]':{
			required:true,
		},
		'data[Agedetallecitaslostsmotive][codigo_sap]':{
			required:true,
		}
	},
	errorPlacement: function(label, element)
	{  label.insertAfter(element);}    
	});
});