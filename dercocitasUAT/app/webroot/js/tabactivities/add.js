$(function(){
var validator = $("#TabactivityAddForm").validate({  
     rules: {  
         'data[Tabactivity][code]':{
		 	required:true
		 },
		'data[Tabactivity][description]':{
			required:true
		}, 
		'data[Tabactivity][tabactivitytype_id]':{
			required:true
		},
		'data[Tabactivity][state]':{
			required:true
		},
		
     }, 
     errorPlacement: function(label, element) {  

             label.insertAfter(element);  
     }    
 });
 });