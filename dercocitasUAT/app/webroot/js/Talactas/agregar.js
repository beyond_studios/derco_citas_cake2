var url;
$(document).ready(function(){
	url = $('base').attr('href');
	validateForm();
	iniFechas();
});

function validateForm(){
	$('#modificarForm').validate({
		submitHandler:function(form){
			var $list = 'input[type=submit],input[type=button],input[type=reset],button';
			$(form).find($list).attr('disabled','disabled');
			form.submit();
		},
		errorPlacement: function(label, element){	
			if(element.hasClass('msg_error_1'))
				label.insertAfter(element.next());
			else if (element.hasClass('msg_error_2'))
				label.insertAfter(element.next().next());
			else if (element.hasClass('msg_error_3'))
				label.insertAfter(element.next().next().next());
			else
				label.insertAfter(element);
		}   
	});
}

function iniFechas() {
	$("#TalactaFechaUsuario").datepicker({
		showOn: 'button', 
		buttonImage: getUrl('img/calendar.gif'), 
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		minDate: -0,
		dateFormat: 'dd/mm/yy'
	}, $.datepicker.regional['es']);
}