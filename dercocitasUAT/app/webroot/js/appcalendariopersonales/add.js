/**
 * @author chiusac
 */
$(function(){
	var validator=$('#ApppersonalcalendarAddForm').validate({
	rules:{
		'data[Apppersonalcalendar][initdatetime]':{
			required:true,
		},
		'data[Apppersonalcalendar][enddatetime]':{
			required:true,
		},
	},
	errorPlacement: function(label, element)
	{  label.insertAfter(element);}    
	});
});
$(function(){
	$('#ApppersonalcalendarInitdatetime').timepicker({
		hourGrid: 4,
		minuteGrid: 10

	});
	$('#ApppersonalcalendarEnddatetime').timepicker({
		hourGrid: 4,
		minuteGrid: 10

	});
});
