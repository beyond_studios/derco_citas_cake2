/**
 * @author Ronald
 */
var url="";
var mymsn = new messaje();
$(function(){
	url = $('base').attr('href');
	$('#clienteSapForm').submit(function(e){
		return false;
	});
	$('#clienteWebForm').submit(function(e){
		return false;
	});
	mymsn.init();
	setColorClienteTipo();
	
	$('#actualizarCliente').bind('click',function(event){
		event.preventDefault();
		var id = $('#ClienteId').val();
		var placa = $('#VehiculoPlaca').val();
		if(id!=''){
			$.ajax({
				url:url+'clientes/updateClienteWebFromSap/'+id+'/'+placa,
				dataType:'json',
				success:function(data){
					if(data.Success==true){
						fillCliente(data.data.Cliente);
					}
					mymsn.setMsg(data.Mensaje);
					mymsn.open();
				},
				error:function(){
					mymsn.setMsg('Ha ocurrido un error; Contactese con su administrador');
					mymsn.open();
				}
			});			
		}
	});
	
	function fillCliente(Cliente){
		$('#ClienteCodigoSap').val(Cliente.codigo_sap);
		$('#ClienteApellidoPaterno').val(Cliente.apellidoPaterno);
		$('#ClienteNombres').val(Cliente.nombres);
		$('#ClienteDocumentoNumero').val(Cliente.documento_numero);
		$('#ClienteDireccion').val(Cliente.direccion);
		$('#ClienteCiudad').val(Cliente.ciudad);
		$('#ClienteDistrito').val(Cliente.distrito);
		$('#ClienteTelefono').val(Cliente.telefono);
		$('#ClienteEmail').val(Cliente.email);
	}
	inicializarDialogs();
	//openDialog();
});

function openDialog(){
	$('#dialog_placa').dialog("open");
}

function inicializarDialogs(){
	$('#dialog_placa').dialog(
		{
			bgiframe: true,
			resizable: false,
			height: 150,
			width: 250,
			modal: true,
			autoOpen: false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			},
			buttons: {
				"Cancelar": function(){
					$(this).dialog("close");
				},
				"Aceptar": function(){
					var $id = $('#ClienteId').val();
					var $marca = $('#placas').val();
					window.location.href = 'Clientes/actualizarClienteSap' + "/" + $id+"/"+$marca;
					$(this).dialog("close");
				},
			}
		}
	).parent('.ui-dialog').find('.ui-dialog-titlebar-close').hide();
}
