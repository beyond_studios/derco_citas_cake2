messaje.onClose = function(){
	window.location.href = 'clientes/bandejaReprogramar';
}
var mymsn = new messaje();

$(document).ready(function(){
	
	mymsn.init();	

	$('.hasDatepicker').focus(function(){
		$('.hasDatepicker').datepicker('show');
	});

});

function verificarVehicle(miThis){
	var cliVehId = $(miThis).find('option:selected').val();
	checkCitations(cliVehId);
}

function checkCitations(cliVehId){
	console.log(cliVehId);
	if(cliVehId == '' || cliVehId == null) return false;
	
	$.ajax({
		url:getUrl('agedetallecitas/getCitaClientJson/'+cliVehId),
		async : false,
		dataType:'json',
		success:function(response, postdata){
			if(response.susses && response.cita){
				mymsn.setMsg(response.message);
				
				mymsn.open();
				
				return false;
			}else{
				if(!response.cita){
					//$('#buscadorForm').attr('method','get');
					//$('#buscadorForm').attr('action', $('#buscadorForm').attr('url2')+'/'+cliVehId);
					//$('#buscadorForm').submit();
					window.location.href = $('#buscadorForm').attr('url2')+'/'+cliVehId;
				}
			}
		},
		error:function(){
			$('#AgeclientesVehiculoId').find('option').attr('selected', false);
			$($('#AgeclientesVehiculoId').find('option').get(0)).attr('selected', 'selected');
			mymsn.setMsn('ERROR AL RECUPERAR LOS DATOS\nINTENTE NUEVAMENTE');
			mymsn.open();
		}
	});
}
