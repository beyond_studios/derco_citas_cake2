/**
 * @author Ronald Tineo
 */
$(function(){
	var validator=$('#ClienteAddclientesForm').validate({
	rules:{
		'data[Cliente][placa]':{
			required:true
		},
		'data[Cliente][email]':{
			required:true
		},
		'data[Cliente][telefono]':{
			required:true
		},
		'data[Cliente][celular]':{
			required:true
		},
		'data[Cliente][username]':{
			required:true
		}
	},
	errorPlacement: function(label, element)
	{  label.insertAfter(element);}    
	});
	
	$('#ClienteAddclientesForm').submit(function(e){
		if ($('#ClienteAddclientesForm').valid()) {
			$('#imgLoader').show();
			$('#btnGuardar').hide();
			$('#btnReset').hide();
			return true;			
		}else{
			return false;
		}

	});
	setColorClienteTipo();
});
