$(document).ready(function(){
	$("#dialog_cliente").dialog({
		bgiframe: true,
		resizable: false,
		height: 370,
		width: 470,
		modal: true,
		autoOpen: false,
		overlay: {
			backgroundColor: '#000',
			opacity: 0.5
		}
	});
	
	clientes.init();
});

function abrirDialog(id_div){
	$("#"+id_div).dialog('open');
}

var clientes = {
	init:function(){
		jQuery("#clientes").jqGrid({ 
			url:getUrl('clientes/getClientes?d=0'), 
			datatype: "json", 
			colNames:['id', 'TP','Codigo', 'Nro Doc', 'Nombres','distrito','telefono','celular','email'], 
			colModel:[ 
				{name:'id',index:'id',hidden:true}, 
				{name:'documento_tipo',index:'documento_tipo',width: 30, align:"right", editable:true,editoptions:{size:40},editrules:{edithidden:true,required:true}},
				{name:'codigo_sap', hiden:false ,index:'codigo_sap', width: 100, editable:false},
				{name:'documento_numero',index:'documento_numero', width:120,  editable:true,editoptions:{size:40},editrules:{edithidden:true,required:true}},
				{name:'nombres', index:'nombres', width: 190, editable:true,editoptions:{size:40,maxlength:"45"}},
				{name:'distrito', index:'distrito',hidden:true}, 
				{name:'telefono', index:'telefono',hidden:true}, 
				{name:'celular',index:'celular',hidden:true}, 
				{name:'email',index:'email',hidden:true} 
			], 
			rowNum:10, 
			rowList:[10,20,30], 
			pager: '#clientes-pager', 
			sortname: 'Cliente.documento_tipo', //no se pa q sirve
			viewrecords: true, 
			sortorder: "desc", 
			caption:false,
			height:'225',
			ondblClickRow: function(id) {
				if (!(id == null)) {
					var gr = jQuery("#clientes").jqGrid('getRowData',id);
					if( gr != null ){
						baseSetCliente(gr.id,gr.documento_tipo,gr.documento_numero,"",gr.nombres,gr.distrito,gr.telefono,gr.celular,gr.email);
						$("#dialog_cliente").dialog('close');
					}
				}		
			},
			editurl:getUrl('Clientes/setCliente?d=0'),
		}); 
		
		jQuery("#clientes").jqGrid('navGrid','#clientes-pager',
			{edit:false,add:false,del:false,search:false},
			{width:420, clearAfterEdit:false,reloadAfterSubmit:true, closeOnEscape:true,afterSubmit:clientes.afterSubmit},
			{width:420, clearAfterAdd:true,reloadAfterSubmit:true, closeOnEscape:true, afterSubmit:clientes.afterSubmit},
			{reloadAfterSubmit:true, closeOnEscape:true}	
		);
		 
		$("#clientes").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : true});
		
		//jQuery("#ubigeos").jqGrid('hideCol',["phone"]);
	},
	afterSubmit: function(response, postdata) {
		var success = true;
    	var message = ""
    	var json = eval('(' + response.responseText + ')');
    	if(json.errors) {
    		success = false;
    		for(i=0; i < json.errors.length; i++) {
    			message += json.errors[i] + '<br/>';
    		}
    	}
    	var new_id = "1";
		return [success,message,new_id];
    }
}