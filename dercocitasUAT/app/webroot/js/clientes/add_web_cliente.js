$(document).ready(function(){
	validateForm();
});

function validateForm(){
	$('#ClienteAddWebClienteForm').validate({
		submitHandler: function(form){
			var $list = 'input[type=submit],input[type=button],input[type=reset],button';
			$(form).find($list).attr('disabled','disabled');
			form.submit();
			return false;
		},
		errorPlacement: function(label, element){
			if (element.hasClass('msg_error_1')) 
				label.insertAfter(element.next());
			else 
				if (element.hasClass('msg_error_2')) 
					label.insertAfter(element.next().next());
				else 
					if (element.hasClass('msg_error_3')) 
						label.insertAfter(element.next().next().next());
					else 
						label.insertAfter(element);
		}
	});
}

function validIpDoc(){
	var option = $('#ClienteDocumentoTipo').find('option:selected');
	var value = option.val();
	
	$('#ClienteDocumentoNumero').attr('value', "");
	
	if(value == "DN"){
		$('#ClienteDocumentoNumero').attr('maxlength', "8");
		$('#ClienteDocumentoNumero').attr('minlength', "8");
	}else if(value == "RU"){
		$('#ClienteDocumentoNumero').attr('maxlength', "11");
		$('#ClienteDocumentoNumero').attr('minlength', "11");
	}else{
		$('#ClienteDocumentoNumero').attr('maxlength', "25");
		$('#ClienteDocumentoNumero').attr('minlength', "1");
	}
}
