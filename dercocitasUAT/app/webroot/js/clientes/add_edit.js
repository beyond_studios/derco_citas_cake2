$().ready(function(){
	$('#cabecera td').css('border',0);
	$('#cabecera input').css('width','200px');
	$('#cabecera select').css('width','200px');
	$('#WorclientDepartamento').css('width','180px');
	
	$('.box').css('height','25px');
	setInicializarTipoPersona();
	validateForm();
});


function validateForm(){
	$('#client').validate({
		submitHandler:function(form){
			var $list = 'input[type=submit],input[type=button],input[type=reset],button';
			$(form).find($list).attr('disabled','disabled');
			form.submit();
			return false;
		},
		errorPlacement: function(label, element){	
			if(element.hasClass('msg_error_1'))
				label.insertAfter(element.next());
			else if (element.hasClass('msg_error_2'))
				label.insertAfter(element.next().next());
			else if (element.hasClass('msg_error_3'))
				label.insertAfter(element.next().next().next());
			else
				label.insertAfter(element);
		}   
	});
}

function baseSetCliente(id,ubigeo,departamento,provincia,distrito){
	$('#CLienteId').attr('value',id);
	$('#WorclientDepartamento').attr('value',departamento);
	$('#WorclientProvincia').attr('value',provincia);
	$('#WorclientDistrito').attr('value',distrito);
}

function setInicializarTipoPersona(){
	var tipoPersona = $('#WorclientTipopersonaId').find('option:selected').val();
	if(tipoPersona == 1){
		$('#WorclientSocialreason').removeClass('required').attr('readonly',true);
		$('#WorclientRuc').removeClass('required').attr('readonly',true);
		$('#WorclientAddressSocialreason').removeClass('required').attr('readonly',true);
		
		$('#WorclientDni').addClass('required').attr('readonly','').attr('value','').next().removeClass('hide');
		$('#WorclientName').addClass('required').attr('readonly','').attr('value','').next().removeClass('hide');
		$('#WorclientAddress').addClass('required').attr('readonly','').attr('value','').next().removeClass('hide');
		$('#WorclientPhone').addClass('required').attr('readonly','').attr('value','').next().removeClass('hide');
	}else{
		$('#WorclientSocialreason').addClass('required').attr('readonly','').next().removeClass('hide');
		$('#WorclientRuc').addClass('required').attr('readonly','').next().removeClass('hide');
		$('#WorclientAddressSocialreason').addClass('required').attr('readonly','').next().removeClass('hide');
		
		$('#WorclientDni').removeClass('required').attr('readonly','').attr('value','').next().addClass('hide');
		$('#WorclientName').removeClass('required').attr('readonly','').attr('value','').next().addClass('hide');
		$('#WorclientAddress').removeClass('required').attr('readonly','').attr('value','').next().addClass('hide');
		$('#WorclientPhone').removeClass('required').attr('readonly','').attr('value','').next().addClass('hide');
	}
}

function setTipoPersona(){
	var tipoPersona = $('#WorclientTipopersonaId').find('option:selected').val();
	
	if(tipoPersona == 1){
		$('#WorclientSocialreason').removeClass('required').attr('readonly',true).attr('value','').next().addClass('hide');
		$('#WorclientRuc').removeClass('required').attr('readonly',true).attr('value','').next().addClass('hide');
		$('#WorclientAddressSocialreason').removeClass('required').attr('readonly',true).attr('value','').next().addClass('hide');
		
		$('#WorclientDni').addClass('required').attr('readonly','').attr('value','').next().removeClass('hide');
		$('#WorclientName').addClass('required').attr('readonly','').attr('value','').next().removeClass('hide');
		$('#WorclientAddress').addClass('required').attr('readonly','').attr('value','').next().removeClass('hide');
		$('#WorclientPhone').addClass('required').attr('readonly','').attr('value','').next().removeClass('hide');
	}else{
		$('#WorclientSocialreason').addClass('required').attr('readonly','').attr('value','').next().removeClass('hide');
		$('#WorclientRuc').addClass('required').attr('readonly','').attr('value','').next().removeClass('hide');
		$('#WorclientAddressSocialreason').addClass('required').attr('readonly','').attr('value','').next().removeClass('hide');
		
		$('#WorclientDni').removeClass('required').attr('readonly','').attr('value','').next().addClass('hide');
		$('#WorclientName').removeClass('required').attr('readonly','').attr('value','').next().addClass('hide');
		$('#WorclientAddress').removeClass('required').attr('readonly','').attr('value','').next().addClass('hide');
		$('#WorclientPhone').removeClass('required').attr('readonly','').attr('value','').next().addClass('hide');
	}
}