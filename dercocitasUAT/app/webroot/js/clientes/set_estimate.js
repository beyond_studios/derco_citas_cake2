var TIPOREPUESTO_ID = 1;

$().ready(function(){
	$('#cabecera td').css('border',0);
	$('#cabecera input').css('width','200px');
	$('#cabecera select').css('width','200px');
	
	$('.box').css('height','25px');
	
	validateForm();
});


function validateForm(){
	$('#setEstimate').validate({
		submitHandler:function(form){
			var $list = 'input[type=submit],input[type=button],input[type=reset],button';
			$(form).find($list).attr('disabled','disabled');
			form.submit();
			return false;
		},
		errorPlacement: function(label, element){	
			if(element.hasClass('msg_error_1'))
				label.insertAfter(element.next());
			else if (element.hasClass('msg_error_2'))
				label.insertAfter(element.next().next());
			else
				label.insertAfter(element);
		}   
	});
}

function baseSetInsurance(miThis){
	var insurance = miThis.parent().find('select option:selected').html();
	$('#WorestimateWorinsuranceName').attr('value',insurance);
}

// INTERACTUAN CON EL BUSCADOR DE CLIENTES ##########################################
function baseSetCliente(id,documento_tipo,documento_numero,apellidos,nombres,distrito,telefono,celular,email){
	$('#ClienteId').attr('value',id);
	$('#ClienteDocumentoTipo').attr('value',documento_tipo);
	$('#ClienteDocumentoNumero').attr('value',documento_numero);
	$('#ClienteApellidoPaterno').attr('value',apellidos+', '+nombres);
	$('#ClienteDistrito').attr('value',distrito);
	$('#ClienteTelefono').attr('value',telefono);
	$('#ClienteCelular').attr('value',celular);
	$('#ClienteEmail').attr('value',email);
}

// INTERACTUAN CON EL SELECCIONADOR DE PLACAS ##########################################
function setPlaca(){
	var placa= $('#AgedetallecitaVehiculosclienteId').find('option:selected').val();
	if(placa != 0){		
		$('#AgedetallecitaMarca').attr('value',placa);
		$('#AgedetallecitaModelo').attr('value','eeee');
	}
}