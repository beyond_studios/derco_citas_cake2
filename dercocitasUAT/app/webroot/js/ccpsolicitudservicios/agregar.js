var url;
$(document).ready(function(){
	url = $('base').attr('href');
	inicializarFechas();
	
	inicializarDialogo();
	presupuestos.init();
	
	$('#agregarForm').validate({
		submitHandler: function(form){
			//verificamos que haya elegido una tarea
			checado = $("#tableros").find('input[type=checkbox]:checked').length;
			
			if (checado == '0'){
				alert('Seleccione algun servicio');
				return false;	
			}else{
				form.submit();
			}
		}
	});
});

function inicializarFechas(){
	$(".dialog_fecha").datepicker({
		showOn: 'button',
		buttonImage:url+'img/derco/defecto/aventura/calendar.gif',
		buttonImageOnly: true, 
		buttonText: 'Cal',
		dateFormat: 'dd-mm-yy'
	});
}

function abrirDialog(id_div){
	$("#"+id_div).dialog('open');
}

function inicializarDialogo(id_div){
	$("#dialog_presupuesto").dialog(
		{
			bgiframe: true,
			resizable: false,
			height: 340,
			width: 540,
			modal: true,
			autoOpen: false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}
	);
}

var presupuestos = {
	init:function(){
		jQuery("#presupuestos").jqGrid({ 
			url:url+'ccpsolicitudservicios/getPresupuestos?q=2', 
			datatype: "json", 
			colNames:['Ot','Pres.', 'Fecha', 'Asesor','Chasis','Cliente','Placa','Fecha entrega','Fecha aprobacion'], 
			colModel:[ 
				{name:'nroOt',index:'ot.DocNro', width:55,align:"right"}, 
				{name:'nropre',index:'ot.DocNro', width:55,align:"right", sortname:false, sortorder: false}, 
				{name:'fecha',index:'ot.DocFch', width:75, align:"lefth"},
				{name:'asesor',index:'ot.DocUsrAbr', width:90, align:"lefth"}, 
				{name:'chasis',index:'ot.AutNumSer', width:130},
				{name:'cliente',index:'ot.CliApe',width:90},
				{name:'placa',index:'ot.AutNumMat',hidden:true},
				{name:'fechaentrega',index:'ot.DocFecProm',hidden:true},
				{name:'preFechaAprob',index:'ot.DocAseFch',hidden:true}], 
			rowNum:10, 
			rowList:[10,20,30], 
			pager: '#presupuestos-pager', 
			sortname: 'ot.DocNro', 
			viewrecords: true, 
			sortorder: "desc", 
			caption:false,
			height:'195',
			onSelectRow: function(id){
				if (!(id == null)) {
					var ret = jQuery("#presupuestos").jqGrid('getRowData',id);
					$("#ccpsolicitudserviciosPresupuestoNumero").attr('value',ret.nropre);
					$("#ccpsolicitudserviciosOtNumero").attr('value',ret.nroOt);
					$("#ccpsolicitudserviciosOtCliente").attr('value',ret.cliente);
					$("#ccpsolicitudserviciosOtAsesor").attr('value',ret.asesor);
					$("#ccpsolicitudserviciosOtPlaca").attr('value',ret.placa);
					$("#ccpsolicitudserviciosOtFechaCreacion").attr('value',ret.fecha);
					$("#ccpsolicitudserviciosOtFechaEntrega").attr('value',ret.fechaentrega);
					$("#presupuesto_fecha_aprobacion").attr('value',ret.preFechaAprob);
					
					$("#ccpsolicitudserviciosOtNumero").parent().find('label.error').html('');
				}
			}
		}); 
		
		jQuery("#presupuestos").jqGrid('navGrid','#presupuestos-pager',{edit:false,add:false,del:false});
		  $("#presupuestos").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : true});
		
	}
}