var url;
$(document).ready(function(){
	url = $('base').attr('href');
	inicializarFechas();
	InicializarTextos();
	vistapreviaArchivos();
	$('#agregarForm').validate({
		submitHandler: function(form){
			//verificamos que haya elegido una tarea
			checado = $("#tableros").find('input[type=checkbox]:checked').length;
			
			if (checado == '0'){
				alert('Seleccione algun servicio');
				return false;	
			}else{
				form.submit();
			}
		}
	});
});

function inicializarFechas(){
	$("#ccpsolicitudserviciosFechaPropuestaEntrega").datepicker({
		showOn: 'button', 
		buttonImage: getUrl('img/calendar.gif'), 
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		minDate: -0,
		dateFormat: 'dd/mm/yy'
	}, $.datepicker.regional['es']);
}

function vistapreviaArchivos(){
	var texto = $(this);
	if(texto.val()!=''){
		var imagen='<a href="'+texto.val()+'">vista previa</a>'
		//$(imagen).insertAfter(texto);
	}
}

function InicializarTextos(){
	$('#ccpsolicitudserviciosCcparchivopresupuesto').bind('change', vistapreviaArchivos);
	$('#ccpsolicitudserviciosCcparchivoorden').bind('change', vistapreviaArchivos);
}

function eliminarArchivoPresupuesto(thisobj){
	var imageneliminar=$(thisobj);
	var html="";
	jConfirm('¿Desea Eliminar este Archivo?','Ventana de Confirmación',function(e){
		if (e){
			var celda=imageneliminar.parent();
			html='<input id="ccpsolicitudserviciosCcparchivopresupuesto" class="required" type="file" size="15" name="data[ccpsolicitudservicios][ccparchivopresupuesto]">'+
			'<span class="campoObligatorio">*</span>'+
			'<div class="clear"></div>'+
			'<div>'+
			'<label class="error" style="display: none;" generated="true" for="ccpsolicitudserviciosCcparchivopresupuesto"></label>'+
			'</div>';
			
			$(celda).html(html);
		}	
	});
}
function eliminarArchivoOrden(thisobj){
	var imageneliminar=$(thisobj);
	var html="";
	jConfirm('¿Desea Eliminar este Archivo?','Ventana de Confirmación',function(e){
		if (e){
			var celda=imageneliminar.parent();
			html='<input id="ccpsolicitudserviciosCcparchivoorden" class="required" type="file" size="15" name="data[ccpsolicitudservicios][ccparchivoorden]">'+
				'<span class="campoObligatorio">*</span>'+
				'<div class="clear"></div>'+
				'<div>'+
				'<label class="error" style="display: none;" generated="true" for="ccpsolicitudserviciosCcparchivoorden"></label>'+
				'</div>';
			
			$(celda).html(html);
		}	
	});
}
