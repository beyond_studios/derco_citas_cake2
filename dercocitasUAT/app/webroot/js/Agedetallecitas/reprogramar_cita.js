var mymsn = new messaje();
var action = false;
var url;
var mensajeError = "Hay errores de Conexion... Consulte con su Administrador";
var vehicle = new vehicles();

$(document).ready(function(){
	url = $('base').attr('href');
	var reprogramarId = $('#BaseCitarepogramarId').val();
	iniFechas();
	validateForm();
	mymsn.init();
	vehicle.init();
	inicializarDialogs();
	if($('#AgedetallecitaIsPost').val() == 1){
		getHorariosCliente();	
	}
	
	$('#ConfirmReprogramarCitaForm').submit(function(e){
		e.preventDefault();
		$('#ConfirmClienteid').val($('#AgedetallecitaClienteId').val());
		if($('#ConfirmReprogramarCitaForm').valid()){
			//$('#buttonConfirmSubmit').hide();
			$('#buttonConfirmSubmit').attr('disabled', true);
			$('#imgLoader').show();
			$.ajax({
				url:url+'agedetallecitas/updateEmailPhoneClienteJson',
				type:'POST',
				data:$('#ConfirmReprogramarCitaForm').serialize(),
				dataType:'json',
				success: function(data){
					if(data.Success==true){
						$.ajax({
								type: 'POST',
								url: url+'agedetallecitas/agregarCitaAjax/'+reprogramarId,
								data: $('#modificarForm').serialize(),
								dataType: 'json',
								success: function(data){
									if(data.Success==true){
										$('#dialog_confirm').dialog("close");
										var $list = 'input[type=submit],input[type=button],input[type=reset],button';
										$('#modificarForm').find($list).attr('disabled','disabled');
										$('#mensajeFinal').dialog().data('id',data.id);
										$('#mensajeFinaltexto').html(data.Mensaje);
										$('#mensajeFinal').dialog("open");
									}else{
										$('#dialog_confirm').dialog("close");
										showButtonSubmitOfConfirm();
										mymsn.setMsg(data.Mensaje);
										mymsn.open();
									}
								},
								error: function(){
									showButtonSubmitOfConfirm();
									mymsn.setMsg(mensajeError);
									mymsn.open();
								}
						});
					}else {
						showButtonSubmitOfConfirm();
						mymsn.setMsg(data.Mensaje);
						mymsn.open();
					}
				},
				error: function(){
					showButtonSubmitOfConfirm();
					mymsn.setMsg(mensajeError);
					mymsn.open();
				}
			});
		}
		return false;
	});
	
	$('#modificarForm').submit(function(e){
		e.preventDefault();
		console.log($('#modificarForm').valid());
		
		if ($('#modificarForm').valid()) {
			getDatosConfirm();
			$('#dialog_confirm').dialog("open");
		}
		return false;
	});
	
	//color al tipo de cliente
	setColorClienteTipo();
});

function showButtonSubmitOfConfirm(){
	$('#buttonConfirmSubmit').show();
	$('#imgLoader').hide();
}

function getDatosConfirm(){
	$('#ConfirmNombre').html($('#ClienteApellidoPaterno').val());
	$('#ConfirmNumeroDocumento').html($('#ClienteDocumentoNumero').val());
	$('#ConfirmPlaca').html($('#AgedetallecitaPlaca').val());
	$('#ConfirmLocal').html($('#SecprojectId option:selected').html());
	//obtenemos el check de la tabla
	$('#ConfirmFechaHoraCita').html($('#AgedetallecitaFecha').val()+" "+ $('#HorariosClientes input:checked').parent().parent().children(':first').html());
	$('#ConfirmEmail').val($('#ClienteEmail').val());
	$('#ConfirmTelefono').val($('#ClienteTelefono').val());
}

function closeDialog(){
	$('#dialog_confirm').dialog("close");
}
function inicializarDialogs(){
	var $urlafterSave = $('#ConfirmUrlAfterSave').val();
	$('#dialog_confirm').dialog(
		{
			bgiframe: true,
			resizable: false,
			height: 450,
			width: 500,
			modal: true,
			autoOpen: false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		}
	);
	
	$('#mensajeFinal').dialog(
		{
			bgiframe: true,
			resizable: false,
			height: 200,
			width: 500,
			modal: true,
			autoOpen: false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			},
			buttons: {
				"Aceptar": function(){
					var $id = $(this).data('id');
					window.location.href = $urlafterSave+"/"+$id;
					$(this).dialog("close");
				},
				"Cancelar": function(){
					var $id = $(this).data('id');
					window.location.href = $urlafterSave+"/"+$id;
					$(this).dialog("close");
				},
			}
		}
	).parent('.ui-dialog').find('.ui-dialog-titlebar-close').hide();
}

function validateForm(){
	$('#modificarForm').validate({
		submitHandler:function(form){
			//var $list = 'input[type=submit],input[type=button],input[type=reset],button';
			//$(form).find($list).attr('disabled','disabled');
			//form.submit();
			return false;
		},
		errorPlacement: function(label, element){	
			if(element.hasClass('msg_error_1'))
				label.insertAfter(element.next());
			else if (element.hasClass('msg_error_2'))
				label.insertAfter(element.next().next());
			else if (element.hasClass('msg_error_3'))
				label.insertAfter(element.next().next().next());
			else
				label.insertAfter(element);
		}   
	});

	var v = jQuery("#buscadorForm").validate({
			submitHandler: function(form) {
				var option = $('#Talcitacalendar2MotivoId').find('option:selected');
				var value = option.val();
				
				if((value == "") || (value == '0')){ alert('SELECCIONE EL MOTIVO DE SERVICIO'); return false; }
				
				$('#BuscadorAgecitacalendarioId').val(option.attr('agecitacalendario_id'));
				
				jQuery(form).ajaxSubmit({
					target: "#programacion"
				});
			},
			errorPlacement: function(label, element){	
				if(element.hasClass('msg_error_1'))
					label.insertAfter(element.next());
				else if (element.hasClass('msg_error_2'))
					label.insertAfter(element.next().next());
				else if (element.hasClass('msg_error_3'))
					label.insertAfter(element.next().next().next());
				else
					label.insertAfter(element);
			} 
		});
}

function iniFechas() {
	$("#AgedetallecitaFecha").datepicker({
		showOn: 'button', 
		buttonImage: getUrl('img/calendar.gif'), 
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		minDate: -0,
		dateFormat: 'dd/mm/yy',
		onSelect: function(textoFecha, objDatepicker){
	      $('#buttonSubmit').addClass('hide');
		  $('#listaPrincipal').addClass('hide');
	   }
	}, $.datepicker.regional['es']);
	
	var dates = $("#BuscadorFechaInicial, #BuscadorFechaFinal").datepicker({
		showOn: 'button', 
		buttonImage: getUrl('img/calendar.gif'), 
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'dd/mm/yy',
		onSelect: function(selectedDate) {
			var option = this.id == "BuscadorFechaInicial" ? "minDate" : "maxDate",
				instance = $(this).data( "datepicker" ),
				date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings);
			dates.not(this).datepicker("option", option, date);
		}
	}, $.datepicker.regional['es']);
}


function baseSetVehicle(id,placa,marca,modelo,codigo_vehiculo,cliente_id,ultimo_mant,siguiente_mant,
	documento_tipo,documento_numero,apellidos,nombres,distrito,telefono,celular,email
	,marca_id){
		
	$('#AgedetallecitaMarca').val(marca);
	$('#AgedetallecitaModelo').val(modelo);
	$('#AgedetallecitaCodigoUnidad').val(codigo_vehiculo);
	$('#AgedetallecitaPlaca').val(placa);
	$('#AgedetallecitaAgeclientesVehiculoId').val(id);
	
	baseSetCliente(id,documento_tipo,documento_numero,apellidos,nombres,distrito,telefono,celular,email,marca_id);	
}

// INTERACTUAN CON EL BUSCADOR DE CLIENTES ##########################################
function baseSetCliente(cliente_id, documento_tipo, documento_numero, apellidos, nombres, distrito, telefono, celular, email, marca_id){
	$('#AgedetallecitaClienteId').attr('value', id);
	$('#ClienteDocumentoTipo').attr('value', documento_tipo);
	$('#ClienteDocumentoNumero').attr('value', documento_numero);
	$('#ClienteApellidoPaterno').attr('value', nombres);
	$('#ClienteDistrito').attr('value', distrito);
	$('#ClienteTelefono').attr('value', telefono);
	$('#ClienteCelular').attr('value', celular);
	$('#ClienteEmail').attr('value', email);
	
	getSecproject(marca_id);
}

function getSecproject(marca_id){
	var cliVehId = $('#AgedetallecitaAgeclientesVehiculoId').val();
	var secprojects = $('#SecprojectId');
	
	$('#buttonSubmit').addClass('hide');
	$('#listaPrincipal').html("");

	if(!(marca_id == "")){
		//validamos si la marca se puede generar su cita
		if(!checkCitations(cliVehId)) marca_id = "";
	}

	if((marca_id == "")){
		secprojects.html('<option value="" selected>Seleccionar</option>').attr('disabled', true);
		$('#AgemotivoservicioId').html('<option value="" selected>Seleccionar</option>').attr('disabled', true);
		$('#AgetipomantenimientoId').html('<option value="" selected>Seleccionar</option>').attr('disabled', true).removeClass('required');
		$('#AgedetallecitaAgetiposervicioId').html('<option value="" selected>Seleccionar</option>').attr('disabled', true);
		return false;
	}
	
	secprojects.next().removeClass('hide');
	$.ajax({
		url:getUrl('secprojects/getSecprojecsMarcaJson/'+marca_id),
		async : false,
		dataType:'json',
		success:function(response, postdata){
			secprojects.next().addClass('hide');
			secprojects.html('<option value="" selected>Seleccionar</option>');
			if(!response.susses){
				secprojects.attr('disabled', true);
				alert(response.errors.msg);
				return false;
			}
			
			$.each(response.data, function(i,val){
				secprojects.append('<option value="'+i+'">'+val.name+'</option>');
			});
			secprojects.attr('disabled', false);
		},
		error:function(){
			secprojects.html('<option selected>Error...</option>');
			secprojects.attr('disabled', true);
			secprojects.next().addClass('hide');
		}
	});
}

function checkCitations(cliVehId){
	var r_return = true;
	$.ajax({
		url:getUrl('agedetallecitas/getCitaClientJson/'+cliVehId),
		async : false,
		dataType:'json',
		success:function(response, postdata){
			if(response.susses && response.cita){
				r_return = false;
				mymsn.setMsg(response.message);
				mymsn.open();
			}
		},
		error:function(){
			r_return = false;
			
			$(miThis).find('option').attr('selected', false);
			$($(miThis).find('option').get(0)).attr('selected', 'selected');
			mymsn.setMsn('ERROR AL RECUPERAR LOS DATOS\nINTENTE NUEVAMENTE');
			mymsn.open();
		}
	});
	return r_return;
}

function getMotivoServicio(miThis){
	var option = $(miThis).find('option:selected')
	var id = option.val();
	var selectHijo = $('#AgemotivoservicioId');
	var marcaId = $('#MarcaId').val();
	$('#buttonSubmit').addClass('hide');
	$('#listaPrincipal').html("");
	
	if((id == "")){
		selectHijo.html('<option value="" selected>Seleccionar</option>').attr('disabled', true);
		$('#AgetipomantenimientoId').html('<option value="" selected>Select...</option>').attr('disabled', true).removeClass('required');
		return false;
	}
	
	selectHijo.next().removeClass('hide');
	$.ajax({
		url:getUrl('agemotivoservicios/getMotivoServicioJson/'+id+'/'+marcaId),
		async : false,
		dataType:'json',
		success:function(response, postdata){
			selectHijo.next().addClass('hide');
			selectHijo.html('<option value="" selected>Seleccionar</option>');
			if(!response.susses){
				selectHijo.attr('disabled', true);
				alert(response.errors.msg);
				return false;
			}
			
			$.each(response.data, function(i,val){
				selectHijo.append('<option agecitacalendario_id="'+val.agecitacalendario_id+'" value="'+i+'">'+val.name+'</option>');
			});
			
			selectHijo.attr('disabled', false);
		},
		error:function(){
			selectHijo.html('<option selected>Error...</option>');
			selectHijo.attr('disabled', true);
			selectHijo.next().addClass('hide');
		}
	});
}

function getTipoMantenimiento(miThis){
	var option = $(miThis).find('option:selected')
	var id = option.val();
	var selectHijo = $('#AgetipomantenimientoId');
	var selectHijo2 = $('#AgedetallecitaAgetiposervicioId');
	
	$('#buttonSubmit').addClass('hide');
	$('#listaPrincipal').html("");
	
	if((id == "")){
		selectHijo.html('<option value="" selected>Seleccionar</option>').attr('disabled', true);
		selectHijo.removeClass('required');
		selectHijo2.html('<option value="" selected>Seleccionar</option>').attr('disabled', true);
		return false;
	}
	
	selectHijo.next().removeClass('hide');
	$.ajax({
		url:getUrl('agetipomantenimientos/getTipoMantenimientoJson/'+id),
		async : true,
		dataType:'json',
		success:function(response, postdata){
			selectHijo.next().addClass('hide');
			selectHijo.html('<option value="" selected>Seleccionar</option>');
			if(!response.susses){
				selectHijo.attr('disabled', true);
				return false;
			}
			
			$.each(response.data, function(i,val){
				selectHijo.append('<option value="'+i+'">'+val.name+'</option>');
			});
			selectHijo.attr('disabled', false);
			selectHijo.addClass('required');
		},
		error:function(){
			selectHijo.html('<option selected>Error...</option>');
			selectHijo.attr('disabled', true);
			selectHijo.next().addClass('hide');
			selectHijo.removeClass('required');
		}
	});
	
	selectHijo2.next().removeClass('hide');
	$.ajax({
		url:getUrl('agetiposervicios/getTiposerviciosJson/'+id),
		async : true,
		dataType:'json',
		success:function(response, postdata){
			selectHijo2.next().addClass('hide');
			selectHijo2.html('<option value="" selected>Seleccionar</option>');
			if(!response.susses){
				selectHijo2.attr('disabled', true);
				return false;
			}
			
			$.each(response.data, function(i,val){
				selectHijo2.append('<option mantenimiento="'+val.maintenance+'" value="'+i+'">'+val.name+'</option>');
			});
			selectHijo2.attr('disabled', false);
		},
		error:function(){
			selectHijo2.html('<option selected>Error...</option>');
			selectHijo2.attr('disabled', true);
			selectHijo2.next().addClass('hide');
		}
	});
	
	verTipomantenimiento($('#AgemotivoservicioId'));
}

function verTipomantenimiento(miThis){
	var v = $(miThis).find('option:selected').val();
	var m = $(miThis).find('option:selected').attr('mantenimiento');
	
	$('#divTipomantenimientoEtiqueta').addClass('hide');
	$('#divTipomantenimiento').addClass('hide');
	$('#AgetipomantenimientoId').val("").removeClass('required');
	
	if (!(v == "" || v == "0")) {
		if(m == true || m == 1 || m == "true"){
			$('#divTipomantenimientoEtiqueta').removeClass('hide');
			$('#divTipomantenimiento').removeClass('hide');
			$('#AgetipomantenimientoId').addClass('required');
		}
	}	
		
}

function getHorariosCliente(){
	var option = $('#AgemotivoservicioId').find('option:selected');
	var valor = option.val();
	var fecha = $('#AgedetallecitaFecha').val();
	
	if((fecha == "") || (fecha == null)){ alert('SELECCIONE UNA FECHA'); return false; } 
	if((valor == "") || (valor == '0')){ alert('SELECCIONE EL MOTIVO DE SERVICIO'); return false; }
	
	$('#buttonSubmit').addClass('hide');
	
	$.ajax({
		type: "POST",
		url:getUrl('agedetallecitas/getHorariosCliente'),
		async : false,
		data : { 
			fechaini: fecha, 
			agecitacalendario_id: option.attr('agecitacalendario_id')
		},
		success: function(html){
			$('#listaPrincipal').html(html);
			$('#listaPrincipal').removeClass('hide');
			seleccionar_primero();
		}
	});
	return false;
}

// VALIDAR QUE SELECCIONE ALMENOS UN HORARIO
function seleccionar_primero(){
	var allCheckbox = $('#modificarForm').find('input[type=checkbox]');
	
	if(allCheckbox.length != 0) $('#buttonSubmit').removeClass('hide');
	
	$(allCheckbox.get(0)).attr('checked','checked');
}
	
function alternarChecks(check) {
	$('#modificarForm').find('input[type=checkbox]').attr('checked',false);
    check.checked = true;   
}


function getMotivoServicioBuscador(miThis){
	var option = $(miThis).find('option:selected')
	var id = option.val();
	var selectHijo = $('#Talcitacalendar2MotivoId');
	
	if((id == "")){
		selectHijo.html('<option value="" selected>Seleccionar</option>').attr('disabled', true);
		$('#Talcitacalendar2Direccion').val("");
		return false;
	}
	
	$('#Talcitacalendar2Direccion').val(option.attr('direccion'));
	
	selectHijo.next().removeClass('hide');
	$.ajax({
		url:getUrl('agemotivoservicios/getMotivoServicioJson/'+id),
		async : false,
		dataType:'json',
		success:function(response, postdata){
			selectHijo.next().addClass('hide');
			selectHijo.html('<option value="" selected>Seleccionar</option>');
			if(!response.susses){
				selectHijo.attr('disabled', true);
				alert(response.errors.msg);
				return false;
			}
			
			$.each(response.data, function(i,val){
				selectHijo.append('<option agecitacalendario_id="'+val.agecitacalendario_id+'" value="'+i+'">'+val.name+'</option>');
			});
			
			selectHijo.attr('disabled', false);
		},
		error:function(){
			selectHijo.html('<option selected>Error...</option>');
			selectHijo.attr('disabled', true);
			selectHijo.next().addClass('hide');
		}
	});
}