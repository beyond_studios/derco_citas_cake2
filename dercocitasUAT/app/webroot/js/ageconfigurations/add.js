var url;
$(document).ready(function(){
	url = $('base').attr('href');
	
    $("#AgeconfigurationAgregarageconfigurationForm").validate({
	rules: {
			"data[Ageconfiguration][secorganization_id]": {
				required: true,
			},
			"data[Ageconfiguration][id]":{
			    required:true,
		    },
		    "data[Ageconfiguration][secrole_id]":{
			    required:true,
		    },
			"data[Ageconfiguration][horacortecitas]":{
			    required:true,
		    },		    
		    "data[Ageconfiguration][citasadicionales]":{
			    required:true,
		    }
		},
    });
	
	$('#AgeconfigurationSecorganizationId').bind('change',cargarRol);	
	
	function cargarRol(){
		var secorganization = $('#AgeconfigurationSecorganizationId').val();
		$.ajax({
			url:url+"Ageconfigurations/listroles",
			data:"data[Ageconfiguration][secorganization_id]="+secorganization,
			type:"POST",
			success:function(data){
				$('#AgeconfigurationSecroleId').html(data);
			}
		});
	}
});