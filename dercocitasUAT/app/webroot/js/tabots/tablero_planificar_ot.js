var url;
var refrescar;
var planificar;
var projectId;
var pulsos = 40;
var duracion = 60;
var tiempoActualizacion=60000; //5 Segundos
var fechaElegida;
var dia;
var mes;
var anio;
var horario_inicio;
var horario_fin;
var hora_ancho;
$(document).ready(function(){
	//TableroFecha el campo de fecha
	refrescar = $('#TableroRefrescar').val();
	planificar = $('#TableroPlanificar').val();
	projectId = $('#TableroProjectId').val();
	horario_inicio=$('#TableroHorarioInicio').val();
	horario_fin=$('#TableroHorarioFin').val();
	hora_ancho=$('#TableroHoraAncho').val();
	
	// obtenemos los parametros de fecha (D M Y)
	fechaElegida = $('#TableroFechaElegida').val();
	dia = fechaElegida.substring(0, 2);
	mes = fechaElegida.substring(3, 5);
	anio = fechaElegida.substring(6);
	
	//console.debug(planificar); // para ver en debug
	url = $('base').attr('href');
	f_programar();
	f_ejecutarOts();
	f_fechaHora();
	inicializarFechas();
});
	function inicializarFechas(){
		$('#TableroFecha').datepicker({
			showOn: 'button',
			buttonImage: url+('/app/webroot/img/calendar.gif'),
			buttonImageOnly: true,
			buttonText: 'Calendario'
		}, $.datepicker.regional['es']);
	}
	// ---------------------------------------- PARAMETROS DEL TABLERO ----------------------------------------
	// obtenemos los parametros necesarios para el tablero
	// seteamos los parametros del parpadeo: function blink(pulsos, duracion) {...}
	// permite mostrar una imagen mientras se carga un elemento
	function showLoading(elemento, mensaje) {
		var msj = mensaje ? mensaje : '';
		elemento.html('<div id="showLoading" align="center">' + msj + '<img src='+url+'/app/webroot/img/loader.gif></img></div>');
	}
	
	// ---------------------------------------- FUNCIONES DEL TABLERO ----------------------------------------
	// permite actualizar las ots a programar
	function f_programar() {
		var programar = $('#otsColumnaProgramar');
		showLoading(programar,null);
		$.ajax({
		url:url+('tabots/otsColumnaProgramar/'+projectId+'/'+refrescar+'/'+planificar),
		success:function(data){
			$('#showLoading').hide();
			programar.html(data);
		},
		}); 
	}
	
	// permite actualizar el seguimiento de las ots (planeadas vs. ejecutadas)
	function f_ejecutarOts() {
		var tablacentral=$('#ejecutarOts');
		showLoading(tablacentral,null);
		$.ajax({
			url:url+('tabtasks/tableroEjecutarTaskAjax/'+projectId+'/'+dia+'/'+mes+'/'+anio+'/'+refrescar+'/'+planificar),
			success:function(data){
				$('#showLoading').hide();
				tablacentral.html(data);
			},
		});
	}
	
	// Actualizar la hora cada minuto
	function f_fechaHora() {
		var cabecerafecha=$('#cabeceraFecha');
		$.ajax({
			url:url+('comun/horaActual'),
			success:function(data){
				cabecerafecha.html(data);
			},
		});
		f_reglaHora();
	}
	
	// Actualizar la regla cada minuto
	function f_reglaHora() {
		var reglaHora=$('#reglaHora');
		$.ajax({
			url:url+('tabots/desplazamientoRegla/'+horario_inicio+'/'+horario_fin+'/'+hora_ancho+'/'+projectId+'/'+dia+'/'+mes+'/'+anio),
			success:function(data){
				reglaHora.html(data);
			},
		});
	}
	var HoraIntervalo = setInterval(f_fechaHora, tiempoActualizacion); // Actualiza la Fecha cada minuto esta linea tiene que estar descomentada	
	// permite actualizar el semaforo de las ots suspendidas
	function f_otsSuspendidas() {
		// mostramos estado de las ots suspendidas
		var otsSuspendidas = new Ajax.Updater(
			'otsSuspendidas', 
			'<?php echo $html->url("/tabexecutedtasks/alarmaOtsSuspendidas")?>', 
			{
				method: 'get'
			}
		);
	}
	
	// permite actualizar el mensaje enviado al tablero
	function f_msgTablero() {
		var ejecutarOts = new Ajax.Updater(
			'msgTablero', 
			'<?php echo $html->url("/tabots/mensajeTablero/".$projectId."/".$refrescar."/".$planificar)?>', 
			{
				method: 'get'
			}
		);
	}
	
	// permite actualizar la fecha y hora actual

	
	// permite actualizar las columnas desde ejecucion hasta entrega
	function f_ejecucionOts() {
		f_ejecutarOts();
		f_reglaHora();
		//f_inspeccion();
	}
	
	// permite actualizar el contenido del tablero
	function actualizarPagina() {
		f_fechaHora();
		f_programar();
		f_ejecutarOts();
	}
	
	// permite ocultar el mensaje
	function ocultarMensaje() {
		if($('flashMessage')) {$('flashMessage').style.display = 'none';}
	}
	
	// ---------------------------------------- FUNCIONES DEL PROCESO ----------------------------------------
	function crearTask(id) {
		var url1 =url+'tabtasks/index/'+id;
		var w = window.open(url1, 'AgregarTareaAlOt', 
	        'scrollbars=yes,resizable=yes,width=850,height=400,top=100,left=200,status=no,location=no,toolbar=no');
	}

	function planificarTask(Tabtask_id) {
		var url1 =url+'tabplannedtasks/mostrarPlanificados/'+Tabtask_id; //va mostrar mes en ves de task
		var w = window.open(url1, 'Planificar_task', 
	        'scrollbars=yes,resizable=yes,width=700,height=350,top=100,left=200,status=no,location=no,toolbar=no');
	}
		
	function enviar() {
		var fecha = $('#TableroFecha').val();
		var dia = fecha.substring(0, 2);
		var mes = fecha.substring(3, 5);
		var anio = fecha.substring(6);
		//var url = "";
		if(refrescar == 1) {
			url1 = url+'tabots/tableroMostrarPlanificacionOt/'+projectId+'/'+dia+'/'+mes+'/'+anio;
		} else {
			if(planificar == 1) {
				url1 = url+'tabots/tableroPlanificarOt/'+projectId+'/'+dia+'/'+mes+'/'+anio;
			} else {
				url1 = url+'tabots/tableroEjecutarOt/'+projectId+'/'+dia+'/'+mes+'/'+anio;
			}
		}
		window.location.href = (url1);
	}
	
	function autentificarse(otplaneada_id, tipo, iniciado, mostrarOpcionJefeTaller) {
	    var url1 = url+'tabexecutedtasks/confirmarResponsable/'+otplaneada_id+"/"+tipo+"/"+iniciado+"/"+mostrarOpcionJefeTaller+"/"+projectId;
		//alert (url1);
	    var w = window.open(url1, 'autentificarse', 
	    	'scrollbars=no,resizable=yes,width=350,height=400,top=100,left=200,status=no,location=no,toolbar=no');
	}
	
	function listarSuspendidas() {
		var habilitarAcciones = (refrescar == 1) ? 0 : 1;
	    var url = "<?php echo $html->url('/tabexecutedtasks/listarOtsSuspendidas/'.$projectId)?>" + "/" + habilitarAcciones;
	    var w = window.open(url, 'listarSuspendidas', 
	        'scrollbars=yes,resizable=yes,width=1024,height=780,top=100,left=200,status=no,location=no,toolbar=no');
	}
	
	function suspenderOt(otejecutada_id, usuarioIdLogin) {
	    var url = "<?php echo $html->url('/tabexecutedtasks/suspenderOt/')?>" + otejecutada_id + "/" + usuarioIdLogin;
	    var w = window.open(url, 'suspenderOt', 
	    	'scrollbars=yes,resizable=yes,width=800,height=450,top=100,left=200,status=no,location=no,toolbar=no');
	}
	
	function suspenderTarea(otejecutada_id) {
	    var url1 = url+'tabexecutedtasks/suspenderTarea/'+ otejecutada_id+'/'+projectId;
	    var w = window.open(url1, 'suspenderOt', 
	    	'scrollbars=no,resizable=yes,width=400,height=450,top=100,left=200,status=no,location=no,toolbar=no');
	}
	
	function anularOt(otplaneada_id) {
		var url = "<?php echo $html->url('/tabots/confirmarAnularOt/'); ?>"+otplaneada_id;
	    var w = window.open(url, 'anularOt', 
	    	'scrollbars=yes,resizable=yes,width=800,height=450,top=100,left=200,status=no,location=no,toolbar=no');
	}