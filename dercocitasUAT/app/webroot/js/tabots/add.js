$(function(){
	var validator=$('#TabotAddForm').validate({
		rules:{
			'data[Tabot][document]':{
				required:true
			},
			'data[Tabot][description]':{
				required:true
			}
		},
		errorPlacement: function(label, element)
		{  label.insertAfter(element);}  
	});
});