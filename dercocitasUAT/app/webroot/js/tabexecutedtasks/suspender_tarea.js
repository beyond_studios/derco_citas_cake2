$(document).ready(function(){
});
	
$(function(){
		var validator=$('#TabexecutedtaskSuspenderTareaForm').validate({
		rules:{
			'data[Tabexecutedtask][tabactivities_id]':{
				required:true,
			},
			'data[Tabexecutedtask][comments]':{
				required:true,
			},
			'data[Tabexecutedtask][id]':{
				required:true,
			},
			'data[Tabexecutedtask][password]':{
				required:true,
			},			
		},
		errorPlacement: function(label, element)
		{  label.insertAfter(element);}    
		});
});

function validarHoraMinuto() {
		var resultado = true;
		var duracionH = $('TalotejecutadaHoras');
		var duracionM = $('TalotejecutadaMinutos');
		var eDuracion = $('TalotejecutadaDuracion');
		
		if(duracionH.value == 0 && duracionM.value == 0) {
			resultado = resultado && false;
			eDuracion.style.display = "block";
		} else {
			resultado = resultado && true;
			eDuracion.style.display = "none";
		}
		return resultado;
	}
	
	function enviarFormulario() {
		var resultado = true;
		resultado = validarObligatorios() && resultado;
		
		return resultado;
	}
	
	function terminar() {
		$('#TabexecutedtaskSupenderTerminar').val(1);
		$('#TabexecutedtaskSuspenderTareaForm').submit();
		
		/*var formulario = $('modificarForm');
		if(enviarFormulario() && confirm('¿Está seguro de terminar la tarea?')) {
			$('TabexecutedtaskSupenderTerminar').value=1;
			formulario.submit();
		}*/
	}
	
	$('modificarForm').onsubmit = function() {
		return enviarFormulario();
	}