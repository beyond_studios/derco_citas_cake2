var url;
$(document).ready(function(){
	url = $('base').attr('href');
	
    $("#AsesorservicioAddForm").validate({
	rules: {
			"data[Asesorservicio][id]":{
			    required:true,
		    },
			"data[Asesorservicio][secproject_id]":{
			    required:true,
		    },
			"data[Asesorservicio][secrole_id]":{
			    required:true,
		    },
		    "data[Asesorservicio][codigo_sap]":{
			    required:true,
		    },
		    "data[Asesorservicio][secperson_id]": {
				required: true,
			},
		},
    });
	
	$('#AsesorservicioSecorganizationId').bind('change',cargarSucursal);
	
	function cargarSucursal(){
		var secorganization = $('#AsesorservicioSecorganizationId').val();
		$.ajax({
			url:url+"Asesorservicios/listprojects",
			data:"data[Asesorservicio][secorganization_id]="+secorganization,
			type:"POST",
			success:function(data){
				$('#AsesorservicioSecprojectId').html(data);
			},
			complete:function(){
				cargarRolPersonas();
			}
		});
	}
	
	function cargarRolPersonas(){
		var secproject = $('#AsesorservicioSecorganizationId').val();
		$.ajax({
			url:url+"Asesorservicios/listrolpersonas",
			data:"data[Asesorservicio][secorganization_id]]="+secproject,
			type:"POST",
			success:function(data){
				$('#AsesorservicioSecpersonId').html(data);
			}
		});
	}
});