var vehicles = function(){
	
}

vehicles.prototype = {
	init: function(){
		var cliId = 0;
		
		// GENERAMOS EL DIALOGO
		$("#dialog_vehicle").dialog({
			bgiframe: true,
			resizable: false,
			height: 370,
			width: 720,
			modal: true,
			autoOpen: false,
			overlay: {
				backgroundColor: '#000',
				opacity: 0.5
			}
		});
		
		// GENERAMOS LA GRILLA
		jQuery("#vehicles").jqGrid({ 
			url:getUrl('AgeclientesVehiculos/getVehicles?cliId='+cliId), 
			datatype: "json", 
			colNames:[	'id','TP','Codigo', 'Nro Doc', 'Cliente','distrito','telefono','celular','email','Tipo Cliente Cod',' Tipo Cliente',
						'Placa','Marca', 'Modelo', 'Codigo','cli_id','Ult. Mant.','Sig. Mant'
						,'marca_id'], 
			colModel:[ 
				{name:'id',index:'AgeclientesVehiculo.id',hidden:true}, 
				
				{name:'documento_tipo',index:'Cliente.documento_tipo',width: 30, align:"right", hidden:false},
				{name:'codigo_sap', hidden:false ,index:'Cliente.codigo_sap', width: 120, align:"right"},
				{name:'documento_numero',index:'Cliente.documento_numero', width:120, hidden:false, align:"right"},
				{name:'nombres', index:'Cliente.nombres', width: 220},
				{name:'distrito', index:'Cliente.distrito',hidden:true}, 
				{name:'telefono', index:'Cliente.telefono',hidden:true}, 
				{name:'celular',index:'Cliente.celular',hidden:true}, 
				{name:'email',index:'Cliente.email',hidden:true},
				{name:'cliente_tipo',index:'Cliente.cliente_tipo',hidden:true},
				{name:'str_cliente_tipo',index:'Cliente.str_cliente_tipo',hidden:true},
				
				{name:'placa',index:'AgeclientesVehiculo.placa',width: 90, align:"right", editable:true,editoptions:{size:40},editrules:{edithidden:true,required:true}},
				{name:'marca',index:'AgeclientesVehiculo.marca',width: 90, align:"left", editable:true,editoptions:{size:40},editrules:{edithidden:true,required:true}},
				{name:'modelo',index:'AgeclientesVehiculo.modelo',width: 165, align:"right", hidden:true, editable:true,editoptions:{size:40},editrules:{edithidden:true,required:true}},
				{name:'codigo_vehiculo',index:'AgeclientesVehiculo.codigo_vehiculo',width: 30, align:"right", hidden:true},
				{name:'cliente_id',index:'AgeclientesVehiculo.cliente_id',width: 30, align:"right", editable:true, hidden:true},
				{name:'ultimo_mant',index:'AgeclientesVehiculo.ultimo_mant',width: 30, align:"right", editable:true, hidden:true},
				{name:'siguiente_mant',index:'AgeclientesVehiculo.siguiente_mant',width: 30, align:"right", hidden:true},
				
				{name:'marca_id',index:'Marca.id',width: 30, align:"right", hidden:true}
				
			], 
			rowNum:10, 
			rowList:[10,20,30], 
			pager: '#vehicles-pager', 
			sortname: 'Cliente.documento_tipo', //no se pa q sirve
			viewrecords: true, 
			sortorder: "desc", 
			caption:false,
			height:'225',
			ondblClickRow: function(id) {
				if (!(id == null)) {
					var gr = jQuery("#vehicles").jqGrid('getRowData',id);
					if( gr != null ){
						baseSetVehicle(gr.id,gr.placa,gr.marca,gr.modelo,gr.codigo_vehiculo,gr.cliente_id,gr.ultimo_mant,gr.siguiente_mant       
										,gr.documento_tipo,gr.documento_numero,"",gr.nombres,gr.distrito,gr.telefono,gr.celular,gr.email
										,gr.marca_id,gr.cliente_tipo,gr.str_cliente_tipo
						);
						
						if (gr.marca_id == 12) {
							$.ajax({
								url: getUrl("Chevrolets/validCar/" + gr.placa),
								dataType: 'json',
								success: function(res){
									alert(res.msg);
									baseSetVehicle(gr.id, gr.placa, gr.marca, gr.modelo, gr.codigo_vehiculo, gr.cliente_id, gr.ultimo_mant, gr.siguiente_mant, gr.documento_tipo, gr.documento_numero, "", gr.nombres, gr.distrito, gr.telefono, gr.celular, gr.email, gr.marca_id, gr.cliente_tipo, gr.str_cliente_tipo);
									$("#dialog_vehicle").dialog('close');
								},
								error: function(){
									alert('No se ha podido validar el vehículo');
								}
							});
						}else{
							baseSetVehicle(gr.id,gr.placa,gr.marca,gr.modelo,gr.codigo_vehiculo,gr.cliente_id,gr.ultimo_mant,gr.siguiente_mant       
											,gr.documento_tipo,gr.documento_numero,"",gr.nombres,gr.distrito,gr.telefono,gr.celular,gr.email
											,gr.marca_id,gr.cliente_tipo,gr.str_cliente_tipo
							);
							$("#dialog_vehicle").dialog('close');
						}
					}
				}		
			},
			editurl:getUrl('Clientes/setCliente?d=0'),
		}); 
		
		jQuery("#vehicles").jqGrid('navGrid','#vehicles-pager',
			{edit:false,add:false,del:false,search:false},
			{width:420, clearAfterEdit:false,reloadAfterSubmit:true, closeOnEscape:true,afterSubmit:vehicles.afterSubmit},
			{width:420, clearAfterAdd:true,reloadAfterSubmit:true, closeOnEscape:true, afterSubmit:vehicles.afterSubmit},
			{reloadAfterSubmit:true, closeOnEscape:true}	
		);
		 
		$("#vehicles").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : true});
	},
	afterSubmit: function(response, postdata) {
		var success = true;
    	var message = ""
    	var json = eval('(' + response.responseText + ')');
    	if(json.errors) {
    		success = false;
    		for(i=0; i < json.errors.length; i++) {
    			message += json.errors[i] + '<br/>';
    		}
    	}
    	var new_id = "1";
		return [success,message,new_id];
    },
	setCliId: function(cliId){
		this.cliId = cliId;
	},
	open: function(){
		$("#dialog_vehicle").dialog('open');
	}
}