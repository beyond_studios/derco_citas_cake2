<?php
class Agetiposervicio extends AppModel{
		public $name = 'Agetiposervicio';
		public $displayField = 'description';
		public $validate = array(		       					
			'description' => array(
					'notEmpty' => array(
				           				'rule' => 'notEmpty',  
				            			'last' => true
									),
											         
					'maxLength' => array(
								        'rule' => array('maxLength', '60'),  
								        'last' => true
						   			 )
				
					/*'descripexist'=> array(
					            		'rule' => 'descripexist',
					            		'last' => true
									),	*/
			),
	       					
			'codigo_sap' => array(
					'notEmpty' => array(
				           				'rule' => 'notEmpty',  
				            			'last' => true
									),
											         
					'maxLength' => array(
								        'rule' => array('maxLength', '5'),  
								        'last' => true
						   			 ), 
			),
	       					
			'agemotivoservicio_id' => array(
					'notEmpty' => array(
				           				'rule' => 'notEmpty',  
				            			'last' => true
									),
											         
					'maxLength' => array(
								        'rule' => array('maxLength', '5'),  
								        'last' => true
						   			 ), 
			),			
			'estado' => array('notEmpty')
		);
	
		public $hasMany = array(
			'Agedetallecita' => array(
				'className' => 'Agedetallecita',
				'foreignKey' => 'agetiposervicio_id',
				'dependent' => false,
				'conditions' => '',
				'fields' => '',
				'order' => '',
				'limit' => '',
				'offset' => '',
				'exclusive' => '',
				'finderQuery' => '',
				'counterQuery' => ''
			),
		);
		
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		'Agemotivoservicio' => array(
			'className' => 'Agemotivoservicio',
			'foreignKey' => 'agemotivoservicio_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);


	public function descripexist(){
		$data=$this->data;
		if(empty($data['Agetiposervicio']['id'])){
  			$existemp = $this->find('count', array(
				'conditions' => array(
					'upper(Agetiposervicio.description)' => strtoupper($data['Agetiposervicio']['description'])
				),
				'recursive'=>-1
			)); 		
		}else{
  			$existemp = $this->find('count', array(
				'conditions' => array(
					'upper(Agetiposervicio.description)' => strtoupper($data['Agetiposervicio']['description']),
					'Agetiposervicio.id != ' => $data['Agetiposervicio']['id']
				),
				'recursive'=>-1					
			)); 			
		}
							  
	   return $existemp < 1;
    }
	
	function getTiposervicio(){
		return $this->query("select * from agetiposervicios AS Agetiposervicio");
	}
}