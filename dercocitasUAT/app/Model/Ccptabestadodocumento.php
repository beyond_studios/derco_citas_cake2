<?php
class Ccptabestadodocumento extends AppModel {
   var $name = 'Ccptabestadodocumento';
   //var $displayField = 'descripcion';
   
   function getEstados(){
		$conditions=array('estado'=>'AC');
		$lista=$this->findAll($conditions);
		$estados=array();
		foreach($lista as $id => $item){
			$estados[$item['Ccptabestadodocumento']['id']]=$item['Ccptabestadodocumento']['descripcion'];
		}
		return (!empty($estados) && isset($estados))?$estados:array();
   }
   
   function getIdEstadoPorDescripcion($descripcion){
   		$estados=$this->getEstados();
		$estadoFind=0;
		foreach($estados as $id => $item){
			if($descripcion==$item){
				$estadoFind=$id;
			}
		}
		return (!empty($estadoFind) && isset($estadoFind))?$estadoFind:0;
   }
}
?>