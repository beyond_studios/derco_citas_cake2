<?php
class Agedetallecita extends AppModel
{
	public $name='Agedetallecita';
	public $status = array();
	public $statusSap = array();
	
	public $validate = array(
		'modelo' => array('notEmpty' =>array('rule'=>'notEmpty','last' => true)),
		'agemotivoservicio_id' => array('notEmpty' =>array('rule'=>'notEmpty','last' => true)),
		'secproject_id' => array('notEmpty' =>array('rule'=>'notEmpty','last' => true)),
		'agetipomantenimiento_id' => array('notEmpty' =>array('rule'=>'notEmpty','last' => true)),
		'agecitacalendario_id' => array('notEmpty' =>array('rule'=>'notEmpty','last' => true))
    );

	public $virtualFields = array(
		'con_cita'=>"Agedetallecita.fechadecita > now()"
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed
		
	public $belongsTo = array(
		'Agecitacalendariodia' => array('className' => 'Agecitacalendariodia','foreignKey' => 'agecitacalendariodia_id'),
		'Agetiposervicio' => array('className' => 'Agetiposervicio','foreignKey' => 'agetiposervicio_id'),
		'Asesorservicio' => array('className' => 'Asesorservicio','foreignKey' => 'asesor_id'),
		'Cliente' => array('className' => 'Cliente','foreignKey' => 'cliente_id'),
		'Agemotivoservicio' => array('className' => 'Agemotivoservicio','foreignKey' => 'agemotivoservicio_id'),
		'Secproject' => array('className' => 'Secproject','foreignKey' => 'secproject_id'),
		'Agetipomantenimiento' => array('className' => 'Agetipomantenimiento','foreignKey' => 'agetipomantenimiento_id'),
		'Agecitacalendario' => array('className' => 'Agecitacalendario','foreignKey' => 'agecitacalendario_id'),
		'Createdsecperson' => array('className' => 'Secperson','foreignKey' => 'createdsecperson_id', 'fields'=>array('appaterno', 'apmaterno', 'firstname')),
		'Reschedulesecperson' => array('className' => 'Secperson','foreignKey' => 'reschedulesecperson_id', 'fields'=>array('appaterno', 'apmaterno', 'firstname')),
		'Deletesecperson' => array('className' => 'Secperson','foreignKey' => 'deletesecperson_id', 'fields'=>array('appaterno', 'apmaterno', 'firstname'))
	);
		
	/**ARMA LOA CONDICIONES DEL BUSCADOR
	 * @AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * @FECHA: 2012-05-18
	 * @param object $dt
	 * @param object $dtLog
	 * @return array() formatoCakePhp
	 */
	function getConditionsBuscador($dt, $dtLog){
		//CODICION FECHAS
		$cndF = $this->_condicionFecha(
			empty($dt['bsc']['f_ini'])?'':$dt['bsc']['f_ini']
			,empty($dt['bsc']['f_fin'])?'':$dt['bsc']['f_fin']
			,$dt['bsc']['f_campo']
		);
		
		//CONDICION POR CRITERIO
		$cndCrt = array();
		if(!empty($dt['bsc']['crt']) && !empty($dt['bsc']['vlr'])){
			$cndCrt = strpos($dt['bsc']['crt'], 'numero');
			if($cndCrt) $cndCrt = $this->_condicionDocumento($dt['bsc']['crt'], trim($dt['bsc']['vlr']));
			else $cndCrt = array('1'=>$dt['bsc']['crt'].' LIKE \'%'.trim($dt['bsc']['vlr']).'%\'');
		}
		
		//CONDICION DE ACUERDO AL ESTADO
		$cndStd = empty($dt['bsc']['std'])?array():array('Agedetallecita.estado'=>sprintf("%s",$dt['bsc']['std']));
		
		//$cndB = array("Agedetallecita.estado"=>'AC');
		//return $cndB + $cndF + $cndCrt;
		return $cndF + $cndCrt + $cndStd;
	}
	
	/**AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * esta funcion trabaja a la par con la funcion agregarCita ya que es utilizada para reprogramar una cita:
	 * acciones realizadas: 
	 * genera una nueva cita agregarCita { realizado antes de llamar a esta funcion (setReprogramar)}
	 * cambiar estado del detalle de la cita
	 * actualizar el campo available de citas por dia.
	 * @param object $detallecitaId
	 * @return 
	 */
	function setReprogramar($detallecitaId, $dt, $dtLog){
		$detallecita_db = $this->find('first',array(
			'fields'=>array(
				'Agedetallecita.id','Agedetallecita.estado',
				'Agecitacalendariodia.id','Agecitacalendariodia.available'
			),
			'conditions'=>array('Agedetallecita.id'=>$detallecitaId),
			'recursive'=>0
		));
		
		if(!in_array($detallecita_db['Agedetallecita']['estado'],array('AC'))) return array(false, 'ESTADO NO PERMITIDO PARA REPROGRAMACION');
		
		$detallecita_db['Agedetallecita']['estado'] = 'RE';  // estado REPROGRAMADO
		$detallecita_db['Agedetallecita']['reschedulesecperson_id'] = empty($dtLog['Secperson']['id'])?$this->userWebSecpersonId:$dtLog['Secperson']['id'];
		$detallecita_db['Agedetallecita']['rescheduledate'] = $this->fechaHoraActual();
		$detallecita_db['Agedetallecita']['reschedulecomment'] = $dt['Agedetallecita']['reschedulecomment'];  
		
		if(!$this->save($detallecita_db)) return array(false, 'DETALLE DE CITA, ESTADO NO ACTUALIZADO');
		
		$detallecita_db['Agecitacalendariodia']['available']++; 		
		if(!$this->Agecitacalendariodia->save($detallecita_db)) return array(false, 'DETALLE DE CITA, ESTADO NO ACTUALIZADO');
		
		return array(true, 'SE REPROGRAMO LA CITA');
	}
	
	/**AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * @param object $detallecitaId
	 * @return 
	 */
	function setEliminar($dt, $dtLog){
		app::import('Model','Agecitacalendariodia'); 		$this->Agecitacalendariodia = new Agecitacalendariodia();
		
		$detallecita_db = $this->find('first',array(
			'fields'=>array(
				'Agedetallecita.id','Agedetallecita.estado',
				'Agecitacalendariodia.id','Agecitacalendariodia.available'
			),
			'conditions'=>array('Agedetallecita.id'=>$dt['Agedetallecita']['id']),
			'recursive'=>0
		));
		
		if(!in_array($detallecita_db['Agedetallecita']['estado'],array('AC'))) return array(false, 'ESTADO NO PERMITIDO PARA ELIMINAR');
		
		
		$detallecita_db['Agedetallecita']['estado'] = 'EL';  // estado REPROGRAMADO
		$detallecita_db['Agedetallecita']['deletesecperson_id'] = empty($dtLog['Secperson']['id'])?$this->userWebSecpersonId:$dtLog['Secperson']['id'];
		$detallecita_db['Agedetallecita']['deletedate'] = $this->fechaHoraActual();
		$detallecita_db['Agedetallecita']['deletecomment'] = $dt['Agedetallecita']['deletecomment'];  
		
		if(!$this->save($detallecita_db)) return array(false, 'DETALLE DE CITA, ESTADO NO ACTUALIZADO');
		
		$detallecita_db['Agecitacalendariodia']['available']++; 
		if(!$this->Agecitacalendariodia->save($detallecita_db)) return array(false, 'DETALLE DE CITA, ESTADO NO ACTUALIZADO');
		
		return array(true, 'SE ELIMINO LA CITA PROGRAMADA');
	}
	
	/** 
	 * Permite solicitar una cita para el taller segun su numero de serie de cars (taller).
	 * @return 
	 * @param $dertallervehiculo_id Object usado para mostrar los datos del vehiculo del cliente.
	 * ANTES citaTaller en controlador derwebsolicitudes
	 */
	function agregarCita($dt, $dtLog){
		//modelos a importar
		app::import('Model', 'Agecitacalendariodia');	$this->Agecitacalendariodia = new Agecitacalendariodia();
		app::import('Model', 'Agetipomantenimiento');	$this->Agetipomantenimiento = new Agetipomantenimiento();
		app::import('Model', 'Agecitacalendariodia');	$this->Agecitacalendariodia = new Agecitacalendariodia();
		
		//agregando la validacion de una misma cita en el dia
		$citas = $this->getCitaClient($dt['Agedetallecita']['ageclientesVehiculo_id']);
		if(!empty($citas)){
			$mensaje = "El cliente: "
				.$citas[0]['Cliente']['nombres']
				." tiene una cita pendiente para el dia: "
				.substr($citas[0]['Agecitacalendariodia']['initDateTime'], 0 , 10)
				." [".substr($citas[0]['Agecitacalendariodia']['initDateTime'], 11, 5)
				." - ".substr($citas[0]['Agecitacalendariodia']['endDateTime'], 11, 5)
				."]";
			return array(false, $mensaje);
		}
		
       // Creando el array de talcitacalendarday
		foreach($dt['Agecitacalendariodia'] as $clave=>$valor) {
			$citacalendariodia['Agecitacalendariodia'] = array(
				'id' => substr($clave, strlen('radio')),
				'checked' => $valor,
				'available' => -1
			);
		}
		
		//BLOQUEAMOS EL REGISTRO PARA EVITAR CONCURRENCIA
		$campos = array('Agecitacalendariodia.available, Agecitacalendariodia.initDateTime, Agecitacalendario.secproject_id');
		$sql="select Agecitacalendariodia.available, Agecitacalendariodia.initDateTime,Agecitacalendariodia.agecitacalendario_id from agecitacalendariodias Agecitacalendariodia where id=".$citacalendariodia['Agecitacalendariodia']['id']." LOCK IN SHARE MODE";
		$citacalendariodia_db=$this->query($sql);
		$citacalendariodia_db=$citacalendariodia_db[0];

		if(!($citacalendariodia_db['Agecitacalendariodia']['available']))
			return array(false, __('WEBSOLICITUD_MENSAJE_CITA_TALLER_NO_HAY_CITAS_POR_RESERVA', true));
			
		// actualizamos el calendario de citas de taller
		$dt['Agecitacalendariodia']['id'] = $citacalendariodia['Agecitacalendariodia']['id'];
		$dt['Agecitacalendariodia']['agecitacalendario_id'] = $dt['Agecitacalendario']['id'];
		$dt['Agecitacalendariodia']['available'] = $citacalendariodia_db['Agecitacalendariodia']['available'] + $citacalendariodia['Agecitacalendariodia']['available'];
		$dt['Agecitacalendariodia']['detail'] = 'Ninguno.';
		
		if(!$this->Agecitacalendariodia->save($dt['Agecitacalendariodia']))
			return array(false, __('WEBSOLICITUD_MENSAJE_CITA_TALLER_REGISTRO_ERROR', true));
		
		// RECUPERAMOS LA SUCURSAL EN LA CUAL SE TRABAJO	
		$sql="select Agecitacalendario.secproject_id from agecitacalendarios Agecitacalendario where id=".$citacalendariodia_db['Agecitacalendariodia']['agecitacalendario_id'];
		$projectElegido=$this->query($sql);
		$projectElegido=$projectElegido[0];
		
		$citacalendariodia_db['Agecitacalendario']['secproject_id']=$projectElegido['Agecitacalendario']['secproject_id'];			
		
		//recuperamos la orden estandar
		$ordenEstandar='';
		if(!empty($dt['Agetipomantenimiento']['id']) && isset($dt['Agetipomantenimiento']['id'])){
			$ordenEstandar=$this->Agetipomantenimiento->findById($dt['Agetipomantenimiento']['id']);
		}
		
		// registramos la cita de taller
		$dt['Agedetallecita']['agecitacalendariodia_id'] = $citacalendariodia['Agecitacalendariodia']['id'];
		$dt['Agedetallecita']['tipoMantenimiento'] = empty($ordenEstandar)?"":$ordenEstandar['Agetipomantenimiento']['description'];
		$dt['Agedetallecita']['fechadecita'] = $this->configurarFechaYMD($dt['Agedetallecita']['fecha']).' '.substr($citacalendariodia_db['Agecitacalendariodia']['initDateTime'], 11); 	
		$dt['Agedetallecita']['asesor_id'] = $this->getAsesorDetallecita($projectElegido['Agecitacalendario']['secproject_id'],$dt['Agemotivoservicio']['id']);
		
		// usuario y fecha que genera la cita
		//$dt['Agedetallecita']['createdsecperson_id'] = empty($dtLog['Secperson']['id'])?$this->userWebSecpersonId:$dtLog['Secperson']['id'];
        if($dt['Agedetallecita']['citaweb']!='1'){
            $dt['Agedetallecita']['createdsecperson_id'] = $dtLog['Secperson']['id'];
        }else{
            $dt['Agedetallecita']['createdsecperson_id'] = $this->userWebSecpersonId;
        }
		$fechaActual = $this->fechaHoraActual();
		$dt['Agedetallecita']['fechaRegistro'] = $fechaActual;
		$dt['Agedetallecita']['horaRegistro'] = $fechaActual;
//		debug($dt);
//		debug($dtLog); die;
		
		// campos necesarios para la edicion
		$dt['Agedetallecita']['agemotivoservicio_id'] = $dt['Agemotivoservicio']['id'];
		$dt['Agedetallecita']['secproject_id'] = $dt['Secproject']['id'];
		$dt['Agedetallecita']['agetipomantenimiento_id'] = $dt['Agetipomantenimiento']['id'];
		$dt['Agedetallecita']['agecitacalendario_id'] = $dt['Agecitacalendario']['id'];
		
		foreach($dt['Agedetallecita'] as $key=>$value){
			if(empty($value)) unset($dt['Agedetallecita'][$key]);
		}
		
		if(!$this->save($dt['Agedetallecita'])){
			$this->validates();
			$errors = $this->validationErrors;
			//debug($errors);
			return array(false, __('WEBSOLICITUD_MENSAJE_CITA_TALLER_REGISTRO_ERROR***', true));
		}
			
		
		$agedetallecita_id = $this->getLastInsertId();
		return array(true, __('TALLER_CITA_REGISTRADA', true), 'id'=>$agedetallecita_id);
	}
	
	function getAsesorDetallecita($projecId,$agemotivoservicio){
		$asesor = "select  Asesor.id
			from (
			    select Asesorservicio.id as id, 
			        sum(CASE WHEN Agedetallecita.id IS NULL THEN 0 ELSE 1 END) as nro_citas
			    from asesorservicios Asesorservicio
			    LEFT JOIN agedetallecitas Agedetallecita ON (Agedetallecita.asesor_id = Asesorservicio.id AND Agedetallecita.estado = 'AC')
			    WHERE Asesorservicio.status = 'AC' AND Asesorservicio.secproject_id = '$projecId'
			    group by Agedetallecita.asesor_id
			) Asesor
			ORDER BY Asesor.nro_citas ASC
			LIMIT 1";
//		debug($asesor); die;	
		$asesor = $this->query($asesor);
		if(empty($asesor)) return 0;
		return $asesor[0]['Asesor']['id'];
		
		/*
		$asesor = $this->query("select Asesorservicio.id,Asesorservicio.agemotivoservicio_id FROM asesorservicios Asesorservicio 
							WHERE Asesorservicio.status = 'AC' AND Asesorservicio.secproject_id = $projecId 
							AND (Asesorservicio.agemotivoservicio_id = $agemotivoservicio or Asesorservicio.agemotivoservicio_id is null)
							ORDER BY Asesorservicio.id ASC");
		$asesorid = 0;
		$asesoreswithmotivoservicio = array();
		$asesoreswithoutmotivoservicio = array();
		foreach($asesor as $id => $item){
			if(!empty($item['Asesorservicio']['agemotivoservicio_id'])){
				$asesoreswithmotivoservicio[$id] = $item;
			}else{
				$asesoreswithoutmotivoservicio[$id] = $item;
			}
		}
		//elegimos un asesor aleatoriamente
		if(!empty($asesoreswithmotivoservicio) && isset($asesoreswithmotivoservicio)){
			$max = sizeof($asesoreswithmotivoservicio);
			$indice = rand(0,$max);
			$asesorid = $asesoreswithmotivoservicio[$indice]['Asesorservicio']['id'];
		}elseif(!empty($asesoreswithoutmotivoservicio) && isset($asesoreswithoutmotivoservicio)){
			$max = sizeof($asesoreswithoutmotivoservicio);
			$indice = rand(0,$max);
			$asesorid = $asesoreswithoutmotivoservicio[$indice]['Asesorservicio']['id'];
		}
		return $asesorid;*/
	}
	
	function updateOTSAPTRAMAXML($id,$otsap=null,$tramaxml,$mensaje,$estadoenvioSap){
		if(empty($id)) return false;
		$agedetalle['Agedetallecita']['id'] = $id;
		if(!empty($otsap) && isset($otsap)) $agedetalle['Agedetallecita']['otsap'] = $otsap;
		if(!empty($tramaxml) && isset($tramaxml)) $agedetalle['Agedetallecita']['tramaxml'] = $tramaxml;
		if(!empty($mensaje) && isset($mensaje)) $agedetalle['Agedetallecita']['mensaje'] = $mensaje;
		$agedetalle['Agedetallecita']['estadoenviosap'] = $estadoenvioSap;
		if($this->save($agedetalle)) return true;
		else return false;
	}
	
	/**AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-03-189
	 * RECUPERA LA LISTA DE VEHICULOS DE UN CLIENTE Y NOS ENTREGA SI ESTA O NO CON ALGUNA CITA DE TALLER PENDIENTE
	 * @param object $clientId [optional]
	 * @return 
	 */
	public function getCitaClient($cliVehId, $afterNow = true){
		if(empty($cliVehId)) return array();
		$afterNow = ($afterNow)?"Agedetallecita.fechadecita > now()":"1=1";
		
		$vehicles = $this->query(
			"select AgeclientesVehiculo.id, AgeclientesVehiculo.codigo_vehiculo, AgeclientesVehiculo.marca, AgeclientesVehiculo.modelo, AgeclientesVehiculo.placa
				  ,CONCAT(AgeclientesVehiculo.marca, ' Modelo:', AgeclientesVehiculo.modelo, ' Placa:', AgeclientesVehiculo.placa) as label 
		          ,Cliente.nombres ,Agecitacalendariodia.initDateTime, Agecitacalendariodia.endDateTime 
			FROM ageclientes_vehiculos AgeclientesVehiculo 
			JOIN clientes Cliente ON Cliente.id = AgeclientesVehiculo.cliente_id
			JOIN agedetallecitas Agedetallecita ON (Agedetallecita.placa = AgeclientesVehiculo.placa
                AND Agedetallecita.cliente_id = AgeclientesVehiculo.cliente_id)
			JOIN agecitacalendariodias Agecitacalendariodia ON Agecitacalendariodia.id = Agedetallecita.agecitacalendariodia_id
			WHERE Agedetallecita.estado != 'EL' AND Agedetallecita.estado != 'RE'  AND AgeclientesVehiculo.id =  '$cliVehId' AND $afterNow
			ORDER BY Agedetallecita.id ASC"
		);
		return $vehicles;
	}
	
	/**
	 * @author: Ronald Tineo
	 * @return: retorna las citas por cliente, para la web del cliente 
	 */
	public function getDetalleCitasPorCliente($clienteId){
		$citas = $this->query(
			"SELECT Agedetallecita.id, Agedetallecita.otsap, Agedetallecita.fechadecita, Agedetallecita.placa ,Agemotivoservicio.description,
				Agedetallecita.fechaRegistro, Agedetallecita.estado
				FROM agedetallecitas Agedetallecita 
				INNER JOIN agemotivoservicios Agemotivoservicio ON(Agedetallecita.agemotivoservicio_id = Agemotivoservicio.id)
				WHERE Agedetallecita.cliente_id = $clienteId and (Agedetallecita.estado='EL' or Agedetallecita.estado='RE')  ORDER BY id DESC limit 100"
		);
		if(empty($citas)) return array();
		return $citas;
	}
	
	/**
	 * @author: Ronald Tineo
	 * @return: retorna las citas por cliente, para la web del cliente 
	 */
	public function getDetalleCitasReprogramarPorCliente($clienteId){
		$citas = $this->query(
			"SELECT Agedetallecita.id, Agedetallecita.otsap, Agedetallecita.fechadecita, Agedetallecita.placa ,Agemotivoservicio.description,
				Agedetallecita.fechaRegistro, Agedetallecita.estado,
				Agecitacalendariodia.initDateTime, Agecitacalendariodia.endDateTime
				FROM agedetallecitas Agedetallecita 
					JOIN agemotivoservicios Agemotivoservicio ON(Agedetallecita.agemotivoservicio_id = Agemotivoservicio.id)
					JOIN agecitacalendariodias Agecitacalendariodia ON(Agedetallecita.agecitacalendariodia_id = Agecitacalendariodia.id)
				WHERE Agedetallecita.cliente_id = $clienteId AND Agedetallecita.estado = 'AC' AND Agedetallecita.fechadecita > now() ORDER BY id DESC"
		);
		if(empty($citas)) return array();
		return $citas;
	}
	
	public function getCountTipoServicios($secproject_id, $motivoservicio_id,$fechatoday){
		$sql = "SELECT count(*) as cantidad,Agetipomantenimiento.tiposervicio FROM agedetallecitas Agedetallecita 
				INNER JOIN agetipomantenimientos Agetipomantenimiento ON(Agedetallecita.agetipomantenimiento_id = Agetipomantenimiento.id)
				INNER JOIN agecitacalendariodias Agecitacalendariodia on(Agedetallecita.agecitacalendariodia_id = Agecitacalendariodia.id) 
				WHERE Agedetallecita.secproject_id = $secproject_id and Agedetallecita.agemotivoservicio_id = $motivoservicio_id and Agedetallecita.estado = 'AC'
				AND date(Agecitacalendariodia.initDateTime) = '$fechatoday'
				GROUP BY Agetipomantenimiento.tiposervicio";
		$result = $this->query($sql);
		return $result;
	}
	
	public function getCantidadCitasSucursalMarca($mes,$anio){
		$sql = "SELECT Agedetallecita.marca,Agedetallecita.secproject_id, count(*)as total FROM agedetallecitas Agedetallecita 
			INNER JOIN agecitacalendariodias Agecitacalendariodia on(Agedetallecita.agecitacalendariodia_id = Agecitacalendariodia.id)
			WHERE
			MONTH(Agecitacalendariodia.initDateTime) = $mes AND YEAR(Agecitacalendariodia.initDateTime) = $anio 
			AND Agedetallecita.estado='AC' GROUP BY Agedetallecita.marca,Agedetallecita.secproject_id ORDER BY Agedetallecita.marca ASC";
		$retu = $this->query($sql);
		return $retu;
	}
	
	public function getCantidadCitasSucursalTipoMantenimiento($mes,$anio){
		$sql = "SELECT Agedetallecita.agetiposervicio_id,Agedetallecita.secproject_id, count(*)as total FROM agedetallecitas Agedetallecita 
			INNER JOIN agecitacalendariodias Agecitacalendariodia ON(Agedetallecita.agecitacalendariodia_id = Agecitacalendariodia.id)
			WHERE
			Agedetallecita.agetiposervicio_id IS NOT NULL AND
			MONTH(Agecitacalendariodia.initDateTime) = $mes AND YEAR(Agecitacalendariodia.initDateTime) = $anio 
			AND Agedetallecita.estado='AC' GROUP BY Agedetallecita.agetiposervicio_id,Agedetallecita.secproject_id ORDER BY Agedetallecita.agetiposervicio_id ASC";
		$retu = $this->query($sql);
		return $retu;
	}
	
	public function existsCita($agecitacalendario_id,$fechaInicial,$appcastLunes,$appcastMartes,$appcastMiercoles,$appcastJueves,$appcastViernes,$appcastSabado,$appcastDomingo){
		app::import('Model', 'Appcasttime'); $this->Appcasttime = new Appcasttime;
		app::import('Model', 'Agecitacalendariodia'); $this->Agecitacalendariodia = new Agecitacalendariodia;
		app::import('Model', 'Agedetallecita'); $this->Agedetallecita = new Agedetallecita;
		//obtener los casttimes
		$appcastTimes = $this->Appcasttime->find('all',array(
					'conditions'=>array('appcast_id' => array($appcastLunes,$appcastMartes,$appcastMiercoles,$appcastJueves,$appcastViernes,$appcastSabado,$appcastDomingo), 
										'Appcasttime.status' => 'AC'),
					'recursive'=>-1
				));
		//pr($appcastTimes);
		//obtener los agecitacalendariodia de acuerdo al agecitacalendario y la fecha
		$agecitacalendariodia = $this->Agecitacalendariodia->find('all',array('conditions'=>array('Agecitacalendariodia.initDateTime >= '=>$fechaInicial,
																									'Agecitacalendariodia.agecitacalendario_id'=>$agecitacalendario_id,
																									'Agecitacalendariodia.estado'=>'AC'),
																				'recursive'=>-1));
		$mes=date('m',strtotime($fechaInicial));
		$dia=date('d',strtotime($fechaInicial));
		$anio=date('Y',strtotime($fechaInicial));
		$unixtimeinicial = strtotime($fechaInicial.' 00:00:00');
		$unixtimefinal = mktime(23,59,59,12,31,$anio);
		$fechas = array();
		while($unixtimeinicial <= $unixtimefinal){
			$fechas[] = date('Y-m-d',$unixtimeinicial);
			$dia++;
			$unixtimeinicial = mktime(0,0,0,$mes,$dia,$anio);
		}
		//pr($fechas);
		$agecitacalendarioIds = array();
		foreach($agecitacalendariodia as $id => $item){
			foreach($appcastTimes as $id1 => $item1){
				foreach($fechas as $id2 => $item2){
					if($item['Agecitacalendariodia']['initDateTime'] == $item2.' '.$item1['Appcasttime']['inittime']){
						$agecitacalendarioIds[] = $item['Agecitacalendariodia']['id'];
					}
				}
			}
		}
		
		//consulta en agedetalle citas si con esos ids hay citas registradas
		$agedetallecitas = $this->Agedetallecita->find('all',array('conditions'=>array('Agedetallecita.agecitacalendariodia_id'=>$agecitacalendarioIds,
																						'Agedetallecita.estado'=>'AC'),
																	'recursive'=>-1));
		pr($agedetallecitas);
		
	}
}