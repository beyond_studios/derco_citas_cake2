<?php
class Agemotivoservicio extends AppModel
{
	public $name = 'Agemotivoservicio';	
	public $displayField = 'description';   
	public $validate = array(
		'description' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								),
						'isUnique' =>array(
								'rule'=>'isUnique',
								'last' => true
								),
						'maxLength' =>array(
            					'rule'    => array('maxLength', '60'),
								'last' => true
								)   
					),	       					
			'codigo_sap' => array(
					'notEmpty' => array(
				           				'rule' => 'notEmpty',  
				            			'last' => true
									),
					'maxLength' => array(
								        'rule' => array('maxLength', '5'),  
								        'last' => true
						   			 )
			),
    );

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	
  public $hasMany = array(
		'Agetipomantenimiento' => array(
			'className' => 'Agetipomantenimiento',
			'foreignKey' => 'agemotivoservicio_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Agetiposervicio' => array(
			'className' => 'Agetiposervicio',
			'foreignKey' => 'agemotivoservicio_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Agecitacalendario' => array(
			'className' => 'Agecitacalendario',
			'foreignKey' => 'agemotivoservicio_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	/** ############################################################################
	 * ###################### ACCIONES MODIFICADAS #################################
	 * ########## AUTOR: VENTURA RUEDA, JOSE ANTONIO ###############################
	 * */
	
	
	function obtenerListaServicios($conditions=array()){
		return $this->find('list',array(
			'conditions'=>$conditions,
			'fields'=>array('Agemotivoservicio.id', 'Agemotivoservicio.description'),
			'order'=>array('Agemotivoservicio.description'=>'ASC')
		));
	}
	
	function getMotivoServicio($secprojectId,$marcaId = null){
		if(empty($secprojectId)) return array();
		$conditionMarcas = "";
		if(!empty($marcaId) && isset($marcaId)){
			$conditionMarcas = "AND Agegruposmarcas.marca_id = $marcaId";
		}
		
		$motivos = $this->query(
			"select distinct Agemotivoservicio.id, Agemotivoservicio.description, Agecitacalendario.id
			from agecitacalendarios Agecitacalendario
			    JOIN agemotivoservicios Agemotivoservicio ON Agemotivoservicio.id = Agecitacalendario.agemotivoservicio_id
				JOIN agegrupos_marcas Agegruposmarcas ON(Agegruposmarcas.agegrupo_id = Agecitacalendario.agegrupo_id)
			where Agecitacalendario.status = 'AC' and  Agemotivoservicio.status = 'AC' and Agegruposmarcas.status = 'AC'
			and Agecitacalendario.secproject_id = $secprojectId $conditionMarcas"
		);
		
		return $motivos;
	}	
}
?>