<?php
class Ccplavadoseguimiento extends AppModel{
	public $name = 'Ccplavadoseguimiento';
	
	function getNroSeguimiento($nrocars){
		$nroSeg = "select count(id) as nro_seguimiento from ccplavadoseguimientos where talot_nrocars = '$nrocars'";
		$nroSeg = $this->query($nroSeg);
		return empty($nroSeg)?0:$nroSeg['0']['0']['nro_seguimiento'];
	}
	
	function getLavadoCoordinadorAprobado($nrocars){
		$nroSeg = "select count(id) as nro_seguimiento from ccplavadoseguimientos 
					where talot_nrocars = '$nrocars' 
							AND ccplavadoseguimientosestado_id = 2
							AND secperson_coordinador_id is not null";
		$nroSeg = $this->query($nroSeg);
		return empty($nroSeg)?0:$nroSeg['0']['0']['nro_seguimiento'];
	}
	
	function setLavar($data, $dtLog){
		app::import('Model', 'Talot');			$this->Talot = new Talot;
		
		// VERIFICAMOS EL NUMERO DE REGISTRO DE LAVADO
		$data['Ccplavadoseguimiento']['nro_seguimiento'] = $this->getNroSeguimiento($data['Ccplavadoseguimiento']['talot_nrocars'])+1;
		
		// RECUPERAMOS LOS DATOS DE LA OT
		$ot = $this->Talot->findByNrocars($data['Ccplavadoseguimiento']['talot_nrocars']);
		
		$data['Ccplavadoseguimiento']['talot_id'] = $ot['Talot']['id'];
		$data['Ccplavadoseguimiento']['ccplavadoseguimientosestado_id'] = 1;
		$data['Ccplavadoseguimiento']['secperson_lavador_id'] = $dtLog['Secperson']['id'];
		$data['Ccplavadoseguimiento']['fecha_lavador'] = date('Y-m-d H:i:s');
		
		// GUARDAMOS EL NUEVO REGISTRO
		$this->create();
		if(!$this->save($data)){
			return array(false, 'REGISTRO NO GUARDADO');
		}
		
		return array(true, 'LOS DATOS FUERON GUARDADOS');
	}
	
	function setCoordinador($data, $dtLog){
		//RECUPERAMOS EL SEGUIMIENTO Y VERIFICAMOS SU ESTADO
		$seg_db = $this->findById($data['Ccplavadoseguimiento']['id']);
		if($seg_db['Ccplavadoseguimiento']['ccplavadoseguimientosestado_id'] != 1) return array(false, 'NO SE PUEDE CAMBIAR EL ESTADO DEL SEGUIMIENTO');
		
		//VERIFICAMOS QUE NO TENGA APROBADO OTRO SEGUIMIENTO DE LA MISMA OT
		$nroCoordAprob = $this->getLavadoCoordinadorAprobado($seg_db['Ccplavadoseguimiento']['talot_nrocars']);
		if(!empty($nroCoordAprob)) return array(false, 'YA APROBO OTRO SEGUIMIENTO');
		
		$data['Ccplavadoseguimiento']['ccplavadoseguimientosestado_id'] = 2;
		$data['Ccplavadoseguimiento']['secperson_coordinador_id'] = $dtLog['Secperson']['id'];
		$data['Ccplavadoseguimiento']['fecha_coordinador'] = date('Y-m-d H:i:s');
		
		// GUARDAMOS EL NUEVO REGISTRO
		if(!$this->save($data)){
			return array(false, 'REGISTRO NO GUARDADO');
		}
		
		return array(true, 'LOS DATOS FUERON GUARDADOS');
	}
	
	function setJefe($data, $dtLog){
		//RECUPERAMOS EL SEGUIMIENTO Y VERIFICAMOS SU ESTADO
		$seg_db = $this->findById($data['Ccplavadoseguimiento']['id']);
		if($seg_db['Ccplavadoseguimiento']['ccplavadoseguimientosestado_id'] != 1) return array(false, 'NO SE PUEDE CAMBIAR EL ESTADO DEL SEGUIMIENTO');
		
		$data['Ccplavadoseguimiento']['ccplavadoseguimientosestado_id'] = 2;
		$data['Ccplavadoseguimiento']['secperson_jefetaller_id'] = $dtLog['Secperson']['id'];
		$data['Ccplavadoseguimiento']['fecha_jefetaller'] = date('Y-m-d H:i:s');
		
		// GUARDAMOS EL NUEVO REGISTRO
		if(!$this->save($data)){
			return array(false, 'REGISTRO NO GUARDADO');
		}
		return array(true, 'LOS DATOS FUERON GUARDADOS');
	}
	
}
