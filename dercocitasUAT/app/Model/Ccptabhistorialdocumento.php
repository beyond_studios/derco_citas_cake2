<?php
/**
 * Ccptabhistorialdocumento Model
 *
 * @property Talot $Talot
 * @property Ccptabestadodocumento $Ccptabestadodocumento
 */
class Ccptabhistorialdocumento extends AppModel {
	
	/**MODIFICADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * 
	 * @param object $ot_numero
	 * @return 
	 */
	public function obtenerEstadoCcpPotNumeroOt($ot_numero){
		$sql="SELECT 
			Ccptabestadodocumento.id,
			Ccptabestadodocumento.descripcion
	 		FROM (select max(id) as id,ot_numero from ccptabhistorialdocumentos group by ot_numero) as CcptabhistorialTemp
	  		INNER JOIN ccptabhistorialdocumentos Ccptabhistorialdocumento on(CcptabhistorialTemp.id=Ccptabhistorialdocumento.id)
	  		INNER JOIN ccptabestadodocumentos Ccptabestadodocumento on(Ccptabestadodocumento.id=Ccptabhistorialdocumento.ccptabestadodocumento_id)
	 		WHERE Ccptabhistorialdocumento.ot_numero='".$ot_numero."' and Ccptabestadodocumento.estado='AC' LIMIT 1";	
		$estadoCCP=$this->query($sql);
		return(!empty($estadoCCP) && isset($estadoCCP))?$estadoCCP:array();
   }
   
   /**MODIFICADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * 
	 * @param object $ot_numero
	 * @return 
	 */
   	function getAsesorescars($carscod){
		$sql = "select distinct codasesor from talots Talot where estado in(0,2) and codlocal = '$carscod' order by codasesor";
		$asesores = array();
		$lista = $this->query($sql);
		
		foreach($lista as $id => $item){
			$asesores[$item['Talot']['codasesor']]=$item['Talot']['codasesor'];
		}
		
		return $asesores;
	}
    
	 /**MIGRADO POR. VENTURA RUEDA, JOSE ANTOIO
     * FECHA: 2013-05-16
     * @return 
     */
	function getCndBsc_indexDocumentos($data){
		$condicion = " 1=1 ";
		if(!empty($data['estadodocumento_id']) && isset($data['estadodocumento_id'])){
			$condicion .= " AND Ccptabhistorialdocumento.ccptabestadodocumento_id = ".$data['estadodocumento_id'];
		}
		//filtro asesor
		if(!empty($data['codasesor']) && isset($data['codasesor'])){
			$condicion .= " AND Talot.codasesor = '".$data['codasesor']."'";
		}
		//filtro placa 
		if(!empty($data['placa']) && isset($data['placa'])){
			$condicion .= " AND Talot.placa LIKE '%".$data['placa']."%'";
		}
		//filtro numero cars 
		if(!empty($data['nrocars']) && isset($data['nrocars'])){
			$condicion .= " AND Talot.nrocars LIKE '%".$data['nrocars']."%'";
		}
		
		$cndBasic = "Talot.codlocal in ('P035', 'P057') AND Talot.ClasePedido in ('2000','4000')";
		return $cndBasic.' AND '.$condicion;
	}
	
    /**MIGRADO POR. VENTURA RUEDA, JOSE ANTOIO
     * FECHA: 2013-05-16
     * @return 
     */
	function cambiarEstado($estadoInicial,$estadoFinal,$numero_ot,$comentario=null,$usuario=null,$id_solicitud=null){
   		$estadoInicial=(!empty($estadoInicial) && isset($estadoInicial))?$estadoInicial:'';
		$estadoFinal=(!empty($estadoFinal) && isset($estadoFinal))?$estadoFinal:'';
		$numero_ot=(!empty($numero_ot) && isset($numero_ot))?$numero_ot:'';
		$usuario=(!empty($usuario) && isset($usuario))?$usuario:'';
		$respuesta=$this->verificarPasoDeEstados($estadoInicial,$estadoFinal);
		
		
		//validamos los Estados
		if($respuesta['respuesta']){
			//validamos que este bien ingresado la OT
			if(!empty($numero_ot) && isset($numero_ot)){
				//validamos si existe OT
				if(true){ //$this->Ccppresupuesto->ValidarOT($numero_ot)){
					//control de concurrencia
					$estadoTemp=$this->bloquearRegistro($numero_ot);
					if($estadoTemp[0]['Ccptabestadodocumento']['id']!=$estadoInicial){
						$respuesta['respuesta']=false;
						$respuesta['msg']="El estado actual ha cambiado";	
					}else{
						$data['Ccptabhistorialdocumento']['fecha']=$this->fechaHoraActual();
						$data['Ccptabhistorialdocumento']['ot_numero']=$numero_ot;
						$data['Ccptabhistorialdocumento']['ccptabestadodocumento_id']=$estadoFinal;
						$data['Ccptabhistorialdocumento']['comentario']=$comentario;
						$data['Ccptabhistorialdocumento']['secperson_id']=$usuario;
						
						// RECUPERAMOS EL ID DEL TALOT
						app::import('Model', 'Talot');		$this->Talot = new Talot();
						$this->Talot->recursive = -1;
						$talot_db = $this->Talot->findByNrocars($data['Ccptabhistorialdocumento']['ot_numero']);
						$data['Ccptabhistorialdocumento']['talot_id']=$talot_db['Talot']['id'];
						
						//$data['Ccptabhistorialdocumento']['ccpsolicitudservicio_id']=(!empty($id_solicitud) && isset($id_solicitud))?$id_solicitud:null;
						$this->create();
						if($this->save($data['Ccptabhistorialdocumento'])){
							$respuesta['respuesta']=true;
							$respuesta['msg']="El Proceso se actualizó satisfactoriamente";		
						}else{
							$respuesta['respuesta']=false;
							$respuesta['msg']="Error al cambiar estado";								
						}
					}
					//fin control de Concurrencia
				}else{
					$respuesta['respuesta']=false;
					$respuesta['msg']='OT no existe';				
				}
			}else{
				$respuesta['respuesta']=false;
				$respuesta['msg']='Valor invalido para OT';
			}
		}else{
			$respuesta['respuesta']=false;
			$respuesta['msg']='Estado no permitido';
		}
		return $respuesta;
   }
   
   /**MIGRADO POR. VENTURA RUEDA, JOSE ANTOIO
     * FECHA: 2013-05-16
     * @return 
     */
   function verificarPasoDeEstados($estadoInicial,$estadoFinal){
		switch($estadoInicial){
			case 0://Estado Inicial
				if(in_array($estadoFinal, array(1)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 1://Pendiente de Entrega
				if(in_array($estadoFinal, array(2)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 2://Salida Unidad
				if(in_array($estadoFinal, array(1)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			default: return array('respuesta'=>false,'msg'=>'Estado no Permitido');break;
		}
	}
	
	/**MIGRADO POR. VENTURA RUEDA, JOSE ANTOIO
     * FECHA: 2013-05-16
     * @return 
     */
	function bloquearRegistro($ot_numero){
   		$sql="SELECT 
			Ccptabhistorialdocumento.id,
			Ccptabestadodocumento.id,
			Ccptabestadodocumento.descripcion
	 		FROM (select max(id) as id,ot_numero from ccptabhistorialdocumentos group by ot_numero) as CcptabhistorialTemp
	  		INNER JOIN ccptabhistorialdocumentos Ccptabhistorialdocumento on(CcptabhistorialTemp.id=Ccptabhistorialdocumento.id)
	  		INNER JOIN ccptabestadodocumentos Ccptabestadodocumento on(Ccptabestadodocumento.id=Ccptabhistorialdocumento.ccptabestadodocumento_id)
	 		WHERE Ccptabhistorialdocumento.ot_numero='".$ot_numero."' and Ccptabestadodocumento.estado='AC' LIMIT 1 LOCK IN SHARE MODE";
			
		$estadoCCP=$this->query($sql);
		if(!empty($estadoCCP) && isset($estadoCCP)){//entrara cuando viene del estado 1
		}else{
			//validamos que la ot exista en cars
			$estadoCCP[0]['Ccptabestadodocumento']['id']=0;
			$estadoCCP[0]['Ccptabestadodocumento']['descripcion']='Pendiente';
		}
		return(!empty($estadoCCP) && isset($estadoCCP))?$estadoCCP:array();
   }
}
