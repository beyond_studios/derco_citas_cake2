<?php 
/**
 * By: Ronald Tineo
 * date: 15-01-2013
 * */
App::uses('Validation', 'Utility');
App::uses('AppModel', 'Model');
class Talot extends AppModel {
    public $name = 'Talot';


	//The Associations below have been created with all possible keys, those that are not needed can be removed		
	public $hasMany = array(
		'Ccpsolicitudservicio' => array(
			'className' => 'Ccpsolicitudservicio',
			'foreignKey' => 'talot_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	function validateDataOT_TALLER_PERU($params){
		$response= array('ESTADO'=>false,'MENSAJE'=>'');
		$mensaje = "";
		$estado = true;
		//ADD VALIDATION FOR THE DATA TYPES
		if(!Validation::notEmpty($params['Sociedad'])){
			$mensaje = $mensaje.' Sociedad is not Empty \n';
		}elseif(!Validation::maxLength($params['Sociedad'],4)){
			$mensaje = $mensaje.' Sociedad maxLength(4) \n';
		}
		//VALIDATE FECHA YYYYMMDD
		if(!Validation::notEmpty($params['FechaOT'])){
			$mensaje = $mensaje.' FechaOT is not Empty \n';
		}elseif(!Validation::minLength($params['FechaOT'],10)){
			$mensaje = $mensaje.' FechaOT minLenght(10) \n';
		}elseif(!Validation::maxLength($params['FechaOT'],10)){
			$mensaje = $mensaje.' FechaOT maxLength(10) \n';
		}elseif(!Validation::date($params['FechaOT'])){
			$mensaje = $mensaje.' FechaOT incorrect format(YYY-MM-DD) \n';
		}
		
		//VALIDATE TIME HHMMSS
		if(!Validation::notEmpty($params['HoraOT'])){
			$mensaje = $mensaje.' HoraOT is not Empty \n';
		}elseif(!Validation::minLength($params['HoraOT'],8)){
			$mensaje = $mensaje.' HoraOT minLenght(8) \n';
		}elseif(!Validation::maxLength($params['HoraOT'],8)){
			$mensaje = $mensaje.' HoraOT maxLength(8) \n';
		}elseif(!$this->validateTimeWebService($params['HoraOT'])){
			$mensaje = $mensaje.' HoraOT incorrect format(HH:MM:SS) \n';
		}

		if(!Validation::notEmpty($params['FechaCita'])){
			//$mensaje = $mensaje.' FechaCita is not Empty \n';
		}elseif(!Validation::minLength($params['FechaCita'],10)){
			$mensaje = $mensaje.' FechaCita minLenght(10) \n';
		}elseif(!Validation::maxLength($params['FechaCita'],10)){
			$mensaje = $mensaje.' FechaCita maxLength(10) \n';
		}elseif(!Validation::date($params['FechaCita'])){
			$mensaje = $mensaje.' FechaCita incorrect format(YYYY-MM-DD) \n';
		}
		
		if(!Validation::notEmpty($params['HoraCita'])){
			//$mensaje = $mensaje.' HoraCita is not Empty \n';
		}elseif(!Validation::minLength($params['HoraCita'],8)){
			$mensaje = $mensaje.' HoraCita minLenght(8) \n';
		}elseif(!Validation::maxLength($params['HoraCita'],8)){
			$mensaje = $mensaje.' HoraCita maxLength(8) \n';
		}elseif(!$this->validateTimeWebService($params['HoraCita'])){
			$mensaje = $mensaje.' HoraCita incorrect format(HH:MM:SS) \n';
		}
		
		if(!Validation::notEmpty($params['NroOT'])){
			$mensaje = $mensaje.' NroOT is not empty \n';
		}elseif(!Validation::maxLength($params['NroOT'],10)){
			$mensaje = $mensaje.' NroOT maxLength(10) \n';
		}

		if(!Validation::notEmpty($params['DetalleSolicitud'])){
			//$mensaje = $mensaje.' DetalleSolicitud is not empty \n';
		}elseif(!Validation::maxLength($params['DetalleSolicitud'],250)){
			$mensaje = $mensaje.' DetalleSolicitud maxLength(250) \n';
		}
		
		if(!Validation::notEmpty($params['TipoCliente'])){
			$mensaje = $mensaje.' TipoCliente is not empty \n';
		}elseif(!Validation::maxLength($params['TipoCliente'],2)){
			$mensaje = $mensaje.' TipoCliente maxLength(2) \n';
		}
		
		if(!Validation::notEmpty($params['NombreCliente'])){
			$mensaje = $mensaje.' NombreCliente is not empty \n';
		}
		
		if(!Validation::notEmpty($params['IDClienteSAP'])){
			$mensaje = $mensaje.' IDClienteSAP is not empty \n';
		}elseif(!Validation::maxLength($params['IDClienteSAP'],16)){
			$mensaje = $mensaje.' IDClienteSAP maxLength(16) \n';
		}
		
		if(!Validation::notEmpty($params['TipoDocFiscal'])){
			$mensaje = $mensaje.' TipoDocFiscal is not empty \n';
		}elseif(!Validation::maxLength($params['TipoDocFiscal'],2)){
			$mensaje = $mensaje.' TipoDocFiscal maxLength(2) \n';
		}
		
		if(!Validation::notEmpty($params['NroDocFiscal'])){
			$mensaje = $mensaje.' NroDocFiscal is not Empty \n';
		//}elseif(!(Validation::numeric($params['NroDocFiscal'])) || !($this->starts_with($params['NroDocFiscal'],"."))){
		//	$mensaje = $mensaje.' NroDocFiscal is numeric or char(".")\n';
		}elseif(!Validation::maxLength($params['TipoDocFiscal'],16)){
			$mensaje = $mensaje.' NroDocFiscal maxLength(16) \n';
		}

		if(!Validation::notEmpty($params['CodAsesor'])){
			$mensaje = $mensaje.' CodAsesor is not empty \n';
		}elseif(!Validation::maxLength($params['CodAsesor'],40)){
			$mensaje = $mensaje.' CodAsesor maxLength(40) \n';
		}
		
		if(!Validation::notEmpty($params['NumCono'])){
			//$mensaje = $mensaje.' NumCono is not empty \n';
		}elseif(!Validation::maxLength($params['NumCono'],10)){
			$mensaje = $mensaje.' NumCono maxLength(10) \n';
		}
		
		if(!Validation::notEmpty($params['FechaPromEnt'])){
			//$mensaje = $mensaje.' FechaPromEnt is not empty \n';
		}elseif(!Validation::minLength($params['FechaPromEnt'],10)){
			$mensaje = $mensaje.' FechaPromEnt minLenght(10) \n';
		}elseif(!Validation::maxLength($params['FechaPromEnt'],10)){
			$mensaje = $mensaje.' FechaPromEnt maxLength(10) \n';
		}elseif(!Validation::date($params['FechaPromEnt'])){
			$mensaje = $mensaje.' FechaPromEnt incorrect format(YYYY-MM-DD) \n';
		}
		
		if(!Validation::notEmpty($params['HoraPromEnt'])){
			//$mensaje = $mensaje.' HoraPromEnt is not empty \n';
		}elseif(!Validation::minLength($params['HoraPromEnt'],8)){
			$mensaje = $mensaje.' HoraPromEnt minLenght(8) \n';
		}elseif(!Validation::maxLength($params['HoraPromEnt'],8)){
			$mensaje = $mensaje.' HoraPromEnt maxLength(8) \n';
		}elseif(!$this->validateTimeWebService($params['HoraPromEnt'])){
			$mensaje = $mensaje.' HoraPromEnt incorrect format(HH:MM:SS) \n';
		}
		
		if(!Validation::notEmpty($params['CentroCod'])){
			$mensaje = $mensaje.' CentroCod is not empty \n';
		}elseif(!Validation::maxLength($params['CentroCod'],4)){
			$mensaje = $mensaje.' CentroCod maxLength(4) \n';
		}
		
		if(!Validation::notEmpty($params['Placa'])){
			$mensaje = $mensaje.' Placa is not empty \n';
		}elseif(!Validation::maxLength($params['Placa'],15)){
			$mensaje = $mensaje.' Placa maxLength(15) \n';
		}
		
		if(!Validation::notEmpty($params['IDUnidad'])){
			$mensaje = $mensaje.' IDUnidad is not empty \n';
		}elseif(!Validation::maxLength($params['IDUnidad'],22)){
			$mensaje = $mensaje.' Placa maxLength(22) \n';
		}
		
		if(!Validation::notEmpty($params['NroPresupuesto'])){
			//$mensaje = $mensaje.' NroPresupuesto is not empty \n';
		}elseif(!Validation::maxLength($params['NroPresupuesto'],10)){
			$mensaje = $mensaje.' NroPresupuesto maxLength(10) \n';
		}
		
		if(!Validation::notEmpty($params['FechaIniPresupuesto'])){
			//$mensaje = $mensaje.' FechaIniPresupuesto is not empty \n';
		}elseif(!Validation::minLength($params['FechaIniPresupuesto'],10)){
			$mensaje = $mensaje.' FechaIniPresupuesto minLenght(10) \n';
		}elseif(!Validation::maxLength($params['FechaIniPresupuesto'],10)){
			$mensaje = $mensaje.' FechaIniPresupuesto maxLength(10) \n';
		}elseif(!Validation::date($params['FechaIniPresupuesto'])){
			$mensaje = $mensaje.' FechaIniPresupuesto incorrect format(YYYY-MM-DD) \n';
		}
		
		if(!Validation::notEmpty($params['FechaFinPresupuesto'])){
			//$mensaje = $mensaje.' FechaFinPresupuesto is not empty \n';
		}elseif(!Validation::minLength($params['FechaFinPresupuesto'],10)){
			$mensaje = $mensaje.' FechaFinPresupuesto minLenght(10) \n';
		}elseif(!Validation::maxLength($params['FechaFinPresupuesto'],10)){
			$mensaje = $mensaje.' FechaFinPresupuesto maxLength(10) \n';
		}elseif(!Validation::date($params['FechaFinPresupuesto'])){
			$mensaje = $mensaje.' FechaFinPresupuesto incorrect format(YYYY-MM-DD) \n';
		}
		
		if(!Validation::notEmpty($params['ClasePedido'])){
			$mensaje = $mensaje.' ClasePedido is not empty \n';
		}elseif(!Validation::maxLength($params['ClasePedido'],4)){
			$mensaje = $mensaje.' ClasePedido maxLength(4) \n';
		}
		
		if(!Validation::notEmpty($params['MotivoPedido'])){
			$mensaje = $mensaje.' MotivoPedido is not empty \n';
		}elseif(!Validation::maxLength($params['MotivoPedido'],3)){
			$mensaje = $mensaje.' MotivoPedido maxLength(3) \n';
		}
	
		if(!Validation::notEmpty($params['Chasis'])){
			$mensaje = $mensaje.' Chasis is not empty \n';
		}elseif(!Validation::maxLength($params['Chasis'],35)){
			$mensaje = $mensaje.' Chasis maxLength(35) \n';
		}
		
		if(!Validation::notEmpty($params['EstadoOT'])){
			$mensaje = $mensaje.' EstadoOT is not empty \n';
		}elseif(!Validation::maxLength($params['EstadoOT'],4)){
			$mensaje = $mensaje.' EstadoOT maxLength(4) \n';
		}
		
		if(!Validation::notEmpty($params['EstadoUnidad'])){
			$mensaje = $mensaje.' EstadoUnidad is not empty \n';
		}elseif(!Validation::maxLength($params['EstadoUnidad'],2)){
			$mensaje = $mensaje.' EstadoUnidad maxLength(2) \n';
		}
		
		if(!Validation::notEmpty($params['UbicacionUnidad'])){
			//$mensaje = $mensaje.' UbicacionUnidad is not empty \n';
		}elseif(!Validation::maxLength($params['UbicacionUnidad'],10)){
			$mensaje = $mensaje.' UbicacionUnidad maxLength(10) \n';
		}
		
		
		if(!Validation::notEmpty($params['Marca'])){
			$mensaje = $mensaje.' Marca is not empty \n';
		}elseif(!Validation::maxLength($params['Marca'],30)){
			$mensaje = $mensaje.' Marca maxLength(30) \n';
		}
		
		if(!Validation::notEmpty($params['Modelo'])){
			$mensaje = $mensaje.' Modelo is not empty \n';
		}elseif(!Validation::maxLength($params['Modelo'],40)){
			$mensaje = $mensaje.' Modelo maxLength(40) \n';
		}
		
		if(!Validation::notEmpty($params['Color'])){
			$mensaje = $mensaje.' Color is not empty \n';
		}elseif(!Validation::maxLength($params['Color'],30)){
			$mensaje = $mensaje.' Color maxLength(30) \n';
		}
		
		if(!Validation::notEmpty($params['Concesionario'])){
			$mensaje = $mensaje.' Concesionario is not empty \n';
		}elseif(!Validation::maxLength($params['Concesionario'],4)){
			$mensaje = $mensaje.' Concesionario maxLength(4) \n';
		}
		
		if(!Validation::notEmpty($params['FechaTrasladoDeposito'])){
			//$mensaje = $mensaje.' FechaTrasladoDeposito incorrect format \n';
		}elseif(!Validation::minLength($params['FechaTrasladoDeposito'],10)){
			$mensaje = $mensaje.' FechaTrasladoDeposito minLenght(10) \n';
		}elseif(!Validation::maxLength($params['FechaTrasladoDeposito'],10)){
			$mensaje = $mensaje.' FechaTrasladoDeposito maxLength(10) \n';
		}elseif(!Validation::date($params['FechaTrasladoDeposito'])){
			$mensaje = $mensaje.' FechaTrasladoDeposito incorrect format(YYYY-MM-DD) \n';
		}
		
		if(!Validation::notEmpty($params['FechaRecepcionPDI'])){
			//$mensaje = $mensaje.' FechaRecepcionPDI incorrect format \n';
		}elseif(!Validation::minLength($params['FechaRecepcionPDI'],10)){
			$mensaje = $mensaje.' FechaRecepcionPDI minLenght(10) \n';
		}elseif(!Validation::maxLength($params['FechaRecepcionPDI'],10)){
			$mensaje = $mensaje.' FechaRecepcionPDI maxLength(10) \n';
		}elseif(!Validation::date($params['FechaRecepcionPDI'])){
			$mensaje = $mensaje.' FechaRecepcionPDI incorrect format(YYYY-MM-DD) \n';
		}
		
		if(!Validation::notEmpty($params['UtilizacionVehiculo'])){
			$mensaje = $mensaje.' UtilizacionVehiculo is not empty \n';
		}elseif(!Validation::maxLength($params['UtilizacionVehiculo'],2)){
			$mensaje = $mensaje.' UtilizacionVehiculo maxLength(2) \n';
		}

		if(!Validation::notEmpty($params['CodSAPCiaAseguradora'])){
			$mensaje = $mensaje.' CodSAPCiaAseguradora is not empty \n';
		}elseif(!Validation::maxLength($params['CodSAPCiaAseguradora'],10)){
			$mensaje = $mensaje.' CodSAPCiaAseguradora maxLength(10) \n';
		}
		
		if(!empty($mensaje) && isset($mensaje)){
			$estado = false;
		}
		return $response = array('ESTADO'=>$estado,'MENSAJE'=>$mensaje);

	}
	
	function ZDBM_CREA_OT_TALLER_PERU($params){
		ini_set("max_execution_time",0);
		$response = array('ESTADO'=>'NOOK','MENSAJE'=>'Incorrect Process','HOST'=>'');
		$validation = $this->validateDataOT_TALLER_PERU($params);
		$hostCliente = $_SERVER['REMOTE_ADDR'];
		$interface = Configure::read('InterfaceSap');
		if(!$validation['ESTADO']){
			$response['MENSAJE'] = $validation['MENSAJE'];
			return $response;
		//}elseif($this->validarServerCliente($hostCliente,$interface)){
		}elseif(true){
			//SAVE DATA ON THE DATABASE TABLE TALOT
			$talot = $this->convertDataWebServiceToTalot($params);
			//IF EXISTS TALOT
			$this->begin();
			$sql = "select Talot.id from talots Talot where Talot.empresacars='".$talot['empresacars']."' 
							and Talot.nrocars='".$talot['nrocars']."' limit 1 FOR UPDATE";
			$datatalot = $this->query($sql);
			
			if(!empty($datatalot) && isset($datatalot)){
				$talot['id'] = $datatalot[0]['Talot']['id'];
				$talot['update_datetime'] = $this->fechaHoraActual(); 
				$mensaje = 'Orden Existe';
			}else{
				$talot['insert_datetime'] = $this->fechaHoraActual();
				$talot['update_datetime'] = $this->fechaHoraActual();
				$mensaje = 'Orden No existe';
			}
			//add information ip cliente
			$talot['host'] = $hostCliente;

			if($this->save($talot)){
				$response['ESTADO'] = 'OK';
				$response['MENSAJE'] = $mensaje.', se ingresaron los datos correctamente';
				$response['HOST'] = $hostCliente;
				$this->commit();
			}else{
				$this->rollback();
			}
		}else{
			$response['ESTADO'] = 'NOOK';
			$response['MENSAJE'] = 'No, local computer authorized';
			$response['HOST'] = '';
		}
		return $response;
	}
	
	function validarServerCliente($hostCliente,$interface){
		App::import('Vendor', 'datos');
		$datos = new Datos();
		$interfaces = $datos->getDatos('interfacesSapIpDns');
		if(strpos($interfaces[$interface][0],$hostCliente)!==false || strpos($interfaces[$interface][1],$hostCliente)!==false) return true;
		return false;
	}
	
	function convertDataWebServiceToTalot($params){
		$talot = array();
		//id, plaworkorder_id, nrocars, empresacars, descripcion, propietario, carscliente, placa, marca, modelo, tipoorden, ordenfecha, 
		//ordenhora, documento, tipodocumento, planificado, parcial, terminado, codlocal, local, codasesor, ruc, chassis, autocars, version,
		// nroreservacars, estado, nrocono, nombreasesor, color, fechaprometida, horaprometida, kilometraje, fechaentrega, planificacion, 
		//nroremito, propuesta, observacion, fechaPlanificacion, FDP, FFP, FEC, FMC, FFT, dermecanico_id, secperson_id, FPT, FMPT, adelantoProg, 
		//libforzada, fecha_liberacion, usuarioLiberacion, fecha_traslado, predestino_id, codgas, carsNumeroModelo, maq_veces_terminado, DocPreOtGe, DocAseFch, DocAseFchL
		
		//'Sociedad','FechaOT','HoraOT' ,'FechaCita' ,'HoraCita','NroOT','DetalleSolicitud','TipoCliente','NombreCliente','IDClienteSAP',
		//'TipoDocFiscal','NroDocFiscal','CodAsesor' ,'NumCono','FechaPromEnt','HoraPromEnt' ,'CentroCod','Placa','IDUnidad','NroPresupuesto'
		//'FechaIniPresupuesto','FechaFinPresupuesto','ClasePedido','MotivoPedido','Chasis','EstadoOT' ,'EstadoUnidad','UbicacionUnidad','Marca','Modelo' 
		//	'Color','Concesionario','FechaTrasladoDeposito','FechaRecepcionPDI','UtilizacionVehiculo'
		
		$talot['nrocars'] = $params['NroOT'];
		//$talot['descripcion'] = 'Fecha: '.$params['FechaOT'].' hora:'.$params['HoraOT'].' Sociedad: '.$params['Sociedad'].' Detalle: '.$params['DetalleSolicitud'];
		
		if($this->isNotEmpty($params['FechaOT'])){
			$talot['ordenfecha'] = $this->getDateFromStringWebservice($params['FechaOT']);
		}
		if($this->isNotEmpty($params['HoraOT'])){
			$talot['ordenhora'] = $this->getTimeFromStringWebservice($params['HoraOT']);
		}
		//NO TIENE COLUMNA EN TALOTS SE CREO EL CAMPO
		if($this->isNotEmpty($params['UtilizacionVehiculo'])){
			$talot['UtilizacionVehiculo'] = $params['UtilizacionVehiculo'];
		}
		if($this->isNotEmpty($params['FechaRecepcionPDI'])){
			$talot['FechaRecepcionPDI'] = $this->getDateFromStringWebservice($params['FechaRecepcionPDI']);
		}
		if($this->isNotEmpty($params['Concesionario'])){
			$talot['Concesionario'] = $params['Concesionario'];
		}
		if($this->isNotEmpty($params['ClasePedido'])){
			$talot['ClasePedido'] = $params['ClasePedido'];
		}
		if($this->isNotEmpty($params['EstadoUnidad'])){
			$talot['EstadoUnidad'] = $params['EstadoUnidad'];
		}
		if($this->isNotEmpty($params['UbicacionUnidad'])){
			$talot['UbicacionUnidad'] = $params['UbicacionUnidad'];
		}
		if($this->isNotEmpty($params['TipoCliente'])){
			$talot['TipoCliente'] = $params['TipoCliente'];
		}
		if($this->isNotEmpty($params['IDUnidad'])){
			$talot['IDUnidad'] = $params['IDUnidad'];
		}
		if($this->isNotEmpty($params['FechaCita'])){
			$talot['FechaCita'] = $params['FechaCita'];
		}
		if($this->isNotEmpty($params['HoraCita'])){
			$talot['HoraCita'] = $params['HoraCita'];
		}
		if($this->isNotEmpty($params['CodSAPCiaAseguradora'])){
			$talot['CodSAPCiaAseguradora'] = $params['CodSAPCiaAseguradora'];
		}

		//-----------------FIN DE SIN COLUMNA ---------------
		//---------------- TIENEN COLUMNA EN TALOTS ---------
		if($this->isNotEmpty($params['Sociedad'])){
			$talot['empresacars'] = $params['Sociedad'];
		}
		if($this->isNotEmpty($params['MotivoPedido'])){
			$talot['tipoorden'] = $params['MotivoPedido'];
		}
		if($this->isNotEmpty($params['DetalleSolicitud'])){
			$talot['descripcion'] = $params['DetalleSolicitud'];
		}
		if($this->isNotEmpty($params['IDClienteSAP'])){
			$talot['carscliente'] = $params['IDClienteSAP'];
		}
		if($this->isNotEmpty($params['NombreCliente'])){
			$talot['propietario'] = $params['NombreCliente'];
		}
		if($this->isNotEmpty($params['Placa'])){
			$talot['placa'] = $params['Placa'];
		}
		if($this->isNotEmpty($params['Marca'])){
			$talot['marca'] = $params['Marca'];
		}
		if($this->isNotEmpty($params['Modelo'])){
			$talot['modelo'] = $params['Modelo'];
		}
		if($this->isNotEmpty($params['NroDocFiscal'])){
			$talot['documento'] = $params['NroDocFiscal'];
		}
		if($this->isNotEmpty($params['TipoDocFiscal'])){
			$talot['tipodocumento'] = $params['TipoDocFiscal'];
		}
		if($this->isNotEmpty($params['CentroCod'])){
			$talot['codlocal'] = $params['CentroCod'];
		}
		if($this->isNotEmpty($params['NroPresupuesto'])){
			$talot['DocPreOtGe'] = $params['NroPresupuesto'];
		}
		if($this->isNotEmpty($params['CodAsesor'])){
			$talot['codasesor'] = $params['CodAsesor'];
		}
		if($this->isNotEmpty($params['EstadoOT'])){
			$talot['estado'] = $params['EstadoOT'];
		}
		if($this->isNotEmpty($params['NumCono'])){
			$talot['nrocono'] = $params['NumCono'];
		}
		if($this->isNotEmpty($params['Chasis'])){
			$talot['chassis'] = $params['Chasis'];
		}
		if($this->isNotEmpty($params['FechaPromEnt'])){
			$talot['fechaprometida'] = $this->getDateFromStringWebservice($params['FechaPromEnt']);
		}
		if($this->isNotEmpty($params['HoraPromEnt'])){
			$talot['horaprometida'] = $this->getTimeFromStringWebservice($params['HoraPromEnt']);
		}
		if($this->isNotEmpty($params['FechaTrasladoDeposito'])){
			$talot['fecha_traslado'] = $this->getDateFromStringWebservice($params['FechaTrasladoDeposito']);
		}
		if($this->isNotEmpty($params['Color'])){
			$talot['color'] = $params['Color'];
		}
		if($this->isNotEmpty($params['FechaIniPresupuesto'])){
			$talot['DocAseFch'] = $this->getDateFromStringWebservice($params['FechaIniPresupuesto']);
		}
		if($this->isNotEmpty($params['FechaFinPresupuesto'])){
			$talot['DocAseFchL'] = $this->getDateFromStringWebservice($params['FechaFinPresupuesto']);
		}	

		return $talot;
	}
}
