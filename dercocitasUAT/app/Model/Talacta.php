<?php
class Talacta extends AppModel {
   public $name = 'Talacta';
   public $validate = array(
//   		'fechaUsuario'=>array('required'=>true, 'allowEmpty'=>false, 'message'=>"TIPO ACTA: Obligatorio"),
//   		'descripcion'=>array('required'=>true, 'allowEmpty'=>false, 'message'=>"TIPO ACTA: Obligatorio")
	);
   
   public function setGuardarActa($dt,$usuario=null){		
		app::import('Model','Talot');			$this->Talot = new Talot;		
		
		$this->Talot->recursive = -1;
		$talot = $this->Talot->findByNrocars($dt['Talacta']['ot'],array('id'));		
		
		$dt['Talacta']['talot_id'] = $talot['Talot']['id'];	
		$dt['Dervendedor']['fechaUsuario'] = $dt['Talacta']['fechaUsuario'].' '.$dt['Talacta']['hora'].':'.$dt['Talacta']['minuto'].':00';
		$dt['Talacta']['fechaUsuario'] = $this->getDateFormatDB($dt['Talacta']['fechaUsuario']).' '.$dt['Talacta']['hora'].':'.$dt['Talacta']['minuto'].':00';		
		$dt['Talacta']['fechaSistema'] = $this->getDateFormatDB('', null, null, $now=1);
		$dt['Talacta']['secperson_id'] = $usuario;
		$dtActa = $dt;

		$this->create();
		if(!$this->save($dtActa) )return false;
		return true;
   }
   
   public function getTalactaByOT($talot_id){
   	$sql="select Talacta.fechaUsuario, Talacta.descripcion, concat(Secperson.firstName,' ',Secperson.appaterno, ' ', Secperson.apmaterno) as Usuario
		 from talactas Talacta left join secpeople Secperson on(Talacta.secperson_id=Secperson.id) where Talacta.talot_id=".$talot_id;
   	$lista=$this->query($sql);
	return (!empty($lista) && isset($lista))?$lista:array();
   }
   
   public function guardarActaMultiple($datos,$comentario,$ot_numero,$usuario){
   		//primero guardamos el comentario inicial
		app::import('Model', 'Talot');		$this->Talot = new Talot;
				
		$this->Talot->recursive = -1;
		$talot = $this->Talot->findByNrocars($ot_numero,array('id'));
		
		if(empty($talot)) return array(false,'OT no Existe');
		$talot = $talot['Talot']['id'];		
		$dt['Talacta']['talot_id']=$talot;
		$dt['Talacta']['secperson_id'] = $usuario;
		$dt['Talacta']['fechaUsuario'] = date("Y-m-d H:i:s");		
		$dt['Talacta']['fechaSistema'] = date('Y-m-d H:i:s');
		$dt['Talacta']['descripcion'] = $comentario;
		$this->create();
		if(!$this->save($dt)) return array(false,'Error al Registrar Acta');
		if(!empty($datos) && isset($datos)){
	   		foreach($datos as $id => $item){
	   			$dt['Talacta']['talot_id']=$talot;
				$dt['Talacta']['secperson_id'] = $usuario;
				$dt['Talacta']['fechaUsuario'] = date("Y-m-d H:i:s");		
				$dt['Talacta']['fechaSistema'] = date('Y-m-d H:i:s');
				$dt['Talacta']['descripcion'] = $item['comentario'];
	   			$this->create();
				if(!$this->save($dt)) return array(false,'Error al Registrar Acta');
	   		}			
		}
		return array(true,'Acta registrado');
   }
}