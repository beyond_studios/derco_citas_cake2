<?php
class Viewccptabhistorialdocumento extends AppModel{
	public $name = 'Viewccptabhistorialdocumento';
	
	public $belongsTo = array(
		'Talot'=>array(
			'className'=>'Talot',
			'foreingkey'=>'talot_id'
		),
		'Ccptabhistorialdocumento'=>array(
			'className'=>'Ccptabhistorialdocumento',
			'foreingkey'=>'ccptabhistorialdocumento_id'
		)
	);
}
