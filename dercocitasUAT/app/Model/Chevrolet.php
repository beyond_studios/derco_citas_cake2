<?php
class Chevrolet extends AppModel{
	public $name = 'Chevrolet';
	public $meses = array('1'=>'Ene','2'=>'Feb','3'=>'Mar','4'=>'Abr','5'=>'May','6'=>'Jun','7'=>'Jul',
		'8'=>'Ago','9'=>'Set','10'=>'Oct','11'=>'Nov','12'=>'Dic');	
		
	public function validFecha($fecha){
		$fecha = $this->getDateFormatDB($fecha);
		$intervalYear = 2;  // intervalo en años
		
		// vefificamos la fecha de vencimiento
		$car = $this->find('first', array(
			'fields'=>array("1"=>"DATE_SUB(CURDATE(),INTERVAL $intervalYear YEAR) <= '$fecha' AS activo")
		));
	 
	 	if(empty($car['0']['activo'])){
			return array('errors'=>false, 'msg'=>'La unidad se encuentra actualmente FUERA del periodo de garantia.  ES FACTIBLE PROGRAMAR CITA');
		}else{
			$car = $this->find('first', array(
				'fields'=>array(
					'1'=>"EXTRACT(YEAR FROM DATE_ADD(DATE_ADD('$fecha', INTERVAL 1 DAY),INTERVAL $intervalYear YEAR)) as anio",
					'2'=>"EXTRACT(MONTH FROM DATE_ADD(DATE_ADD('$fecha', INTERVAL 1 DAY),INTERVAL $intervalYear YEAR)) as mes",
					'3'=>"EXTRACT(DAY FROM DATE_ADD(DATE_ADD('$fecha', INTERVAL 1 DAY),INTERVAL $intervalYear YEAR)) as dia"
				)
			));
			
			return array('errors'=>false, 
				'msg'=>sprintf("La unidad se encuentra actualmente DENTRO del periodo de garantia. La unidad podrá ser atendida en los talleres de DERCO a partir del día %s de %s del %s",$car['0']['dia'], $this->meses[$car['0']['mes']], $car['0']['anio']) );
		}
	}
	
	public function validCar($field, $value){
		$intervalYear = 2;  // intervalo en años
		
		$car = $this->find('first', array('conditions'=>array("Chevrolet.$field"=>$value)));
		
		if(empty($car)) return array('errors'=>true, 'msg'=>'La PLACA que esta consultando no existe en el sistema. Validar vigencia de garantia con el VIN');
		
		// vefificamos la fecha de vencimiento
		$car = $this->find('first', array(
			'conditions'=>array("1"=>"Chevrolet.$field = '$value' AND DATE_SUB(CURDATE(),INTERVAL $intervalYear YEAR) <= Chevrolet.fecha_base")
		));
	 
		if(empty($car)){
			return array('errors'=>false, 'msg'=>'La unidad se encuentra actualmente FUERA del periodo de garantia.  ES FACTIBLE PROGRAMAR CITA');
		}else{
			$car = $this->find('first', array(
				'fields'=>array(
					'1'=>"EXTRACT(YEAR FROM DATE_ADD(DATE_ADD(fecha_base, INTERVAL 1 DAY),INTERVAL $intervalYear YEAR)) as anio",
					'2'=>"EXTRACT(MONTH FROM DATE_ADD(DATE_ADD(fecha_base, INTERVAL 1 DAY),INTERVAL $intervalYear YEAR)) as mes",
					'3'=>"EXTRACT(DAY FROM DATE_ADD(DATE_ADD(fecha_base, INTERVAL 1 DAY),INTERVAL $intervalYear YEAR)) as dia"
				),
				'conditions'=>array("1"=>"Chevrolet.$field = '$value'")
			));
			
			return array('errors'=>false, 
				'msg'=>sprintf("La unidad se encuentra actualmente DENTRO del periodo de garantia. La unidad podrá ser atendida en los talleres de DERCO a partir del día %s de %s del %s",$car['0']['dia'], $this->meses[$car['0']['mes']], $car['0']['anio']) );
		}
			
		
	}
	
	function getConditionsBuscador($dt, $dtLog){
		//CODICION FECHAS
		$cndF = $this->_condicionFecha(
			empty($dt['bsc']['f_ini'])?'':$dt['bsc']['f_ini']
			,empty($dt['bsc']['f_fin'])?'':$dt['bsc']['f_fin']
			,$dt['bsc']['f_campo']
		);
		
		//CONDICION POR CRITERIO
		$cndCrt = array();
		if(!empty($dt['bsc']['crt']) && !empty($dt['bsc']['vlr'])){
			$cndCrt = strpos($dt['bsc']['crt'], 'numero');
			if($cndCrt) $cndCrt = $this->_condicionDocumento($dt['bsc']['crt'], trim($dt['bsc']['vlr']));
			else $cndCrt = array('1'=>$dt['bsc']['crt'].' LIKE \'%'.trim($dt['bsc']['vlr']).'%\'');
		}
		
		//CONDICION DE ACUERDO AL ESTADO
		$cndStd = empty($dt['bsc']['std'])?array():array('Agedetallecita.estado'=>sprintf("%s",$dt['bsc']['std']));
		
		//$cndB = array("Agedetallecita.estado"=>'AC');
		return $cndF + $cndCrt + $cndStd;
	}
}