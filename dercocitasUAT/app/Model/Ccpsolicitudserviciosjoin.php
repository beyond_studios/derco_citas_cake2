<?php
class Ccpsolicitudserviciosjoin extends AppModel{
	public $name = 'Ccpsolicitudserviciosjoin';
	public $useTable = false;
	
	var $query = "SELECT
         			Ccpsolicitudservicio.id,
					Ccpsolicitudservicio.ot_numero,
					Ccpsolicitudservicio.ot_placa,
					Ccpsolicitudservicio.ot_cliente,
					Ccpsolicitudservicio.fecha_emitido,
					Ccpsolicitudservicio.fechaaprobacioncompania,
					Ccpsolicitudservicio.presupuesto_numero,
					Ccptiposervicio.descripcion,
					Ccpsolicitudservicio.fecha_aprobacion,
					Ccpsolicitudservicio.presupuesto_fecha_aprobacion,
					Ccpsolicitudservicioestado.id,
					Ccpsolicitudservicioestado.descripcion,
					Ccpsolicitudservicio.ot_fecha_creacion,
					Ccpsolicitudservicio.horas_planchado,
					Ccpsolicitudservicio.horas_panio,
					CONCAT(Secperson.appaterno,', ',Secperson.firstname) AS asesor,
					Talot.propietario,Talot.ordenfecha,Talot.codasesor,Talot.fechaprometida,Talot.horaprometida,Talot.ordenhora,
					Talot.estado,Talot.descripcion,Talot.marca,Talot.placa,Talot.modelo,Talot.nrocars,
					Talot.nrocono,Talot.carscliente,Talot.chassis,Talot.tipoorden,Talot.id,
					Ccptabhistorial.ccptabestado_id,
					Ccptabhistorial.descripcion
				FROM ccpsolicitudservicios AS Ccpsolicitudservicio
        		JOIN ccpsolicitudservicioestados AS Ccpsolicitudservicioestado ON Ccpsolicitudservicioestado.id = Ccpsolicitudservicio.ccpsolicitudservicioestado_id
        		JOIN ccptiposervicios AS Ccptiposervicio ON Ccptiposervicio.id = Ccpsolicitudservicio.ccptiposervicio_id
				JOIN secpeople AS Secperson ON Secperson.id = Ccpsolicitudservicio.secperson_id
				LEFT JOIN talots as Talot ON Talot.nrocars = Ccpsolicitudservicio.ot_numero -- and empresacars in ('DP','DC')
				LEFT JOIN (SELECT Ccptabhistorial.ot_numero,Ccptabhistorial.ccptabestado_id,Ccptabestado.descripcion,CcptabhistorialTemp.ccpsolicitudservicio_id
 							FROM (
								SELECT MAX(id) as id,ot_numero,ccpsolicitudservicio_id
								FROM ccptabhistoriales
								WHERE 1=1 AND av_cndFecha
								GROUP BY ot_numero,ccpsolicitudservicio_id) as CcptabhistorialTemp
  							INNER JOIN ccptabhistoriales Ccptabhistorial on(CcptabhistorialTemp.id=Ccptabhistorial.id)
                			LEFT JOIN ccptabestados Ccptabestado on(Ccptabhistorial.ccptabestado_id=Ccptabestado.id)) as Ccptabhistorial
							ON(Ccpsolicitudservicio.ot_numero = Ccptabhistorial.ot_numero) 
							AND Ccpsolicitudservicio.id =Ccptabhistorial.ccpsolicitudservicio_id";
	/**
	 * Overridden paginate method - group by week, away_team_id and home_team_id
	 */
	public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
		$sql = str_replace("av_cndFecha", "1=1", $this->query);

		if ($conditions) $sql .= ' WHERE '.$conditions;
		
		if(!empty($order)){
			foreach($order as $field => $value){
				$orden[]= $field." ".$value;
			}
			$ordenado = implode($orden, ',');
			$sql.= "ORDER BY " . $ordenado. " ";
		}
		
		$sql.= " LIMIT ".$limit." OFFSET ".($page-1)*$limit;

		$results = $this->query($sql);	
		
		app::import('Model', 'Ccptabhistorial'); $this->Ccptabhistorial = new Ccptabhistorial();
		
		foreach($results as $key => $solicitudservicio){
			$results[$key]['Ccpsolicitudservicio']['fecha_emitido'] = $this->getDateFormatView($solicitudservicio['Ccpsolicitudservicio']['fecha_emitido']);
			$results[$key]['Ccpsolicitudservicio']['presupuesto_fecha_aprobacion'] = $this->getDateFormatView($solicitudservicio['Ccpsolicitudservicio']['presupuesto_fecha_aprobacion']);
			$results[$key]['Ccpsolicitudservicio']['fecha_aprobacion'] = $this->getDateFormatView($solicitudservicio['Ccpsolicitudservicio']['fecha_aprobacion']);
			$results[$key]['Ccpsolicitudservicio']['ot_fecha_creacion'] = $this->getDateFormatView($solicitudservicio['Ccpsolicitudservicio']['ot_fecha_creacion']);
			$results[$key]['Ccpsolicitudservicio']['asesor'] = $solicitudservicio[0]['asesor'];
			$results[$key]['Ccpsolicitudservicio']['color']=$this->Ccptabhistorial->obtenerDiferenciaDias(date('d-m-Y'),$results[$key]['Ccpsolicitudservicio']['ot_fecha_creacion']);
		}
			
		return $results;
	}						
							
	/**
	 * Overridden paginateCount method
	 */
	public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
	    $sql = str_replace("av_cndFecha", "1=1", $this->query);
		if ($conditions) $sql .= ' WHERE '.$conditions;
	    $this->recursive = $recursive;
	    $results = $this->query($sql);
	    return count($results);
	}
	
	public function getConditionsBsc($dt, $dtLg){
		// IMPLEMENTACION DEL BUSCADOR Y SUS FINTROS
		$condicion = '1=1';
		
		$fechaSol = $this->_condicionFecha(
			empty($dt['fechaIni'])?'':$dt['fechaIni']
			,empty($dt['fechaFin'])?'':$dt['fechaFin']
			,'Ccpsolicitudservicio.fecha_emitido'
			,'100'
		);
		$fechaSol = empty($fechaSol)?"1=1":$fechaSol['100'];
		
		$fechaOt = $this->_condicionFecha(
			empty($dt['fechaIniOt'])?'':$dt['fechaIniOt']
			,empty($dt['fechaFinOt'])?'':$dt['fechaFinOt']
			,'Ccpsolicitudservicio.ot_fecha_creacion'
			,'100'
		);
		$fechaOt = empty($fechaOt)?"1=1":$fechaOt['100'];
		
		$condicion.=" AND $fechaSol AND $fechaOt ";
		
		if(!empty($dt['ccptiposervicio_id'])) $condicion .= " AND Ccpsolicitudservicio.ccptiposervicio_id =".$dt['ccptiposervicio_id'];
		if(!empty($dt['ccpsolicitudservicioestado_id'])) $condicion .= " AND Ccpsolicitudservicio.ccpsolicitudservicioestado_id =".$dt['ccpsolicitudservicioestado_id'];
		if(!empty($dt['nrocars'])) $condicion .= " AND Ccpsolicitudservicio.ot_numero like '%".$dt['nrocars']."%'";
		if(!empty($dt['placa'])) $condicion .= " AND Ccpsolicitudservicio.ot_placa like '%".$dt['placa']."%'";
		if(!empty($dt['estadootccp_id'])) $condicion .= " AND Ccptabhistorial.ccptabestado_id = ".$dt['estadootccp_id']."";
		
		//puede darse el caso de que la ot aun no este en talots
		if(!empty($dt['carsestado_id'])){
			$estadoCars = $dt['carsestado_id']; 
			$condicion .= " AND Talot.estado = '$estadoCars'"; 
		}else  $condicion .= " AND Talot.estado in ('0000','0060','0070','0090','0095')"; 
		
		$cndBasic = "Talot.empresacars in ('PU03') AND Talot.codlocal in ('P035', 'P057') AND Talot.ClasePedido in ('2000','4000') AND Talot.tipoorden in ('PEF','PEG','PEH','PEI')";
		return $cndBasic." AND ".$condicion;
	}
	
	public function getCndIndexSolService($dt, $dtLg){
		// IMPLEMENTACION DEL BUSCADOR Y SUS FINTROS
		$condicion = '1=1';
		
		$fechaSol = $this->_condicionFecha(
			empty($dt['fechaIni'])?'':$dt['fechaIni']
			,empty($dt['fechaFin'])?'':$dt['fechaFin']
			,'Ccpsolicitudservicio.fecha_emitido'
			,'100'
		);
		$fechaSol = empty($fechaSol)?"1=1":$fechaSol['100'];
		
		$fechaOt = $this->_condicionFecha(
			empty($dt['fechaIniOt'])?'':$dt['fechaIniOt']
			,empty($dt['fechaFinOt'])?'':$dt['fechaFinOt']
			,'Ccpsolicitudservicio.ot_fecha_creacion'
			,'100'
		);
		$fechaOt = empty($fechaOt)?"1=1":$fechaOt['100'];
		$condicion.=" AND $fechaSol AND $fechaOt ";
		
		
		
		// agregamos las condiciones por fecha de Aprobacion
		$fechaApb = $this->_condicionFecha(
			empty($dt['fechaIniApb'])?'':$dt['fechaIniApb'], 
			empty($dt['fechaFinApb'])?'':$dt['fechaFinApb'], 
			'Ccptabhistorial.fecha'
			,'100');
		
		$cndFechaApb = " 1=1 ";
		if(!empty($fechaApb)){
			// CONDICION INTERNA (CONSULTA DE BUSQUEDA)
			$fechaApb = $this->_condicionFecha(
				empty($dt['fechaIniApb'])?'':$dt['fechaIniApb'], 
				empty($dt['fechaFinApb'])?'':$dt['fechaFinApb'],
				'ccptabhistoriales.fecha',
				'100');
			$fechaApb = empty($fechaApb)?"1=1":$fechaApb['100'];
			$cndFechaApb = sprintf("ccptabhistoriales.ccptabestado_id >= 7 AND  %s",$fechaApb);
			
			$condicion .= " AND Ccptabhistorial.ccptabestado_id >= 7 ";
		}
		
		
		if(!empty($dt['ccptiposervicio_id'])) $condicion .= " AND Ccpsolicitudservicio.ccptiposervicio_id =".$dt['ccptiposervicio_id'];
		if(!empty($dt['ccpsolicitudservicioestado_id'])) $condicion .= " AND Ccpsolicitudservicio.ccpsolicitudservicioestado_id =".$dt['ccpsolicitudservicioestado_id'];
		if(!empty($dt['nrocars'])) $condicion .= " AND Ccpsolicitudservicio.ot_numero like '%".$dt['nrocars']."%'";
		if(!empty($dt['placa'])) $condicion .= " AND Ccpsolicitudservicio.ot_placa like '%".$dt['placa']."%'";
		if(!empty($dt['estadootccp_id'])) $condicion .= " AND Ccptabhistorial.ccptabestado_id = ".$dt['estadootccp_id']."";
		
		//AVENTURA: AGREGAMOS LAS CONDICIONES DE CARS
		if(empty($dt['carsestado_id'])){
			$stdCars = array('0000','0060','0070','0090','0095','0103','0999');
			$condicion .= sprintf(" AND (Talot.estado IN ('%s') OR Talot.estado is null) ", implode("','", $stdCars));
		}else{
			$condicion .= sprintf(" AND (Talot.estado IN ('%s') OR Talot.estado is null) ", $dt['carsestado_id']);
		}
		
		//aventura: cndBasic add the day 20130731
		$cndBasic = "Talot.empresacars in ('PU03') AND Talot.codlocal in ('P035', 'P057') AND Talot.ClasePedido in ('2000','4000')";
		return $cndBasic." AND ".$condicion;
	}	
	
	/**
	 * CREADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * @param object $dt
	 * @param object $params
	 * @param object $login
	 * @param object $total [optional]
	 * @return 
	 */
	function getTalotCars($sistemaCcp){
		app::import('Model', 'Ccpsolicitudservicio'); 		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio();
		app::import('Model', 'Talot'); 		$this->Talot = new Talot();
		app::import('Model', 'Ccptabhistorial'); 		$this->Ccptabhistorial = new Ccptabhistorial();
			
		$this->Talot = new Talot();
		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
						
		foreach($sistemaCcp as $key => $talot){
			$sistemaCcp[$key]['Talot']['ot_numero'] = trim($talot['Ccpsolicitudservicio']['ot_numero']);
			$sistemaCcp[$key]['Talot']['ot_pre_estado'] = $talot['Ccpsolicitudservicio']['ot_pre_estado'];
			$sistemaCcp[$key]['Talot']['presupuesto_numero'] = trim($talot['Ccpsolicitudservicio']['presupuesto_numero']);
			$sistemaCcp[$key]['Talot']['ot_fecha_creacion'] = $this->getDateFormatView($talot['Ccpsolicitudservicio']['ot_fecha_creacion']);
			$sistemaCcp[$key]['Talot']['ot_asesor'] = trim($talot['Ccpsolicitudservicio']['ot_asesor']);
			$sistemaCcp[$key]['Talot']['chasis_vin'] = trim($talot['Ccpsolicitudservicio']['chasis_vin']);
			$sistemaCcp[$key]['Talot']['ot_cliente'] = trim($talot['Ccpsolicitudservicio']['ot_cliente']);
			$sistemaCcp[$key]['Talot']['ot_placa'] = trim($talot['Ccpsolicitudservicio']['ot_placa']);
			$sistemaCcp[$key]['Talot']['ot_fecha_entrega'] = $this->getDateFormatView($talot['Ccpsolicitudservicio']['ot_fecha_entrega']);
			$sistemaCcp[$key]['Talot']['pre_fecha_aprobacion'] = $this->getDateFormatView($talot['Ccpsolicitudservicio']['pre_fecha_aprobacion']);
			
			//prueba con tooltip desde cars
			$sistemaCcp[$key]['Talot']['ot_estado'] = $talot['Ccpsolicitudservicio']['ot_estado'];
			$sistemaCcp[$key]['Talot']['ot_marca'] = $talot['Ccpsolicitudservicio']['ot_marca'];
			$sistemaCcp[$key]['Talot']['ot_modelo'] = $talot['Ccpsolicitudservicio']['ot_modelo'];
			$sistemaCcp[$key]['Talot']['ot_version'] = $talot['Ccpsolicitudservicio']['ot_version'];
			$sistemaCcp[$key]['Talot']['ot_descripcion'] = $talot['Ccpsolicitudservicio']['ot_descripcion'];
			$sistemaCcp[$key]['Talot']['ot_nombre_asesor'] = $talot['Ccpsolicitudservicio']['ot_nombre_asesor'];
			$sistemaCcp[$key]['Talot']['ot_tipo_ot'] = $talot['Ccpsolicitudservicio']['ot_tipo_ot'];
			$sistemaCcp[$key]['Talot']['ot_hora_recibida'] = $talot['Ccpsolicitudservicio']['ot_hora_recibida'];
			$sistemaCcp[$key]['Talot']['ot_hora_prometida'] = $talot['Ccpsolicitudservicio']['ot_hora_prometida'];
			$sistemaCcp[$key]['Talot']['ot_numero_cono'] = $talot['Ccpsolicitudservicio']['ot_numero_cono'];
			
			$sistemaCcp[$key]['tooltip']['Talot'] =$sistemaCcp[$key]['Talot'];
			$tooltip = $this->Talot->findByNrocars($talot['Ccpsolicitudservicio']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$sistemaCcp[$key]['tooltip']['Talot']['acta']=true;
			else $sistemaCcp[$key]['tooltip']['acta'] = false;

			//obtenemos el color de la ot
			$sistemaCcp[$key]['Ccpsolicitudservicio']['color']=$this->Ccptabhistorial->obtenerDiferenciaDias(date('d-m-Y'),$sistemaCcp[$key]['Ccpsolicitudservicio']['ot_fecha_creacion']);
			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistorial->getSolicitudServicioAndCcpEstado(trim($talot['Ccpsolicitudservicio']['ot_numero']));
			
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->getDateFormatView($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$sistemaCcp[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$sistemaCcp[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$sistemaCcp[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$sistemaCcp[$key]['Ccpsolicitudservicio']=array();
					$sistemaCcp[$key]['Ccptiposervicio']=array();
					$sistemaCcp[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$sistemaCcp[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$sistemaCcp[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			unset($solicitudServicio);
		} 
		debug($sistemaCcp);
		return $sistemaCcp;
	}	
	
	
		
//	/**RECUPERA LAS CONDICIONES DE BUSQUEDA
//	* AUTOR: VENTURA RUEDA, JOSE ANTONIO
//	* @return 
//	*/
//	public function getConditionsBuscador($dt, $dtLog){
//		//MODELOS UTILIZADOS
//		app::import('Model','Ccptabhistorial'); 		$this->Ccptabhistorial = new Ccptabhistorial();
//		app::import('Model','Ccpsolicitudservicio'); 		 $this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
//		
//		//CONDICIONES PARA LA SOLICITUD DE SERVICIO
//		$ot_solicitudes = array();
//		$cndSolicitudBsc = array('201'=>'Ccpsolicitudservicio.ot_numero is not null');
//		$cndSolStd = empty($dt['bsc']['ccpsolicitudservicioestado_id'])?array():array('Ccpsolicitudservicio.ccpsolicitudservicioestado_id'=>sprintf("%s",$dt['bsc']['ccpsolicitudservicioestado_id']));
//		$cndSolTpSrv = empty($dt['bsc']['ccptiposervicio_id'])?array():array('Ccpsolicitudservicio.ccptiposervicio_id'=>sprintf("%s",$dt['bsc']['ccptiposervicio_id']));
//		
//		
//		$fechaSol = $this->_condicionFecha(
//			empty($dt['bsc']['f_ini'])?'':$dt['bsc']['f_ini']
//			,empty($dt['bsc']['f_fin'])?'':$dt['bsc']['f_fin']
//			,'Ccpsolicitudservicio.fecha_emitido'
//			,'100'
//		);
//		
//		$cndSolicitudBsc = $cndSolicitudBsc + $fechaSol + $cndSolStd + $cndSolTpSrv;
//		if(count($cndSolicitudBsc)>1){
//			$ot_solicitudes = $this->Ccpsolicitudservicio->find('list', array('conditions'=>$cndSolicitudBsc, 'fields'=>array('Ccpsolicitudservicio.ot_numero','Ccpsolicitudservicio.ot_numero'))); 
//			$ot_solicitudes = empty($ot_solicitudes)?array('201'=>'SistemaPlanificadorCcp.nrocars IN(\'0\')'):array('201'=>'SistemaPlanificadorCcp.nrocars IN(\''.implode('\',\'', $ot_solicitudes).'\')');
//		}
//			
//		// CONDICIONES PARA LA OT
//		$cndF_2 = $this->_condicionFecha(
//			empty($dt['bsc']['f_ini_2'])?'':$dt['bsc']['f_ini_2']
//			,empty($dt['bsc']['f_fin_2'])?'':$dt['bsc']['f_fin_2']
//			,'SistemaPlanificadorCcp.FECHA'
//			,'101'
//		);
//		
//		//CONDITIONS SELECT
//		$cndAsesor = empty($dt['bsc']['asesor_id'])?array():array('SistemaPlanificadorCcp.USUARIO_OT'=>sprintf("%s",$dt['bsc']['asesor_id']));
//		$cndNroCars = empty($dt['bsc']['nrocars'])?array():array('SistemaPlanificadorCcp.OT'=>sprintf("%s",$dt['bsc']['nrocars']));
//		$cndPlaca = empty($dt['bsc']['placa'])?array():array('SistemaPlanificadorCcp.PLACA'=>sprintf("%s",$dt['bsc']['placa']));
//		$cndPreEstado = empty($dt['bsc']['preestado_id'])?array():array('SistemaPlanificadorCcp.OT_PRE_ESTADO'=>sprintf("%s",$dt['bsc']['preestado_id']));
//		
//		//RECUPERAMOS las ots que se encuentran en estado
//		$cndStdTabHist=array();
//		if(!empty($dt['bsc']['estadootccp_id']) && isset($dt['bsc']['estadootccp_id'])){
//			if(strlen($dt['bsc']['estadootccp_id'])==6){
//				if(strpos('1', $dt['bsc']['estadootccp_id']) !== false){// si tiene uno en su cadena
//					$cndStdTabHist = array();
//				}else{
//					$cndStdTabHist = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($dt['bsc']['estadootccp_id']);
//					$cndStdTabHist = empty($cndStdTabHist)?array():'AND SistemaPlanificadorCcp.nrocars IN(\''.implode('\',\'', $cndStdTabHist).'\')';
//				}
//			}else{
//				if($dt['bsc']['estadootccp_id']==1){
//					$cndStdTabHist=array();
//				}else{				
//					$cndStdTabHist = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($dt['bsc']['estadootccp_id']);
//					$cndStdTabHist = array('202'=>empty($cndStdTabHist)?'AND ot.OT IN(\'0\')':'AND ot.OT IN(\''.implode('\',\'', $cndStdTabHist).'\')');
//				}
//			}
//		}
//		
//		//puede darse el caso de que la ot aun no este en talots
//		$cndCarsStd = empty($dt['bsc']['carsestado_id'])?array("SistemaPlanificadorCcp.OT_ESTADO"=>array('0','2')):array('SistemaPlanificadorCcp.OT_ESTADO'=>sprintf("%s",$dt['bsc']['carsestado_id']-1));
//		$estadoCars = empty($dt['bsc']['carsestado_id'])?"0,2":$dt['bsc']['carsestado_id']-1;
//		if($estadoCars==2) $cndCarsStd = array('301'=>"(SistemaPlanificadorCcp.OT_ESTADO = '$estadoCars')");
//		else $cndCarsStd = array('301'=>"(SistemaPlanificadorCcp.OT_ESTADO IN ($estadoCars) OR SistemaPlanificadorCcp.OT_ESTADO is null)");
//				
//		//$cndB = array("Agedetallecita.estado"=>'AC');
//		return $cndF_2 + $cndAsesor + $cndCarsStd + $cndNroCars + $cndPlaca + $cndStdTabHist + $cndPreEstado;
//	}
}
	