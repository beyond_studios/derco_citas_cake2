<?php
App::uses('AppModel', 'Model');
class AgeclientesVehiculo extends AppModel
{
	public $name = 'AgeclientesVehiculo';
   
	public $validate = array(
		'description' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								)
					)
    );
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	
	public $belongsTo = array(
		'Cliente' => array(
			'className' => 'Cliente',
			'foreignKey' => 'cliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Modelo' => array(
			'className' => 'Modelo',
			'foreignKey' => 'modelo_id',
			'fields' => '',
			'order' => ''
		)
	);
	/**RECUPERA LAS CONDICIONES PARA EL GRID DE CLIENTE
	   AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-03-21
	 * @param object $params
	 * @param object $filters
	 * @param object $dtLog
	 * @return 
	 */
	function conditionsBuscadorGrid($params, $filters, $dtLog){
		$condicion ="1=1";
		if($params['_search'] == 'true'){
			
			foreach($filters['rules'] as $rule){
				list($model,$field) = explode(".", $rule['field']);
				$condicion .= " ".$filters['groupOp']." $model.$field LIKE ('%".$rule['data']."%')";
			}				
		}
		$condicion .= " AND AgeclientesVehiculo.estado in('AC')";
		return $condicion;
	}
	
	/**AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-03-189
	 * RECUPERA LA LISTA DE VEHICULOS DE UN CLIENTE Y NOS ENTREGA SI ESTA O NO CON ALGUNA CITA DE TALLER PENDIENTE
	 * @param object $clientId [optional]
	 * @return 
	 */
	public function getVehileClient($clientId = 0, $cndStr = "1=1"){
		if(empty($clientId)) return array();
		
		$vehicles = "select AgeclientesVehiculo.id, 
				AgeclientesVehiculo.codigo_vehiculo, 
				AgeclientesVehiculo.marca, 
				Modelo.description, 
				AgeclientesVehiculo.placa
				,CONCAT(AgeclientesVehiculo.marca, ' Modelo:', CASE WHEN Modelo.description is null THEN 'NONE' ELSE Modelo.description END, ' Placa:', AgeclientesVehiculo.placa) as label 
			    ,SUM(CASE WHEN Agedetallecita.id IS NOT NULL THEN 
			    	(CASE WHEN Agedetallecita.fechadecita > now() THEN 1 ELSE 0 END)
			    	ELSE 0 END) AS nro_citas
			FROM ageclientes_vehiculos AgeclientesVehiculo 
			LEFT JOIN modelos Modelo ON (Modelo.id = AgeclientesVehiculo.modelo_id)
			LEFT JOIN agedetallecitas Agedetallecita ON (Agedetallecita.placa = AgeclientesVehiculo.placa
                AND Agedetallecita.cliente_id = AgeclientesVehiculo.cliente_id)
			WHERE AgeclientesVehiculo.cliente_id =  '$clientId' AND $cndStr
			group by AgeclientesVehiculo.id";
//		debug($vehicles);
		$vehicles = $this->query($vehicles);
		return $vehicles;
	}
}