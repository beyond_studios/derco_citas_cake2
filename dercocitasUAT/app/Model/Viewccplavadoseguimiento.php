<?php
class Viewccplavadoseguimiento extends AppModel {
	public $name = 'Viewccplavadoseguimiento';						
	public $virtualFields=array(
		'ot_numero' => 'OT',          				//Numero de OT generad
		'presupuesto_numero' => 'PRESUPUESTO',		//Numero de presupuesto
		'ot_fecha_creacion' => 'FECHA',         		//Fecha que se gener? el la OT  *
		'ot_asesor' => 'USUARIO_OT',  				//Usuario que genero la OT(Asesor de Servicio del CCP)
		'ot_fecha_entrega' => 'FECHA_PROM',   		//Fecha Prometida
		'ot_cliente' => 'CLIENTE',  					//AQUI SERA EL CLIENTE
		'chasis_vin' => 'NRO_SERIE', 					//Numero de chasis
		'ot_placa' => 'PLACA',							//Nuemero de Placa
		'pre_fecha_aprobacion' => 'FECHA_APROB_ASEG',	//fecha aprobacion de seguro * cia ceguros
		'ot_estado' => 'OT_ESTADO',						//Estado de la ot
		'ot_pre_estado' => 'OT_PRE_ESTADO',				//Estado de la ot con respecto al presupuesto
		'ot_marca' => 'MARCA',         				//Numero de OT generad
		'ot_modelo' => 'MODELO',         				//Numero de OT generad
		'ot_color' => 'COLOR',					//Color								
		'ot_version' => 'VERSION',         				//Numero de OT generad
		'ot_descripcion' => 'DESCRIPCION',         				//Numero de OT generad
		'ot_nombre_asesor' => 'NOMBRE_ASESOR',         				//Numero de OT generad
		'ot_tipo_ot' => 'TIPO_OT',         				//Numero de OT generad
		'ot_hora_recibida' => 'HORA_RECIBIDA',        				//Numero de OT generad
		'ot_hora_prometida' => 'HORA_PROMETIDA',         				//Numero de OT generad
		'ot_numero_cono' => 'NRO_CONO'        				//Numero de OT generad
	);
	
	public function getConditionsBsc($dtBsc, $dtLg, $conSeguimiento=0){
		//STATIC VALUES
		$stdCars=array('0000','0060','0070','0090','0095','0103','0999');
		
		//USE MODEL
   		app::import('Model', 'Ccplavadoseguimiento'); 		$this->Ccplavadoseguimiento = new Ccplavadoseguimiento;
		
		//VALORES NECESARIOS
		$secproject_carscod = $dtLg['carscod'];
		
		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$ot_solicitudes = '';

		//RECUPERAMOS las ots que se encuentran en ccpestado
		$ot_ccphistorial='';
		if(!empty($dtBsc['estadootccp_id']) && isset($dtBsc['estadootccp_id'])){
			$ot_ccphistorial = "AND ot.segestado_id = '".$dtBsc['estadootccp_id']."'";
		}
		
		//CONDICIONES DE BUSQUEDA POR FECHA
		$fechaOt = "";	
		if(!empty($dtBsc['fechaIni']) && !empty($dtBsc['fechaFin'])) {
			$fechaInicialOt = date("Y-m-d 00:00:00", strtotime($dtBsc['fechaIni']));
			$fechaFinalOt = date("Y-m-d 23:59:59", strtotime($dtBsc['fechaFin']));
			$fechaOt = " AND ot.fecha_lavador BETWEEN '" . $fechaInicialOt ."' AND '" . $fechaFinalOt ."' ";
		}
		elseif(!empty($dtBsc['fechaIni'])){
			$fechaInicialOt = date("Y-m-d 00:00:00", strtotime($dtBsc['fechaIni']));
			$fechaFinalOt = date("Y-m-d  23:59:59");
			$fechaOt = " AND ot.fecha_lavador  BETWEEN '" . $fechaInicialOt ."' AND '" . $fechaFinalOt ."' ";				
		}
		elseif(!empty($dtBsc['fechaFin'])){
			$fechaInicialOt = date("2009-01-01 00:00:00");
			$fechaFinalOt = date("Y-m-d  23:59:59", strtotime($dtBsc['fechaFin']));
			$fechaOt = " AND ot.fecha_lavador BETWEEN '" . $fechaInicialOt ."' AND '" . $fechaFinalOt ."' ";				
		}
		
		// SI ES NECESARIO QUE TENGA UN SEGUIMIENTO
		$conSeguimiento = $conSeguimiento?"":"";
		
		//FILTROS DE CARS
		$condicion_cars = sprintf("ot.COMPANIA in ('PU03') AND ot.TIPO_OT in ('PEF','PEG','PEH','PEI') AND ot.OT_ESTADO IN ('%s') AND ot.TALLER in ('%s', 'P057') AND ot.CLASE_PEDIDO in ('2000','4000')",implode("','", $stdCars),$secproject_carscod);
		
		if(!empty($dtBsc['placa']) && isset($dtBsc['placa'])){
			$placa_= " AND ot.PLACA like '%".$dtBsc['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_.$ot_ccphistorial;

		if(!empty($dtBsc['chasis'])) $condicion_cars .= " AND ot.NRO_SERIE like '%".$dtBsc['chasis']."%'";
		if(!empty($dtBsc['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$dtBsc['nrocars'].'%\'';
		
		$condicion_cars = str_replace('ot.', 'Viewccplavadoseguimiento.', $condicion_cars);
		return $condicion_cars;
	}
	/**
	 * 
	 * @param object $data
	 * @param object $params
	 * @param object $login
	 * @param object $total [optional]
	 * @return 
	 */
	public function getTalotCarsLavador($presupuestos){
		app::import('Model', 'Talot');		$this->Talot = new Talot();
		app::import('Model', 'Ccptabhistorial');		$this->Ccptabhistorial= new Ccptabhistorial;
		app::import('Model', 'Ccpsolicitudservicio');		$this->Ccpsolicitudservicio= new Ccpsolicitudservicio;
		app::import('Model', 'Ccplavadoseguimiento');		$this->Ccplavadoseguimiento= new Ccplavadoseguimiento;
		
		foreach($presupuestos as $key => $presupuesto){
			//aventura
			$presupuestos[$key]['Talot']['seg_id'] = trim($presupuesto['Viewccplavadoseguimiento']['seg_id']);
			$presupuestos[$key]['Talot']['nro_seguimiento'] = trim($presupuesto['Viewccplavadoseguimiento']['nro_seguimiento']);
			$presupuestos[$key]['Talot']['segestado_id'] = trim($presupuesto['Viewccplavadoseguimiento']['segestado_id']);
			$presupuestos[$key]['Talot']['segestado_descripcion'] = trim($presupuesto['Viewccplavadoseguimiento']['segestado_descripcion']);
			
			$presupuestos[$key]['Talot']['secperson_lavador_id'] = trim($presupuesto['Viewccplavadoseguimiento']['secperson_lavador_id']);
			$presupuestos[$key]['Talot']['secperson_coordinador_id'] = trim($presupuesto['Viewccplavadoseguimiento']['secperson_coordinador_id']);
			$presupuestos[$key]['Talot']['secperson_jefetaller_id'] = trim($presupuesto['Viewccplavadoseguimiento']['secperson_jefetaller_id']);
			
			$presupuestos[$key]['Talot']['fecha_lavador'] = trim($presupuesto['Viewccplavadoseguimiento']['fecha_lavador']);
			$presupuestos[$key]['Talot']['segmotivo_descripcion'] = trim($presupuesto['Viewccplavadoseguimiento']['segmotivo_descripcion']);
			$presupuestos[$key]['Talot']['seg_comentario_lavado'] = trim($presupuesto['Viewccplavadoseguimiento']['seg_comentario_lavado']);
			
			//AVENTURA: CALCULADOS
			$presupuestos[$key]['Talot']['nro_max_segimiento'] = $this->Ccplavadoseguimiento->getNroSeguimiento(trim($presupuesto['Viewccplavadoseguimiento']['ot_numero']));
			$presupuestos[$key]['Talot']['nro_coordinador_aprobado'] = $this->Ccplavadoseguimiento->getLavadoCoordinadorAprobado(trim($presupuesto['Viewccplavadoseguimiento']['ot_numero']));
			
			
			$presupuestos[$key]['Talot']['ot_numero'] = trim($presupuesto['Viewccplavadoseguimiento']['ot_numero']);
			$presupuestos[$key]['Talot']['ot_pre_estado'] = $presupuesto['Viewccplavadoseguimiento']['ot_pre_estado'];
			$presupuestos[$key]['Talot']['presupuesto_numero'] = trim($presupuesto['Viewccplavadoseguimiento']['presupuesto_numero']);
			$presupuestos[$key]['Talot']['ot_fecha_creacion'] = $this->fecha_Mysql_Php($presupuesto['Viewccplavadoseguimiento']['ot_fecha_creacion']);
			$presupuestos[$key]['Talot']['ot_asesor'] = trim($presupuesto['Viewccplavadoseguimiento']['ot_asesor']);
			$presupuestos[$key]['Talot']['chasis_vin'] = trim($presupuesto['Viewccplavadoseguimiento']['chasis_vin']);
			$presupuestos[$key]['Talot']['ot_cliente'] = trim($presupuesto['Viewccplavadoseguimiento']['ot_cliente']);
			$presupuestos[$key]['Talot']['ot_placa'] = trim($presupuesto['Viewccplavadoseguimiento']['ot_placa']);
			$presupuestos[$key]['Talot']['ot_fecha_entrega'] = $this->fecha_Mysql_Php($presupuesto['Viewccplavadoseguimiento']['ot_fecha_entrega']);
			$presupuestos[$key]['Talot']['pre_fecha_aprobacion'] = $this->fecha_Mysql_Php($presupuesto['Viewccplavadoseguimiento']['pre_fecha_aprobacion']);
			
			//prueba con tooltip desde cars
			$presupuestos[$key]['Talot']['ot_estado'] = $presupuesto['Viewccplavadoseguimiento']['ot_estado'];
			$presupuestos[$key]['Talot']['ot_marca'] = $presupuesto['Viewccplavadoseguimiento']['ot_marca'];
			$presupuestos[$key]['Talot']['ot_modelo'] = $presupuesto['Viewccplavadoseguimiento']['ot_modelo'];
			$presupuestos[$key]['Talot']['ot_color'] = trim($presupuesto['Viewccplavadoseguimiento']['ot_color']);
			$presupuestos[$key]['Talot']['ot_version'] = $presupuesto['Viewccplavadoseguimiento']['ot_version'];
			$presupuestos[$key]['Talot']['ot_descripcion'] = $presupuesto['Viewccplavadoseguimiento']['ot_descripcion'];
			$presupuestos[$key]['Talot']['ot_nombre_asesor'] = $presupuesto['Viewccplavadoseguimiento']['ot_nombre_asesor'];
			$presupuestos[$key]['Talot']['ot_tipo_ot'] = $presupuesto['Viewccplavadoseguimiento']['ot_tipo_ot'];
			$presupuestos[$key]['Talot']['ot_hora_recibida'] = $presupuesto['Viewccplavadoseguimiento']['ot_hora_recibida'];
			$presupuestos[$key]['Talot']['ot_hora_prometida'] = $presupuesto['Viewccplavadoseguimiento']['ot_hora_prometida'];
			$presupuestos[$key]['Talot']['ot_numero_cono'] = $presupuesto['Viewccplavadoseguimiento']['ot_numero_cono'];
	
			$presupuestos[$key]['tooltip']['Talot'] =$presupuestos[$key]['Talot'];
			$tooltip = $this->Talot->findByNrocars($presupuesto['Viewccplavadoseguimiento']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$presupuestos[$key]['tooltip']['Talot']['acta']=true;
			else $presupuestos[$key]['tooltip']['acta'] = false;
	
			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistorial->getSolicitudServicioAndCcpEstado(trim($presupuesto['Viewccplavadoseguimiento']['ot_numero']));
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->Ccpsolicitudservicio->fecha_Mysql_Php($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$presupuestos[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$presupuestos[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$presupuestos[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$presupuestos[$key]['Ccpsolicitudservicio']=array();
					$presupuestos[$key]['Ccptiposervicio']=array();
					$presupuestos[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			
			unset($presupuestos[$key]['Viewccplavadoseguimiento']);
		} 
	
		return $presupuestos;
	}
}