<?php
class Ccptablero extends AppModel
{
	public $name = 'Ccptablero'; 
	public $displayField = 'descripcion'; 
	public $validate = array(
		/*'description' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								),
						'isUnique' =>array(
								'rule'=>'isUnique',
								'last' => true
								),
						'maxLength' =>array(
            					'rule'    => array('maxLength', '60'),
								'last' => true
								)  
					)*/
    );

	//The Associations below have been created with all possible keys, those that are not needed can be removed		
	public $hasMany = array(
		'Ccptabhistorial' => array(
			'className' => 'Ccptabhistorial',
			'foreignKey' => 'ccpsolicitudservicio_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	
	public $belongsTo = array(
		'Talot' => array(
			'className' => 'Talot',
			'foreignKey' => 'talot_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
		
}
?>