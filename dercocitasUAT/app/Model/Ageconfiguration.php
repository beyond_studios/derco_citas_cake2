<?php
App::uses('AppModel', 'Model');
/**
 * Ageconfiguration Model
 *
 * @property Secorganization $Secorganization
 * @property Secrole $Secrole
 */
class Ageconfiguration extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'secorganization_id' => array('rule' => array('numeric')),
		'secrole_id' => array('rule' => array('numeric')),
		'status' => array('rule' => array('notempty')),
		'horacortecitas' => array(
			//'time' => array(
				//'rule' => array('time'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			//),
		),
		
	'citasadicionales'=> array(								       							
						'numeric' => array(
						        'rule' => 'numeric',	
								'allowEmpty' => true, 			        
								'last' => true
				   				),  
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Secorganization' => array(
			'className' => 'Secorganization',
			'foreignKey' => 'secorganization_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Secrole' => array(
			'className' => 'Secrole',
			'foreignKey' => 'secrole_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	public function ageconfigurationRegistroAnterior($data)
	{
		if (!empty($data)&&isset($data)) {
			$idageconfiguration = $this->find('list',array('fields'=>array('id','id'),'conditions'=>array('status'=>'AC')));
			$this->deleteAll($idageconfiguration);
			return true;
		} else {
			return false;
		}
						
	}
}
