<?php
class Agedetallecitaslost extends AppModel
{
	public $name='Agedetallecitaslost';
	public $status = array();
	public $statusSap = array();
	public $motivonocitas = array('1'=>'Motivo 1', '2'=>'Motivo_2');

/*	
	public $validate = array(
		// 'modelo' => array('notEmpty' =>array('rule'=>'notEmpty','last' => true)),
		'agemotivoservicio_id' => array('notEmpty' =>array('rule'=>'notEmpty','last' => true)),
		'secproject_id' => array('notEmpty' =>array('rule'=>'notEmpty','last' => true)),
		'agetipomantenimiento_id' => array('notEmpty' =>array('rule'=>'notEmpty','last' => true))
    );
*/
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		'Agetiposervicio' => array('className' => 'Agetiposervicio','foreignKey' => 'agetiposervicio_id'),
		'Cliente' => array('className' => 'Cliente','foreignKey' => 'cliente_id'),
		'Agemotivoservicio' => array('className' => 'Agemotivoservicio','foreignKey' => 'agemotivoservicio_id'),
		'Secproject' => array('className' => 'Secproject','foreignKey' => 'secproject_id'),
		'Agetipomantenimiento' => array('className' => 'Agetipomantenimiento','foreignKey' => 'agetipomantenimiento_id'),
		'Createdsecperson' => array('className' => 'Secperson','foreignKey' => 'createdsecperson_id', 'fields'=>array('appaterno', 'apmaterno', 'firstname')),
		'Agedetallecitaslostsmotive' => array('className' => 'Agedetallecitaslostsmotive','foreignKey' => 'agedetallecitaslostsmotive_id'),
		
	);
		
	/**ARMA LOA CONDICIONES DEL BUSCADOR
	 * @AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * @FECHA: 2012-05-18
	 * @param object $dt
	 * @param object $dtLog
	 * @return array() formatoCakePhp
	 */
	function getConditionsBuscador($dt, $dtLog){
		$count = 1;
		$dt_tmp = array();
		$dt_tmpIds = array();
		
		foreach($dt['bsc'] as $key => $value){
			if(!empty($value)){
				$pos = strpos($key, '_id');
				if($pos !== false){
					$dt_tmpIds[sprintf("%s.%s", 'Agedetallecitaslost', $key)] = trim($value);	
				}else{
					$dt_tmp = array_merge($dt_tmp, array("$count"=>sprintf("%s.%s", 'Agedetallecitaslost', $key).' LIKE \'%'.trim($value).'%\''));
					$count++;
				}	
			}  
		}
		$conditions = $dt_tmp + $dt_tmpIds;
		return $conditions;	
	}
	
	/**AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * esta funcion trabaja a la par con la funcion agregarCita ya que es utilizada para reprogramar una cita:
	 * acciones realizadas: 
	 * genera una nueva cita agregarCita { realizado antes de llamar a esta funcion (setReprogramar)}
	 * cambiar estado del detalle de la cita
	 * actualizar el campo available de citas por dia.
	 * @param object $detallecitaId
	 * @return 
	 */
	function setReprogramar($detallecitaId, $dt, $dtLog){
		$detallecita_db = $this->find('first',array(
			'fields'=>array(
				'Agedetallecitaslost.id','Agedetallecitaslost.estado',
				'Agecitacalendariodia.id','Agecitacalendariodia.available'
			),
			'conditions'=>array('Agedetallecitaslost.id'=>$detallecitaId),
			'recursive'=>0
		));
		
		if(!in_array($detallecita_db['Agedetallecitaslost']['estado'],array('AC'))) return array(false, 'ESTADO NO PERMITIDO PARA REPROGRAMACION');
		
		$detallecita_db['Agedetallecitaslost']['estado'] = 'RE';  // estado REPROGRAMADO
		$detallecita_db['Agedetallecitaslost']['reschedulesecperson_id'] = $dtLog['Secperson']['id'];
		$detallecita_db['Agedetallecitaslost']['rescheduledate'] = $this->fechaHoraActual();
		$detallecita_db['Agedetallecitaslost']['reschedulecomment'] = $dt['Agedetallecitaslost']['reschedulecomment'];  
		
		if(!$this->save($detallecita_db)) return array(false, 'DETALLE DE CITA, ESTADO NO ACTUALIZADO');
		
		$detallecita_db['Agecitacalendariodia']['available']++; 		
		if(!$this->Agecitacalendariodia->save($detallecita_db)) return array(false, 'DETALLE DE CITA, ESTADO NO ACTUALIZADO');
		
		return array(true, 'SE REPROGRAMO LA CITA');
	}
	
	/**AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * @param object $detallecitaId
	 * @return 
	 */
	function setEliminar($dt, $dtLog){
		app::import('Model','Agecitacalendariodia'); 		$this->Agecitacalendariodia = new Agecitacalendariodia();
		
		$detallecita_db = $this->find('first',array(
			'fields'=>array(
				'Agedetallecitaslost.id','Agedetallecitaslost.estado',
				'Agecitacalendariodia.id','Agecitacalendariodia.available'
			),
			'conditions'=>array('Agedetallecitaslost.id'=>$dt['Agedetallecitaslost']['id']),
			'recursive'=>0
		));
		
		if(!in_array($detallecita_db['Agedetallecitaslost']['estado'],array('AC'))) return array(false, 'ESTADO NO PERMITIDO PARA ELIMINAR');
		
		
		$detallecita_db['Agedetallecitaslost']['estado'] = 'EL';  // estado REPROGRAMADO
		$detallecita_db['Agedetallecitaslost']['deletesecperson_id'] = $dtLog['Secperson']['id'];
		$detallecita_db['Agedetallecitaslost']['deletedate'] = $this->fechaHoraActual();
		$detallecita_db['Agedetallecitaslost']['deletecomment'] = $dt['Agedetallecitaslost']['deletecomment'];  
		
		if(!$this->save($detallecita_db)) return array(false, 'DETALLE DE CITA, ESTADO NO ACTUALIZADO');
		
		$detallecita_db['Agecitacalendariodia']['available']++; 
		if(!$this->Agecitacalendariodia->save($detallecita_db)) return array(false, 'DETALLE DE CITA, ESTADO NO ACTUALIZADO');
		
		return array(true, 'SE ELIMINO LA CITA PROGRAMADA');
	}
	
	/** 
	 * Permite solicitar una cita para el taller segun su numero de serie de cars (taller).
	 * @return 
	 * @param $dertallervehiculo_id Object usado para mostrar los datos del vehiculo del cliente.
	 * ANTES citaTaller en controlador derwebsolicitudes
	 */
	function agregarCita($dt, $dtLog){
		//modelos a importar
		app::import('Model', 'Agecitacalendariodia');	$this->Agecitacalendariodia = new Agecitacalendariodia();
		app::import('Model', 'Agetipomantenimiento');	$this->Agetipomantenimiento = new Agetipomantenimiento();
		app::import('Model', 'Agecitacalendariodia');	$this->Agecitacalendariodia = new Agecitacalendariodia();
		
		//agregando la validacion de una misma cita en el dia
		$citas = array();
		
        //recuperamos la orden estandar
		$ordenEstandar='';
		if(!empty($dt['Agetipomantenimiento']['id']) && isset($dt['Agetipomantenimiento']['id'])){
			$ordenEstandar=$this->Agetipomantenimiento->findById($dt['Agetipomantenimiento']['id']);
		}
		
		// registramos la cita de taller
		$dt['Agedetallecitaslost']['tipoMantenimiento'] = empty($ordenEstandar)?"":$ordenEstandar['Agetipomantenimiento']['description'];	
		$dt['Agedetallecitaslost']['createdsecperson_id'] = $dtLog['Secperson']['id'];
		$fechaActual = $this->fechaHoraActual();
		$dt['Agedetallecitaslost']['fechaRegistro'] = $fechaActual;
		$dt['Agedetallecitaslost']['horaRegistro'] = $fechaActual;
		
		// campos necesarios para la edicion
		$dt['Agedetallecitaslost']['agemotivoservicio_id'] = $dt['Agemotivoservicio']['id'];
		$dt['Agedetallecitaslost']['secproject_id'] = $dt['Secproject']['id'];
		$dt['Agedetallecitaslost']['agetipomantenimiento_id'] = $dt['Agetipomantenimiento']['id'];
		
		$dt['Agedetallecitaslost'] = array_merge($dt['Agedetallecitaslost'], $dt['Cliente']);
		
		foreach($dt['Agedetallecitaslost'] as $key=>$value){
			if(empty($value)) unset($dt['Agedetallecitaslost'][$key]);
		}

		if(!$this->save($dt['Agedetallecitaslost'])){
			$this->validates();
			$errors = $this->validationErrors;
			return array(false, __('WEBSOLICITUD_MENSAJE_CITA_TALLER_REGISTRO_ERROR***', true));
		}
			
		
		$agedetallecitaslost_id = $this->getLastInsertId();
		
		// se envia email confirmando su cita
//		if(!$this->enviarEmailConfirmacionCitaTaller(
//			$dt['Agedetallecitaslost']['cliente_id'], 
//			$citacalendariodia_db['Agecitacalendario']['secproject_id'], 
//			$agedetallecitaslost_id)) 
//			return array(false, __('CLIENTE_ERROR_EN_EL_ENVIO_DE_CONFIRMACION_CITA_TALLER_POR_EMAIL', true));
			
		return array(true, __('TALLER_CITA_REGISTRADA', true), 'id'=>$agedetallecitaslost_id);
	}
	
	function getAsesorDetallecita($projecId,$agemotivoservicio){
		$asesor = $this->query(
			"select  Asesor.id
			from (
			    select Asesorservicio.id as id, 
			        sum(CASE WHEN Agedetallecitaslost.id IS NULL THEN 0 ELSE 1 END) as nro_citas
			    from asesorservicios Asesorservicio
			    LEFT JOIN agedetallecitaslostslosts Agedetallecitaslost ON (Agedetallecitaslost.asesor_id = Asesorservicio.id AND Agedetallecitaslost.estado = 'AC')
			    WHERE Asesorservicio.status = 'AC' AND Asesorservicio.secproject_id = '$projecId'
			    group by Agedetallecitaslost.asesor_id
			) Asesor
			ORDER BY Asesor.nro_citas ASC
			LIMIT 1"
		);
		if(empty($asesor)) return 0;
		return $asesor[0]['Asesor']['id'];
		
		/*
		$asesor = $this->query("select Asesorservicio.id,Asesorservicio.agemotivoservicio_id FROM asesorservicios Asesorservicio 
							WHERE Asesorservicio.status = 'AC' AND Asesorservicio.secproject_id = $projecId 
							AND (Asesorservicio.agemotivoservicio_id = $agemotivoservicio or Asesorservicio.agemotivoservicio_id is null)
							ORDER BY Asesorservicio.id ASC");
		$asesorid = 0;
		$asesoreswithmotivoservicio = array();
		$asesoreswithoutmotivoservicio = array();
		foreach($asesor as $id => $item){
			if(!empty($item['Asesorservicio']['agemotivoservicio_id'])){
				$asesoreswithmotivoservicio[$id] = $item;
			}else{
				$asesoreswithoutmotivoservicio[$id] = $item;
			}
		}
		//elegimos un asesor aleatoriamente
		if(!empty($asesoreswithmotivoservicio) && isset($asesoreswithmotivoservicio)){
			$max = sizeof($asesoreswithmotivoservicio);
			$indice = rand(0,$max);
			$asesorid = $asesoreswithmotivoservicio[$indice]['Asesorservicio']['id'];
		}elseif(!empty($asesoreswithoutmotivoservicio) && isset($asesoreswithoutmotivoservicio)){
			$max = sizeof($asesoreswithoutmotivoservicio);
			$indice = rand(0,$max);
			$asesorid = $asesoreswithoutmotivoservicio[$indice]['Asesorservicio']['id'];
		}
		return $asesorid;*/
	}
	
	function updateOTSAPTRAMAXML($id,$otsap=null,$tramaxml,$mensaje,$estadoenvioSap){
		if(empty($id)) return false;
		$agedetalle['Agedetallecitaslost']['id'] = $id;
		if(!empty($otsap) && isset($otsap)) $agedetalle['Agedetallecitaslost']['otsap'] = $otsap;
		if(!empty($tramaxml) && isset($tramaxml)) $agedetalle['Agedetallecitaslost']['tramaxml'] = $tramaxml;
		if(!empty($mensaje) && isset($mensaje)) $agedetalle['Agedetallecitaslost']['mensaje'] = $mensaje;
		$agedetalle['Agedetallecitaslost']['estadoenviosap'] = $estadoenvioSap;
		if($this->save($agedetalle)) return true;
		else return false;
	}
	
	/**AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-03-189
	 * RECUPERA LA LISTA DE VEHICULOS DE UN CLIENTE Y NOS ENTREGA SI ESTA O NO CON ALGUNA CITA DE TALLER PENDIENTE
	 * @param object $clientId [optional]
	 * @return 
	 */
	public function getCitaClient($cliVehId, $afterNow = true){
		if(empty($cliVehId)) return array();
		$afterNow = ($afterNow)?"Agedetallecitaslost.fechadecita > now()":"1=1";
		
		$vehicles = $this->query(
			"select AgeclientesVehiculo.id, AgeclientesVehiculo.codigo_vehiculo, AgeclientesVehiculo.marca, AgeclientesVehiculo.modelo, AgeclientesVehiculo.placa
				  ,CONCAT(AgeclientesVehiculo.marca, ' Modelo:', AgeclientesVehiculo.modelo, ' Placa:', AgeclientesVehiculo.placa) as label 
		          ,Cliente.nombres ,Agecitacalendariodia.initDateTime, Agecitacalendariodia.endDateTime 
			FROM ageclientes_vehiculos AgeclientesVehiculo 
			JOIN clientes Cliente ON Cliente.id = AgeclientesVehiculo.cliente_id
			JOIN agedetallecitaslostslosts Agedetallecitaslost ON (Agedetallecitaslost.placa = AgeclientesVehiculo.placa
                AND Agedetallecitaslost.cliente_id = AgeclientesVehiculo.cliente_id)
			JOIN agecitacalendariodias Agecitacalendariodia ON Agecitacalendariodia.id = Agedetallecitaslost.agecitacalendariodia_id
			WHERE Agedetallecitaslost.estado != 'EL' AND Agedetallecitaslost.estado != 'RE'  AND AgeclientesVehiculo.id =  '$cliVehId' AND $afterNow
			ORDER BY Agedetallecitaslost.id ASC"
		);
		return $vehicles;
	}
	
	/**
	 * @author: Ronald Tineo
	 * @return: retorna las citas por cliente, para la web del cliente 
	 */
	public function getDetalleCitasPorCliente($clienteId){
		$citas = $this->query(
			"SELECT Agedetallecitaslost.id, Agedetallecitaslost.otsap, Agedetallecitaslost.fechadecita, Agedetallecitaslost.placa ,Agemotivoservicio.description,
				Agedetallecitaslost.fechaRegistro, Agedetallecitaslost.estado
				FROM agedetallecitaslostslosts Agedetallecitaslost 
				INNER JOIN agemotivoservicios Agemotivoservicio ON(Agedetallecitaslost.agemotivoservicio_id = Agemotivoservicio.id)
				WHERE Agedetallecitaslost.cliente_id = $clienteId ORDER BY id DESC"
		);
		if(empty($citas)) return array();
		return $citas;
	}
	
	/**
	 * @author: Ronald Tineo
	 * @return: retorna las citas por cliente, para la web del cliente 
	 */
	public function getDetalleCitasReprogramarPorCliente($clienteId){
		$citas = $this->query(
			"SELECT Agedetallecitaslost.id, Agedetallecitaslost.otsap, Agedetallecitaslost.fechadecita, Agedetallecitaslost.placa ,Agemotivoservicio.description,
				Agedetallecitaslost.fechaRegistro, Agedetallecitaslost.estado
				FROM agedetallecitaslostslosts Agedetallecitaslost 
				INNER JOIN agemotivoservicios Agemotivoservicio ON(Agedetallecitaslost.agemotivoservicio_id = Agemotivoservicio.id)
				WHERE Agedetallecitaslost.cliente_id = $clienteId AND estado = 'AC' AND Agedetallecitaslost.fechadecita > now() ORDER BY id DESC"
		);
		if(empty($citas)) return array();
		return $citas;
	}
	
	public function getCountTipoServicios($secproject_id, $motivoservicio_id,$fechatoday){
		$sql = "SELECT count(*) as cantidad,Agetipomantenimiento.tiposervicio FROM agedetallecitaslostslosts Agedetallecitaslost 
				INNER JOIN agetipomantenimientos Agetipomantenimiento ON(Agedetallecitaslost.agetipomantenimiento_id = Agetipomantenimiento.id)
				INNER JOIN agecitacalendariodias Agecitacalendariodia on(Agedetallecitaslost.agecitacalendariodia_id = Agecitacalendariodia.id) 
				WHERE Agedetallecitaslost.secproject_id = $secproject_id and Agedetallecitaslost.agemotivoservicio_id = $motivoservicio_id and Agedetallecitaslost.estado = 'AC'
				AND date(Agecitacalendariodia.initDateTime) = '$fechatoday'
				GROUP BY Agetipomantenimiento.tiposervicio";
		$result = $this->query($sql);
		return $result;
	}
	
	public function getCantidadCitasSucursalMarca($mes,$anio){
		$sql = "SELECT Agedetallecitaslost.marca,Agedetallecitaslost.secproject_id, count(*)as total FROM agedetallecitaslostslosts Agedetallecitaslost 
			INNER JOIN agecitacalendariodias Agecitacalendariodia on(Agedetallecitaslost.agecitacalendariodia_id = Agecitacalendariodia.id)
			WHERE
			MONTH(Agecitacalendariodia.initDateTime) = $mes AND YEAR(Agecitacalendariodia.initDateTime) = $anio 
			AND Agedetallecitaslost.estado='AC' GROUP BY Agedetallecitaslost.marca,Agedetallecitaslost.secproject_id ORDER BY Agedetallecitaslost.marca ASC";
		$retu = $this->query($sql);
		return $retu;
	}
	
	public function getCantidadCitasSucursalTipoMantenimiento($mes,$anio){
		$sql = "SELECT Agedetallecitaslost.agetiposervicio_id,Agedetallecitaslost.secproject_id, count(*)as total FROM agedetallecitaslostslosts Agedetallecitaslost 
			INNER JOIN agecitacalendariodias Agecitacalendariodia ON(Agedetallecitaslost.agecitacalendariodia_id = Agecitacalendariodia.id)
			WHERE
			Agedetallecitaslost.agetiposervicio_id IS NOT NULL AND
			MONTH(Agecitacalendariodia.initDateTime) = $mes AND YEAR(Agecitacalendariodia.initDateTime) = $anio 
			AND Agedetallecitaslost.estado='AC' GROUP BY Agedetallecitaslost.agetiposervicio_id,Agedetallecitaslost.secproject_id ORDER BY Agedetallecitaslost.agetiposervicio_id ASC";
		$retu = $this->query($sql);
		return $retu;
	}
	
	public function existsCita($agecitacalendario_id,$fechaInicial,$appcastLunes,$appcastMartes,$appcastMiercoles,$appcastJueves,$appcastViernes,$appcastSabado,$appcastDomingo){
		app::import('Model', 'Appcasttime'); $this->Appcasttime = new Appcasttime;
		app::import('Model', 'Agecitacalendariodia'); $this->Agecitacalendariodia = new Agecitacalendariodia;
		app::import('Model', 'Agedetallecitaslost'); $this->Agedetallecitaslost = new Agedetallecitaslost;
		//obtener los casttimes
		$appcastTimes = $this->Appcasttime->find('all',array(
					'conditions'=>array('appcast_id' => array($appcastLunes,$appcastMartes,$appcastMiercoles,$appcastJueves,$appcastViernes,$appcastSabado,$appcastDomingo), 
										'Appcasttime.status' => 'AC'),
					'recursive'=>-1
				));
		//pr($appcastTimes);
		//obtener los agecitacalendariodia de acuerdo al agecitacalendario y la fecha
		$agecitacalendariodia = $this->Agecitacalendariodia->find('all',array('conditions'=>array('Agecitacalendariodia.initDateTime >= '=>$fechaInicial,
																									'Agecitacalendariodia.agecitacalendario_id'=>$agecitacalendario_id,
																									'Agecitacalendariodia.estado'=>'AC'),
																				'recursive'=>-1));
		$mes=date('m',strtotime($fechaInicial));
		$dia=date('d',strtotime($fechaInicial));
		$anio=date('Y',strtotime($fechaInicial));
		$unixtimeinicial = strtotime($fechaInicial.' 00:00:00');
		$unixtimefinal = mktime(23,59,59,12,31,$anio);
		$fechas = array();
		while($unixtimeinicial <= $unixtimefinal){
			$fechas[] = date('Y-m-d',$unixtimeinicial);
			$dia++;
			$unixtimeinicial = mktime(0,0,0,$mes,$dia,$anio);
		}
		//pr($fechas);
		$agecitacalendarioIds = array();
		foreach($agecitacalendariodia as $id => $item){
			foreach($appcastTimes as $id1 => $item1){
				foreach($fechas as $id2 => $item2){
					if($item['Agecitacalendariodia']['initDateTime'] == $item2.' '.$item1['Appcasttime']['inittime']){
						$agecitacalendarioIds[] = $item['Agecitacalendariodia']['id'];
					}
				}
			}
		}
		
		//consulta en agedetalle citas si con esos ids hay citas registradas
		$agedetallecitaslostslosts = $this->Agedetallecitaslost->find('all',array('conditions'=>array('Agedetallecitaslost.agecitacalendariodia_id'=>$agecitacalendarioIds,
																						'Agedetallecitaslost.estado'=>'AC'),
																	'recursive'=>-1));
		pr($agedetallecitaslostslosts);
		
	}
}