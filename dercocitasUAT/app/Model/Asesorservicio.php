<?php
App::uses('AppModel', 'Model');
/**
 * Asesorservicio Model
 *
 * @property Secproject $Secproject
 * @property Secperson $Secperson
 */
class Asesorservicio extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
 	public $validate = array(
		'secperson_id' => array('rule' => array('numeric')),
		'secproject_id' => array('rule' => array('numeric')),		
		'status' => array('rule' => array('notempty')),
		'codigo_sap' => array('rule' => array('notempty'))
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Secproject' => array(
			'className' => 'Secproject',
			'foreignKey' => 'secproject_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Secperson' => array(
			'className' => 'Secperson',
			'foreignKey' => 'secperson_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Agemotivoservicio' => array(
			'className' => 'Agemotivoservicio',
			'foreignKey' => 'agemotivoservicio_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
		public $hasMany = array(
		'Agedetallecita' => array(
			'className' => 'Agedetallecita',
			'foreignKey' => 'asesor_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);
		function unicSecproject_Secrol($data){
		if(!empty($data['Asesorservicio']['id']) && isset($data['Asesorservicio']['id'])){
			$condition = array('Asesorservicio.id !='=>$data['Asesorservicio']['id'],'Asesorservicio.status'=>'AC','Asesorservicio.secperson_id'=>$data['Asesorservicio']['secperson_id'],
								'Asesorservicio.secproject_id'=>$data['Asesorservicio']['secproject_id']);
			$asesorservicio = $this->find('first',array('conditions'=>$condition));
			if(!empty($asesorservicio) && isset($asesorservicio)) return false;
		}else{
			$condition = array('Asesorservicio.status'=>'AC','Asesorservicio.secperson_id'=>$data['Asesorservicio']['secperson_id'],
								'Asesorservicio.secproject_id'=>$data['Asesorservicio']['secproject_id']);
			$asesorservicio = $this->find('first',array('conditions'=>$condition));
			if(!empty($asesorservicio) && isset($asesorservicio)) return false;
		}
		return true;
	}
	public function ValidarCodigo($data)
	{
		if(!empty($data['Asesorservicio']['id']) && isset($data['Asesorservicio']['id'])) {
			$conditionAc = array('Asesorservicio.id !='=>$data['Asesorservicio']['id'],'Asesorservicio.status'=>'AC','Asesorservicio.codigo_sap'=>$data['Asesorservicio']['codigo_sap']);
			$codigoSapAc = $this->find('count',array('conditions'=>$conditionAc));				
			$conditionDe = array('Asesorservicio.id !='=>$data['Asesorservicio']['id'],'Asesorservicio.status'=>'DE','Asesorservicio.codigo_sap'=>$data['Asesorservicio']['codigo_sap']);
			$codigoSapDe = $this->find('count',array('conditions'=>$conditionDe));	
			if(($codigoSapAc!=0)||($codigoSapDe!=0))return FALSE;	
			else {
				return TRUE;
			}	
		} else {							
			$conditionAc = array('Asesorservicio.status'=>'AC','Asesorservicio.codigo_sap'=>$data['Asesorservicio']['codigo_sap']);
			$codigoSapAc = $this->find('count',array('conditions'=>$conditionAc));				
			$conditionDe = array('Asesorservicio.status'=>'DE','Asesorservicio.codigo_sap'=>$data['Asesorservicio']['codigo_sap']);
			$codigoSapDe = $this->find('count',array('conditions'=>$conditionDe));	
			if(($codigoSapAc!=0)||($codigoSapDe!=0))return FALSE;	
			else {
				return TRUE;
			}		
		}
								
	}
}
