<?php
App::uses('AppModel', 'Model');
 class AgegruposMarca extends AppModel
 {
    public $name = 'AgegruposMarca';

    public $validate = array(
		'agegrupo_id' => array('rule' => array('numeric')),
		'marca_id' => array('rule' => array('numeric')),
		'marca_id' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'message' => 'selecione 1 o mas marcas'
								),
						'numeric' =>array(
            					'rule'    => 'numeric',
								'message' => 'ingrese dato numerico'
								)      
					),
		'status' => array('rule' => array('notempty'))
	);

    public $belongsTo = array(
		'Agegrupo' => array(
			'className' => 'Agegrupo',
			'foreignKey' => 'agegrupo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Marca' => array(
			'className' => 'Marca',
			'foreignKey' => 'marca_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	
	function setMarcas($agegrupoId, $dt){
		// DESACTIVAMOS TODAS LAS ASIGNACIONES DE LAS MARCAS ANTERIORES
		if(!$this->updateAll(
			array('AgegruposMarca.status'=>"'DE'"),
			array('AgegruposMarca.agegrupo_id'=>$agegrupoId, 'AgegruposMarca.status'=>'AC'))
		) return array(false, __('lasMarcasAsignadasNoSeActualizaron', true));
		
		// PROCEDEMOS A GUARDAR LAS NUEVAS MARCAS ASIGNADAS A LA SUCURSAL
		foreach($dt['AgegruposMarca'] as $marca){
			$marca['AgegruposMarca']['agegrupo_id'] = $agegrupoId;
			$marca['AgegruposMarca']['status'] = 'AC';
			
			$marca_db = $this->find('first', array(
				'conditions'=>array('AgegruposMarca.marca_id'=>$marca['AgegruposMarca']['marca_id'], 'AgegruposMarca.agegrupo_id'=>$agegrupoId),
				'recursive'=>-1
			));
			
			if(empty($marca_db)) $this->create();
			else $marca['AgegruposMarca']['id'] = $marca_db['AgegruposMarca']['id'];
			
			if(!$this->save($marca)) return array(false, __("laMarcaNoSeGuardo", true));
			unset($marca_db);
		}
	
		return array(true, 'losDatosFueronGuardados');
	}
	
	
	
	

 }
?>