<?php
//App::uses('AppModel', 'Model');
class SistemaPlanificadorCcp extends AppModel {
	public $name = 'SistemaPlanificadorCcp';
	public $virtualFields=array(
		'ot_numero'=>'OT',							// Numero de OT generad
		'ot_modelo'=>'MODELO',
		'ot_marca'=>'MARCA',
		'ot_version'=>'VERSION',
		'ot_descripcion'=>'DESCRIPCION',
		'ot_nombre_asesor'=>'NOMBRE_ASESOR',
		'ot_tipo_ot'=>'TIPO_OT',
		'ot_hora_recibida'=>'HORA_RECIBIDA',
		'ot_hora_prometida'=>'HORA_PROMETIDA',
		'ot_numero_cono'=>'NRO_CONO',
		'presupuesto_numero'=>'PRESUPUESTO',		// Numero de presupuesto
		'ot_fecha_creacion'=>'FECHA',				// Fecha que se gener? el la OT  *
		'ot_asesor'=>'USUARIO_OT',					// Usuario que genero la OT(Asesor de Servicio del CCP)
		'ot_fecha_entrega'=>'FECHA_PROM',			// Fecha Prometida
		'ot_cliente'=>'CLIENTE',					// AQUI SERA EL CLIENTE
		'chasis_vin'=>'NRO_SERIE',					// Numero de chasis
		'ot_placa'=>'PLACA',				// Numero de chasis
		'color'=>'COLOR',							// Nuemero de Placa
		'pre_fecha_aprobacion'=>'FECHA_APROB_ASEG',	// fecha aprobacion de seguro * cia ceguros
		'ot_fecha_aprobacion_compania'=>'FECHA_APROB_ASEG',	// fecha aprobacion compania
		'ot_estado'=>'OT_ESTADO',					// Estado de la ot
		'ot_pre_estado'=>'OT_PRE_ESTADO',			// Estado de la ot con respecto al presupuesto
		'ot_fecha_presupuesto_listo'=>'FECHA_PRES',	// fecha de presupuesto listo
		'ot_clase_pedido'=>'CLASE_PEDIDO'
		// FECHA_PRESUP		-- Fecha que se envi� el presupuesto y fecha presupuesto listo
	);
	
	/**RECUPERA LAS CONDICIONES DE BUSQUEDA
	* AUTOR: VENTURA RUEDA, JOSE ANTONIO
	* @return 
	*/
	public function getConditionsBuscador($dt, $dtLog){
		//MODELOS UTILIZADOS
		app::import('Model','Ccptabhistorial'); 		$this->Ccptabhistorial = new Ccptabhistorial();
		app::import('Model','Ccpsolicitudservicio'); 		 $this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
		
		//CONDICIONES PARA LA SOLICITUD DE SERVICIO
		$ot_solicitudes = array();
		$cndSolicitudBsc = array('201'=>'Ccpsolicitudservicio.ot_numero is not null');
		$cndSolStd = empty($dt['bsc']['ccpsolicitudservicioestado_id'])?array():array('Ccpsolicitudservicio.ccpsolicitudservicioestado_id'=>sprintf("%s",$dt['bsc']['ccpsolicitudservicioestado_id']));
		$cndSolTpSrv = empty($dt['bsc']['ccptiposervicio_id'])?array():array('Ccpsolicitudservicio.ccptiposervicio_id'=>sprintf("%s",$dt['bsc']['ccptiposervicio_id']));
		
		
		$fechaSol = $this->_condicionFecha(
			empty($dt['bsc']['f_ini'])?'':$dt['bsc']['f_ini']
			,empty($dt['bsc']['f_fin'])?'':$dt['bsc']['f_fin']
			,'Ccpsolicitudservicio.fecha_emitido'
			,'100'
		);
		
		$cndSolicitudBsc = $cndSolicitudBsc + $fechaSol + $cndSolStd + $cndSolTpSrv;
		if(count($cndSolicitudBsc)>1){
			$ot_solicitudes = $this->Ccpsolicitudservicio->find('list', array('conditions'=>$cndSolicitudBsc, 'fields'=>array('Ccpsolicitudservicio.ot_numero','Ccpsolicitudservicio.ot_numero'))); 
			$ot_solicitudes = empty($ot_solicitudes)?array('201'=>'SistemaPlanificadorCcp.nrocars IN(\'0\')'):array('201'=>'SistemaPlanificadorCcp.nrocars IN(\''.implode('\',\'', $ot_solicitudes).'\')');
		}
			
		// CONDICIONES PARA LA OT
		$cndF_2 = $this->_condicionFecha(
			empty($dt['bsc']['f_ini_2'])?'':$dt['bsc']['f_ini_2']
			,empty($dt['bsc']['f_fin_2'])?'':$dt['bsc']['f_fin_2']
			,'SistemaPlanificadorCcp.FECHA'
			,'101'
		);
		
		//CONDITIONS SELECT
		$cndAsesor = empty($dt['bsc']['asesor_id'])?array():array('SistemaPlanificadorCcp.USUARIO_OT'=>sprintf("%s",$dt['bsc']['asesor_id']));
		$cndCarsStd = empty($dt['bsc']['carsestado_id'])?array("SistemaPlanificadorCcp.OT_ESTADO"=>array('0','2')):array('SistemaPlanificadorCcp.OT_ESTADO'=>sprintf("%s",$dt['bsc']['carsestado_id']-1));
		$cndNroCars = empty($dt['bsc']['nrocars'])?array():array('SistemaPlanificadorCcp.OT'=>sprintf("%s",$dt['bsc']['nrocars']));
		$cndPlaca = empty($dt['bsc']['placa'])?array():array('SistemaPlanificadorCcp.PLACA'=>sprintf("%s",$dt['bsc']['placa']));
		$cndPreEstado = empty($dt['bsc']['preestado_id'])?array():array('SistemaPlanificadorCcp.OT_PRE_ESTADO'=>sprintf("%s",$dt['bsc']['preestado_id']));
		
		//RECUPERAMOS las ots que se encuentran en estado
		$cndStdTabHist=array();
		if(!empty($dt['bsc']['estadootccp_id']) && isset($dt['bsc']['estadootccp_id'])){
			if(strlen($dt['bsc']['estadootccp_id'])==6){
				if(strpos('1', $dt['bsc']['estadootccp_id']) !== false){// si tiene uno en su cadena
					$cndStdTabHist = array();
				}else{
					$cndStdTabHist = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($dt['bsc']['estadootccp_id']);
					$cndStdTabHist = empty($cndStdTabHist)?array():'AND SistemaPlanificadorCcp.nrocars IN(\''.implode('\',\'', $cndStdTabHist).'\')';
				}
			}else{
				if($dt['bsc']['estadootccp_id']==1){
					$cndStdTabHist=array();
				}else{				
					$cndStdTabHist = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($dt['bsc']['estadootccp_id']);
					$cndStdTabHist = array('202'=>empty($cndStdTabHist)?'AND ot.OT IN(\'0\')':'AND ot.OT IN(\''.implode('\',\'', $cndStdTabHist).'\')');
				}
			}
		}
		
		//$cndB = array("Agedetallecita.estado"=>'AC');
		return $cndF_2 + $cndAsesor + $cndCarsStd + $cndNroCars + $cndPlaca + $cndStdTabHist + $cndPreEstado;
	}
	
	/**
	 * CREADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * @param object $data
	 * @param object $params
	 * @param object $login
	 * @param object $total [optional]
	 * @return 
	 */
	function getTalotCars($sistemaCcp){
		app::import('Model', 'Ccpsolicitudservicio'); 		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio();
		app::import('Model', 'Talot'); 		$this->Talot = new Talot();
		app::import('Model', 'Ccptabhistorial'); 		$this->Ccptabhistorial = new Ccptabhistorial();
			
		$this->Talot = new Talot();
		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
								
		foreach($sistemaCcp as $key => $talot){
			$sistemaCcp[$key]['Talot']['ot_numero'] = trim($talot['SistemaPlanificadorCcp']['ot_numero']);
			$sistemaCcp[$key]['Talot']['ot_pre_estado'] = $talot['SistemaPlanificadorCcp']['ot_pre_estado'];
			$sistemaCcp[$key]['Talot']['presupuesto_numero'] = trim($talot['SistemaPlanificadorCcp']['presupuesto_numero']);
			$sistemaCcp[$key]['Talot']['ot_fecha_creacion'] = $this->getDateFormatView($talot['SistemaPlanificadorCcp']['ot_fecha_creacion']);
			$sistemaCcp[$key]['Talot']['ot_asesor'] = trim($talot['SistemaPlanificadorCcp']['ot_asesor']);
			$sistemaCcp[$key]['Talot']['chasis_vin'] = trim($talot['SistemaPlanificadorCcp']['chasis_vin']);
			$sistemaCcp[$key]['Talot']['ot_cliente'] = trim($talot['SistemaPlanificadorCcp']['ot_cliente']);
			$sistemaCcp[$key]['Talot']['ot_placa'] = trim($talot['SistemaPlanificadorCcp']['ot_placa']);
			$sistemaCcp[$key]['Talot']['ot_fecha_entrega'] = $this->getDateFormatView($talot['SistemaPlanificadorCcp']['ot_fecha_entrega']);
			$sistemaCcp[$key]['Talot']['pre_fecha_aprobacion'] = $this->getDateFormatView($talot['SistemaPlanificadorCcp']['pre_fecha_aprobacion']);
			
//			$sistemaCcp[$key]['Talot']['DocAseFchL_update'] = $talot['SistemaPlanificadorCcp']['talot_DocAseFchL_update'];
//			$sistemaCcp[$key]['Talot']['DocAseFch_update'] = $talot['SistemaPlanificadorCcp']['talot_DocAseFch_update'];
			
			//prueba con tooltip desde cars
			$sistemaCcp[$key]['Talot']['ot_estado'] = $talot['SistemaPlanificadorCcp']['ot_estado'];
			$sistemaCcp[$key]['Talot']['ot_marca'] = $talot['SistemaPlanificadorCcp']['ot_marca'];
			$sistemaCcp[$key]['Talot']['ot_modelo'] = $talot['SistemaPlanificadorCcp']['ot_modelo'];
			$sistemaCcp[$key]['Talot']['ot_version'] = $talot['SistemaPlanificadorCcp']['ot_version'];
			$sistemaCcp[$key]['Talot']['ot_descripcion'] = $talot['SistemaPlanificadorCcp']['ot_descripcion'];
			$sistemaCcp[$key]['Talot']['ot_nombre_asesor'] = $talot['SistemaPlanificadorCcp']['ot_nombre_asesor'];
			$sistemaCcp[$key]['Talot']['ot_tipo_ot'] = $talot['SistemaPlanificadorCcp']['ot_tipo_ot'];
			$sistemaCcp[$key]['Talot']['ot_hora_recibida'] = $talot['SistemaPlanificadorCcp']['ot_hora_recibida'];
			$sistemaCcp[$key]['Talot']['ot_hora_prometida'] = $talot['SistemaPlanificadorCcp']['ot_hora_prometida'];
			$sistemaCcp[$key]['Talot']['ot_numero_cono'] = $talot['SistemaPlanificadorCcp']['ot_numero_cono'];
			
			$sistemaCcp[$key]['tooltip']['Talot'] =$sistemaCcp[$key]['Talot'];
			$tooltip = $this->Talot->findByNrocars($talot['SistemaPlanificadorCcp']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$sistemaCcp[$key]['tooltip']['Talot']['acta']=true;
			else $sistemaCcp[$key]['tooltip']['acta'] = false;

			//obtenemos el color de la ot
			$sistemaCcp[$key]['Talot']['color']=$this->Ccptabhistorial->obtenerDiferenciaDias(date('d-m-Y'),$sistemaCcp[$key]['SistemaPlanificadorCcp']['ot_fecha_creacion']);
			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistorial->getSolicitudServicioAndCcpEstado(trim($talot['SistemaPlanificadorCcp']['ot_numero']));
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->Ccptabhistorial->getDateFormatView($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$sistemaCcp[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$sistemaCcp[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$sistemaCcp[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$sistemaCcp[$key]['Ccpsolicitudservicio']=array();
					$sistemaCcp[$key]['Ccptiposervicio']=array();
					$sistemaCcp[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$sistemaCcp[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$sistemaCcp[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			unset($solicitudServicio[0]);
			unset($sistemaCcp[$key]['SistemaPlanificadorCcp']);
		} 
		
		return $sistemaCcp;
	}	

	function getTalotCarsResponsableDeposito($sistemaCcp){
		app::import('Model', 'Ccpsolicitudservicio'); 		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio();
		app::import('Model', 'Talot'); 		$this->Talot = new Talot();
		app::import('Model', 'Ccptabhistorial'); 		$this->Ccptabhistorial = new Ccptabhistorial();
			
		$this->Talot = new Talot();
		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
								
		foreach($sistemaCcp as $key => $talot){
			$sistemaCcp[$key]['Talot']['ot_numero'] = trim($talot['SistemaPlanificadorCcp']['ot_numero']);
			$sistemaCcp[$key]['Talot']['ot_pre_estado'] = $talot['SistemaPlanificadorCcp']['ot_pre_estado'];
			$sistemaCcp[$key]['Talot']['presupuesto_numero'] = trim($talot['SistemaPlanificadorCcp']['presupuesto_numero']);
			$sistemaCcp[$key]['Talot']['ot_fecha_creacion'] = $this->getDateFormatView($talot['SistemaPlanificadorCcp']['ot_fecha_creacion']);
			$sistemaCcp[$key]['Talot']['ot_asesor'] = trim($talot['SistemaPlanificadorCcp']['ot_asesor']);
			$sistemaCcp[$key]['Talot']['chasis_vin'] = trim($talot['SistemaPlanificadorCcp']['chasis_vin']);
			$sistemaCcp[$key]['Talot']['ot_cliente'] = trim($talot['SistemaPlanificadorCcp']['ot_cliente']);
			$sistemaCcp[$key]['Talot']['ot_placa'] = trim($talot['SistemaPlanificadorCcp']['ot_placa']);
			$sistemaCcp[$key]['Talot']['ot_fecha_entrega'] = $this->getDateFormatView($talot['SistemaPlanificadorCcp']['ot_fecha_entrega']);
			$sistemaCcp[$key]['Talot']['pre_fecha_aprobacion'] = $this->getDateFormatView($talot['SistemaPlanificadorCcp']['pre_fecha_aprobacion']);
			
			//prueba con tooltip desde cars
			$sistemaCcp[$key]['Talot']['ot_estado'] = $talot['SistemaPlanificadorCcp']['ot_estado'];
			$sistemaCcp[$key]['Talot']['ot_marca'] = $talot['SistemaPlanificadorCcp']['ot_marca'];
			$sistemaCcp[$key]['Talot']['ot_modelo'] = $talot['SistemaPlanificadorCcp']['ot_modelo'];
			$sistemaCcp[$key]['Talot']['color'] = $talot['SistemaPlanificadorCcp']['color'];
			$sistemaCcp[$key]['Talot']['ot_version'] = $talot['SistemaPlanificadorCcp']['ot_version'];
			$sistemaCcp[$key]['Talot']['ot_descripcion'] = $talot['SistemaPlanificadorCcp']['ot_descripcion'];
			$sistemaCcp[$key]['Talot']['ot_nombre_asesor'] = $talot['SistemaPlanificadorCcp']['ot_nombre_asesor'];
			$sistemaCcp[$key]['Talot']['ot_tipo_ot'] = $talot['SistemaPlanificadorCcp']['ot_tipo_ot'];
			$sistemaCcp[$key]['Talot']['ot_hora_recibida'] = $talot['SistemaPlanificadorCcp']['ot_hora_recibida'];
			$sistemaCcp[$key]['Talot']['ot_hora_prometida'] = $talot['SistemaPlanificadorCcp']['ot_hora_prometida'];
			$sistemaCcp[$key]['Talot']['ot_numero_cono'] = $talot['SistemaPlanificadorCcp']['ot_numero_cono'];
			
			$sistemaCcp[$key]['tooltip']['Talot'] =$sistemaCcp[$key]['Talot'];
			$tooltip = $this->Talot->findByNrocars($talot['SistemaPlanificadorCcp']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$sistemaCcp[$key]['tooltip']['Talot']['acta']=true;
			else $sistemaCcp[$key]['tooltip']['acta'] = false;
			
			//recuperamos el comentario de los estados de deposito
			$comentarioDeposito = $this->Ccptabhistorial->getComentarioDeposito(trim($talot['SistemaPlanificadorCcp']['ot_numero']));
			if(!empty($comentarioDeposito) && isset($comentarioDeposito)) $sistemaCcp[$key]['tooltip']['Talot']['comentarioDeposito']=$comentarioDeposito[0][0]['comentario'];
			else $sistemaCcp[$key]['tooltip']['Talot']['comentarioDeposito'] = null;
			
			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistorial->getSolicitudServicioAndCcpEstado(trim($talot['SistemaPlanificadorCcp']['ot_numero']));
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->Ccptabhistorial->getDateFormatView($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$sistemaCcp[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$sistemaCcp[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$sistemaCcp[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$sistemaCcp[$key]['Ccpsolicitudservicio']=array();
					$sistemaCcp[$key]['Ccptiposervicio']=array();
					$sistemaCcp[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$sistemaCcp[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$sistemaCcp[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			unset($solicitudServicio[0]);
			unset($sistemaCcp[$key]['SistemaPlanificadorCcp']);
		} 
		
		return $sistemaCcp;
	}
	
	function getOtPresupuesto($nro_cars){
		$otPresupuesto = $this->find('first',array(
			'conditions'=>array(
				'SistemaPlanificadorCcp.COMPANIA'=>array('PU03'),
				'SistemaPlanificadorCcp.OT'=>$nro_cars
			),
			'order'=>array('SistemaPlanificadorCcp.FECHA_PRES'=>'ASC')
		));
		
		if(empty($otPresupuesto)) return $otPresupuesto;
		else{
			$otPresupuesto['0']['0'] = $otPresupuesto['SistemaPlanificadorCcp'];
			$otPresupuesto['0']['0']['ot_fecha_entrega'] = $this->getDateFormatView($otPresupuesto['0']['0']['ot_fecha_entrega']);
			$otPresupuesto['0']['0']['ot_fecha_creacion'] = $this->getDateFormatView($otPresupuesto['0']['0']['ot_fecha_creacion']);
			$otPresupuesto['0']['0']['FECHA_PRESUP'] = $this->getDateFormatView($otPresupuesto['0']['0']['FECHA_PRESUP']);
			$otPresupuesto['0']['0']['FECHA_PRESUP']= ($otPresupuesto['0']['0']['FECHA_PRESUP']=='31-12-1969' || $otPresupuesto['0']['0']['FECHA_PRESUP']=='01-01-1753')?'':$otPresupuesto['0']['0']['FECHA_PRESUP'];
			$otPresupuesto['0']['0']['ot_fecha_aprobacion_compania'] = $this->getDateFormatView($otPresupuesto['0']['0']['ot_fecha_aprobacion_compania']);
			$otPresupuesto['0']['0']['ot_fecha_aprobacion_compania']= ($otPresupuesto['0']['0']['ot_fecha_aprobacion_compania']=='31-12-1969' || $otPresupuesto['0']['0']['ot_fecha_aprobacion_compania']=='01-01-1753')?'':$otPresupuesto['0']['0']['ot_fecha_aprobacion_compania'];

		} 
		
		return $otPresupuesto['0']['0'];
	}
	
	/** MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * user for controller/model
	 * 		ccpsolicitudservicios/index
	 *  
	 * FECHA: 2013-04-25
	 */
	function cndBsc_getTalotCarsNew($dt, $dtLg){
		//MODELOS A UTILIZAR
		app::import('Model','Ccpsolicitudservicio');	$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
		app::import('Model','Ccptabhistorial');		$this->Ccptabhistorial = new Ccptabhistorial;
		
		// VALORES NECESARIOS
		$secproject_carscod = $dtLg['Secproject']['carscod'];
		
 		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$condicionSolicitud = 'Ccpsolicitudservicio.ot_numero is not null';
		$fechaSol = $this->_condicionFecha(
			empty($dt['fechaIni'])?'':$dt['fechaIni']
			,empty($dt['fechaFin'])?'':$dt['fechaFin']
			,'Ccpsolicitudservicio.fecha_emitido'
			,'100'
		);
		$fechaSol = empty($fechaSol)?"":sprintf(" AND %s",$fechaSol['100']);
		
		
		if(!empty($fechaSol)) $condicionSolicitud .= $fechaSol;
		if(!empty($dt['ccptiposervicio_id'])) $condicionSolicitud .= ' AND Ccpsolicitudservicio.ccptiposervicio_id = '.$dt['ccptiposervicio_id'];
		if(!empty($dt['ccpsolicitudservicioestado_id'])) $condicionSolicitud .= ' AND Ccpsolicitudservicio.ccpsolicitudservicioestado_id = '.$dt['ccpsolicitudservicioestado_id'];
	
		//RECUPERO LOS OT DE LAS SOLICITUDES
		$ot_solicitudes = '';
		if($condicionSolicitud != 'Ccpsolicitudservicio.ot_numero is not null'){
			$ot_solicitudes = $this->Ccpsolicitudservicio->find('list', array(
				'conditions'=>array($condicionSolicitud),
				'fields'=>array('Ccpsolicitudservicio.id', 'Ccpsolicitudservicio.ot_numero')
			));   
	
			$ot_solicitudes = empty($ot_solicitudes)?'AND ot.OT IN(\'0\')':'AND ot.OT IN(\''.implode('\',\'', $ot_solicitudes).'\')';
		}
		
		//RECUPERAMOS las ots que se encuentran en estado
		$ot_ccphistorial='';
		//RECUPERAMOS las ots que se encuentran en estado
		$ot_ccphistorial='';
		if(!empty($dt['estadootccp_id']) && isset($dt['estadootccp_id'])){
			if(strlen($dt['estadootccp_id'])==17){
				if(strpos('1', $dt['estadootccp_id']) !== false){// si tiene uno en su cadena
					$ot_ccphistorial = "";
				}else{
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($dt['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'';
				}
			}else{
				if($dt['estadootccp_id']==1){
					//SI ES ESTADO PENDIENTE DEBE MOSTRAR SOLO ESTADO PENDIENTE
					$estadoCcp="2,3,4,5,6,7,8,9,10,11,12,13,14,15,21,22";//lavado y ccasesor
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($estadoCcp);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT NOT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT NOT IN(\'0\')';
				}else{				
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($dt['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT IN(\'0\')';
				}
			}
		}
		
		//FILTROS DE CARS
		$condicion_cars="";  
		if(!empty($dt['placa']) && isset($dt['placa'])){
			$placa_= " AND ot.PLACA like '%".$dt['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_;
		
		$fechaCars = $this->_condicionFecha(
			empty($dt['fechaIniOt'])?'':$dt['fechaIniOt']
			,empty($dt['fechaFinOt'])?'':$dt['fechaFinOt']
			,'ot.FECHA'
			,'100'
		);
		$fechaCars = empty($fechaCars)?" AND 1=1":sprintf(" AND %s",$fechaCars['100']);
		
		if(!empty($fechaCars)) $condicion_cars .= $fechaCars;
		
		// CONDICION POR ASESOR
		$condicion_cars .= empty($dt['asesor_id'])?" AND 1=1" //sprintf(" AND ot.USUARIO_OT = '%s'", trim($dtLg['userName']))
			:sprintf(" AND LTRIM(RTRIM(ot.USUARIO_OT)) = '%s'", trim($dt['asesor_id']));
			
		if(!empty($dt['carsestado_id'])){
			$estadoCars = $dt['carsestado_id']; 
			$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'"; 
		}else{
			$condicion_cars .= " AND ot.OT_ESTADO in ('0000','0060','0070','0090','0095')";  // solo hay dos estado IN ('0','2')
		}  
		
		if(!empty($dt['preestado_id'])) $condicion_cars .= ' AND ot.OT_PRE_ESTADO = '.$dt['preestado_id'];
		if(!empty($dt['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$dt['nrocars'].'%\'';
		
		//"ot.TALLER = '$secproject_carscod'";
		$cndBasic = "ot.COMPANIA in ('PU03') AND ot.TALLER in ('P035', 'P057') AND ot.CLASE_PEDIDO in ('2000','4000') AND ot.TIPO_OT in ('PEF','PEG','PEH','PEI')";
		
		$cndTotal = "$cndBasic $condicion_cars $ot_solicitudes $ot_ccphistorial";
		$cndTotal = str_replace("ot.", "SistemaPlanificadorCcp.", $cndTotal);
		
		return $cndTotal;
	}

	
	function cndBsc_getTalotCarsResponsableDeposito($dt, $dtLg){
		//MODELOS A UTILIZAR
		app::import('Model','Ccpsolicitudservicio');	$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
		app::import('Model','Ccptabhistorial');		$this->Ccptabhistorial = new Ccptabhistorial;
		
		// VALORES NECESARIOS
		$secproject_carscod = $dtLg['Secproject']['carscod'];
		
		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$ot_solicitudes = '';
		
		//RECUPERAMOS las ots que se encuentran en estado
		$ot_ccphistorial='';
		if(!empty($dt['estadootccp_id']) && isset($dt['estadootccp_id'])){
			if(strlen($dt['estadootccp_id'])==17){
				if(strpos('1', $dt['estadootccp_id']) !== false){// si tiene uno en su cadena
					$ot_ccphistorial = "";
				}else{
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($dt['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'';
				}
			}else{
				if($dt['estadootccp_id']==1){
					//SI ES ESTADO PENDIENTE DEBE MOSTRAR SOLO ESTADO PENDIENTE
					$estadoCcp="2,3,4,5,6,7,8,9,10,11,12,13,14,15,21,22";//lavado y ccasesor
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($estadoCcp);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT NOT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT NOT IN(\'0\')';
				}else{				
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($dt['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT IN(\'0\')';
				}
			}
		}

		//FILTROS DE CARS
		$condicion_cars="";  
		if(!empty($dt['placa']) && isset($dt['placa'])){
			$placa_= " AND ot.PLACA like '%".$dt['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_;
				
		$fechaCars = $this->_condicionFecha(
			empty($dt['fechaIniOt'])?'':$dt['fechaIniOt']
			,empty($dt['fechaFinOt'])?'':$dt['fechaFinOt']
			,'ot.FECHA'
			,'100'
		);
					
		if(!empty($dt['carsestado_id'])){
			$estadoCars = $dt['carsestado_id']; 
			$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'"; 
		}else  $condicion_cars .= " AND ot.OT_ESTADO in ('0000','0060','0070','0090','0095')";  // solo hay dos estado IN ('0','2')
		
		if(!empty($dt['preestado_id'])) $condicion_cars .= ' AND ot.OT_PRE_ESTADO = '.$dt['preestado_id'];
		if(!empty($dt['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$dt['nrocars'].'%\'';
		if(!empty($dt['chasis'])) $condicion_cars .= ' AND ot.NRO_SERIE like \'%'.$dt['chasis'].'%\'';
		
		
		//"ot.TALLER = '$secproject_carscod'";
		$cndBasic = "ot.COMPANIA in ('PU03') AND ot.TALLER in ('P035', 'P057') AND ot.CLASE_PEDIDO in ('2000','4000') AND ot.TIPO_OT in ('PEF','PEG','PEH','PEI')";
		
		$cndTotal = "$cndBasic $condicion_cars $ot_solicitudes $ot_ccphistorial";
		$cndTotal = str_replace("ot.", "SistemaPlanificadorCcp.", $cndTotal);
		
		return $cndTotal;
	}

	function cndBsc_getTalotCarsGerente($dt, $dtLg){
		//MODELOS A UTILIZAR
		app::import('Model','Ccpsolicitudservicio');	$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
		app::import('Model','Ccptabhistorial');		$this->Ccptabhistorial = new Ccptabhistorial;
		
		// VALORES NECESARIOS
		$secproject_carscod = $dtLg['Secproject']['carscod'];
		
 		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$condicionSolicitud = 'Ccpsolicitudservicio.ot_numero is not null';
		$fechaSol = $this->_condicionFecha(
			empty($dt['fechaIni'])?'':$dt['fechaIni']
			,empty($dt['fechaFin'])?'':$dt['fechaFin']
			,'Ccpsolicitudservicio.fecha_emitido'
			,'100'
		);
		$fechaSol = empty($fechaSol)?"":sprintf(" AND %s",$fechaSol['100']);
		
		
		if(!empty($fechaSol)) $condicionSolicitud .= $fechaSol;
		if(!empty($dt['ccptiposervicio_id'])) $condicionSolicitud .= ' AND Ccpsolicitudservicio.ccptiposervicio_id = '.$dt['ccptiposervicio_id'];
		if(!empty($dt['ccpsolicitudservicioestado_id'])) $condicionSolicitud .= ' AND Ccpsolicitudservicio.ccpsolicitudservicioestado_id = '.$dt['ccpsolicitudservicioestado_id'];
	
		//RECUPERO LOS OT DE LAS SOLICITUDES
		$ot_solicitudes = '';
		if($condicionSolicitud != 'Ccpsolicitudservicio.ot_numero is not null'){
			$ot_solicitudes = $this->Ccpsolicitudservicio->find('list', array(
				'conditions'=>array($condicionSolicitud),
				'fields'=>array('Ccpsolicitudservicio.id', 'Ccpsolicitudservicio.ot_numero')
			));   
	
			$ot_solicitudes = empty($ot_solicitudes)?'AND ot.OT IN(\'0\')':'AND ot.OT IN(\''.implode('\',\'', $ot_solicitudes).'\')';
		}
		
		//RECUPERAMOS las ots que se encuentran en estado
		$ot_ccphistorial='';
		if(!empty($dt['estadootccp_id']) && isset($dt['estadootccp_id'])){
			if(strlen($dt['estadootccp_id'])==17){
				if(strpos('1', $dt['estadootccp_id']) !== false){// si tiene uno en su cadena
					$ot_ccphistorial = "";
				}else{
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($dt['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'';
				}
			}else{
				if($dt['estadootccp_id']==1){
					//SI ES ESTADO PENDIENTE DEBE MOSTRAR SOLO ESTADO PENDIENTE
					$estadoCcp="2,3,4,5,6,7,8,9,10,11,12,13,14,15,21,22";//lavado y ccasesor
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($estadoCcp);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT NOT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT NOT IN(\'0\')';
				}else{				
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($dt['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT IN(\'0\')';
				}
			}
		}
		
		//FILTROS DE CARS
		$condicion_cars="";  
		if(!empty($dt['placa']) && isset($dt['placa'])){
			$placa_= " AND ot.PLACA like '%".$dt['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_;
		
		$fechaCars = $this->_condicionFecha(
			empty($dt['fechaIniOt'])?'':$dt['fechaIniOt']
			,empty($dt['fechaFinOt'])?'':$dt['fechaFinOt']
			,'ot.FECHA'
			,'100'
		);
		$fechaCars = empty($fechaCars)?" AND 1=1":sprintf(" AND %s",$fechaCars['100']);
		
		if(!empty($fechaCars)) $condicion_cars .= $fechaCars;
		
		// CONDICION POR ASESOR
		$condicion_cars .= empty($dt['asesor_id'])?" AND 1=1" //sprintf(" AND ot.USUARIO_OT = '%s'", trim($dtLg['userName']))
			:sprintf(" AND LTRIM(RTRIM(ot.USUARIO_OT)) = '%s'", trim($dt['asesor_id']));
			
		if(!empty($dt['carsestado_id'])){
			$estadoCars = $dt['carsestado_id']; 
			$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'"; 
		}else  $condicion_cars .= " AND ot.OT_ESTADO in ('0000','0060','0070','0090','0095')";  // solo hay dos estado IN ('0','2')
		
		if(!empty($dt['preestado_id'])) $condicion_cars .= ' AND ot.OT_PRE_ESTADO = '.$dt['preestado_id'];
		if(!empty($dt['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$dt['nrocars'].'%\'';
		
		//"ot.TALLER = '$secproject_carscod'";
		$cndBasic = "ot.COMPANIA in ('PU03') AND ot.TALLER in ('P035', 'P057') AND ot.CLASE_PEDIDO in ('2000','4000') AND ot.TIPO_OT in ('PEF','PEG','PEH','PEI')";
		
		$cndTotal = "$cndBasic $condicion_cars $ot_solicitudes $ot_ccphistorial";
		$cndTotal = str_replace("ot.", "SistemaPlanificadorCcp.", $cndTotal);
		
		return $cndTotal;
	}
	function cndBsc_getTalotCarsAsistente($dt, $dtLg){
		//MODELOS A UTILIZAR
		app::import('Model','Ccpsolicitudservicio');	$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
		app::import('Model','Ccptabhistorial');		$this->Ccptabhistorial = new Ccptabhistorial;
		
		// VALORES NECESARIOS
		$secproject_carscod = $dtLg['Secproject']['carscod'];
		
		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$ot_solicitudes = '';
		
		//RECUPERAMOS las ots que se encuentran en estado
		$ot_ccphistorial='';
		if(!empty($dt['estadootccp_id']) && isset($dt['estadootccp_id'])){
			if(strlen($dt['estadootccp_id'])==11){
				if(strpos('1', $dt['estadootccp_id']) !== false){// si tiene uno en su cadena
					$ot_ccphistorial = "";
				}else{
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($dt['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'';
				}
			}else{
				if($dt['estadootccp_id']==1){
					//SI ES ESTADO PENDIENTE DEBE MOSTRAR SOLO ESTADO PENDIENTE
					$estadoCcp="2,3,4,5,6,7,8,9,10,11,12,13,14,15,21,22";//lavado y ccasesor
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($estadoCcp);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT NOT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT NOT IN(\'0\')';
				}else{				
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($dt['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT IN(\'0\')';
				}
			}
		}

		//FILTROS DE CARS
		$condicion_cars="";  
		if(!empty($dt['placa']) && isset($dt['placa'])){
			$placa_= " AND ot.PLACA like '%".$dt['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_;
				
		$fechaCars = $this->_condicionFecha(
			empty($dt['fechaIniOt'])?'':$dt['fechaIniOt']
			,empty($dt['fechaFinOt'])?'':$dt['fechaFinOt']
			,'ot.FECHA'
			,'100'
		);
					
		if(!empty($dt['carsestado_id'])){
			$estadoCars = $dt['carsestado_id']; 
			$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'"; 
		}else  $condicion_cars .= " AND ot.OT_ESTADO in ('0000','0060','0070','0090','0095')";  // solo hay dos estado IN ('0','2')
		
		if(!empty($dt['preestado_id'])) $condicion_cars .= ' AND ot.OT_PRE_ESTADO = '.$dt['preestado_id'];
		if(!empty($dt['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$dt['nrocars'].'%\'';
		if(!empty($dt['chasis'])) $condicion_cars .= ' AND ot.NRO_SERIE like \'%'.$dt['chasis'].'%\'';
		
		
		//"ot.TALLER = '$secproject_carscod'";
		$cndBasic = "ot.COMPANIA in ('PU03') AND ot.TALLER in ('P035', 'P057') AND ot.CLASE_PEDIDO in ('2000','4000') AND ot.TIPO_OT in ('PEF','PEG','PEH','PEI')";
		
		$cndTotal = "$cndBasic $condicion_cars $ot_solicitudes $ot_ccphistorial";
		$cndTotal = str_replace("ot.", "SistemaPlanificadorCcp.", $cndTotal);
		
		return $cndTotal;
	}
	
	/**MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-05-17
	 * @param object $sistemaCcp
	 * @return 
	 */
	function getTalotCarsAsistente($sistemaCcp){
		app::import('Model', 'Ccpsolicitudservicio'); 		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio();
		app::import('Model', 'Talot'); 		$this->Talot = new Talot();
		app::import('Model', 'Ccptabhistorial'); 		$this->Ccptabhistorial = new Ccptabhistorial();
		app::import('Model', 'Ccptabhistorialdocumento'); 		$this->Ccptabhistorialdocumento = new Ccptabhistorialdocumento();
			
		$this->Talot = new Talot();
		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
								
		foreach($sistemaCcp as $key => $talot){
			$sistemaCcp[$key]['Talot']['ot_numero'] = trim($talot['SistemaPlanificadorCcp']['ot_numero']);
			$sistemaCcp[$key]['Talot']['ot_pre_estado'] = $talot['SistemaPlanificadorCcp']['ot_pre_estado'];
			$sistemaCcp[$key]['Talot']['presupuesto_numero'] = trim($talot['SistemaPlanificadorCcp']['presupuesto_numero']);
			$sistemaCcp[$key]['Talot']['ot_fecha_creacion'] = $this->getDateFormatView($talot['SistemaPlanificadorCcp']['ot_fecha_creacion']);
			$sistemaCcp[$key]['Talot']['ot_asesor'] = trim($talot['SistemaPlanificadorCcp']['ot_asesor']);
			$sistemaCcp[$key]['Talot']['chasis_vin'] = trim($talot['SistemaPlanificadorCcp']['chasis_vin']);
			$sistemaCcp[$key]['Talot']['ot_cliente'] = trim($talot['SistemaPlanificadorCcp']['ot_cliente']);
			$sistemaCcp[$key]['Talot']['ot_placa'] = trim($talot['SistemaPlanificadorCcp']['ot_placa']);
			$sistemaCcp[$key]['Talot']['ot_fecha_entrega'] = $this->getDateFormatView($talot['SistemaPlanificadorCcp']['ot_fecha_entrega']);
			$sistemaCcp[$key]['Talot']['pre_fecha_aprobacion'] = $this->getDateFormatView($talot['SistemaPlanificadorCcp']['pre_fecha_aprobacion']);
			
			//prueba con tooltip desde cars
			$sistemaCcp[$key]['Talot']['ot_estado'] = $talot['SistemaPlanificadorCcp']['ot_estado'];
			$sistemaCcp[$key]['Talot']['ot_marca'] = $talot['SistemaPlanificadorCcp']['ot_marca'];
			$sistemaCcp[$key]['Talot']['ot_modelo'] = $talot['SistemaPlanificadorCcp']['ot_modelo'];
			$sistemaCcp[$key]['Talot']['color'] = $talot['SistemaPlanificadorCcp']['color'];
			$sistemaCcp[$key]['Talot']['ot_version'] = $talot['SistemaPlanificadorCcp']['ot_version'];
			$sistemaCcp[$key]['Talot']['ot_descripcion'] = $talot['SistemaPlanificadorCcp']['ot_descripcion'];
			$sistemaCcp[$key]['Talot']['ot_nombre_asesor'] = $talot['SistemaPlanificadorCcp']['ot_nombre_asesor'];
			$sistemaCcp[$key]['Talot']['ot_tipo_ot'] = $talot['SistemaPlanificadorCcp']['ot_tipo_ot'];
			$sistemaCcp[$key]['Talot']['ot_hora_recibida'] = $talot['SistemaPlanificadorCcp']['ot_hora_recibida'];
			$sistemaCcp[$key]['Talot']['ot_hora_prometida'] = $talot['SistemaPlanificadorCcp']['ot_hora_prometida'];
			$sistemaCcp[$key]['Talot']['ot_numero_cono'] = $talot['SistemaPlanificadorCcp']['ot_numero_cono'];
			
			$sistemaCcp[$key]['tooltip']['Talot'] =$sistemaCcp[$key]['Talot'];
			$tooltip = $this->Talot->findByNrocars($talot['SistemaPlanificadorCcp']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$sistemaCcp[$key]['tooltip']['Talot']['acta']=true;
			else $sistemaCcp[$key]['tooltip']['acta'] = false;
			
			//recuperamos el comentario de los estados de deposito
			$comentarioDeposito = $this->Ccptabhistorial->getComentarioDeposito(trim($talot['SistemaPlanificadorCcp']['ot_numero']));
			if(!empty($comentarioDeposito) && isset($comentarioDeposito)) $sistemaCcp[$key]['tooltip']['Talot']['comentarioDeposito']=$comentarioDeposito[0][0]['comentario'];
			else $sistemaCcp[$key]['tooltip']['Talot']['comentarioDeposito'] = null;
			
			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistorial->getSolicitudServicioAndCcpEstado(trim($talot['SistemaPlanificadorCcp']['ot_numero']));
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->Ccpsolicitudservicio->getDateFormatView($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$sistemaCcp[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$sistemaCcp[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$sistemaCcp[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$sistemaCcp[$key]['Ccpsolicitudservicio']=array();
					$sistemaCcp[$key]['Ccptiposervicio']=array();
					$sistemaCcp[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$sistemaCcp[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
					$estadodocumento=$this->Ccptabhistorialdocumento->obtenerEstadoCcpPotNumeroOt(trim($talot['SistemaPlanificadorCcp']['ot_numero']));
					if(!empty($estadodocumento) && isset($estadodocumento)){
						$sistemaCcp[$key]['Ccptabestadodocumento'] = $estadodocumento[0]['Ccptabestadodocumento'];
					}
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$sistemaCcp[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			unset($solicitudServicio[0]);
			unset($sistemaCcp[$key]['SistemaPlanificadorCcp']);
		} 
		
		return $sistemaCcp;
	}
	
	function cndBsc_getTalotCarsProbador($dt, $dtLg){
		//MODELOS A UTILIZAR
		app::import('Model','Ccpsolicitudservicio');	$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
		app::import('Model','Ccptabhistorial');		$this->Ccptabhistorial = new Ccptabhistorial;
		
		// VALORES NECESARIOS
		$secproject_carscod = $dtLg['Secproject']['carscod'];
		
		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$ot_solicitudes = '';
		
		//RECUPERAMOS las ots que se encuentran en estado
		$ot_ccphistorial='';
		if(!empty($dt['estadootccp_id']) && isset($dt['estadootccp_id'])){
			if(strlen($dt['estadootccp_id'])==6){
				if(strpos('1', $dt['estadootccp_id']) !== false){// si tiene uno en su cadena
					$ot_ccphistorial = "";
				}else{
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($dt['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'';
				}
			}else{
				if($dt['estadootccp_id']==1){
					//SI ES ESTADO PENDIENTE DEBE MOSTRAR SOLO ESTADO PENDIENTE
					$estadoCcp="2,3,4,5,6,7,8,9,10,11,12,13,14,15,21,22";//lavado y ccasesor
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($estadoCcp);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT NOT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT NOT IN(\'0\')';
				}else{				
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($dt['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT IN(\'0\')';
				}
			}
		}

		//FILTROS DE CARS
		$condicion_cars="";  
		if(!empty($dt['placa']) && isset($dt['placa'])){
			$placa_= " AND ot.PLACA like '%".$dt['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_;
				
		$fechaCars = $this->_condicionFecha(
			empty($dt['fechaIniOt'])?'':$dt['fechaIniOt']
			,empty($dt['fechaFinOt'])?'':$dt['fechaFinOt']
			,'ot.FECHA'
			,'100'
		);
					
		if(!empty($dt['carsestado_id'])){
			$estadoCars = $dt['carsestado_id']; 
			$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'"; 
		}else  $condicion_cars .= " AND ot.OT_ESTADO in ('0000','0060','0070','0090','0095')";  // solo hay dos estado IN ('0','2')
		
		if(!empty($dt['preestado_id'])) $condicion_cars .= ' AND ot.OT_PRE_ESTADO = '.$dt['preestado_id'];
		if(!empty($dt['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$dt['nrocars'].'%\'';
		if(!empty($dt['chasis'])) $condicion_cars .= ' AND ot.NRO_SERIE like \'%'.$dt['chasis'].'%\'';
		
		
		//"ot.TALLER = '$secproject_carscod'";
		$cndBasic = "ot.COMPANIA in ('PU03') AND ot.TALLER in ('P035', 'P057') AND ot.CLASE_PEDIDO in ('2000','4000') AND ot.TIPO_OT in ('PEF','PEG','PEH','PEI')";
		
		$cndTotal = "$cndBasic $condicion_cars $ot_solicitudes $ot_ccphistorial";
		$cndTotal = str_replace("ot.", "SistemaPlanificadorCcp.", $cndTotal);
		
		return $cndTotal;
	}
	
	function getTalotCarsProbador($sistemaCcp){
		app::import('Model', 'Ccpsolicitudservicio'); 		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio();
		app::import('Model', 'Talot'); 		$this->Talot = new Talot();
		app::import('Model', 'Ccptabhistorial'); 		$this->Ccptabhistorial = new Ccptabhistorial();
			
		$this->Talot = new Talot();
		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
								
		foreach($sistemaCcp as $key => $talot){
			$sistemaCcp[$key]['Talot']['ot_numero'] = trim($talot['SistemaPlanificadorCcp']['ot_numero']);
			$sistemaCcp[$key]['Talot']['ot_pre_estado'] = $talot['SistemaPlanificadorCcp']['ot_pre_estado'];
			$sistemaCcp[$key]['Talot']['presupuesto_numero'] = trim($talot['SistemaPlanificadorCcp']['presupuesto_numero']);
			$sistemaCcp[$key]['Talot']['ot_fecha_creacion'] = $this->getDateFormatView($talot['SistemaPlanificadorCcp']['ot_fecha_creacion']);
			$sistemaCcp[$key]['Talot']['ot_asesor'] = trim($talot['SistemaPlanificadorCcp']['ot_asesor']);
			$sistemaCcp[$key]['Talot']['chasis_vin'] = trim($talot['SistemaPlanificadorCcp']['chasis_vin']);
			$sistemaCcp[$key]['Talot']['ot_cliente'] = trim($talot['SistemaPlanificadorCcp']['ot_cliente']);
			$sistemaCcp[$key]['Talot']['ot_placa'] = trim($talot['SistemaPlanificadorCcp']['ot_placa']);
			$sistemaCcp[$key]['Talot']['ot_fecha_entrega'] = $this->getDateFormatView($talot['SistemaPlanificadorCcp']['ot_fecha_entrega']);
			$sistemaCcp[$key]['Talot']['pre_fecha_aprobacion'] = $this->getDateFormatView($talot['SistemaPlanificadorCcp']['pre_fecha_aprobacion']);
			
			//prueba con tooltip desde cars
			$sistemaCcp[$key]['Talot']['ot_estado'] = $talot['SistemaPlanificadorCcp']['ot_estado'];
			$sistemaCcp[$key]['Talot']['ot_marca'] = $talot['SistemaPlanificadorCcp']['ot_marca'];
			$sistemaCcp[$key]['Talot']['ot_modelo'] = $talot['SistemaPlanificadorCcp']['ot_modelo'];
			$sistemaCcp[$key]['Talot']['color'] = $talot['SistemaPlanificadorCcp']['color'];
			$sistemaCcp[$key]['Talot']['ot_version'] = $talot['SistemaPlanificadorCcp']['ot_version'];
			$sistemaCcp[$key]['Talot']['ot_descripcion'] = $talot['SistemaPlanificadorCcp']['ot_descripcion'];
			$sistemaCcp[$key]['Talot']['ot_nombre_asesor'] = $talot['SistemaPlanificadorCcp']['ot_nombre_asesor'];
			$sistemaCcp[$key]['Talot']['ot_tipo_ot'] = $talot['SistemaPlanificadorCcp']['ot_tipo_ot'];
			$sistemaCcp[$key]['Talot']['ot_hora_recibida'] = $talot['SistemaPlanificadorCcp']['ot_hora_recibida'];
			$sistemaCcp[$key]['Talot']['ot_hora_prometida'] = $talot['SistemaPlanificadorCcp']['ot_hora_prometida'];
			$sistemaCcp[$key]['Talot']['ot_numero_cono'] = $talot['SistemaPlanificadorCcp']['ot_numero_cono'];
			
			$sistemaCcp[$key]['tooltip']['Talot'] =$sistemaCcp[$key]['Talot'];
			$tooltip = $this->Talot->findByNrocars($talot['SistemaPlanificadorCcp']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$sistemaCcp[$key]['tooltip']['Talot']['acta']=true;
			else $sistemaCcp[$key]['tooltip']['acta'] = false;
			
			//recuperamos el comentario de los estados de deposito
			$comentarioDeposito = $this->Ccptabhistorial->getComentarioDeposito(trim($talot['SistemaPlanificadorCcp']['ot_numero']));
			if(!empty($comentarioDeposito) && isset($comentarioDeposito)) $sistemaCcp[$key]['tooltip']['Talot']['comentarioDeposito']=$comentarioDeposito[0][0]['comentario'];
			else $sistemaCcp[$key]['tooltip']['Talot']['comentarioDeposito'] = null;
			
			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistorial->getSolicitudServicioAndCcpEstado(trim($talot['SistemaPlanificadorCcp']['ot_numero']));
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->Ccptabhistorial->getDateFormatView($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$sistemaCcp[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$sistemaCcp[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$sistemaCcp[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$sistemaCcp[$key]['Ccpsolicitudservicio']=array();
					$sistemaCcp[$key]['Ccptiposervicio']=array();
					$sistemaCcp[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$sistemaCcp[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$sistemaCcp[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			unset($solicitudServicio[0]);
			unset($sistemaCcp[$key]['SistemaPlanificadorCcp']);
		} 
		
		return $sistemaCcp;
	}
	
	
	/**RECUEPRA LAS CONDICIONES DE LA VISTAS
	 * ccptabhistoriales/reporte
	 * 
	 * DESARROLLADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * @param object $dt
	 * @return 
	 */
	function getReporte_cndBsc($dt, $dtLg=null){
		//si no envia valores retornamos condicion no valida
		if(empty($dt['placa']) && empty($dt['nrocars'])) return "1=2";
		
		//conditions cars add date 20130731
		$condicion_cars = "ot.TALLER in ('P035', 'P057') AND ot.CLASE_PEDIDO in ('2000','4000')";

		if(!empty($dt['carsestado_id'])){
			$estadoCars = $dt['carsestado_id']; 
			$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'"; 
		}else  $condicion_cars .= " AND ot.OT_ESTADO in ('0000','0060','0070','0090','0095','0103','0999')";  // solo hay dos estado IN ('0','2')
		
		$placa_=" ";
		if(!empty($dt['placa']) && isset($dt['placa'])){
			$placa_= " AND ot.PLACA like '%".$dt['placa']."%'";
		}
		
		$condicion_cars = $condicion_cars.$placa_;
		
		//SI NO TIENE DATO EN NINGUNO DE LOS CAMPOS RETORNA VACIO 
		if(!empty($dt['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$dt['nrocars'].'%\'';
	
		$condicion_cars = str_replace("ot.", "SistemaPlanificadorCcp.", $condicion_cars);
		return 	$condicion_cars;
	}
	
	function getReporte_complete($presupuestos){
		app::import('Model', 'Talot'); 			$this->Talot = new Talot();
		app::import('Model', 'Ccptabhistorial'); 			$this->Ccptabhistorial= new Ccptabhistorial;
		app::import('Model', 'Ccpsolicitudservicio'); 			$this->Ccpsolicitudservicio= new Ccpsolicitudservicio;
		
		foreach($presupuestos as $key => $presupuesto){
			$presupuestos[$key]['Talot']['ot_numero'] = trim($presupuesto['SistemaPlanificadorCcp']['ot_numero']);
			$presupuestos[$key]['Talot']['ot_nombre_asesor'] = trim($presupuesto['SistemaPlanificadorCcp']['ot_nombre_asesor']);
			$presupuestos[$key]['Talot']['ot_cliente'] = trim($presupuesto['SistemaPlanificadorCcp']['ot_cliente']);
			$presupuestos[$key]['Talot']['ot_placa'] = trim($presupuesto['SistemaPlanificadorCcp']['ot_placa']);
			$presupuestos[$key]['Talot']['ot_pre_estado'] = $presupuesto['SistemaPlanificadorCcp']['ot_pre_estado'];
			$presupuestos[$key]['Talot']['ot_asesor'] = trim($presupuesto['SistemaPlanificadorCcp']['ot_asesor']);
			$presupuestos[$key]['Talot']['ot_fecha_presupuesto_listo'] = $this->getDateFormatView($presupuesto['SistemaPlanificadorCcp']['ot_fecha_presupuesto_listo']);
			$presupuestos[$key]['Talot']['ot_fecha_creacion'] = $this->getDateFormatView($presupuesto['SistemaPlanificadorCcp']['ot_fecha_creacion']);
			$presupuestos[$key]['Talot']['ot_fecha_aprobacion_compania'] = $this->getDateFormatView($presupuesto['SistemaPlanificadorCcp']['ot_fecha_aprobacion_compania']);
			
			//EL PRIMER ESTADO
			$keyestados=0;
			$presupuestos[$key]['Estados'][$keyestados]=array(
															'id'=>1,
															'descripcion'=>'OT Creada',
															'responsable'=>trim($presupuesto['SistemaPlanificadorCcp']['ot_nombre_asesor']),
															'fechaInicio'=>$presupuestos[$key]['Talot']['ot_fecha_creacion'],
															'fechaFin'=>$presupuestos[$key]['Talot']['ot_fecha_creacion'],
															'duracion'=>'00:00',
															'proceso'=>$this->Ccptabhistorial->getProcesosPorCcpEstado(1),
															'estado'=>'Finalizado');
			
			$solicitudServicio=$this->Ccptabhistorial->getReporte(trim($presupuesto['SistemaPlanificadorCcp']['ot_numero']));
			
			//PARA SIMPLIFICAR OPERACIONES ASIGNAREMOS LA FECHA DE INICIO PARA ERL PROXIMO PROCESO COMO LA FECHA DE FIN DEL ANTERIOR PROCESO
			foreach($solicitudServicio as $id => $item){
				if(!empty($solicitudServicio[$id-1]['Ccptabhistoriale']['fecha']) && isset($solicitudServicio[$id-1]['Ccptabhistoriale']['fecha'])){
					$solicitudServicio[$id]['Ccptabhistoriale']['fechaInicio']=$solicitudServicio[$id-1]['Ccptabhistoriale']['fecha'];	
				}else{
					$solicitudServicio[$id]['Ccptabhistoriale']['fechaInicio'] = $this->getDateFormatDB($presupuestos[$key]['Talot']['ot_fecha_creacion']);
				}
			}

			$estado = array();
			$ampliacion = false;
			//SE AGREGAN TODAS LAS SOLICITUDES DE LA OT
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				$idsolicitud = $this->Ccptabhistorial->getPrimerSolitudServicioIdByNroOt($solicitudServicio[0]['Ccptabhistoriale']['ot_numero']);
				$datosSolicitud = $this->Ccptabhistorial->getSolicitudServicioPorSolicitud($idsolicitud);
				
				//pr($datosSolicitud);
				if($solicitudServicio[0]['Ccptabhistoriale']['ccptabestado_id']==2){
					$keyestados += 1;
					$estado['id'] = $solicitudServicio[0]['Ccptabhistoriale']['ccptabestado_id'];
					$estado['descripcion'] = $solicitudServicio[0]['Ccptabestado']['descripcion'];
					$estado['responsable'] = $solicitudServicio[0][0]['nombreCompleto'];
					$estado['fechaInicio'] = $this->getDateFormatView($presupuesto['SistemaPlanificadorCcp']['ot_fecha_creacion']);
					$estado['fechaFin'] = $this->getDateFormatViewHours($solicitudServicio[0]['Ccptabhistoriale']['fecha']);
					$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
					$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
					$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
					$presupuestos[$key]['Estados'][$keyestados]=$estado;
					unset($solicitudServicio[0]);
				}
				
				if(!empty($datosSolicitud)){
					$keyestados += 1;
					$estado['id'] = 16;//temporal
					$estado['descripcion'] = 'Presupuesto Listo';
					$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
					$estado['fechaInicio'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['ot_fecha_creacion']);
					$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
					$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
					$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
					$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
					$presupuestos[$key]['Estados'][$keyestados]=$estado;
								
					$keyestados += 1;
					$estado['id'] = 17;//temporal
					$estado['descripcion'] = 'Presupuesto Aprobado Compania de Seguro';
					$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
					$estado['fechaInicio'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
					$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fechaaprobacioncompania']);
					$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
					$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
					$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
					$presupuestos[$key]['Estados'][$keyestados]=$estado;				
					unset($datosSolicitud);
				}
					
				$i=0;
				foreach($solicitudServicio as $id => $item){
					// SI HUBO AMPLIACION EN EL REGISTRO ANTERIOR Y SE FUE DEPOSITO
					if($ampliacion){
						if($item['Ccptabhistoriale']['ccptabestado_id'] == 2){
							//LLENAMOS CON ESTADO DEPOSITO
							$keyestados += 1;
							$estado['id'] = $item['Ccptabhistoriale']['ccptabestado_id'];
							$estado['descripcion'] = $item['Ccptabestado']['descripcion'];
							$estado['responsable'] = $item[0]['nombreCompleto'];
							$estado['fechaInicio'] = $this->getDateFormatView($presupuesto['SistemaPlanificadorCcp']['ot_fecha_creacion']);
							$estado['fechaFin'] = $this->getDateFormatViewHours($item['Ccptabhistoriale']['fecha']);
							$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
							$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
							$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
							$presupuestos[$key]['Estados'][$keyestados]=$estado;
							
							//LLENAMOS LOS DATOS PRESUPUESTO LISTO Y PRESUPUESTO COMPANIA DE LA SOLICITUD DE SERVICIO
							$ccpSiguiente = $this->Ccptabhistorial->getSiguienteCcpTabHistorial($item['Ccptabhistoriale']['id']);
							if(!empty($ccpSiguiente) && isset($ccpSiguiente)){
								$datosSolicitud = $this->Ccptabhistorial->getSolicitudServicioPorSolicitud($ccpSiguiente['Ccptabhistoriale']['ccpsolicitudservicio_id']);
								$keyestados += 1;
								$estado['id'] = 16;//temporal
								$estado['descripcion'] = 'Presupuesto Listo';
								$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
								$estado['fechaInicio'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['ot_fecha_creacion']);
								$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
								$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;
								
								$keyestados += 1;
								$estado['id'] = 17;//temporal
								$estado['descripcion'] = 'Presupuesto Aprobado Compania de Seguro';
								$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
								$estado['fechaInicio'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
								$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fechaaprobacioncompania']);
								$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;
							}
							
						}elseif($item['Ccptabhistoriale']['ccptabestado_id'] == 3){
							//LLENAMOS LOS DATOS PRESUPUESTO LISTO Y PRESUPUESTO COMPANIA DE LA SOLICITUD DE SERVICIO
							$datosSolicitud = $this->Ccptabhistorial->getSolicitudServicioPorSolicitud($solicitudservicio_id);
							$keyestados += 1;
							$estado['id'] = 16;//TEMPORAL
							$estado['descripcion'] = 'Presupuesto Listo';
							$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
							$estado['fechaInicio'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['ot_fecha_creacion']);
							$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
							$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
							$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
							$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
							$presupuestos[$key]['Estados'][$keyestados]=$estado;
							
							$keyestados += 1;
							$estado['id'] = 17;//TEMPORAL
							$estado['descripcion'] = 'Presupuesto Aprobado Compania de Seguro';
							$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
							$estado['fechaInicio'] =$this->getDateFormatViewHours( $datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
							$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fechaaprobacioncompania']);
							$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
							$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
							$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
							$presupuestos[$key]['Estados'][$keyestados]=$estado;
							
							$keyestados += 1;
							$estado['id'] = $item['Ccptabhistoriale']['ccptabestado_id'];
							$estado['descripcion'] = $item['Ccptabestado']['descripcion'];
							$estado['responsable'] = $item[0]['nombreCompleto'];
							$estado['fechaInicio'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fechaaprobacioncompania']);
							$estado['fechaFin'] = $this->getDateFormatViewHours($item['Ccptabhistoriale']['fecha']);
							$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
							$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
							$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
							$presupuestos[$key]['Estados'][$keyestados]=$estado;
							
							//ACA ABAJO LLENAR LOS ESTADOS OBSERVADO Y RECHAZADO
							if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado'])){
								$keyestados += 1;
								$estado['id'] = 18;//temporal para solicitud de servicio aprobada
								$estado['descripcion'] = 'Solicitud Servicio Aprobada';
								$estado['responsable'] = $item[0]['nombreCompleto'];
								$estado['fechaInicio'] = $this->getDateFormatViewHours($item['Ccptabhistoriale']['fecha']);
								$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']);
								$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;								
							}
							if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado'])){
								$keyestados += 1;
								$estado['id'] = 19;//temporal para solicitud de servicio habilitada
								$estado['descripcion'] = 'Solicitud Servicio Habilitada';
								$estado['responsable'] = $item[0]['nombreCompleto'];
								$estado['fechaInicio'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']);
								$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado']);
								$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;								
							}
							if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado'])){
								$keyestados += 1;
								$estado['id'] = 20;//temporal para solicitud de servicio rechazada
								$estado['descripcion'] = 'Solicitud Servicio Rechazada';
								$estado['responsable'] = $item[0]['nombreCompleto'];
								$estado['fechaInicio'] = $this->getDateFormatViewHours($item['Ccptabhistoriale']['fecha']);
								$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado']);
								$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;								
							}
						}
					}else{
//-.-
						if($i>0){
							if($item['Ccptabhistoriale']['ccptabestado_id'] == 3){
								//LLENAMOS LOS DATOS PRESUPUESTO LISTO Y PRESUPUESTO COMPANIA DE LA SOLICITUD DE SERVICIO
								$solicitudservicio_id = $item['Ccptabhistoriale']['ccpsolicitudservicio_id'];
								$datosSolicitud = $this->Ccptabhistorial->getSolicitudServicioPorSolicitud($solicitudservicio_id);
								//pr($datosSolicitud);
								$keyestados += 1;
								$estado['id'] = 16;//TEMPORAL
								$estado['descripcion'] = 'Presupuesto Listo';
								$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
								$estado['fechaInicio'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['ot_fecha_creacion']);
								$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
								$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;
								
								$keyestados += 1;
								$estado['id'] = 17;//TEMPORAL
								$estado['descripcion'] = 'Presupuesto Aprobado Compania de Seguro';
								$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
								$estado['fechaInicio'] =$this->getDateFormatViewHours( $datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
								$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fechaaprobacioncompania']);
								$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;
								
								$keyestados += 1;
								$estado['id'] = $item['Ccptabhistoriale']['ccptabestado_id'];
								$estado['descripcion'] = $item['Ccptabestado']['descripcion'];
								$estado['responsable'] = $item[0]['nombreCompleto'];
								$estado['fechaInicio'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fechaaprobacioncompania']);
								$estado['fechaFin'] = $this->getDateFormatViewHours($item['Ccptabhistoriale']['fecha']);
								$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;
								
								//ACA ABAJO LLENAR LOS ESTADOS OBSERVADO Y RECHAZADO
								if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado'])){
									$keyestados += 1;
									$estado['id'] = 18;//temporal para solicitud de servicio aprobada
									$estado['descripcion'] = 'Solicitud Servicio Observada';
									$estado['responsable'] = $item[0]['nombreCompleto'];
									$estado['fechaInicio'] = $this->getDateFormatViewHours($item['Ccptabhistoriale']['fecha']);
									$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']);
									$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
									$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
									$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
									$presupuestos[$key]['Estados'][$keyestados]=$estado;								
								}
								if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado'])){
									$keyestados += 1;
									$estado['id'] = 19;//temporal para solicitud de servicio habilitada
									$estado['descripcion'] = 'Solicitud Servicio Habilitada';
									$estado['responsable'] = $item[0]['nombreCompleto'];
									$estado['fechaInicio'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']);
									$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado']);
									$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
									$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
									$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
									$presupuestos[$key]['Estados'][$keyestados]=$estado;								
								}
								if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado'])){
									$keyestados += 1;
									$estado['id'] = 20;//temporal para solicitud de servicio rechazada
									$estado['descripcion'] = 'Solicitud Servicio Rechazada';
									$estado['responsable'] = $item[0]['nombreCompleto'];
									$estado['fechaInicio'] = $this->getDateFormatViewHours($item['Ccptabhistoriale']['fecha']);
									$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado']);
									$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
									$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
									$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
									$presupuestos[$key]['Estados'][$keyestados]=$estado;								
								}
							}else{
									$keyestados += 1;
									$estado['id'] = $item['Ccptabhistoriale']['ccptabestado_id'];
									$estado['descripcion'] = $item['Ccptabestado']['descripcion'];
									$estado['responsable'] = $item[0]['nombreCompleto'];
									$estado['fechaInicio'] = $this->getDateFormatViewHours($item['Ccptabhistoriale']['fecha']);
									$estado['fechaInicio'] = $this->getDateFormatViewHours($item['Ccptabhistoriale']['fechaInicio']);
									$estado['fechaFin'] = $this->getDateFormatViewHours($item['Ccptabhistoriale']['fecha']);
									$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
									$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
									$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
									$presupuestos[$key]['Estados'][$keyestados]=$estado;
									
								}						
						}else{
							if($i==0){
								if($item['Ccptabhistoriale']['ccptabestado_id'] == 3){
									//LLENAMOS LOS DATOS PRESUPUESTO LISTO Y PRESUPUESTO COMPANIA DE LA SOLICITUD DE SERVICIO
									$solicitudservicio_id = $item['Ccptabhistoriale']['ccpsolicitudservicio_id'];
									$datosSolicitud = $this->Ccptabhistorial->getSolicitudServicioPorSolicitud($solicitudservicio_id);
									//ACA ABAJO LLENAR LOS ESTADOS OBSERVADO Y RECHAZADO
									if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado'])){
										$keyestados += 1;
										$estado['id'] = 18;//temporal para solicitud de servicio aprobada
										$estado['descripcion'] = 'Solicitud de Servicio Observada';
										$estado['responsable'] = $item[0]['nombreCompleto'];
										$estado['fechaInicio'] = $this->getDateFormatViewHours($item['Ccptabhistoriale']['fecha']);
										$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']);
										$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
										$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
										$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
										$presupuestos[$key]['Estados'][$keyestados]=$estado;								
									}
									if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado'])){
										$keyestados += 1;
										$estado['id'] = 19;//temporal para solicitud de servicio habilitada
										$estado['descripcion'] = 'Solicitud Servicio Habilitada';
										$estado['responsable'] = $item[0]['nombreCompleto'];
										$estado['fechaInicio'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']);
										$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado']);
										$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
										$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
										$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
										$presupuestos[$key]['Estados'][$keyestados]=$estado;								
									}
									if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado'])){
										$keyestados += 1;
										$estado['id'] = 20;//temporal para solicitud de servicio rechazada
										$estado['descripcion'] = 'Solicitud Servicio Rechazada';
										$estado['responsable'] = $item[0]['nombreCompleto'];
										$estado['fechaInicio'] = $this->getDateFormatViewHours($item['Ccptabhistoriale']['fecha']);
										$estado['fechaFin'] = $this->getDateFormatViewHours($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado']);
										$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
										$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
										$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
										$presupuestos[$key]['Estados'][$keyestados]=$estado;								
									}										
								}
							}
							$keyestados += 1;
							$estado['id'] = $item['Ccptabhistoriale']['ccptabestado_id'];
							$estado['descripcion'] = $item['Ccptabestado']['descripcion'];
							$estado['responsable'] = $item[0]['nombreCompleto'];
							$estado['fechaInicio'] = $this->getDateFormatViewHours($item['Ccptabhistoriale']['fecha']);
							$estado['fechaInicio'] = $this->getDateFormatViewHours($item['Ccptabhistoriale']['fechaInicio']);
							$estado['fechaFin'] = $this->getDateFormatViewHours($item['Ccptabhistoriale']['fecha']);
							$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
							$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
							$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
							$presupuestos[$key]['Estados'][$keyestados]=$estado;
						}
					}

					if($item['Ccptabhistoriale']['ccptabestado_id']==13){
						$ampliacion = true;
						$solicitudservicio_id=$item['Ccptabhistoriale']['ccpsolicitudservicio_id'];
					}else $ampliacion = false;
					
					unset($estado);	
					$i++;
				}
			}
			unset($presupuestos[$key]['0']);
			
		} 
		return $presupuestos;		
	}
}