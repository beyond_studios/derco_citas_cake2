<?php
class Marca extends AppModel
{
	public $name = 'Marca';
   
	public $validate = array(
		'codigo' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								),
						'isUnique' =>array(
								'rule'=>'isUnique',
								'last' => true
								),
						'maxLength' =>array(
            					'rule'    => array('maxLength', '20'),
								'last' => true
								),
						'alphaNumeric'=> array(
            					'rule' =>'alphaNumeric',
            					'last' => true
								),			     
					),
		'description' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								),
						'isUnique' =>array(
								'rule'=>'isUnique',
								'last' => true
								),
						'maxLength' =>array(
            					'rule'    => array('maxLength', '60'),
								'last' => true
								)  
					),
		'portal' => array(
						'maxLength' =>array(
            					'rule'    => array('maxLength', '60'),
								'last' => true
								)  
					)
    );

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $hasAndBelongsToMany = array('Secproject','Agegrupo');
	
	public $hasMany = array(
		'MarcasSecproject' => array(
			'className' => 'MarcasSecproject',
			'foreignKey' => 'marca_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	public function getMarcasClient($clientId = 0, $clientVehicleId = 0){
		if(empty($clientId) && empty($clientVehicleId)) return array();
		
		$cndClient = empty($clientId)?"1=1":"AgeclientesVehiculo.cliente_id = '$clientId'";
		$cndVehicle = empty($clientVehicleId)?"1=1":"AgeclientesVehiculo.id = '$clientVehicleId'";
		
		$marcasClient = $this->query(
			"select Marca.id, AgeclientesVehiculo.id, AgeclientesVehiculo.codigo_vehiculo, AgeclientesVehiculo.marca, AgeclientesVehiculo.modelo, AgeclientesVehiculo.placa
			from marcas AS Marca
				JOIN ageclientes_vehiculos AgeclientesVehiculo ON AgeclientesVehiculo.marca = Marca.description
			WHERE $cndClient AND $cndVehicle"
		);
		return $marcasClient;
	}
	
	public function getAllMarcasForRegistroClientes(){
		$marcas = array();
		$marcasClient = $this->query("select distinct(description) from marcas Marca");
		foreach($marcasClient as $id=>$item){
			$marcas[$item['Marca']['description']] = $item['Marca']['description']; 
		}
		return $marcas;
	}
}
?>