<?php
class Ccpsolicitudservicio extends AppModel
{
	public $name = 'Ccpsolicitudservicio'; 
	
   //The Associations below have been created with all possible keys, those that are not needed can be removed		
	public $hasMany = array(
		'Ccptabhistorial' => array(
			'className' => 'Ccptabhistorial',
			'foreignKey' => 'ccpsolicitudservicio_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '', 
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
   public $belongsTo = array(
						'Ccpsolicitudservicioestado' =>
							array('className' => 'Ccpsolicitudservicioestado',
								  'foreignKey' => 'Ccpsolicitudservicioestado_id'
							),
						'Ccpmotivorechazo' =>
							array('className' => 'Ccpmotivorechazo',
								  'foreignKey' => 'Ccpmotivorechazo_id'
							),
						'Secperson'=>
							array('className' => 'Secperson',
									'foreignKey' => 'secperson_id'
							),
						'Ccptipocliente'=>
							array('className' => 'Ccptipocliente',
									'foreignKey' => 'ccptipocliente_id'
							),
						'Ccptiposervicio'=>
							array('className' => 'Ccptiposervicio',
									'foreignKey' => 'ccptiposervicio_id'
							),
						'Ccppedidorepuesto'=>
							array('className' => 'Ccppedidorepuesto',
									'foreignKey' => 'ccppedidorepuesto_id'
							),
						'Talot'=>
							array('className' => 'Talot',
									'foreignKey' => 'talot_id'
							)
						);
						
	/**AVENTURA
	 * RECUPERA EL TOTAL DE REGISTROS ENCONTRADOS
	 * @param object $condicion
	 * @return 
	 */					
	function buscarTodosTotal($condicion=null, $cndFechaApb = "1=1"){
		$sql = "SELECT
         			COUNT(Ccpsolicitudservicio.id) AS total
				from ccpsolicitudservicios AS Ccpsolicitudservicio
        		JOIN ccpsolicitudservicioestados AS Ccpsolicitudservicioestado ON Ccpsolicitudservicioestado.id = Ccpsolicitudservicio.ccpsolicitudservicioestado_id
        		JOIN ccptiposervicios AS Ccptiposervicio ON Ccptiposervicio.id = Ccpsolicitudservicio.ccptiposervicio_id
				JOIN secpeople AS Secperson ON Secperson.id = Ccpsolicitudservicio.secperson_id
				LEFT JOIN talots as Talot ON Talot.nrocars = Ccpsolicitudservicio.ot_numero and empresacars in ('DP','DC')
				LEFT JOIN (SELECT Ccptabhistorial.ot_numero,Ccptabhistorial.ccptabestado_id,Ccptabestado.descripcion,CcptabhistorialTemp.ccpsolicitudservicio_id
 							FROM (
								SELECT MAX(id) as id,ot_numero,ccpsolicitudservicio_id 
								FROM ccptabhistoriales
									where $cndFechaApb 
								GROUP BY ot_numero,ccpsolicitudservicio_id) as CcptabhistorialTemp
  							INNER JOIN ccptabhistoriales Ccptabhistorial on (CcptabhistorialTemp.id=Ccptabhistorial.id)
                			LEFT JOIN ccptabestados Ccptabestado on(Ccptabhistorial.ccptabestado_id=Ccptabestado.id)) as Ccptabhistorial
							ON(Ccpsolicitudservicio.ot_numero = Ccptabhistorial.ot_numero) 
							AND Ccpsolicitudservicio.id =Ccptabhistorial.ccpsolicitudservicio_id";
		if ($condicion) $sql .= ' WHERE '.$condicion;
		
		$total = $this->query($sql);
		return $total[0][0]['total'];
	
	}
	
	/**AVENTURA
	 * RECUPERA EL ARRAY DE REGISTROS PARA MOSTRAR
	 * @param object $condicion
	 * @return 
	 */
	function buscarTodos($condicion, $order, $limit, $page, $cndFechaApb = "1=1"){
		$this->Ccptabhistorial = new Ccptabhistorial;
		$sql = "SELECT
         			Ccpsolicitudservicio.id,
					Ccpsolicitudservicio.ot_numero,
					Ccpsolicitudservicio.ot_placa,
					Ccpsolicitudservicio.ot_cliente,
					Ccpsolicitudservicio.fecha_emitido,
					Ccpsolicitudservicio.fechaaprobacioncompania,
					Ccpsolicitudservicio.presupuesto_numero,
					Ccptiposervicio.descripcion,
					Ccpsolicitudservicio.fecha_aprobacion,
					Ccpsolicitudservicio.presupuesto_fecha_aprobacion,
					Ccpsolicitudservicioestado.id,
					Ccpsolicitudservicioestado.descripcion,
					Ccpsolicitudservicio.ot_fecha_creacion,
					Ccpsolicitudservicio.horas_planchado,
					Ccpsolicitudservicio.horas_panio,
					CONCAT(Secperson.lastName,', ',Secperson.firstName) AS asesor,
					Talot.propietario,Talot.ordenfecha,Talot.nombreasesor,Talot.fechaprometida,Talot.horaprometida,Talot.ordenhora,
					Talot.estado,Talot.descripcion,Talot.marca,Talot.placa,Talot.modelo,Talot.nrocars,
					Talot.version,Talot.nrocono,Talot.carscliente,Talot.chassis,Talot.tipoorden,Talot.id,
					Ccptabhistorial.ccptabestado_id,
					Ccptabhistorial.descripcion
				from ccpsolicitudservicios AS Ccpsolicitudservicio
        		JOIN ccpsolicitudservicioestados AS Ccpsolicitudservicioestado ON Ccpsolicitudservicioestado.id = Ccpsolicitudservicio.ccpsolicitudservicioestado_id
        		JOIN ccptiposervicios AS Ccptiposervicio ON Ccptiposervicio.id = Ccpsolicitudservicio.ccptiposervicio_id
				JOIN secpeople AS Secperson ON Secperson.id = Ccpsolicitudservicio.secperson_id
				LEFT JOIN talots as Talot ON Talot.nrocars = Ccpsolicitudservicio.ot_numero and empresacars in ('DP','DC')
				LEFT JOIN (SELECT Ccptabhistorial.ot_numero,Ccptabhistorial.ccptabestado_id,Ccptabestado.descripcion,CcptabhistorialTemp.ccpsolicitudservicio_id
 							FROM (
								SELECT MAX(id) as id,ot_numero,ccpsolicitudservicio_id 
								FROM ccptabhistoriales
								WHERE $cndFechaApb 
								GROUP BY ot_numero,ccpsolicitudservicio_id) as CcptabhistorialTemp
  							INNER JOIN ccptabhistoriales Ccptabhistorial on(CcptabhistorialTemp.id=Ccptabhistorial.id)
                			LEFT JOIN ccptabestados Ccptabestado on(Ccptabhistorial.ccptabestado_id=Ccptabestado.id)) as Ccptabhistorial
							ON(Ccpsolicitudservicio.ot_numero = Ccptabhistorial.ot_numero) 
							AND Ccpsolicitudservicio.id =Ccptabhistorial.ccpsolicitudservicio_id";

		if ($condicion) $sql .= ' WHERE '.$condicion;

		if ($order && $limit && $page) {
			$offset = ($page-1)*$limit;
			$sql .= ' ORDER BY '.$order.' LIMIT '.$limit.' OFFSET '.$offset;
		}
		//pr($sql);
		$solicitudservicios = $this->query($sql);
	
		
		foreach($solicitudservicios as $key => $solicitudservicio){
			$solicitudservicios[$key]['Ccpsolicitudservicio']['fecha_emitido'] = $this->getDateFormatView($solicitudservicio['Ccpsolicitudservicio']['fecha_emitido']);
			$solicitudservicios[$key]['Ccpsolicitudservicio']['presupuesto_fecha_aprobacion'] = $this->getDateFormatView($solicitudservicio['Ccpsolicitudservicio']['presupuesto_fecha_aprobacion'],true);
			$solicitudservicios[$key]['Ccpsolicitudservicio']['fecha_aprobacion'] = $this->getDateFormatView($solicitudservicio['Ccpsolicitudservicio']['fecha_aprobacion']);
			$solicitudservicios[$key]['Ccpsolicitudservicio']['ot_fecha_creacion'] = $this->getDateFormatView($solicitudservicio['Ccpsolicitudservicio']['ot_fecha_creacion']);
			$solicitudservicios[$key]['Ccpsolicitudservicio']['asesor'] = $solicitudservicio[0]['asesor'];
			$solicitudservicios[$key]['Ccpsolicitudservicio']['color']=$this->Ccptabhistorial->obtenerDiferenciaDias(date('d-m-Y'),$solicitudservicios[$key]['Ccpsolicitudservicio']['ot_fecha_creacion']);

			unset($solicitudservicios[$key][0]);
		}
	
		return $solicitudservicios;
	}
	
	
	/** AVENTURA
	 * GUARDA LA SOLICITUD DE SERVICIO
	 */
	function setSolicitudServicio($dt,$dtLog){
		// VERIFICAMOS EL PASSWORD DEL USUARIO
		app::import('Model', 'Secperson');			$this->Secperson = new Secperson;
		app::import('Model', 'Ccptabhistorial');			$this->Ccptabhistorial = new Ccptabhistorial;
		app::import('Model', 'Talot');			$this->Talot = new Talot;
		
		$password = $this->Secperson->find('first',array(
			'conditions'=>array('1'=>sprintf("Secperson.status = 'AC' AND Secperson.id = %s", $dtLog['Secperson']['id'])),
			'recursive'=>-1
			//'fields'=>array('Secpassword.password')
		));

		if(!($password['Secperson']['password'] == $dt['ccpsolicitudservicios']['contrasenia_']))  
			return array(false,'VERIFIQUE SU CONTRASENIA');
		
		//preparamos para la subida de archivos
		if(!empty($dt['ccpsolicitudservicios']['ccparchivopresupuesto']) && isset($dt['ccpsolicitudservicios']['ccparchivopresupuesto'])){
			if(!$this->validarTipoArchivoPorNombre($dt['ccpsolicitudservicios']['ccparchivopresupuesto']['name'],'pdf')) return array(false,'Subir archivos en formato PDF');
			else {
				$dt['ccpsolicitudservicios']['archivopresupuesto'] = $dt['ccpsolicitudservicios']['ot_numero'].'_'.'pre'.'.pdf';
				$dt['ccpsolicitudservicios']['ccparchivopresupuesto']['name'] = $dt['ccpsolicitudservicios']['archivopresupuesto'];
			}
		}
		
		if(!empty($dt['ccpsolicitudservicios']['ccparchivoorden']) && isset($dt['ccpsolicitudservicios']['ccparchivoorden'])){
			if(!$this->validarTipoArchivoPorNombre($dt['ccpsolicitudservicios']['ccparchivoorden']['name'],'pdf')) return array(false,'Subir archivos en formato PDF');
			else {
				$dt['ccpsolicitudservicios']['archivoorden'] = $dt['ccpsolicitudservicios']['ot_numero'].'_'.'or'.'.pdf';
				$dt['ccpsolicitudservicios']['ccparchivoorden']['name'] = $dt['ccpsolicitudservicios']['archivoorden'];
			}
		}	
		
		//cuando el Ultimo estado de la Ot esta en Ampiacion debe permitir generar una solicitud
		$otCCP=$this->Ccptabhistorial->obtenerEstadoCcpPotNumeroOt(trim($dt['ccpsolicitudservicios']['ot_numero']));
		if(!empty($otCCP) && isset($otCCP)){
			if($otCCP[0]['Ccptabestado']['id']==13 || $this->Ccptabhistorial->validarEstadoAnterior(trim($dt['ccpsolicitudservicios']['ot_numero']),13) ){
				$cantidadAmpliaciones = $this->Ccptabhistorial->getCantidadAmpliaciones($dt['ccpsolicitudservicios']['ot_numero']);
				$nombreArchivoOrden = $this->getNombreArchivoOrden($dt['ccpsolicitudservicios']['ot_numero']);
				$dt['ccpsolicitudservicios']['archivopresupuesto']=$dt['ccpsolicitudservicios']['ot_numero'].'-'.'amp-'.$cantidadAmpliaciones.'.pdf';
				$dt['ccpsolicitudservicios']['ccparchivopresupuesto']['name'] = $dt['ccpsolicitudservicios']['archivopresupuesto'];
				$dt['ccpsolicitudservicios']['archivoorden']=$nombreArchivoOrden;//obtener aca el nombre del archivo anterior subido
				
			}else{
				// VERIFICAMOS QUE LA OT NO ESTE RELACIONADA A OTRA SOLICITUD (solo se relaciona si la otra solicitud fue anulada)
				$solicitud_anterior = $this->find('first',array(
					'conditions'=>array('1'=>"ot_numero = ".$dt['ccpsolicitudservicios']['ot_numero']." AND ccpsolicitudservicioestado_id <> 5"),
					'recursive'=>-1
				));
				if(!empty($solicitud_anterior))  return array(false,'OT ASIGNADO A OTRA SOLICITUD');			
			}
		}
		
		//GUARDAMOS LOS TIPO SERVICIO (TABLEROS) PARA LOS CUALES SE PUEDE USAR LA SOLICITUD
		$tableros = '';
		foreach($dt['ccptableros'] as $key => $tablero){
			if((substr_count($key, 'servicio_') == 1) && !empty($tablero)){
				$tableros .='-'.$tablero;
			}
		}
		$tableros = substr($tableros,1);
		
		//CAMBIAMOS FECHAS PARA GUARDAR LOS DATOS
		//$dt['ccpsolicitudservicios']['fechapresupuestolisto'] = $this->getDateFormatDB($dt['ccpsolicitudservicios']['fechapresupuestolisto']);
		//$dt['ccpsolicitudservicios']['fechaaprobacioncompania'] = $this->getDateFormatDB($dt['ccpsolicitudservicios']['fechaaprobacioncompania']);
		
		$dt['ccpsolicitudservicios']['ot_fecha_creacion'] = $this->getDateFormatDB($dt['ccpsolicitudservicios']['ot_fecha_creacion']);
		$dt['ccpsolicitudservicios']['ot_fecha_entrega'] = $this->getDateFormatDB($dt['ccpsolicitudservicios']['ot_fecha_entrega']);
		$dt['ccpsolicitudservicios']['fecha_propuesta_entrega'] = $this->getDateFormatDB($dt['ccpsolicitudservicios']['fecha_propuesta_entrega']);
		$dt['ccpsolicitudservicios']['presupuesto_fecha_aprobacion'] = $this->getDateFormatDB($dt['ccpsolicitudservicios']['presupuesto_fecha_aprobacion']);
		$dt['ccpsolicitudservicios']['fecha_emitido'] = $this->getDateFormatDB('', null, null, $now=1);
		$dt['ccpsolicitudservicios']['secperson_id'] = $dtLog['Secperson']['id'];
		$dt['ccpsolicitudservicios']['tablero_ids'] = $tableros;
		
		//COMPLETAMOS LOS DATOS DE LA SOLICITUD
//		$presupuesto = $this->Ccppresupuesto->getPresupuestoNew($dt['ccpsolicitudservicios']['presupuesto_numero'],trim($dt['ccpsolicitudservicios']['ot_numero']));
//		if(empty($presupuesto)) return array(false,'Numero Presupuesto No Existe');
		
//		$dt['ccpsolicitudservicios']['presupuesto_nombre_origina'] = $presupuesto[0][0]['presupuesto_nombre_origina'];
//		$dt['ccpsolicitudservicios']['presupuesto_fecha_creacion'] = $this->getDateFormatDB($presupuesto[0][0]['presupuesto_fecha_creacion']);
		
		//damos formato a las fecha y validacion de los años ingresado por el cliente y generados por el usuario
		$dt['ccpsolicitudservicios']['fechapresupuestolisto'] = $this->getDateFormatDB($dt['ccpsolicitudservicios']['fechapresupuestolisto']);
		$dt['ccpsolicitudservicios']['fechaaprobacioncompania'] = $this->getDateFormatDB($dt['ccpsolicitudservicios']['fechaaprobacioncompania']);

		if(!($this->fechaMayorQue($dt['ccpsolicitudservicios']['fechapresupuestolisto'],$dt['ccpsolicitudservicios']['ot_fecha_creacion']) && $this->fechaMayorQue(date('d-m-Y'),$dt['ccpsolicitudservicios']['fechapresupuestolisto'])))
			return array(false,'Fecha presupuesto listo debe ser mayor que la fecha de creacion de la ot y menor que la fecha actual');
		
		if(!($this->fechaMayorQue($dt['ccpsolicitudservicios']['fechaaprobacioncompania'],$dt['ccpsolicitudservicios']['ot_fecha_creacion']) && $this->fechaMayorQue(date('d-m-Y'),$dt['ccpsolicitudservicios']['fechaaprobacioncompania'])))
			return array(false,'Fecha Aprobacion Compania debe ser mayor que la fecha de creacion de la ot y menor que la fecha actual');
		
		// GUARDAMOS LA SOLICITUD
		$this->create();
		if(!$this->save($dt['ccpsolicitudservicios'])) return array(false,'LA SOLICITUD NO FUE GUARDADA');
		
		//AGREGADO DESDE ACA POR RTINEO
		//GUARDAMOS EN LA TABLA CCPTABHISTORIAL
		//obtener estado actual
		$estadoCCPOT=$this->Ccptabhistorial->obtenerEstadoCcpPotNumeroOt($dt['ccpsolicitudservicios']['ot_numero']);
		if(!empty($estadoCCPOT) && isset($estadoCCPOT)) $estadoActual = $estadoCCPOT[0]['Ccptabestado']['id'];
		else $estadoActual=1;
		
		//se cambia al estado 3 que es con solicitud
		$respuesta=$this->Ccptabhistorial->cambiarEstado($estadoActual,3,$dt['ccpsolicitudservicios']['ot_numero'],'Con Solicitud de Servicio generada',$dtLog['Secperson']['id'],$this->id);
		if(!$respuesta['respuesta']) return array(false, $respuesta['msg'], 'id'=>$this->id);
		
		//aca subimos el archivo
		if(!empty($dt['ccpsolicitudservicios']['ccparchivopresupuesto']) && isset($dt['ccpsolicitudservicios']['ccparchivopresupuesto'])){
			if(!$this->setFile('solicitud',$dt['ccpsolicitudservicios']['ccparchivopresupuesto'])) return array(false,'Archivo presupuesto no se puede subir');
		}
		if(!empty($dt['ccpsolicitudservicios']['ccparchivoorden']) && isset($dt['ccpsolicitudservicios']['ccparchivoorden'])){
			if(!$this->setFile('solicitud',$dt['ccpsolicitudservicios']['ccparchivoorden']))return array(false,'Archivo de orden no se puede subir');
		}
		
		//ACTUALIZAMOS LA ASEGURADORA DE LA OT
		$ot_db = $this->Talot->find('first',array(
			'conditions'=>array("Talot.nrocars"=>$dt['ccpsolicitudservicios']['ot_numero']),
			'fields'=>array('Talot.id'),
			'order'=>array('Talot.id'=>'DESC')
		));
		/*if(!empty($ot_db)){
			$ot_db['Talot']['CodSAPCiaAseguradora'] = $dt['Talot']['CodSAPCiaAseguradora'];	
			if(!$this->Talot->save($ot_db)) return array(false, 'ERROR AL ACTUALIZAR LA OT');
		}
		*/
		return array(true, 'SOLICITUD GUARDADA', 'id'=>$this->id);
	}
	
	/** GUARDAMOS LOS DOCUMENTOS RESPECTIVOS DE LA SOLICITUD 
	 *  ARRAY()
	 */
	function setFile($directorio, $file){
		//debug(DS.'var'.DS.'www'.DS.'html'.DS.$directorio);
		//debug(file_exists(DS.'var'.DS.'www'.DS.'html'.DS.$directorio));
		if(!file_exists(DS.'var'.DS.'www'.DS.'html'.DS.$directorio)) return array(false, 'El directorio '.$directorio.' no existe');
		
		if(!move_uploaded_file($file['tmp_name'], DS.'var'.DS.'www'.DS.'html'.DS.$directorio.DS.$file['name']))  return array(false, 'ARCHIVO_NO_GUARDADO');
				
		return array(true, 'ARCHIVO_GUARDADO');
	}
	
	
	function rechazarSolicitud($dt,$dtLog){
		app::import('Model', 'Secperson');			$this->Secperson = new Secperson;
		
		// CAMBIAMOS DE ESTADO A LA SOLICITUD DE SERVICIO
		$dt['Ccpsolicitudservicio']['fecha_rechazado'] = $this->fechaHoraActual();
		$dt['Ccpsolicitudservicio']['ccpsolicitudservicioestado_id']= 5;
			
		if(!$this->save($dt['Ccpsolicitudservicio'])) return array(false,'LA_SOLICITUD_NO_FUE_GUARDADA');
		
		//VERIFICAMOS QUE AMENOS AYA INGRESADO UN CORREO
		if(empty($dt['Ccpsolicitudservicio']['copia_a']) and empty($dt['Ccpsolicitudservicio']['rechazar_correo']))
			return array(false,'INGRESE_UN_CORREO');
		
		// ENVIAMOS EL CORREO AL CREADOR DE LA SOLICITUD DE SERVICIO
		// recuperamos el nombre de quien rechaza
		$this->Secperson->recursive = -1;
		$person = $this->Secperson->findById($dtLog['Secperson']['id']);
		$person = $person['Secperson']['nombreCompleto'];
		$comentario_rechazar = 'Rechazado por '.$person;
		
		// enviarEmail($correo, $asunto, $mensaje)
		$enviosatisfactorio=true;
		$reclamo['tipo']='Rechazo';
		$reclamo['bn_cont_tipo'] = 'Solicitud Servicio';
		
		$correo = trim($dt['Ccpsolicitudservicio']['rechazar_correo']);
		$correo = empty($dt['Ccpsolicitudservicio']['copia_a'])?$correo:trim($dt['Ccpsolicitudservicio']['copia_a']).$correo;
		//$correo = empty($dt['Reccolaborador']['correo'])?"rtineo@chiusac.com":$dt['Reccolaborador']['correo'];
		
		//recuperamos el numero de presupuesto de la solicitud de servicio
		$solicitudServicio = $this->find('first',array(
			'conditions'=>array('Ccpsolicitudservicio.id'=>$dt['Ccpsolicitudservicio']['id']),
			'fields'=>array('Ccpsolicitudservicio.presupuesto_numero')
		));
		
		// armamos el comentario de rechazo
		$comentario_rechazar .= ': '.$dt['Ccpsolicitudservicio']['comentario_rechazar'];		
		if(!$this->enviarEmail($correo, $dt['Ccpsolicitudservicio']['rechazar_asunto'], $comentario_rechazar))
			return array(false,'CORREO_NO_ENVIADO');
		
		return array(true,'MAQUINARIAS_DOCUMENTO_RECHAZADO');
	}
	
	/**AVENTURA
	 * CAMBIAR LAS FECHAS A FORMATO VISUALIZADO EN PHP
	 * @param object $solicitud
	 * @return 
	 */
	function cambiarFechasMysqlPhp($solicitud){
		$solicitud['Ccpsolicitudservicio']['ot_fecha_creacion'] = empty($solicitud['Ccpsolicitudservicio']['ot_fecha_creacion'])?'':$this->getDateFormatView($solicitud['Ccpsolicitudservicio']['ot_fecha_creacion']);
		$solicitud['Ccpsolicitudservicio']['ot_fecha_entrega'] = empty($solicitud['Ccpsolicitudservicio']['ot_fecha_entrega'])?'':$this->getDateFormatView($solicitud['Ccpsolicitudservicio']['ot_fecha_entrega']);
		$solicitud['Ccpsolicitudservicio']['fecha_propuesta_entrega'] = empty($solicitud['Ccpsolicitudservicio']['fecha_propuesta_entrega'])?'':$this->getDateFormatView($solicitud['Ccpsolicitudservicio']['fecha_propuesta_entrega']);
				
		$solicitud['Ccpsolicitudservicio']['fecha_aprobacion'] = empty($solicitud['Ccpsolicitudservicio']['fecha_aprobacion'])?'':$this->getDateFormatView($solicitud['Ccpsolicitudservicio']['fecha_aprobacion']);
		$solicitud['Ccpsolicitudservicio']['fecha_observado'] = empty($solicitud['Ccpsolicitudservicio']['fecha_observado'])?'':$this->getDateFormatView($solicitud['Ccpsolicitudservicio']['fecha_observado']);
		$solicitud['Ccpsolicitudservicio']['fecha_habilitado'] = empty($solicitud['Ccpsolicitudservicio']['fecha_habilitado'])?'':$this->getDateFormatView($solicitud['Ccpsolicitudservicio']['fecha_habilitado']);
		$solicitud['Ccpsolicitudservicio']['fecha_rechazado'] = empty($solicitud['Ccpsolicitudservicio']['fecha_rechazado'])?'':$this->getDateFormatView($solicitud['Ccpsolicitudservicio']['fecha_rechazado']);
		return $solicitud;
	}
	
	/**ADURAN
	 * OBTIENE AL SOLICITANTE DE SERVICIO DE UNA OT
	 */
	function obtenerSolicitante($ot_id){
		$sql = "SELECT CONCAT(Secperson.lastName, ', ', Secperson.firstName) as solicitante, Secperson.id 
				FROM ccpsolicitudservicios Ccpsolicitudservicio INNER JOIN secpeople Secperson
				ON Ccpsolicitudservicio.secperson_id = Secperson.id
				WHERE Ccpsolicitudservicio.talot_id = ".$ot_id;
		$rs = $this->query($sql);
		if (isset($rs) AND !empty($rs)){
			$solicitante[$rs['0']['Secperson']['id']] = $rs['0']['0']['solicitante'];
		}
		return (isset($solicitante) AND !empty($solicitante))? $solicitante: array();
	}

	/**AVENTURA - CCP - RECUPERA LOS SOLICITANTES DE LA SUCURSAL
	 */	
	function obtenerListaSolicitantes($projectId){
		$sql = "SELECT Secconfiguration.ccpRolSolicitante
				FROM secconfigurations Secconfiguration";
		$configuracion = $this->query($sql);
		$listaSolicitantes = array();	
		if(!empty($configuracion[0])){
			$rolSolicitante = $configuracion[0]['Secconfiguration']['ccpRolSolicitante'];
			$sql = "SELECT CONCAT(Secperson.lastname, ', ', Secperson.firstname)as solicitante, Secperson.id
					FROM secassigns Secassign JOIN secpeople Secperson
					ON Secperson.id = Secassign.secperson_id
					WHERE Secassign.secproject_id = ". $projectId. " 
					AND Secassign.secrole_id = ".$rolSolicitante;
			$solicitantes = $this->query($sql);
			
			foreach($solicitantes as $index => $solicitante){
				$listaSolicitantes[$solicitante['Secperson']['id']] = $solicitante['0']['solicitante'];
			}
		}
		return $listaSolicitantes;
	}
	
	/**ADURAN
	 * OBTIENE LA SOLICITUD DE UNA OT
	 */
	function obtenerDatosOtSolicitud($ot_id){
		// SELECT fecha_propuesta_inicio, fecha_propuesta_fin, fecha_aprobacion, presupuesto_numero 
		$sql = "SELECT fecha_propuesta_entrega, ot_fecha_entrega, fecha_aprobacion, presupuesto_numero 
				FROM ccpsolicitudservicios Ccpsolicitudservicio INNER JOIN secpeople Secperson
				ON Ccpsolicitudservicio.secperson_id = Secperson.id
				WHERE Ccpsolicitudservicio.talot_id = $ot_id AND Ccpsolicitudservicio.Ccpsolicitudservicioestado_id = 2
				ORDER BY Ccpsolicitudservicio.id DESC";
		$rs = $this->query($sql);
		if (isset($rs) AND !empty($rs)){
			$solicitud['Ccpsolicitudservicio'] = $rs['0']['Ccpsolicitudservicio'];
		}
		return (isset($solicitud) AND !empty($solicitud))? $solicitud: array();
	}
	
	/**AVENTURA
	 * Aprueba la solicitud de servicio, verificando si es que la ot ha sido planificada anteriormente
	 * maqcontrolot {control_actual = 1}
	 * @return 
	 */
	function aprobarSolicitud($dt,$numero_ot=null,$dtLog){
		//MODELOS UTILIZADOS
		app::import('Model', 'Ccptabhistorial');		$this->Ccptabhistorial = new Ccptabhistorial;
		
		if(empty($numero_ot)) return array(false,'NUMERO OT NO EXISTE');
		
		//DATOS COMPLEMENTARIOS
		$dt['Ccpsolicitudservicio']['ccpsolicitudservicioestado_id']= 2;
		$dt['Ccpsolicitudservicio']['fecha_aprobacion'] = $this->getDateFormatDB(null,null,null,true);
		$dt['Ccpsolicitudservicio']['fechacomfirmacionentrega'] = $this->getDateFormatDB($dt['Ccpsolicitudservicio']['fechacomfirmacionentrega']);
		if(!$this->save($dt)) return array(false,'SOLICITUD_NO_ACTUALIZADA');
		
		//AGREGADO DESDE ACA POR RTINEO
		//GUARDAMOS EN LA TABLA CCPTABHISTORIAL
		
		//VERIFICAMOS SI LA OT HA PASADO POR DEPOSITO
		if($this->Ccptabhistorial->otpasoPorDeposito($numero_ot)){
			//obtener estado actual
			$estadoCCPOT=$this->Ccptabhistorial->obtenerEstadoCcpPotNumeroOt($numero_ot);
			if(!empty($estadoCCPOT) && isset($estadoCCPOT)) $estadoActual = $estadoCCPOT[0]['Ccptabestado']['id'];
			else $estadoActual=0;
			$respuesta=$this->Ccptabhistorial->cambiarEstado($estadoActual,4,$numero_ot,'Solicitud de servicio aprobada',$dtLog['Secperson']['id'],$this->id);
			if(!$respuesta['respuesta']) return array(false, $respuesta['msg']);

		}else{
			//primero pasar a solicitud de servicio aprobada
			$estadoCCPOT=$this->Ccptabhistorial->obtenerEstadoCcpPotNumeroOt($numero_ot);
			if(!empty($estadoCCPOT) && isset($estadoCCPOT)) $estadoActual = $estadoCCPOT[0]['Ccptabestado']['id'];
			else $estadoActual=0;
			$respuesta=$this->Ccptabhistorial->cambiarEstado($estadoActual,4,$numero_ot,'Solicitud de servicio aprobada',$dtLog['Secperson']['id'],$this->id);
			if(!$respuesta['respuesta']) return array(false, $respuesta['msg']);
			else $estadoActual=4;
			
			//luego inicar ABP automaticamente
			$respuesta=$this->Ccptabhistorial->verificarPasoDeEstados($estadoActual,5);
			if($respuesta['respuesta']){
				//Cambiamos a Estado Iniciado ABP
				$historial['Ccptabhistorial']['ccptabestado_id']=5;
				$historial['Ccptabhistorial']['ot_numero']=$numero_ot;
				$historial['Ccptabhistorial']['fecha']=$this->fechaHoraActual();
				$historial['Ccptabhistorial']['comentario']='ABP Iniciado Automaticamente';
				$historial['Ccptabhistorial']['secperson_id']=$dtLog['Secperson']['id'];
				$historial['Ccptabhistorial']['ccpsolicitudservicio_id']=$this->id;
				$this->Ccptabhistorial->create();
				if(!$this->Ccptabhistorial->save($historial)) return array(false,'Error al Cambiar el estado a ABP INICIADO');
			}else return array(false,$respuesta['msg']);
			
			return array(true, 'CCP_DOCUMENTO_APROBADO_ABP_INICIADO');
		}
		
		return array(true,'DOCUMENTO_ACTUALIZADO');
	}
	
	function validarTipoArchivoPorNombre($name,$tipo){
		$tmp_tipo = explode('.',$name);
		if($tmp_tipo[1] == $tipo)return true;
		return false;
	}
	
	function getNombreArchivoOrden($numero_ot){
		$sql="select Ccpsolicitudservicio.archivoorden 
		from ccpsolicitudservicios Ccpsolicitudservicio where ot_numero='".$numero_ot."' and archivoorden is not null order by id desc";
		$nombre = $this->query($sql);
		return (!empty($nombre) && isset($nombre))?$nombre[0]['Ccpsolicitudservicio']['archivoorden']:'';
	}
}