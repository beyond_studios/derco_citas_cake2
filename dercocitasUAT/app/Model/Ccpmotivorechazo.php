<?php 
class Ccpmotivorechazo extends AppModel{
	public $name='Ccpmotivorechazo';	
	public $displayField = 'descripcion';
	
	public function find_list() {
		$sql = "SELECT 
					Ccpmotivorechazo.id,
					Ccpmotivorechazo.nombre
				FROM 
					ccpmotivorechazos Ccpmotivorechazo";
					
		$list = $this->query($sql);
		$lista = array();
		foreach ($list as $item){
			$lista[$item['Ccpmotivorechazo']['id']] = $item['Ccpmotivorechazo']['nombre'];
		} 
		return $lista;	
	}
}