<?php
App::uses('AppModel', 'Model');
/**
 * Ccptabestado Model
 *
 * @property Ccptabhistoriale $Ccptabhistoriale
 */
class Ccptabestado extends AppModel {
   public $name = 'Ccptabestado';
   //var $displayField = 'descripcion';
   
   //The Associations below have been created with all possible keys, those that are not needed can be removed		
	public $hasMany = array(
		'Ccptabhistorial' => array(
			'className' => 'Ccptabhistorial',
			'foreignKey' => 'ccptabestado_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
   
   function getEstados(){
		$conditions=array('estado'=>'AC');
		$lista=$this->find('all',array('conditions'=>$conditions));
		$estados=array();
		foreach($lista as $id => $item){
			$estados[$item['Ccptabestado']['id']]=$item['Ccptabestado']['descripcion'];
		}
		return (!empty($estados) && isset($estados))?$estados:array();
   }
   
   function getIdEstadoPorDescripcion($descripcion){
   		$estados=$this->getEstados();
		$estadoFind=0;
		foreach($estados as $id => $item){
			if($descripcion==$item){
				$estadoFind=$id;
			}
		}
		return (!empty($estadoFind) && isset($estadoFind))?$estadoFind:0;
   }
}
?>