<?php
App::uses('AppModel', 'Model');
class Agecitacalendariodia extends AppModel
{
	public $name = 'Agecitacalendariodia';
	
	public $validate = array(
		'programed' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								),
						'numeric' =>array(
            					'rule'    => 'numeric',
								'message' => 'ingrese dato numerico'
								)      
					)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		'Agecitacalendario' => array(
			'className' => 'Agecitacalendario',
			'foreignKey' => 'agecitacalendario_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany = array(
		'Agedetallecita' => array(
			'className' => 'Agedetallecita',
			'foreignKey' => 'agecitacalendariodia_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);
	
	public function getAnio($id)
	{
		$sql = "SELECT YEAR(initDateTime) AS year FROM agecitacalendariodias WHERE agecitacalendario_id=".$id." ORDER BY initDateTime ASC";		
				
		$rs = $this->query($sql);
		
		return $rs;
	}
	
	public function getFecha()
	{
		$sql = "SELECT CURDATE() AS fecha";		
				
		$rs = $this->query($sql);
		
		return $rs;
	}
	
	public function getAño()
	{
		$sql = "SELECT YEAR(CURDATE()) AS year";		
				
		$rs = $this->query($sql);
		
		return $rs;
	}
	
	
	function generarCalendario($anio, $talcitacalendar_id,$lunesCastId,$martesCastId,$miercolesCastId,$juevesCastId,$viernesCastId,$sabadoCastId, $domingoCastId) {
		$this->Appcasttime = new Appcasttime;
		$estadoActivo = 'AC';
		
		for($mes=1; $mes<=12; $mes++) {
			$primeraFecha = getDate(mktime(0, 0, 0, $mes, 1, $anio));
			$ultimaFecha = getDate(mktime(0, 0, 0, $mes+1, 0, $anio));
		
			$dia = 1;
			$fecha = $primeraFecha;
			
			while($fecha['0']<=$ultimaFecha['0']) {
				$diaSemana = $fecha['wday'];
				if($diaSemana==0) {
					// caso: domingo
					$cast_id = $domingoCastId;
				}
				if($diaSemana==1){
					//caso lunes
					$cast_id = $lunesCastId;
				}
				if($diaSemana==2){
					//caso martes
					$cast_id = $martesCastId;
				}
				if($diaSemana==3){
					//caso miercoles
					$cast_id = $miercolesCastId;
				}
				if($diaSemana==4){
					//caso jeuves
					$cast_id = $juevesCastId;
				}
				if($diaSemana==5){
					//caso viernes
					$cast_id = $viernesCastId;
				}
				if($diaSemana==6) {
					// caso: sabado
					$cast_id = $sabadoCastId;
				} 
				
				// buscar
				$condicion = array(
					'appcast_id' => $cast_id,
					'Appcasttime.status' => $estadoActivo
				);
				
				$castTimes = $this->Appcasttime->find('all',array('conditions'=>array($condicion)));
				
				// agregar
				foreach($castTimes as $castTime) {
					$this->create();
					$fechaYmd = date('Y-m-d', mktime(0, 0, 0, $mes, $dia, $anio));
					$dt['agecitacalendariodia']['initDateTime'] = $fechaYmd.' '.$castTime['Appcasttime']['inittime'];
					$dt['agecitacalendariodia']['endDateTime'] = $fechaYmd.' '.$castTime['Appcasttime']['endtime'];
					$dt['agecitacalendariodia']['programed'] = $castTime['Appcasttime']['programed'];
					$dt['agecitacalendariodia']['available'] = $castTime['Appcasttime']['programed'];
					$dt['agecitacalendariodia']['agecitacalendario_id'] = $talcitacalendar_id;

					if ($this->save($dt['agecitacalendariodia'])) {
						// nada 
					} else {
						return false;
					}
					
				}
				// incremento
				$dia++;
				$fecha = getDate(mktime(0, 0, 0, $mes, $dia, $anio));
			}// end while
		}// end for
		return true;
	}

	function generarCalendarioDesdFecha($anio,$mesInicial,$diaInicial,$talcitacalendar_id,$lunesCastId,$martesCastId,$miercolesCastId,$juevesCastId,$viernesCastId,$sabadoCastId, $domingoCastId) {		
		$this->Appcasttime = new Appcasttime;
		$estadoActivo = 'AC';
		
		for($mes=$mesInicial; $mes<=12; $mes++) {
			$primeraFecha = getDate(mktime(0, 0, 0, $mes, 1, $anio));
			$ultimaFecha = getDate(mktime(0, 0, 0, $mes+1, 0, $anio));
			
			$dia=($mes==$mesInicial)?$diaInicial:1;
			//$dia = $diaInicial;
			$fecha = $primeraFecha;
			
			while($fecha['0']<=$ultimaFecha['0']) {
				$diaSemana = $fecha['wday'];
				if($diaSemana==0) {
					// caso: domingo
					$cast_id = $domingoCastId;
				}
				if($diaSemana==1){
					//caso lunes
					$cast_id = $lunesCastId;
				}
				if($diaSemana==2){
					//caso martes
					$cast_id = $martesCastId;
				}
				if($diaSemana==3){
					//caso miercoles
					$cast_id = $miercolesCastId;
				}
				if($diaSemana==4){
					//caso jeuves
					$cast_id = $juevesCastId;
				}
				if($diaSemana==5){
					//caso viernes
					$cast_id = $viernesCastId;
				}
				if($diaSemana==6) {
					// caso: sabado
					$cast_id = $sabadoCastId;
				} 
				
				// buscar
				$condicion = array(
					'appcast_id' => $cast_id,
					'Appcasttime.status' => $estadoActivo
				);
				$castTimes = $this->Appcasttime->find('all',array('conditions'=>array($condicion)));
				// agregar
				foreach($castTimes as $castTime) {
					$this->create();
					$fechaYmd = date('Y-m-d', mktime(0, 0, 0, $mes, $dia, $anio));
					$dt['agecitacalendariodia']['initDateTime'] = $fechaYmd.' '.$castTime['Appcasttime']['inittime'];
					$dt['agecitacalendariodia']['endDateTime'] = $fechaYmd.' '.$castTime['Appcasttime']['endtime'];
					$dt['agecitacalendariodia']['programed'] = $castTime['Appcasttime']['programed'];
					$dt['agecitacalendariodia']['available'] = $castTime['Appcasttime']['programed'];
					$dt['agecitacalendariodia']['agecitacalendario_id'] = $talcitacalendar_id;

					if ($this->save($dt['agecitacalendariodia'])) {
						// nada 
					} else {
						return false;
					}
				}
				// incremento
				$dia++;
				$fecha = getDate(mktime(0, 0, 0, $mes, $dia, $anio));
			}// end while
		}// end for
		return true;
	}
	/*
	function updateAll($citaacalendarId,$fecha){
		
		$bolean=false;
		if(!empty($citaacalendarId)&&!empty($fecha)){
			try{
				$sql = "UPDATE agecitacalendariodias SET estado='EL' 
				WHERE estado='AC' 
				AND agecitacalendario_id=".$citaacalendarId." 
				AND DATE(initDateTime)>='".$fecha."'
				AND YEAR(initDateTime)=YEAR('".$fecha."')";
				$rs = $this->query($sql);
				$bolean=true;
			}catch(Exception $e){
				$bolean=false;
			}

		}
		return $bolean;
	}*/

	/** GENERA EL CALENDARIO
	 * AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * @return 
	 */
	function modificarCalendarioGeneradoForm($dt, $dtLog){
		app::import('Model', 'Agecitacalendario'); 		$this->Agecitacalendario = new Agecitacalendario();
		app::import('Model', 'Appcast'); 				$this->Appcast = new Appcast();
		
		$talcitacalendar_id = $dt['Agecitacalendario']['id'];
		//obteniendo las plantillas
		//se comento el diario cast id porque se va IMPLEMENTAR UNO DIARIO que comprenda de lunes a viernes,
		//$diarioCastId = $dt['Agecitacalendariodia']['diario_cast_id'];
		$lunesCastId = $dt['Agecitacalendariodia']['lunes_cast_id'];
		$martesCastId = $dt['Agecitacalendariodia']['martes_cast_id'];
		$miercolesCastId = $dt['Agecitacalendariodia']['miercoles_cast_id'];
		$juevesCastId = $dt['Agecitacalendariodia']['jueves_cast_id'];
		$viernesCastId = $dt['Agecitacalendariodia']['viernes_cast_id'];
		
		$sabadoCastId = $dt['Agecitacalendariodia']['sabado_cast_id'];
		$domingoCastId = $dt['Agecitacalendariodia']['domingo_cast_id'];
		
		// ya que el año es siempre el actual	
		$anio = $dt['Agecitacalendario']['anio'];
		$dt['Fecha']['desde'] = '01-01-'.$anio;
		
		$mes=date('m',strtotime($dt['Fecha']['desde']));
		$dia=date('d',strtotime($dt['Fecha']['desde']));
		$this->Agecitacalendario->unbindModel(array('belongsTo'=>array('Secproject'),'hasMany'=>array('Agecitacalendariodia')));
		$TalCitaCalendar=$this->Agecitacalendario->find('first', array('Agecitacalendario.id'=>$talcitacalendar_id));
		foreach ($dt['Agecitacalendariodia'] as $item=>$valor){ //verificamos que no sobrepase los cupos permitidos en el servicio.
			if ($this->Appcast->getCantidadCitasPorPlantilla($valor)>$TalCitaCalendar['Agecitacalendario']['maximo_citasprogramado'])
				return array(false,__('TALLER_NO_SE_PUEDE_GUARDAR_MAS_DE_CUPOS').$TalCitaCalendar['Agecitacalendario']['maximo_citasprogramado']);
		}
		
		if (!$lunesCastId || !$martesCastId || !$miercolesCastId || !$juevesCastId || !$viernesCastId  || !$sabadoCastId || !$domingoCastId || !$anio || !$talcitacalendar_id) {
			return array(false,'GENERAL_CORRIJA_ERRORES');
		}
		$primeraFecha = date('Y-m-d',mktime(0, 0, 0, $mes, $dia, $anio));
					
		//cambiamos el estado a los calendarios anteriores
		if(!$this->updateAll($talcitacalendar_id,$primeraFecha)) return array(false,'GENERAL_ERROR_GRABACION');
		
		//generamos el Calendario de hoy para delante
		if (!$this->generarCalendarioDesdeFecha($anio,$mes,$dia, $talcitacalendar_id, $lunesCastId,$martesCastId,$miercolesCastId,$juevesCastId,$viernesCastId, $sabadoCastId, $domingoCastId)){
			return array(false,'GENERAL_ERROR_GRABACION');
		}
		
		return array(true, __('TALLER_CITACALENDARDAY_HORARIOS_GENERADOS').$anio);
	}
	
	function generarCalendarioDesdeFecha($anio,$mesInicial,$diaInicial,$talcitacalendar_id,$lunesCastId,$martesCastId,$miercolesCastId,$juevesCastId,$viernesCastId,$sabadoCastId, $domingoCastId) {
		app::import('Model', 'Appcasttime');
		
		$this->Appcasttime = new Appcasttime;
		$estadoActivo = 'AC';
		
		for($mes=$mesInicial; $mes<=12; $mes++) {
			$primeraFecha = getDate(mktime(0, 0, 0, $mes, 1, $anio));
			$ultimaFecha = getDate(mktime(0, 0, 0, $mes+1, 0, $anio));
			
			$dia=($mes==$mesInicial)?$diaInicial:1;
			//$dia = $diaInicial;
			$fecha = $primeraFecha;
			
			while($fecha['0']<=$ultimaFecha['0']) {
				$diaSemana = $fecha['wday'];
				if($diaSemana==0) {
					// caso: domingo
					$cast_id = $domingoCastId;
				}
				if($diaSemana==1){
					//caso lunes
					$cast_id = $lunesCastId;
				}
				if($diaSemana==2){
					//caso martes
					$cast_id = $martesCastId;
				}
				if($diaSemana==3){
					//caso miercoles
					$cast_id = $miercolesCastId;
				}
				if($diaSemana==4){
					//caso jeuves
					$cast_id = $juevesCastId;
				}
				if($diaSemana==5){
					//caso viernes
					$cast_id = $viernesCastId;
				}
				if($diaSemana==6) {
					// caso: sabado
					$cast_id = $sabadoCastId;
				} 
				
				// buscar
				$castTimes = $this->Appcasttime->find('all',array(
					'conditions'=>array('appcast_id' => $cast_id, 'Appcasttime.status' => $estadoActivo)
				));
				//debug($castTimes);
				// agregar
				foreach($castTimes as $castTime) {
					$this->create();
					$fechaYmd = date('Y-m-d', mktime(0, 0, 0, $mes, $dia, $anio));
					$dt['Agecitacalendariodia']['initDateTime'] = $fechaYmd.' '.$castTime['Appcasttime']['inittime'];
					$dt['Agecitacalendariodia']['endDateTime'] = $fechaYmd.' '.$castTime['Appcasttime']['endtime'];
					$dt['Agecitacalendariodia']['programed'] = $castTime['Appcasttime']['programed'];
					$dt['Agecitacalendariodia']['available'] = $castTime['Appcasttime']['programed'];
					$dt['Agecitacalendariodia']['agecitacalendario_id'] = $talcitacalendar_id;

					if (!$this->save($dt['Agecitacalendariodia'])) return false;
					
				}
				// incremento
				$dia++;
				$fecha = getDate(mktime(0, 0, 0, $mes, $dia, $anio));
			}// end while
		}// end for
		return true;
	}
	
	function getCitaCalendarioDia($arrayIdAgecitacalendariodia,$mes,$anio){
		$arrayIdAgecitacalendariodia = implode(",", $arrayIdAgecitacalendariodia);
		$sql = "select Agecitacalendariodia.agecitacalendario_id,sum(programed) as Programed,sum(available) as Available 
				FROM agecitacalendariodias Agecitacalendariodia where Agecitacalendariodia.agecitacalendario_id in($arrayIdAgecitacalendariodia) and 
				MONTH(Agecitacalendariodia.initDateTime) = $mes and YEAR(Agecitacalendariodia.initDateTime) = $anio and Agecitacalendariodia.estado = 'AC' 
				GROUP BY Agecitacalendariodia.agecitacalendario_id";
		$retu = $this->query($sql);
		return $retu;
	}
}
?>