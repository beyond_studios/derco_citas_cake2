<?php
class Agecitacalendario extends AppModel
{
	public $name = 'Agecitacalendario';   
	public $validate = array(
		'maximo_citasprogramado' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								),
						'numeric' =>array(
            					'rule'    => 'numeric',
								'message' => 'ingrese dato numerico'
								)  
					)
    );

	public $virtualFields = array(
	    'programadas' => 'maximo_citasprogramado'
	);
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
		
	public $belongsTo = array(
		'Agegrupo' => array(
			'className' => 'Agegrupo',
			'foreignKey' => 'agegrupo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Secproject' => array(
			'className' => 'Secproject',
			'foreignKey' => 'secproject_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Agemotivoservicio' => array(
			'className' => 'Agemotivoservicio',
			'foreignKey' => 'agemotivoservicio_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany = array(
		'Agecitacalendariodia' => array(
			'className' => 'Agecitacalendariodia',
			'foreignKey' => 'agecitacalendario_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);	
	
	function obtenerTaller($id) {
		$this->recursive = 1;
		$this->unbindModel(array(
				'hasMany'=>array('Agecitacalendariodia')
			));
		$citacalendar = $this->findById($id);
		return $citacalendar['Agemotivoservicio']['description'].' - '.$citacalendar['Agegrupo']['description'];
	}


	/**GENERA UNA CITA DE UN CALENDARIO
	 * AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * @param object $dt
	 * @param object $dtLog
	 * @return 
	 */
	function setCitaCalendario($dt, $dtLog){
		
		if(empty($dt['Agecitacalendario']['id'])){
			unset($dt['Agecitacalendario']['id']);
			
			$cita_db = $this->find('first', array(
				'conditions'=>array(
					'Agecitacalendario.secproject_id' => $dt['Agecitacalendario']['secproject_id'],
					'Agecitacalendario.agemotivoservicio_id' => $dt['Agecitacalendario']['agemotivoservicio_id'],
					'Agecitacalendario.agegrupo_id' => $dt['Agecitacalendario']['agegrupo_id'],
					'Agecitacalendario.status' => 'AC'
				),
				'recursive'=>'-1'
			));
			
			if(!empty($cita_db)) return array(false, 'TALGRUPOSSERVICIOS_MENSAJE_GRUPOS_ESTA_ACTIVADO_TALLER');
			
			$this->create();
			$cndId = "1=1";
		}else{
			// verificamos que no haya sido ingresado
			$cndId = sprintf("Agecitacalendario.id != '%s'", $dt['Agecitacalendario']['id']);
		}
		
		// verificamos que no haya sido ingresado
		$cita_db = $this->find('first', array(
			'conditions'=>array(
				'Agecitacalendario.secproject_id' => $dt['Agecitacalendario']['secproject_id'],
				'Agecitacalendario.agemotivoservicio_id' => $dt['Agecitacalendario']['agemotivoservicio_id'],
				'Agecitacalendario.agegrupo_id' => $dt['Agecitacalendario']['agegrupo_id'],
				'Agecitacalendario.status' => 'AC',
				'1'=>$cndId
			),
			'recursive'=>'-1'
		));
		
		if(!empty($cita_db)) return array(false, 'TALGRUPOSSERVICIOS_MENSAJE_GRUPOS_ESTA_ACTIVADO_TALLER');
				
		// procedemos a guardar
		if(!$this->save($dt)) return array(false, 'GENERAL_CORRIJA_ERRORES');
	
		return array(true, 'GENERAL_REGISTRO_AGREGADO', 'id'=>$this->getInsertId());
	}
	
	function obtenerAniosGeneradoPorCitaCalendar($idCalendar){
		app::import('Model', 'Agecitacalendariodia');		$this->Agecitacalendariodia = new Agecitacalendariodia;
		
		$aniosgenerado = $this->Agecitacalendariodia->find('all', array(
			'conditions'=>array('Agecitacalendariodia.agecitacalendario_id'=>$idCalendar,'Agecitacalendariodia.estado'=>'AC'),
			'recursive'=>-1,
			'fields'=>array('distinct year(Agecitacalendariodia.initDateTime) AS anios')		
		)); 
		
		$lista=array();
		foreach($aniosgenerado as $item=>$value){
			$lista[$value[0]['anios']]=$value[0]['anios'];
		}
		return $lista;
	}
	
	function obtenerProgramacionCitaTaller($calendarId, $fechaInicial, $fechaFinal) {
		$fechaInicial = $this->getDateFormatDB($fechaInicial).' 00:00:00';
		$fechaFinal = $this->getDateFormatDB($fechaFinal).' 23:59:59';
		$sql = "SELECT 
				Agecitacalendariodia.id, Agecitacalendariodia.initDateTime, Agecitacalendariodia.endDateTime, Agecitacalendariodia.available 
				FROM agecitacalendarios Agecitacalendario 
				INNER JOIN agecitacalendariodias Agecitacalendariodia 
				ON Agecitacalendario.id = Agecitacalendariodia.agecitacalendario_id 
				WHERE Agecitacalendariodia.available >= 0 
				AND Agecitacalendario.id = $calendarId
				AND Agecitacalendario.status = 'AC' AND Agecitacalendariodia.estado='AC'
				AND (Agecitacalendariodia.initDateTime BETWEEN '$fechaInicial' AND '$fechaFinal') 
				AND date_format(Agecitacalendariodia.endDateTime,'%Y-%m-%d %H:%i:%s') > '".$this->fechaHoraActual()."' 
				ORDER BY Agecitacalendariodia.initDateTime";
				
		$rs = $this->query($sql);
		return !empty($rs) ? $rs : array();
	}
	
	function obtenerTodosServicioPorGrupo($idGrupo,$idSucursal) {
		if(!empty($idGrupo) && !empty($idSucursal)){
			$sql = "SELECT Agecitacalendario.id,Agemotivoservicio.description
				FROM agecitacalendarios Agecitacalendario
				INNER JOIN Agemotivoservicios Agemotivoservicio
				ON Agecitacalendario.agemotivoservicio_id=Agemotivoservicio.id
				INNER JOIN agegrupos Agegrupo
				ON Agecitacalendario.agegrupo_id=Agegrupo.id
				INNER JOIN agegrupos_marcas AgegruposMarca
				ON AgegruposMarca.agegrupo_id=Agegrupo.id
				WHERE Agemotivoservicio.status='AC' AND AgegruposMarca.status='AC' AND Agegrupo.status='AC' 
				AND Agecitacalendario.status = 'AC' AND Agecitacalendario.id IN (
									SELECT Agecitacalendariodia.agecitacalendario_id
									FROM agecitacalendariodias Agecitacalendariodia
	  								WHERE year(Agecitacalendariodia.initDateTime) = year(now()) and Agecitacalendariodia.estado='AC'
								) AND Agecitacalendario.agegrupo_id=".$idGrupo." AND Agecitacalendario.secproject_id=".$idSucursal."";
			
			$servicios = $this->query($sql);

			foreach($servicios as $servicio) {
				$clave = $servicio['Agecitacalendario']['id'];
				$valor = $servicio['Agemotivoservicio']['description'];
				$lista[$clave] = $valor;
			}
		}
		return (!empty($lista) && isset($lista)) ? $lista : array();// 0=> no existe ningun calendario para taller de servicios
	}

	function obtenerTodosGruposPorSucursal($idSucursal) {
		if(!empty($idSucursal)){
			$sql = "SELECT Agegrupo.id,Agegrupo.description
				FROM agecitacalendarios Agecitacalendario
				INNER JOIN agemotivoservicios Agemotivoservicio
				ON Agecitacalendario.agemotivoservicio_id=Agemotivoservicio.id
				INNER JOIN agegrupos Agegrupo
				ON Agecitacalendario.agegrupo_id=Agegrupo.id
				INNER JOIN agegrupos_marcas AgegruposMarca
				ON AgegruposMarca.agegrupo_id=Agegrupo.id
				WHERE Agemotivoservicio.status='AC' AND AgegruposMarca.status='AC' AND Agegrupo.status='AC' 
				AND Agecitacalendario.status = 'AC' AND Agecitacalendario.id IN (
									SELECT Agecitacalendariodia.agecitacalendario_id
									FROM agecitacalendariodias Agecitacalendariodia
	  								WHERE year(Agecitacalendariodia.initDateTime) = year(now()) and Agecitacalendariodia.estado='AC'
								) AND Agecitacalendario.secproject_id=".$idSucursal."";
			
			$grupos = $this->query($sql);

			foreach($grupos as $grupo) {
				$clave = $grupo['Agegrupo']['id'];
				$valor = $grupo['Agegrupo']['description'];
				$lista[$clave] = $valor;
			}
		}
		return (!empty($lista) && isset($lista)) ? $lista : array();// 0=> no existe ningun calendario para taller de servicios
	}
	
	function getCalendarioAndMotivoServicioBySecproject($ArrayIdsSecproject){
		$ArrayIdsSecproject = implode(",", $ArrayIdsSecproject);
		$sql = "select Agecitacalendario.id, Secproject.id,Agemotivoservicio.id,Agemotivoservicio.description 
				from agecitacalendarios Agecitacalendario 
				inner join secprojects Secproject on(Agecitacalendario.secproject_id = Secproject.id)
				inner join agemotivoservicios Agemotivoservicio on(Agecitacalendario.agemotivoservicio_id = Agemotivoservicio.id)
				where Agecitacalendario.status = 'AC' and Secproject.id in($ArrayIdsSecproject) order by Agemotivoservicio.id asc";
		$retu = $this->query($sql);
		return $retu;
	}
	
}