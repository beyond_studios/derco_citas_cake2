<?php 
class Ccptipocliente extends AppModel{
	public $name='Ccptipocliente';	
	public $displayField = 'descripcion';
	//The Associations below have been created with all possible keys, those that are not needed can be removed		
	public $hasMany = array(
		'Ccpsolicitudservicio' => array(
			'className' => 'Ccpsolicitudservicio',
			'foreignKey' => 'ccptipocliente_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
}
?>