<?php echo $this->element('dialog_message'); ?>
<?php 
		echo $this->Html->css('modulo_taller/derco/derco_ventana.css');
		echo $this->Html->script('clientes/taller_index.js');	
?>

<?php echo $this->element('getBscVehicles'); ?>

<?php echo $this->Html->script('clientes/get_clientes.js'); ?>
<?php echo $this->Html->script('clientes/comun_clientes.js'); ?>
<?php echo $this->Html->script('Agedetallecitas/agregar_cita.js'); ?>


<div class="login width100">
	<h1 style="text-align: left;"><?php echo __('TALLER_TITULO_TALLER_CITA') ?></h1>
					
					
		<?php 
			echo $this->element('solicitud_agregar_cita', array(
				'action'=>array('controller'=>'agedetallecitas', 'action'=>'agregarCita'),
				'citarepogramarId'=>""
			)); 
		?>
				
		<?php echo $this->Form->create('Buscador', array('url'=>'listarProgramacionCitaTaller','class'=>'fomrLogin', 'id'=>'buscadorForm'));?>
		<?php echo $this->Form->hidden('agecitacalendario_id', array())?>
		<div class="titleBar styleGriss" style="text-align: left;">
			<i class="fa fa-calendar-o" aria-hidden="true"></i><b><?php echo __('VENDEDOR_ETIQUETA_CONSULTA_DIAS_HABILES')?></b>
		</div>
			<div class="formTable leftAling ">
				<div class="groupFields col4">
					
					<div class="field">
						<div>
							<label class="leftAling"><?php echo __('AGE_REPORTE_FECHA_DESDE');?></label>
						</div>
						<div>										
							<?php echo $this->Form->input('fechaInicial', array('class'=>'required msg_error_2 soloLectura','readonly'=>true ,'label'=>false, 'div'=>false))?>
							<span class="error"></span>
						</div>
					</div>

					<div class="field">
						<div>
							<label class="leftAling"><?php echo __('AGE_REPORTE_FECHA_HASTA');?></label>
						</div>
						<div>
							<?php echo $this->Form->input('fechaFinal', array('class'=>'required msg_error_2 soloLectura','readonly'=>true, 'label'=>false, 'div'=>false))?>
							<span class="error"></span>
						</div>
					</div>

					<div class="field">
						<div>
							<label class="leftAling"><?php echo __('TALLER_ETIQUETA_TALLER_SUCURSAL');?></label>
						</div>
						<div>
							<select id="Talcitacalendar2Id" class="required msg_error_1 styled-select slate"  onchange="getMotivoServicioBuscador(this);" name="data[Talcitacalendar2][id]">
								<option value=""><?php echo __('Seleccionar', true) ?></option>
								<?php foreach($talleres as $taller){
									echo "<option direccion=\"".utf8_encode($taller['Secproject']['address'])."\" value=\"".$taller['Secproject']['id']."\">".utf8_encode($taller['Secproject']['name'])."</option>";
								} ?>
							</select>
							<span class="error"></span>
						</div>
					</div>

					<div class="field">
						<div>
							<label class="leftAling"><?php echo __('AGE_MOTIVOSERVICIO_DESCRIPCION');?></label>
						</div>
						<div>
							<?php echo $this->Form->select('Talcitacalendar2.motivo_id', array(), array('empty'=>__('Seleccionar'), 'class'=>'required msg_error_1 styled-select slate'))?>
							<span class="error"></span>
						</div>
					</div>
					
					<div class='field' style='padding-top:26px; text-align:center; width:100%'>
						<?php echo $this->Form->button(__('GENERAL_BUSCAR'), array('class'=>'buscar buttomReset guinda', 'div'=>false));?>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		
		<div id="listaCaja" style='overflow: auto; '>
			<div style='max-width:600px;margin:0 auto; display:block'>
			<?php echo $this->Form->input('Talcitacalendar2.direccion', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false)); ?>
			</div>
			<div id="programacion" style="text-align: center;"></div>
			<span>
			</span>
			<div id="mensajeFinal" title="Mensaje" style="text-align: center;">
				<span id="mensajeFinaltexto"></span>
			</div>
			<div id="dialog_confirm" class='custom_table' title="Confirmar Datos de la Cita">
				<?php echo $this->Form->create('Confirm',array('class'=>'fomrLogin','id'=>'ConfirmAgregarCitaForm'));?>
				<?php $url2 = $this->Html->url(array('controller'=>'clientes','action'=>'bandejaReprogramar')); ?>
				<?php echo $this->Form->hidden('urlAfterSave',array('value'=>$url2))?>
				<?php echo $this->Form->hidden('clienteid') ?>
					<table style='width:100%' style="text-align: left;">
					<tr>
						<td class="etiqueta"><label class='rightAling'><?php echo __('AGE_DETALLE_CITA_CONFIRM_SR');?><span class="error"></span></label></td>
						<td class="valor"><label class='leftAling colorTexto' id = "ConfirmNombre"></label></td>
					</tr>
					<tr>
						<td class="etiqueta"><label class='rightAling'><?php echo __('AGE_DETALLE_CITA_CONFIRM_NUMERO_DOCUMENTO');?><span class="error"></span></label></td>
						<td class="valor"><label class='leftAling colorTexto' id = "ConfirmNumeroDocumento"></label></td>
					</tr>
					<tr>
						<td class="etiqueta"><label class='rightAling'><?php echo __('AGE_DETALLE_CITA_CONFIRM_PLACA');?><span class="error"></span></label></td>
						<td class="valor"><label class='leftAling colorTexto' id = "ConfirmPlaca"></label></td>
					</tr>
					<tr>
						<td class="etiqueta"><label class='rightAling'><?php echo __('AGE_DETALLE_CITA_CONFIRM_LOCAL');?><span class="error"></span></label></td>
						<td class="valor"><label class='leftAling colorTexto' id = "ConfirmLocal"></label></td>
					</tr>
					<tr>
						<td class="etiqueta"><label class='rightAling'><?php echo __('AGE_DETALLE_CITA_CONFIRM_FECHA_HORA_CITA');?><span class="error"></span></label></td>
						<td class="valor"><label class='leftAling colorTexto' id = "ConfirmFechaHoraCita"></label></td>
					</tr>
					<tr>
						<td class="etiqueta"><label class='rightAling'><?php echo __('AGE_DETALLE_CITA_CONFIRM_EMAIL');?><span class="error"></span></label></td>
						<td class="valor"><?php echo $this->Form->input('Email',array('label'=>false,'class'=>'required')); ?></td>
					</tr>
					<tr>
						<td class="etiqueta"><label class='rightAling'><?php echo __('AGE_DETALLE_CITA_CONFIRM_TELEFONO');?><span class="error"></span></label></td>
						<td class="valor"><?php echo $this->Form->input('Telefono',array('label'=>false,'class'=>'required')); ?></td>
					</tr>
					<tr>
						<td class="" colspan='2' style='text-align:center'>
							<?php echo $this->Form->submit(__('AGE_DETALLE_CITA_CONFIRM_ACEPTAR'), array('label'=>false,'class'=>'guinda guardar', 'id'=>'buttonConfirmSubmit', 'div'=>false));?>
							<?php echo $this->Form->button(__('AGE_DETALLE_CITA_CONFIRM_CANCELAR'), array('type'=>'button','class'=>'buttomReset cancelar', 'div'=>false, 'onclick'=>'closeDialog()'));?>
						</td>
					</tr>
					</table>
				<?php echo $this->Form->end(); ?>
			</div>
	</div> 
 	

		<div class="logosFooter">
			<img src="dist/images/basLogos.png">
		</div>
	</div>
	<div id="dialog_cliente" title="Buscar cliente">
		<div id="containerclientes">
			<table id="clientes"></table> 
			<div id="clientes-pager"></div>
		</div>
	</div>



</div>