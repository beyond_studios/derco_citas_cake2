<?php echo $this->element('dialog_message'); ?>
<?php 
		echo $this->Html->css('modulo_taller/derco/derco_ventana.css');
		echo $this->Html->script('jquery-validate/jquery.validate.js');
?>
<div class="login width100 reform">
<h1 style="text-align: left;"><?php echo __('ELIMINAR_CITA_CLIENTE');?></h1>
<div class="titleBar" style="text-align: left;">
			<i class="fa fa-user" aria-hidden="true"></i><b>DETALLE DE LA CITA</b>
		</div>
	<div class="tableData styleTable2">
			<?php 
				echo $this->element('solicitud_mostrar_cita',array('webcliente'=>null));
				echo $this->Form->create('Agedetallecita',array('id'=>'delete', 'url'=>"deleteClient/$detallecita_id", 'type'=>'post'));;
				echo $this->Form->hidden('Agedetallecita.id', array('value'=>$detallecita_id));
			?>
		<div class='field' style='padding-top:20px'>
			<div>
				<label class='leftAling'><?php echo __('comentarioEliminar',true); ?></label>
			</div>
			<div>
				<textarea name="data[Agedetallecita][deletecomment]" class="required msg_error_1" style="height: 50px;padding: 5px"><?php echo empty($dt_original)?"":trim($dt_original['Agedetallecita']['deletecomment']); ?></textarea>
				<span class="error"></span>
			</div>
		</div>
		<div style='text-align:center'>
			<?php echo $this->Form->submit(__('AGE_DETALLE_CITA_CONFIRM_ACEPTAR'), array('label'=>false,'div'=>false,'class'=>'guardar guinda', 'id'=>'buttonConfirmSubmit'));?>
			<?php echo "&nbsp;&nbsp;&nbsp;".$this->Form->button(__('AGE_DETALLE_CITA_CONFIRM_CANCELAR'), array('type'=>'button','div'=>false,'class'=>'buttomReset cancelar', 'div'=>false, 'onclick'=>'window.close();'));?>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>