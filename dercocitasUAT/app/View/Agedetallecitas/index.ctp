<?php echo $this->Html->script('Agedetallecitas/index.js'); ?>
<?php echo $this->Html->script('jqueryUI/jquery-ui-1.8.custom.min.js'); ?>
<?php echo $this->Html->css('theme/redmond/jquery-ui-1.7.2.custom.css'); ?> 
		
<?php echo $this->Session->flash();  ?>
<br/><h3 id="tituloTable"><?php echo __('Citas de Taller - Call Center');?></h3>
	
<?php echo $this->Form->create('bsc',array('url' => 'index', 'id'=>'f_bsc'));	?>
<div class="box">	
	<div style="width:30%;float:left;margin-bottom:3px !important;" class="box">
		<div style="padding-bottom:4px;margin-bottom:4px;border-bottom: 1px dashed #369;">
			<?php echo __('AGE_FECHA', TRUE).':&nbsp;&nbsp;' ?>
			<?php echo $this->Form->radio('f_campo',$f_campo, array('legend'=>false,'div'=>false, 'separator'=>'&nbsp;&nbsp;&nbsp;')); ?>
		</div>	
		
		<div style="width:25%;float:left;"><?php echo __('desde', TRUE) ?></div>
		<div style="width:65%;float:left;">
			<?php echo $this->Form->input('f_ini', array('value'=>null ,'id'=>'f_ini',  'div'=>false, 'label'=>false, 'readonly'=>true, 'style'=>'width:70%;')); ?>
			<?php echo $this->Html->image('derco/defecto/calendar_disabled.gif', array('style' => 'cursor: pointer;', 'onclick' => "javascript:$('#f_ini').val('');")); ?>
		</div>
		
		<div style="width:25%;float:left;"><?php echo __('hasta', TRUE) ?></div>
		<div style="width:65%;float:left;">
			<?php echo $this->Form->input('f_fin', array('value'=>null ,'id'=>'f_fin', 'div'=>false, 'label'=>false, 'readonly'=>true, 'style'=>'width:70%;')); ?>
			<?php echo $this->Html->image('derco/defecto/calendar_disabled.gif', array('style' => 'cursor: pointer;', 'onclick' => "javascript:$('#f_fin').val('');")); ?>
		</div>
		<div class="clear"></div>
	</div>
	
	<div style="width:67%;float:left;margin:0px 0px 3px 3px !important;height:81px; " class="box">
		<div style="width:100%">
			<div style="padding-bottom:4px;margin-bottom:4px;border-bottom: 1px dashed #369;">
				<div style="width:30%;float:left;"></div>
				<div style="width:70%;float:left;">
					<?php 	$url = $this->Html->url(array('action'=>'agregarCita'));  
							echo $this->Html->link($this->Html->image('agregar.png', array('alt' => 'Agregar')).__('Add', true), 'javascript:;',
											 array('onclick' => "add('".$url."')",'escape'=>false), null); 
														
							$iconoExportarExcel=$this->Xhtml->image('derco/defecto/exel.gif',array(
								'title'=>__('REPORTE_EXPORTAR_EXCEL'), 'border'=>'0','width'=>'16px','height'=>'16px'));
							echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$this->Html->link($iconoExportarExcel."Exel", 'javascript:;',
            					array('onclick'=>'detCitaExel(this);', 'escape'=>false, 'url_exel'=>$this->Html->url('getIndexExel')),
            					null,false
							);
							?>&nbsp;<?php
							$iconoExportarExcel=$this->Xhtml->image('derco/defecto/exel.gif',array(
								'title'=>__('excelReporteResumen'), 'border'=>'0','width'=>'16px','height'=>'16px'));
							echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$this->Html->link($iconoExportarExcel."Exel Resumen", 'javascript:;',
            					array('onclick'=>'newCitaExel(this);', 'escape'=>false, 'url_exel'=>$this->Html->url('getExcelResumen')),
            					null,false
							); 
					?>
				</div>
				<div class="clear"></div>
			</div>
			
			<!-- CRITERIOS DINAMICOS -->
			<div style="float:left;width:100%;">
				<div style="width:30%;float:left;"><?php echo __('Estado', TRUE) ?></div>
				<div style="width:50%;float:left;">
					<?php echo $this->Form->input('std',array('type'=>'select', 'options'=>$std, 'label' => false, 'empty'=>__('-- Todos --',true), 'div' => false,'style'=>'width:50%;')); ?>
				</div>
			</div>		
			<div style="float:left;width:100%;">	
				<div style="width:30%;float:left;">
					<?php echo $this->Form->input('crt',array('type'=>'select', 'options'=>$crt, 'label' => false, 'div' => false,'style'=>'width:90%;')); ?>
				</div>
				<div style="width:50%;float:left;"><?php echo $this->Form->text('vlr', array('style'=>'width:100%;')); ?></div>
				<div style="width:20%;float:left;text-align:center;"><?php echo $this->Form->submit(__('Search',true),array('style'=>'width:90%;','div'=>false)); ?></div>
			</div>
			
		</div>
	</div>
	<div class="clear"></div>
</div>	
<?php echo $this->Form->end(); ?>
		
<table cellpadding="0" cellspacing="0" class="table" >
	<thead>
	<tr>                     
		<th class="span-3"><?php echo $this->Paginator->sort('Secproject.name',__('Sucursal',true));?></th> 
        <th><?php echo $this->Paginator->sort('Agedetallecita.fechadecita',__('Fecha de la Cita'));?></th>
        <th><?php echo $this->Paginator->sort(__('Hora cita',true));?></th>
        <th class="span-4"><?php echo $this->Paginator->sort(__('Apallidos y Nombres / Razón social',true));?></th>
        <th><?php echo $this->Paginator->sort(__('Marca',true));?></th>
        <th><?php echo $this->Paginator->sort(__('Modelo',true));?></th>
		<th>
			<?php echo $this->Paginator->sort('Agemotivoservicio.description',__('Motivo Servicio',true));?>
		</th>
		<th><?php echo $this->Paginator->sort('Agedetallecita.placa',__('Placa',true));?></th>        
		<th><?php echo $this->Paginator->sort(__('Teléfono',true));?></th>        
		<th><?php echo $this->Paginator->sort('Agedetallecita.fechaRegistro',__('Fecha de Registro'));?></th>
		<th><?php echo __('estado',true);?></th>
		<th class="actionsfijo"><?php  echo __('Actions');?></th>
	
	</tr>			
	</thead>
	
	<tbody>
	<?php  foreach ($agedetallecitas as $agedetallecita):?>
	<tr>
		<td>
			<?php 	echo $agedetallecita['Secproject']['name']; ?>	
		</td>


        <td>	
			<?php 	$url = $this->Html->url(array('controller'=>'Agedetallecitas','action'=>'mostrarCita',$agedetallecita['Agedetallecita']['id'])); 
						echo $this->Html->link($this->Xhtml->date_dmY($agedetallecita['Agedetallecita']['fechadecita']), 'javascript:;',array('onclick' => "mostrar('".$url."')",'escape'=>false), null);; ?>
		</td>


        <td>
			<?php 	echo date("H:i",strtotime($agedetallecita['Agedetallecita']['fechadecita'])); ?>	
		</td>

        <td>
			<?php 	if(!empty($agedetallecita['Cliente']['nombres']))
						echo $agedetallecita['Cliente']['nombres']; 					
					else
						echo $agedetallecita['Cliente']['razonSocial']; ?>	
		</td>


        <td>
			<?php 	echo $agedetallecita['Agedetallecita']['marca']; ?>	
		</td>

        				<td>
			<?php 	echo $agedetallecita['Agedetallecita']['modelo']; ?>	
		</td>

		<td>
			<?php 	echo $agedetallecita['Agemotivoservicio']['description']; ?>	
		</td>

		<td>
			<?php 	echo $agedetallecita['Agedetallecita']['placa']; ?>	
		</td>
		
		<td>
			<?php 	echo $agedetallecita['Cliente']['telefono']; ?>	
		</td>
		<td>
			<?php 	echo $this->Xhtml->date_dmY($agedetallecita['Agedetallecita']['fechaRegistro']); ?>	
		</td>
		<td class="textc">
			<?php echo $this->Xhtml->getStatus($agedetallecita['Agedetallecita']['estado']); ?>
		</td>
		<td class="actionsfijo">
		<?php
			if(!empty($agedetallecita['Agedetallecita']['con_cita']) && in_array($agedetallecita['Agedetallecita']['estado'],array('AC'))){
				$url = $this->Html->url(array('action'=>'delete/'.$agedetallecita['Agedetallecita']['id']));  
				echo $this->Html->link($this->Html->image('eliminar.png', array('alt' => 'Eliminar', 'title'=>'Eliminar')),
								 'javascript:;',
								 array('onclick' => "mostrar('".$url."')",'escape'=>false),
								 null); 
				
				
				$this->Html->link(
					$this->Html->image('eliminar.png', array('title'=>__('desactivar',true), "alt" => "Eliminar")), 
					array('action'=>'delete', $agedetallecita['Agedetallecita']['id']), array('escape'=>false), sprintf(__('estaSeguroEliminar %s?', $agedetallecita['Agedetallecita']['id']))
				);
			
				echo "&nbsp;";	
				$url = $this->Html->url(array('action'=>'reprogramarCita/'.$agedetallecita['Agedetallecita']['id']));  
				echo $this->Html->link($this->Html->image('editar.png', array('alt' => 'Reprogramar', 'title'=>'Reprogramar')),
								 'javascript:;',
								 array('onclick' => "add('".$url."')",'escape'=>false),
								 null); 
			}	
			
			echo "&nbsp;";	
				$url = $this->Html->url(array('action'=>'mostrarCita/'.$agedetallecita['Agedetallecita']['id']));  
				echo $this->Html->link($this->Html->image('mostrar.png', array('alt' => 'Ver', 'title'=>'Ver')),
								 'javascript:;',
								 array('onclick' => "mostrar('".$url."')",'escape'=>false),
								 null); 	 
		?>	
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
</table>

<div id ="paging" class="span-18">
    <?php echo $this->element('paginador'); ?>
</div>
<div class="clear"></div>