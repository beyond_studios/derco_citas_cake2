<?php echo $this->Html->script('Chevrolets/index.js'); ?>
<?php echo $this->Html->script('jqueryUI/jquery-ui-1.8.custom.min.js'); ?>
<?php echo $this->Html->css('theme/redmond/jquery-ui-1.7.2.custom.css'); ?> 
		
<?php echo $this->Session->flash();  ?>
<br/><h3 id="tituloTable"><?php echo __('VALIDACION CHEVROLET');?></h3>
	
<?php echo $this->Form->create('bsc',array('url' => 'index', 'id'=>'f_bsc'));	?>
<div class="box">	
	<div style="width:30%;float:left;margin-bottom:3px !important;" class="box">
		<div style="padding-bottom:4px;margin-bottom:4px;border-bottom: 1px dashed #369;">
			<?php echo __('AGE_FECHA', TRUE).':&nbsp;&nbsp;' ?>
			<?php echo $this->Form->radio('f_campo',$f_campo, array('legend'=>false,'div'=>false, 'separator'=>'&nbsp;&nbsp;&nbsp;')); ?>
		</div>	
		
		<div style="width:25%;float:left;"><?php echo __('desde', TRUE) ?></div>
		<div style="width:65%;float:left;">
			<?php echo $this->Form->input('f_ini', array('value'=>null ,'id'=>'f_ini',  'div'=>false, 'label'=>false, 'readonly'=>true, 'style'=>'width:70%;')); ?>
			<?php echo $this->Html->image('derco/defecto/calendar_disabled.gif', array('style' => 'cursor: pointer;', 'onclick' => "javascript:$('#f_ini').val('');")); ?>
		</div>
		
		<div style="width:25%;float:left;"><?php  ?></div>
		<div style="width:65%;float:left;">
			<?php echo $this->Form->input('f_fin', array('class'=>'hide','value'=>null ,'id'=>'f_fin', 'div'=>false, 'label'=>false, 'readonly'=>true, 'style'=>'width:70%;')); ?>
			<?php //echo $this->Html->image('derco/defecto/calendar_disabled.gif', array('style' => 'cursor: pointer;', 'onclick' => "javascript:$('#f_fin').val('');")); ?>
		</div>
		<div class="clear"></div>
	</div>
	
	<div style="width:67%;float:left;margin:0px 0px 3px 3px !important; " class="box">
		<div style="width:100%">
			<div style="padding-bottom:4px;margin-bottom:4px;border-bottom: 1px dashed #369;">
				<div style="width:30%;float:left;"></div>
				<div style="width:70%;float:left;">
					<?php 	echo '<br/>'
					?>
				</div>
				<div class="clear"></div>
			</div>
			
			<!-- CRITERIOS DINAMICOS -->
			<div style="float:left;width:100%;">
				<div style="width:30%;float:left;"><?php  ?></div>
				<div style="width:50%;float:left;">
					<?php  ?>
				</div>
			</div>		
			<div style="float:left;width:100%;">	
				<div style="width:30%;float:left;">
					<?php echo $this->Form->input('crt',array('type'=>'select', 'options'=>$crt, 'label' => false, 'div' => false,'style'=>'width:90%;')); ?>
				</div>
				<div style="width:50%;float:left;"><?php echo $this->Form->text('vlr', array('style'=>'width:100%;')); ?></div>
				<div style="width:20%;float:left;text-align:center;"><?php echo $this->Form->submit(__('Search',true),array('style'=>'width:90%;','div'=>false)); ?></div>
			</div>
			
		</div>
	</div>
	<div class="clear"></div>
</div>	
<?php echo $this->Form->end(); ?>
		
<table cellpadding="0" cellspacing="0" class="table" >
	<thead>
	<tr>                     
		<th><?php echo $this->Paginator->sort('Chevrolet.placa',__('Placa',true));?></th> 
		<th><?php echo $this->Paginator->sort('Chevrolet.vim',__('VIN'));?></th>
		<th><?php echo $this->Paginator->sort('Chevrolet.fecha_base',__('Fecha Base'));?></th>
		<th><?php echo __('Mensaje',true);?></th>
	</tr>			
	</thead>
	
	<tbody>
	<?php if(!empty($msg)){
		echo "<tr><td colspan=4  style='text-align:center'>".$msg['msg']."</td></tr>";
		$chevrolets = array();
	}elseif(empty($chevrolets) && ($this->data['bsc']['crt'] == 'vim') && !empty($this->data['bsc']['vlr'])){
		echo "<tr><td colspan=4  style='text-align:center'>El VIN que esta consultando no se encuentra en el sistema. El cliente es el primer propietario de la unidad, consultar fecha de inicio según la tarjeta de propiedad</td></tr>";
	}elseif(empty($chevrolets) && ($this->data['bsc']['crt'] == 'placa') && !empty($this->data['bsc']['vlr'])){
		echo "<tr><td colspan=4 style='text-align:center'>El PLACA que esta consultando no se encuentra en el sistema. Validar vigencia de garantía con el VIN</td></tr>";
	}
	?>	
	<?php  foreach ($chevrolets as $chevrolet):?>
	<tr>
		<td style="text-align:center"><?php 	echo $chevrolet['Chevrolet']['placa']; ?></td>
		<td style="text-align:center"><?php 	echo $chevrolet['Chevrolet']['vim']; ?></td>
		<td style="text-align:center"><?php echo $this->Avhtml->date_dmY($chevrolet['Chevrolet']['fecha_base']); ?></td>
		<td>
			<?php echo $chevrolet['Chevrolet']['msg']; ?>	
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
</table>

<div id ="paging" class="span-18">
    <?php if(empty($msg)){
		echo $this->element('paginador');
	}
	?>
</div>
<div class="clear"></div>
<?php // echo $this->element('sql_dump'); ?>