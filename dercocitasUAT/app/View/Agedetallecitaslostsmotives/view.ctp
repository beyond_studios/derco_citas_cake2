<div class="span-9" > 
	<?php echo $this->Session->flash();?>
	<br/>
	<table cellpadding="0" cellspacing="0" class="table">
		<thead>
			<tr>
				<th colspan="2" scope="col">
					<?php echo __('MOTIVO_CITA_NO_ATENDIDA');?>
				</th>
			</tr>
		</thead>
		<tbody>

			<tr>
		 		<td><label><?php echo __('descripcion') ?></label> </td>
		 		<td><?php echo $agedetallecitaslostsmotive['Agedetallecitaslostsmotive']['description']; ?></td>
		 	</tr>
			<tr>
		 		<td><label><?php echo __('estado'); ?></label> </td>
		 		<td><?php echo $agedetallecitaslostsmotive['Agedetallecitaslostsmotive']['status'] == 'AC' ? 
							__('Enable',true)
						:
							($agedetallecitaslostsmotive['Agedetallecitaslostsmotive']['status'] == 'DE'?
								__('Disable',true)
							:
								__('Limited',true))
						; ?></td>
		 	</tr>
		</tbody>
	</table>	
	<br/>
	<div class="span-9 botones" > 
	<?php echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
	<?php echo $this->Form->end();?>
	<?php echo $this->element('actualizar_ver'); ?>
	</div>
</div>