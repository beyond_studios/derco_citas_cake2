<h2><?php echo __('ACTA_TITULO');?></h2>
<br/>
<fieldset>
	<legend><?php echo __('Historial_de_actas'); ?></legend>
	<table id="listaPrincipal">
		<thead>
			<tr>
				<!-- <th><?php echo __('ACTA_CLASIFICACION'); ?></th> -->
				<th><?php echo __('ACTA_FECHA'); ?></th>
				<th><?php echo __('ACTA_HORA'); ?></th>
				<th><?php echo __('CCP_TALACTAS_USUARIO'); ?></th>
				<th><?php echo __('ACTA_DESCRIPCION'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($actas as $acta): ?>
			<tr>
				<td><?php echo substr($acta['Talacta']['fechaUsuario'], 0,10); ?></td>
				<td><?php echo substr($acta['Talacta']['fechaUsuario'], 10,6); ?></td>
				<td><?php echo $acta[0]['Usuario'] ?></td>
				<td><?php echo $acta['Talacta']['descripcion'] ?></td>
			</tr>
			<?php endforeach; ?>
			<tr><td colspan=4>&nbsp;</td></tr>
		</tbody>		
	</table>	
</fieldset>
<?php echo $this->element('actualizar_padre'); ?>
<div style="text-align:center;">
	<?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar', 'onclick'=>'javascript:window.close()'));?>
</div>

<script type="text/javascript">
	colorearTabla('listaPrincipal');
</script>