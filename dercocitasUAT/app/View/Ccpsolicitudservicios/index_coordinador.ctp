<?php echo $this->Html->script('jqueryUI/jquery-ui-1.8.custom.min.js'); ?>
<?php echo $this->Html->css('theme/redmond/jquery-ui-1.7.2.custom.css'); ?> 
<?php echo $this->Html->css('modulo_taller/tooltip.css'); ?> 
<?php echo $this->Html->script('ccpsolicitudservicios/index.js'); ?>

<?php echo $this->Session->flash();  ?>
<br/><h3 id="tituloTable"><?php echo __('SOLICITUD_DE_SERVICIO');?></h3>

<!-- IMPLEMENTACION DEL BUSCADOR -->
<form id="principalForm" method="get" action="<?php echo $this->Html->url('indexCoordinador') ?>">
<div id="buscadorBasico_" class="box">	
	<table id="formBuscador" border="0" cellspacing="0" cellpadding="0" align="center" style="width:100%">
		<input type="hidden" size=15 name="click" id="formBuscadorClick" value="<?php echo empty($data['click'])?'':$data['click']; ?>" size="10"/>
		<tbody>
        	<tr>
            	<td class="etiqueta"><?php echo __('FechaSol').':';?>&nbsp;<?php echo $this->Form->label('formBuscador.fechaIni', __('Desde'));?></td>
            	<td class="valor">
            		<?php echo $this->Form->input('fechaIni', array('name'=>'fechaIni','value'=>empty($data['fechaIni'])?'':$data['fechaIni'] ,'id'=>'f_ini',  'div'=>false, 'label'=>false, 'readonly'=>true, 'style'=>'width:70%;')); ?>
					<?php echo $this->Html->image('derco/defecto/calendar_disabled.gif', array('style' => 'cursor: pointer;', 'onclick' => "javascript:$('#f_ini').val('');")); ?>
            	</td>
				
            	<td class="etiqueta"><?php echo $this->Form->label('formBuscador.fechaFin', __('Hasta'));?></td>
				<td class="valor">
                	<?php echo $this->Form->input('fechaFin', array('name'=>'fechaFin','value'=>empty($data['fechaFin'])?'':$data['fechaFin'] ,'id'=>'f_fin', 'div'=>false, 'label'=>false, 'readonly'=>true, 'style'=>'width:70%;')); ?>
					<?php echo $this->Html->image('derco/defecto/calendar_disabled.gif', array('style' => 'cursor: pointer;', 'onclick' => "javascript:$('#f_fin').val('');")); ?>
            	</td>
				
				<td class="etiqueta"><?php echo $this->Form->label('formBuscador.ccptiposervicio_id', __('Tipo_servicio'));?></td>
				<td  class="valor">
					<select id="formBuscadorCcptiposervicioId" value="<?php echo empty($data['ccptiposervicio_id'])?'':$data['ccptiposervicio_id']; ?>" style="width:125px" name="ccptiposervicio_id">
						<?php foreach($ccptiposervicios as $id => $value): 
							if(empty($data)) echo '<option value="'.$id.'">'.$value.'</option>';
							else{
								if($data['ccptiposervicio_id'] != $id) echo '<option value="'.$id.'">'.($value).'</option>';
								else echo '<option selected="selected" value="'.$id.'">'.($value).'</option>';
							}
						endforeach; ?>
					</select>					
				</td>
				
				<td class="etiqueta"><?php echo $this->Form->label('formBuscador.ccpsolicitudservicioestado_id', __('estadoSolicitud'));?></td>
				<td  class="valor">
					<select id="formBuscadorCcpsolicitudservicioestadoId" value="<?php echo empty($data['ccpsolicitudservicioestado_id'])?'':$data['ccpsolicitudservicioestado_id']; ?>" style="width:125px" name="ccpsolicitudservicioestado_id">
						<?php foreach($ccpsolicitudservicioestados as $id => $value): 
							if(empty($data['ccpsolicitudservicioestado_id'])) echo '<option value="'.$id.'">'.$value.'</option>';
							else{
								if($data['ccpsolicitudservicioestado_id'] != $id) echo '<option value="'.$id.'">'.$value.'</option>';
								else echo '<option selected="selected" value="'.$id.'">'.$value.'</option>';
							}
						endforeach; ?>
					</select>					
				</td>
			<td><?php echo $this->Form->submit(__('Search',true),array('style'=>'width:90%;','div'=>false)); ?></td>	
			</tr>
			
			<tr>
				<td class="etiqueta"><?php echo __('FechaOt').':';?>&nbsp;<?php echo $this->Form->label('formBuscador.fechaIniOt', __('Desde'));?></td>
            	<td class="valor">
            		<?php echo $this->Form->input('fechaIniOt', array('name'=>'fechaIniOt','value'=>empty($data['fechaIniOt'])?'':$data['fechaIniOt'] ,'id'=>'f_ini_2',  'div'=>false, 'label'=>false, 'readonly'=>true, 'style'=>'width:70%;')); ?>
					<?php echo $this->Html->image('derco/defecto/calendar_disabled.gif', array('style' => 'cursor: pointer;', 'onclick' => "javascript:$('#f_ini_2').val('');")); ?>
            	</td>
				
				
            	<td class="etiqueta"><?php echo $this->Form->label('formBuscador.fechaFinOt', __('Hasta'));?></td>
            	<td class="valor">
                	<?php echo $this->Form->input('fechaFinOt', array('name'=>'fechaFinOt','value'=>empty($data['fechaFinOt'])?'':$data['fechaFinOt'] ,'id'=>'f_fin_2', 'div'=>false, 'label'=>false, 'readonly'=>true, 'style'=>'width:70%;')); ?>
					<?php echo $this->Html->image('derco/defecto/calendar_disabled.gif', array('style' => 'cursor: pointer;', 'onclick' => "javascript:$('#f_fin_2').val('');")); ?>
            	</td>
				
				<td class="etiqueta"><?php echo $this->Form->label('formBuscador.nrocars', __('Nro_OT'));?></td>
            	<td class="valor">
            		<input type="text" size=15 name="nrocars" id="formBuscadorNrocars" value="<?php echo empty($data['nrocars'])?'':$data['nrocars']; ?>" size="10"/>
            	</td>
				
				<td class="etiqueta"><?php echo $this->Form->label('formBuscador.carsestado_id', __('CCP_ESTADO_OT_CARS'));?></td>
				<td  class="valor">
					<select id="formBuscadorCarsestadoId" value="<?php echo empty($data['carsestado_id'])?'':$data['carsestado_id']; ?>" style="width:125px" name="carsestado_id">
						<?php foreach($estadosOtCarsSelect as $id => $value): 
							if(empty($data['carsestado_id'])) echo '<option value="'.$id.'">'.$value.'</option>';
							else{
								if($data['carsestado_id'] != $id) echo '<option value="'.$id.'">'.utf8_encode($value).'</option>';
								else echo '<option selected="selected" value="'.$id.'">'.utf8_encode($value).'</option>';
							}
						endforeach; ?>
					</select>					
				</td>
				</td>
				<td>
					<?php 
						//Exportar para Excel
			    		$iconoExportarExcel = $this->Xhtml->image('derco/defecto/MicrosoftExcelDocument.gif', array(
			    			'title'=>__('REPORTE_EXPORTAR_EXCEL'), 
							'border'=>'0',
							'width'=>'16px',
							'height'=>'16px'
						));
						if(!empty($otsCars)){
						echo $this->Html->link($iconoExportarExcel, 'javascript:;', array(
								'onmouseover' => "return cambiarStatusMsg('".__('REPORTE_EXPORTAR_EXCEL')."');", 
								'onmouseout' => "return cambiarStatusMsg('');", 
								'onclick'=>"exportarExcel()", 'escape'=>false), null, false);
						}
						?>				
				</td>	
			</tr>
			
			<tr>
				<td class="etiqueta"><?php //echo $this->Form->label('bsc.nrocars', __('Nro_OT'));?></td>
            	<td class="valor"> <?php //echo $this->Form->text('nrocars', array('style'=>'width:100%;')); ?></td>
				
				<td class="etiqueta"><?php //echo $this->Form->label('bsc.nrocars', __('Nro_OT'));?></td>
            	<td class="valor"> <?php //echo $this->Form->text('nrocars', array('style'=>'width:100%;')); ?></td>
				
				<!-- buscador por placa -->
				<td class="etiqueta"><?php echo $this->Form->label('formBuscador.placa', __('CCP_PLACA'));?></td>
            	<td class="valor">
            		<input type="text" size=15 name="placa" id="formBuscadorPlaca" value="<?php echo empty($data['placa'])?'':$data['placa']; ?>" size="10"/>
            	</td>	
				
				<!-- buscador por estado -->
				<td class="etiqueta"><?php echo $this->Form->label('formBuscador.estadootccp_id', __('CCP_ESTADO_OT_CCP'));?></td>
            	<td class="valor">
					<select id="formBuscadorEstadootccpId" value="<?php echo (!empty($data['estadootccp_id']) && isset($data['estadootccp_id']))?(($data['estadootccp_id']=='9,10,22')?'':$data['estadootccp_id']):''; ?>" style="width:125px" name="estadootccp_id">
						<?php foreach($estadosCcpSelect as $id => $value): 
							if(empty($data['estadootccp_id'])) echo '<option value="'.$id.'">'.$value.'</option>';
							else{
								if($data['estadootccp_id'] != $id) echo '<option value="'.$id.'">'.$value.'</option>';
								elseif($data['estadootccp_id']=='9,10,22') echo '<option value="'.$id.'">'.$value.'</option>';
									else echo '<option selected="selected" value="'.$id.'">'.$value.'</option>';
							}
						endforeach; ?>
					</select>							
            	</td>
				<td>
				</td>
			</tr>
		</tbody>
	</table>
	<div class="clear"></div>
</div>	
<?php echo $this->Form->end(); ?>

<!-- FIN - IMPLEMENTACION DEL BUSCADOR -->
<div id="listaCaja">
	<table id="listaPrincipal" cellpadding="0" cellspacing="0">
		<thead>
		    <tr>
				<th><?php echo $this->Paginator->sort('Ccpsolicitudservicio.ot_numero', __('Nro_Ot'))?></th>
				<th><?php echo $this->Paginator->sort('Ccpsolicitudservicio.ot_cliente', __('Cliente'))?></th>
				<th><?php echo $this->Paginator->sort('Ccpsolicitudservicio.ot_placa', __('CCP_PLACA'))?></th>
			
				<th><?php echo $this->Paginator->sort('Ccpsolicitudservicio.ot_fecha_creacion', __('fechaAperturaOt'))?></th>
				<th><?php echo $this->Paginator->sort('Ccpsolicitudservicio.presupuesto_fecha_aprobacion', __('Fecha_aprobacion_presupuesto'))?></th>
				
				<th><?php echo $this->Paginator->sort('Ccpsolicitudservicio.horas_planchado', __('horasPlanchado'))?></th>
				<th><?php echo $this->Paginator->sort('Ccpsolicitudservicio.horas_panio', __('horasPanio'))?></th>
				<th><?php echo $this->Paginator->sort('Ccpsolicitudservicio.fecha_emitido', __('Fecha_solicitud'))?></th>
			
				<th><?php echo $this->Paginator->sort('Ccptiposervicio.descripcion', __('Tipo_servicio'))?></th>
				<th><?php echo $this->Paginator->sort('Secperson.lastName', __('Asesor'))?></th>
				<th><?php echo __('CCP_ESTADO_OT_CCP')?></th>
				<th><?php echo __('GENERAL_ACCIONES')?></th>
		    </tr>
		</thead>
		<tbody>
			<?php $i=0;?>
		    <?php foreach ($otsCars as $solicitud_servicio): ?> 
		    <tr>
		        <td class="texto">
		        	<?php $i++; ?>
		        	<?php 
						if($i>15){
							$onmouseover = "tooltipArriba";
						}elseif($i>10){
							$onmouseover = "tooltipMedio";
						} else{
							$onmouseover = "tooltipAbajo";
						}
					?>
					
		        	<div class="ToolText" href="#"><?php  echo $solicitud_servicio['Ccpsolicitudservicio']['ot_numero']; ?></div>
					<div class="<?php echo "tooltip $onmouseover" ?>">
						<?php if(!empty($solicitud_servicio['Talot']['id'])): ?>
						<span id='<?php echo $solicitud_servicio['Ccpsolicitudservicio']['ot_numero'].'TTP'?>' style="margin: 5px; background-color: red;">  
							
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td class="detalles etiqueta" width="63px" style="border: hidden;"><?php echo __('OT_CAMPO_PROPIETARIO')?></td>
										<td class="detalles valor" width="285px" style="border: hidden;"><?php echo ($solicitud_servicio['Talot']['propietario']); ?></td>
										<td class="detalles etiqueta" width="66px" style="border: hidden;"><?php echo __('OT_CAMPO_FECHA_ORDEN')?></td>
										<td class="detalles valor" width="100px" style="border: hidden;"><?php echo substr($solicitud_servicio['Talot']['ordenfecha'],0,10); ?> <?php echo $solicitud_servicio['Talot']['ordenhora']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_ETIQUETA_RESPONSABLE_ASESOR')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $solicitud_servicio['Talot']['codasesor']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_ETIQUETA_FECHA_A_ENTREGA')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $solicitud_servicio['Talot']['fechaprometida']; ?> 
										<?php  echo $solicitud_servicio['Talot']['horaprometida']; ?>
										</td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;">
											<?php echo __('OT_CAMPO_DESCRIPCION')?>
											<b style="color: yellow;"><?php echo $estadosOtCarsTooltip[$solicitud_servicio['Talot']['estado']]; ?></b>
										</td>
										<td class="detalles valor" colspan="3" style="padding-bottom: 5px;">
											<div class="div_textarea" style="padding: 2px; overflow: auto; height: 90px; width: 405px;">
												<?php echo utf8_encode(trim($solicitud_servicio['Talot']['descripcion'])); ?>
							            	</div>
										</td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_MARCA')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $solicitud_servicio['Talot']['marca']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_PLACA')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $solicitud_servicio['Talot']['placa']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_MODELO')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $solicitud_servicio['Talot']['modelo']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_NROCARS')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $solicitud_servicio['Talot']['nrocars']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_VERSION')?></td>
										<td class="detalles valor" style="border: hidden;"><?php //echo $solicitud_servicio['Talot']['version']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_NROCONO')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $solicitud_servicio['Talot']['nrocono']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('Tipo_OT')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $solicitud_servicio['Talot']['tipoorden']; ?></td>
									
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('CCPSOLICITUD_TABLERO_ETIQUETA_CHASIS')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $solicitud_servicio['Talot']['chassis']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"></td>
										<td class="detalles valor" style="border: hidden;">
											<?php //echo $xhtml->button(__('MAQUINARIAS_ENVIAR_A_ENTREGA'), array('onclick'=>'enviar_a_entrega('.$solicitud_servicio['Talot']['id'].', "'.$solicitud_servicio['Talot']['nrocars'].'")'));?>							
										</td>
									</tr>
								</tbody>
							</table>
							
						</span>
						<?php endif; ?>					
					</div>
		        </td>
		        <td class="texto"><?php echo ($solicitud_servicio['Ccpsolicitudservicio']['ot_cliente']); ?></td>
		        <td class="texto centrado"><?php echo $solicitud_servicio['Ccpsolicitudservicio']['ot_placa']; ?></td>
			
		        <td class="texto centrado" style="background:<?php echo $solicitud_servicio['Ccpsolicitudservicio']['color']?>"><?php echo substr($solicitud_servicio['Ccpsolicitudservicio']['ot_fecha_creacion'],0,10); ?></td>
		        <td class="texto centrado"><?php echo date('d-m-Y',strtotime($solicitud_servicio['Ccpsolicitudservicio']['fechaaprobacioncompania'])); ?></td>
				
				
		        <td class="centrado"><?php echo $solicitud_servicio['Ccpsolicitudservicio']['horas_planchado']; ?></td>
		        <td class="centrado"><?php echo $solicitud_servicio['Ccpsolicitudservicio']['horas_panio']; ?></td>
				
				<td class="texto centrado"><?php echo substr($solicitud_servicio['Ccpsolicitudservicio']['fecha_emitido'],0,10); ?></td>
				
		        <td class="centrado"><?php echo ($solicitud_servicio['Ccptiposervicio']['descripcion']); ?></td>
		        <td class="texto"><?php echo $solicitud_servicio['Ccpsolicitudservicio']['asesor']; ?></td>
		        <td class="centrado">
					<?php
						if(!empty($solicitud_servicio['Ccpsolicitudservicio'])){
							if($solicitud_servicio['Ccptabhistorial']['ccptabestado_id']==3){
								if($solicitud_servicio['Ccpsolicitudservicioestado']['id']==3 || $solicitud_servicio['Ccpsolicitudservicioestado']['id']==4 || $solicitud_servicio['Ccpsolicitudservicioestado']['id']==5){
									echo empty($solicitud_servicio['Ccptabhistorial'])?'':$solicitud_servicio['Ccptabhistorial']['descripcion'].' - '.$solicitud_servicio['Ccpsolicitudservicioestado']['descripcion'];
								}else echo empty($solicitud_servicio['Ccptabhistorial'])?'':$solicitud_servicio['Ccptabhistorial']['descripcion'];
							}else{
								if($solicitud_servicio['Ccpsolicitudservicioestado']['id']==5){
									echo 'Con Solicitud - Rechazado';
								}else echo $solicitud_servicio['Ccptabhistorial']['descripcion'];
							}
						} else echo empty($solicitud_servicio['Ccptabhistorial'])?'':$solicitud_servicio['Ccptabhistorial']['descripcion'];
					?>
				</td>
		        <td class="accion">
		          	
					<?php 
							echo $this->Xhtml->imagelink('derco/defecto/modificar.png', __('Crear_acta'),
					                	'javascript:;',array('onclick'=>"crearActa('".$solicitud_servicio['Ccpsolicitudservicio']['ot_numero']."')"),
					                	null, array('width'=>'16'));
							echo $this->Xhtml->imagelink('derco/defecto/mostrar.png',__('GENERAL_MOSTRAR'),
		                	'javascript:;',array('onclick'=>"mostrar('".$solicitud_servicio['Ccpsolicitudservicio']['id']."')"),
		                	null, array('width'=>'16'));
							
						if($solicitud_servicio['Ccpsolicitudservicioestado']['id'] == 2){
							if($solicitud_servicio['Ccptabhistorial']['ccptabestado_id']!=13){
								$url = $this->Html->url('/ccpsolicitudservicios/ampliarSolicitud/'.$solicitud_servicio['Ccpsolicitudservicio']['id']); 
								echo $this->Xhtml->imagelink('derco/defecto/ok_siguiente.png', __('CCPSOLICITUD_SERVICIO_ETIQUETA_AMPLIACION_OT'),
			                	'javascript:;',array('onclick'=>"ampliar('".$url."')"),
			                	null, array('width'=>'16'));								
							}
						}
					
						if($solicitud_servicio['Ccpsolicitudservicioestado']['id'] == 1){
							$url = $this->Html->url('/ccpsolicitudservicios/aprobarSolicitud/'.$solicitud_servicio['Ccpsolicitudservicio']['id']); 
							echo $this->Xhtml->imagelink('derco/defecto/ok.png', __('Aprobar'),
			                	'javascript:;',array('onclick'=>"aprobar('".$url."')"),
			                	null, array('width'=>'16'));
								
							echo $this->Xhtml->imagelink('derco/defecto/estado_aviso.png', __('Observar'),
		                	'javascript:;',array('onclick'=>"observarSolicitud('".$solicitud_servicio['Ccpsolicitudservicio']['id']."')"),
		                	null, array('width'=>'16'));
							
							$url = $this->Html->url('/ccpsolicitudservicios/rechazarSolicitud/'.$solicitud_servicio['Ccpsolicitudservicio']['id']);
							echo $this->Xhtml->imagelink('derco/defecto/retirar.png', __('Rechazar'),
			                	'javascript:;',array('onclick'=>"rechazar('".$url."')"),
			                	null, array('width'=>'16'));
								
						}else if(in_array($solicitud_servicio['Ccpsolicitudservicioestado']['id'],array(3,4))){
							$url = $this->Html->url('/ccpsolicitudservicios/aprobarSolicitud/'.$solicitud_servicio['Ccpsolicitudservicio']['id']); 
							echo $this->Xhtml->imagelink('derco/defecto/ok.png', __('Aprobar'),
			                	'javascript:;',array('onclick'=>"aprobar('".$url."')"),
			                	null, array('width'=>'16'));
							
							$url = $this->Html->url('/ccpsolicitudservicios/rechazarSolicitud/'.$solicitud_servicio['Ccpsolicitudservicio']['id']);
							echo $this->Xhtml->imagelink('derco/defecto/retirar.png', __('Rechazar'),
			                	'javascript:;',array('onclick'=>"rechazar('".$url."')"),
			                	null, array('width'=>'16'));
						}
					?>
		        </td>
		    </tr>
		    <?php endforeach; ?>
		</tbody>
	</table>
</div>
<div id ="paging" class="span-18">
    <?php echo $this->element('paginador'); ?>
</div>
<script type="text/javascript">
	//colorearTabla('listaPrincipal');
	
	function crearActa(ot_nro) {
	    var url = "<?php echo $this->Html->url('/talactas/agregar/')?>"+ot_nro;
	    var w = window.open(url, 'agregar', 
	        'scrollbars=yes,resizable=yes,width=650,height=500,top=115,left=200,status=no,location=no,toolbar=no');
	}
	function mostrar(id) {
	    var url = "<?php echo $this->Html->url('/ccpsolicitudservicios/mostrar')?>/"+id;
	    var w = window.open(url, 'mostrar', 
	        'scrollbars=yes,resizable=yes,width=940,height=480,top=115,left=200,status=no,location=no,toolbar=no');
	}
	function mostrar_presupuesto(url) {
		var w = window.open(url,'mostrar','scrollbars=yes,resizable=yes,width=450,height=300,top=150,left=300,status=no,location=no,toolbar=no');
	}
	
	function observarSolicitud(id) {
	    var url = "<?php echo $this->Html->url('/ccpsolicitudservicios/observarSolicitud')?>/"+id;
	    var w = window.open(url, 'mostrar', 
	        'scrollbars=yes,resizable=yes,width=940,height=480,top=115,left=200,status=no,location=no,toolbar=no');
	}
	
	function aprobar(url) {
		var w = window.open(url,'mostrar','scrollbars=yes,resizable=yes,width=500,height=350,top=150,left=300,status=no,location=no,toolbar=no');
	}
	
	function rechazar(url) {
		var w = window.open(url,'mostrar','scrollbars=yes,resizable=yes,width=490,height=420,top=150,left=300,status=no,location=no,toolbar=no');
	}
	
	function observar(url) {
		var w = window.open(url,'mostrar','scrollbars=yes,resizable=yes,width=490,height=350,top=150,left=300,status=no,location=no,toolbar=no');
	}
	function ampliar(url) {
		var w = window.open(url,'Ampliacion','scrollbars=yes,resizable=yes,width=490,height=250,top=150,left=300,status=no,location=no,toolbar=no');
	}
	function exportarExcel(){
		var actionExel = "<?php echo $this->Html->url('/ccpsolicitudservicios/indexCoordinadorExportarExcel')?>";
		
	    var w = window.open('' , 'exportalExcel', 'scrollbars=yes,resizable=yes,width=100,height=100,status=no,location=no,toolbar=no');
		var actionBsc = $('#principalForm').attr('action');
		$('#principalForm').attr('action', actionExel);
		$('#principalForm').attr('target', 'exportalExcel');
		$('#principalForm').submit();
		$('#principalForm').attr('action', actionBsc);	
		$('#principalForm').attr('target', '');
			
	}
</script>