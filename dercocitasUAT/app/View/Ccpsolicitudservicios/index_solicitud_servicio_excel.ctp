<style>
	.titulo{
		background: #005f8c;
		color: #ffffff;
		font-weight:bolder;
		font-size:12pt;
		vertical-align:middle;
	}
	.subtitulo{
		font-weight:bolder;
		font-size:10pt;
		text-align:left;
		vertical-align:middle;
	}
</style>
<table id="listaPrincipalGarantia" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<td colspan="11" class="titulo" style="font-size:14pt;"><?php echo __('REPORTE_SOLICITUD_DE_SERVICIO');?></td>
				</tr>
				<tr height="15">
					<td class="subtitulo"><?php echo __('CCP_REPORTE_FECHA_SOLICTUD');?></td>
					<td class="subtitulo"><?php echo __('GARANTIAS_GENERAL_REPORTES_DESDE').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['fechaIni'])?'':$data['fechaIni']; ?></td>
					<td class="subtitulo"><?php echo __('GARANTIAS_GENERAL_REPORTES_HASTA').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['fechaFin'])?'':$data['fechaFin']; ?></td>
				</tr>
				<tr height="15">
					<td class="subtitulo"><?php echo __('CCP_REPORTE_FECHA_OT');?></td>
					<td class="subtitulo"><?php echo __('GARANTIAS_GENERAL_REPORTES_DESDE').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['fechaIniOt'])?'':$data['fechaIniOt'];?></td>
					<td class="subtitulo"><?php echo __('GARANTIAS_GENERAL_REPORTES_HASTA').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['fechaFinOt'])?'':$data['fechaFinOt'];?></td>
				</tr>	
				<tr height="15">
					<td class="subtitulo"><?php echo __('FechaApb');?></td>
					<td class="subtitulo"><?php echo __('Desde').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['fechaIniApb'])?'':$data['fechaIniApb'];?></td>
					<td class="subtitulo"><?php echo __('Hasta').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['fechaFinApb'])?'':$data['fechaFinApb'];?></td>
				</tr>	
				<tr height="15">
					<td class="subtitulo"><?php echo __('Tipo_servicio').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['ccptiposervicio_id'])?'':$ccptiposervicios[$data['ccptiposervicio_id']]?></td>
					<td class="subtitulo"><?php echo __('CCP_ESTADO_OT_CARS').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['carsestado_id'])?'':$estadosOtCarsTooltip[$data['carsestado_id']-1]?></td> 
					<td class="subtitulo"><?php echo __('estadoSolicitud').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['ccpsolicitudservicioestado_id'])?'':$ccpsolicitudservicioestados[$data['ccpsolicitudservicioestado_id']]?></td>
				</tr>
				<tr height="15">
					<td class="subtitulo"><?php echo __('Nro_OT').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['nrocars'])?'':$data['nrocars'];?></td> 
					<td class="subtitulo"><?php echo __('CCP_PLACA').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['placa'])?'':$data['placa'];?></td>
					<td class="subtitulo"><?php echo __('CCP_ESTADO_OT_CCP').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['estadootccp_id'])?'':$estadosCcpSelect[$data['estadootccp_id']]?></td> 
				</tr>
				<tr>
					<td width="50" class="titulo"><?php echo strtoupper(__('Nro_Ot'));?></td>
					<td width="30" class="titulo"><?php echo strtoupper(__('Cliente'));?></td>
					<td width="75" class="titulo"><?php echo strtoupper(__('CCP_PLACA'));?></td>
					<td width="120" class="titulo"><?php echo strtoupper(__('fechaAperturaOt'));?></td>
					<td width="150" class="titulo"><?php echo strtoupper(__('Fecha_aprobacion_presupuesto'));?></td>		
								
					<td width="80" class="titulo"><?php echo strtoupper(__('horasPlanchado'));?></td>
					<td width="80" class="titulo"><?php echo strtoupper(__('horasPanio'));?></td>

					<td width="80" class="titulo"><?php echo strtoupper(__('Fecha_solicitud'));?></td>
					<td width="60" class="titulo"><?php echo strtoupper(__('Tipo_servicio'));?></td>
					<td width="60" class="titulo"><?php echo strtoupper(__('Asesor'));?></td>
					<td width="70" class="titulo"><?php echo strtoupper(__('CCP_ESTADO_CCP'));?></td>
				</tr>
			</thead>
			<tbody>
			<?php if(!empty($reporte)) { ?>
			    <?php foreach($reporte as $item): ?>
			    <tr>
			        <td style="text-align:center;"><?php echo $item['Ccpsolicitudservicio']['ot_numero'] ?></td>
			    	<td style="text-align:left;"><?php echo ($item['Ccpsolicitudservicio']['ot_cliente']);?></td>
					<td style="text-align:center;"><?php echo $item['Ccpsolicitudservicio']['ot_placa'];?></td>
					<td style="text-align:center;"><?php echo substr($item['Ccpsolicitudservicio']['ot_fecha_creacion'],0,10);?></td>
					<td style="text-align:center;"><?php echo date('d-m-Y',strtotime($item['Ccpsolicitudservicio']['fechaaprobacioncompania'])); ?></td>
					
			        <td><?php echo $item['Ccpsolicitudservicio']['horas_planchado']; ?></td>
			        <td><?php echo $item['Ccpsolicitudservicio']['horas_panio']; ?></td>
					
			    	<td style="text-align:center;"><?php echo empty($item['Ccpsolicitudservicio'])?'':substr($item['Ccpsolicitudservicio']['fecha_emitido'],0,10); ?></td>					
					<td style="text-align:center;"><?php echo empty($item['Ccptiposervicio'])?'': ($item['Ccptiposervicio']['descripcion']); ?></td>
					<td style="text-align:center;"><?php echo $item['Ccpsolicitudservicio']['asesor']; ?></td>
					<td style="text-align:center;">
						<?php
						if(!empty($item['Ccpsolicitudservicio'])){
							if($item['Ccptabhistorial']['ccptabestado_id']==3){
								if($item['Ccpsolicitudservicioestado']['id']==3 || $item['Ccpsolicitudservicioestado']['id']==4 || $item['Ccpsolicitudservicioestado']['id']==5){
									echo empty($item['Ccptabhistorial'])?'':$item['Ccptabhistorial']['descripcion'].' - '.$item['Ccpsolicitudservicioestado']['descripcion'];
								}else echo empty($item['Ccptabhistorial'])?'':$item['Ccptabhistorial']['descripcion'];
							}else{
								if($item['Ccpsolicitudservicioestado']['id']==5){
									echo 'Con Solicitud - Rechazado';
								}else echo $item['Ccptabhistorial']['descripcion'];
							}
						} else echo empty($item['Ccptabhistorial'])?'':$item['Ccptabhistorial']['descripcion'];
					?>
					</td>
			    </tr>
			    <?php endforeach; ?>
			<?php } ?>
			</tbody>
</table>

