<!-- JS/CSS UTILIZADOS -->
<?php $this->layout = 'aventura';
	echo($this->Html->script('ccpsolicitudservicios/rechazar_solicitud.js')); 
	
	echo($this->Html->script('tokeninput/src/jquery.tokeninput.js')); 
	echo $this->Xhtml->css('tokeninput/styles/token-input-facebook');
?>

<script type="text/javascript">
$(document).ready(function(){	
	//autocomplete jquery $responsables
	var datajson = {}; 
	$("#tokenize2").tokenInput('#', datajson, '#CcpsolicitudservicioCopiaA', {
		classes: {
			tokenList: "token-input-list-facebook",
			token: "token-input-token-facebook",
			tokenInvalid: "token-input-token-facebook-invalid",
			tokenDelete: "token-input-delete-token-facebook",
			selectedToken: "token-input-selected-token-facebook",
			highlightedToken: "token-input-highlighted-token-facebook",
			dropdown: "token-input-dropdown-facebook",
			dropdownItem: "token-input-dropdown-item-facebook",
			dropdownItem2: "token-input-dropdown-item2-facebook",
			selectedDropdownItem: "token-input-selected-dropdown-item-facebook",
			inputToken: "token-input-input-token-facebook"
		}
	}); 
});
</script>

<!-- CONTENIDO DE LA PAGINA --> 
<div id="flashMessage" class="message" style="display: none;"><?php echo __('GENERAL_SELECCIONAR_ITEM'); ?></div>
<h2><?php echo __('RECHAZAR_SOLICITUD_DE_SERVICIO'); ?></h2>

<br />
<!-- CREACION DEL FORMULARIO -->
<form id="rechazarSolicitud"  action="<?php echo $this->Html->url('/ccpsolicitudservicios/rechazarSolicitud/'.$solicitud['Ccpsolicitudservicio']['id'])?>" method="post">
<?php echo $this->Form->hidden('Ccpsolicitudservicio.id', array('value'=>$solicitud['Ccpsolicitudservicio']['id'],'class'=>'required', 'size'=>'17', 'readonly'=>true)); ?>

<div class="span-12">
	<div id="reserva" class="span-12">
		<table>
			<tbody>
				<tr>
					<td class="etiqueta"><?php echo __('Correo_electronico'); ?></td>
					<td class="valor span-8 last">
						<?php echo $this->Form->input('Ccpsolicitudservicio.rechazar_correo', array('class'=>'required','size'=>'40','readonly'=>true, 'div'=>false, 'label'=>false
																	,'value'=>empty($this->request->data['Ccpsolicitudservicio']['rechazar_correo'])?$solicitud['Secperson']['email']:$this->request->data['Ccpsolicitudservicio']['rechazar_correo'])); ?>
						<span class="campoObligatorio">*</span>
						<div class="clear"></div>
						<div><label for="CcpsolicitudservicioRechazarCorreo" generated="true" class="error" style="display: none;"></label></div>
					</td>
				</tr>
				<tr>
					<td class="etiqueta"><?php echo __('Copia_A'); ?></td>
					<td class="valor">
						<div id="listaCorreos" style="width:265px">
							<input type="text" name="data[Tqcmensajeresponsablenoconformidad][tqcresponsablenoconformidad_ids]" id="tokenize2" style="display: none; width:265px;"/>
							<?php echo $this->Form->hidden('Ccpsolicitudservicio.copia_a', array('value'=>empty($this->request->data['Ccpsolicitudservicio']['copia_a'])?'':$this->request->data['Ccpsolicitudservicio']['copia_a'])); ?>
						</div>
					</td>
				</tr>
				<tr>
					<td class="etiqueta"><?php echo __('Asunto'); ?></td>
					<td class="valor"><?php echo $this->Form->input('Ccpsolicitudservicio.rechazar_asunto', array('class'=>'required','size'=>'40', 'readonly'=>true, 'div'=>false, 'label'=>false
														,'value'=>empty($this->request->data['Ccpsolicitudservicio']['rechazar_asunto'])?'Se rechazó el presuspuesto Nro: '.$solicitud['Ccpsolicitudservicio']['ot_numero']:$this->request->data['Ccpsolicitudservicio']['rechazar_asunto'])); ?>
						<span class="campoObligatorio">*</span>
						<div class="clear"></div>
						<div><label for="CcpsolicitudservicioRechazarAsunto" generated="true" class="error" style="display: none;"></label></div>
					</td>
				</tr>
				<tr>
					<td class="etiqueta"><?php echo __('Motivo'); ?></td>
					<td class="valor">
						<?php echo $this->Form->input('Ccpsolicitudservicio.ccpmotivorechazo_id',array('type'=>'select','options'=>$motivo_rechazo, 'empty'=>__('seleccionar',true), 'class'=>'required span-4 last', 'div'=>false, 'label'=>false,
									'value'=>empty($this->data['Ccpsolicitudservicio']['ccpmotivorechazo_id'])?null:$this->request->data['Ccpsolicitudservicio']['ccpmotivorechazo_id']
						)); ?>
						<span class="campoObligatorio">*</span>
						<div class="clear"></div>
						<div><label for="CcpsolicitudservicioCcpmotivorechazoId" generated="true" class="error"></label></div>
					</td>
				</tr>
				<tr>
					<td class="etiqueta"><?php echo __('Descripcion'); ?></td>
					<td class="valor">
						<TEXTAREA NAME="data[Ccpsolicitudservicio][comentario_rechazar]" COLS=30 ROWS=5 class="required"><?php echo empty($this->request->data['Ccpsolicitudservicio']['comentario_rechazar'])?'':$this->request->data['Ccpsolicitudservicio']['comentario_rechazar']; ?></TEXTAREA>
						<div><label for="data[Ccpsolicitudservicio][comentario_rechazar]" generated="true" class="error" style="display: none;">Este campo es obligatorio.</label></div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div style="text-align:center;" class="accion">
		<?php if(empty($this->request->data['guardado'])) echo $this->Form->submit(__('GENERAL_GUARDAR'), array('class'=>'guardar', 'div'=>false, 'label'=>false));?>
        <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar','onclick'=>'javascript:window.close()', 'div'=>false, 'label'=>false));?>
	</div>
</div>
</form>
<!-- FIN FORMULARIO -->
<?php echo $this->element('actualizar_padre') ?>