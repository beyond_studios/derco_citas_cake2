
<?php echo $this->Session->flash();?>

<table id="tituloVentana" border="0" cellspacing="0" cellpadding="0" align="center">
    <thead>
        <tr>
            <th class="titulo">
                <?php echo __('SOLICITUD_DE_SERVICIO') ?>
            </th>
            <th>            	
				<?php echo $this->Html->link($this->Html->image('pdf.png', array('alt' => 'Ver', 'title'=>'CCP_ARCHIVO_ORDEN')),
								'javascript:;',array('onclick' => "verPDF('".$solicitud['Ccpsolicitudservicio']['archivoorden']."')",'escape'=>false),
								null, array('width'=>'16'));?>
				&nbsp;	
				<?php echo $this->Html->link($this->Html->image('pdf.png', array('alt' => 'Ver', 'title'=>'CCP_ARCHIVO_PRESUPUESTO')),
								'javascript:;',array('onclick' => "verPDF('".$solicitud['Ccpsolicitudservicio']['archivopresupuesto']."')",'escape'=>false),
								null, array('width'=>'16'));?>
				&nbsp;
				<?php $url = $this->Html->url(array('action'=>'imprimirPdf/'.$solicitud['Ccpsolicitudservicio']['id'])); 
					echo $this->Html->link($this->Html->image('pdf.png', array('alt' => 'Ver', 'title'=>'Imprimir_PDF')),
								'javascript:;',array('onclick' => "rechazar('".$url."')",'escape'=>false),
								null, array('width'=>'16'));?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </th>
        </tr>
    </thead>
</table>

<br />
<!-- CREACION DEL FORMULARIO -->
<div class="span-23" style="height: 350px;">
	<div id="reserva" class="span-11 prepend-1" style="float: left;">
		<fieldset>
			<legend style="font-weight: bold;"><?php echo __('Datos_Ot'); ?></legend>
			<table>
				<tbody>
					<tr>
						<td class="etiqueta span-5"><?php echo $this->Form->label('Ot').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccpsolicitudservicio']['ot_numero']; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo $this->Form->label('Cliente').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccpsolicitudservicio']['ot_cliente']; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo $this->Form->label('Asesor').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccpsolicitudservicio']['ot_asesor']; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo $this->Form->label('Placa').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccpsolicitudservicio']['ot_placa']; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo $this->Form->label('Fecha_Ot').':'; ?></td>
						<td class="valor span-5 last"><?php echo (date('d-m-Y H:i:s',strtotime($solicitud['Ccpsolicitudservicio']['ot_fecha_creacion']))); ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo $this->Form->label('Fecha_Emision_solicitud').':'; ?></td>
						<td class="valor span-5 last"><?php echo (date('d-m-Y H:i:s',strtotime($solicitud['Ccpsolicitudservicio']['fecha_emitido']))); ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo $this->Form->label('Fecha_cliente_entrega').':'; ?></td>
						<td class="valor span-5 last"><?php echo (date('d-m-Y H:i:s',strtotime($solicitud['Ccpsolicitudservicio']['ot_fecha_entrega']))); ?></td>
					</tr>
				</tbody>
				
			</table>
		</fieldset>
		<div id="presupuesto">
				<fieldset>
					<legend style="font-weight: bold;"><?php echo __('CCP_PRESUPUESTO'); ?></legend>
					<table>
						<tbody>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Presupuesto').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccpsolicitudservicio']['presupuesto_numero']; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('CCP_PRESUPUESTO_FECHA_LISTO').':'; ?></td>
						<td class="valor span-5 last"><?php echo (date('d-m-Y',strtotime($solicitud['Ccpsolicitudservicio']['fechapresupuestolisto']))); ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('CCP_PRESUPUESTO_APROBACION_COMPANIA').':'; ?></td>
						<td class="valor span-5 last"><?php echo (date('d-m-Y',strtotime($solicitud['Ccpsolicitudservicio']['fechaaprobacioncompania']))); ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('horasPlanchado').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccpsolicitudservicio']['horas_planchado']; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('horasPanio').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccpsolicitudservicio']['horas_panio']; ?></td>
					</tr>	
						</tbody>
					</table>
				</fieldset>
			</div>
		
		<div id="comentario">
			<fieldset>
				<legend style="font-weight: bold;"><?php echo __('Comentario'); ?></legend>
				<TEXTAREA class=readonly NAME="data[ccpsolicitudservicios][comentario_emitido]" COLS=45 ROWS=1 readonly=readonly><?php echo $solicitud['Ccpsolicitudservicio']['comentario_emitido']; ?></TEXTAREA>
			</fieldset>
		</div>
		
		<?php  if(!empty($solicitud['Ccpsolicitudservicio']['comentario_rechazar'])): ?>
		<div id="comentario2">
			<fieldset>
				<legend style="font-weight: bold;"><?php echo __('Comentario-Rechazar'); ?></legend>
				<TEXTAREA class="readonly" NAME="data[tmp][comentario_rechazar]" COLS=45 ROWS=1 readonly=readonly><?php echo $solicitud['Ccpsolicitudservicio']['comentario_rechazar']; ?></TEXTAREA>
			</fieldset>
		</div>
		<?php endif; ?>
	</div>
	
	<div id="tipo" class="span-11 last" style="float: left;">
		<fieldset>
			<legend style="font-weight: bold;"><?php echo __('Servicio'); ?></legend>
			<table id="contacto">
				<tbody>
					<tr>
						<td class="etiqueta span-5"><?php echo __('aseguradora').':'; ?></td>
						<td class="valor span-5 last"><?php echo $aseguradoras[$solicitud['Ccpsolicitudservicio']['CodSAPCiaAseguradora']]; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Tipo_cliente').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccptipocliente']['descripcion']; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Tipo_servicio').':'; ?></td>
						<td class="valor span-5 last"><?php echo ($solicitud['Ccptiposervicio']['descripcion']); ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Pedido_repuestos').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccppedidorepuesto']['descripcion']; ?></td>
					</tr>
					
					<tr>
						<td class="etiqueta span-5"><?php echo $this->Form->label('Servicio').':'; ?></td>
						<td class="valor span-5 last">
							<table id="tableros">
								<tbody><?php $tableros = explode('-',$solicitud['Ccpsolicitudservicio']['tablero_ids']); ?>
									<?php foreach($ccptableros as $key => $tablero): ?>
									<tr>
										<?php if(in_array($key,$tableros)): ?>
											<td><?php echo $tablero; ?></td>
										<?php endif; ?>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Fecha_propuesta_entrega').':'; ?></td>
						<td class="valor span-5 last"><?php echo (date('d-m-Y',strtotime($solicitud['Ccpsolicitudservicio']['fecha_propuesta_entrega']))); ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Fecha_confirmada_entrega').':'; ?></td>
						<td class="valor span-5 last"><?php echo (date('d-m-Y',strtotime($solicitud['Ccpsolicitudservicio']['fechacomfirmacionentrega']))=='01-01-1970')?'':date('d-m-Y',strtotime($solicitud['Ccpsolicitudservicio']['fechacomfirmacionentrega']));?></td>
					</tr>
				</tbody>
			</table>
		</fieldset>
		
		<?php  if(!empty($solicitud['Ccpsolicitudservicio']['comentario_observar'])): ?>
		<div id="comentario2">
			<fieldset>
				<legend style="font-weight: bold;"><?php echo __('Comentario-Observar'); ?></legend>
				<TEXTAREA class="readonly" NAME="data[tmp][comentario_observar]" COLS=45 ROWS=1 readonly=readonly><?php echo $solicitud['Ccpsolicitudservicio']['comentario_observar']; ?></TEXTAREA>
			</fieldset>
		</div>
		<?php endif; ?>
		
		<?php  if(!empty($solicitud['Ccpsolicitudservicio']['comentario_habilitar'])): ?>
		<div id="comentario2">
			<fieldset>
				<legend style="font-weight: bold;"><?php __('Comentario-Habilitar'); ?></legend>
				<TEXTAREA class="readonly" NAME="data[tmp][comentario_habilitar]" COLS=45 ROWS=1 readonly=readonly><?php echo $solicitud['Ccpsolicitudservicio']['comentario_habilitar']; ?></TEXTAREA>
			</fieldset>
		</div>
		<?php endif; ?>
		
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
<div class="span-23">
	<center>
        <?php echo $this->Form->submit(__('GENERAL_CERRAR'), array('class'=>'cerrar','onclick'=>'javascript:window.close()'));?>
    </center>
</div>
<?php echo $this->element('actualizar_padre') ?>
<script type="text/javascript">
	function rechazar(url) {
		var w = window.open(url,'imprimirPdf','scrollbars=yes,resizable=yes,width=650,height=550,top=150,left=300,status=no,location=no,toolbar=no');
	}
	function verPDF(nombrePDF){
	    var url = "/solicitud/"+nombrePDF;
	    var w = window.open(url, 'verPDF', 
	        'scrollbars=yes,resizable=yes,width=940,height=500,top=115,left=200,status=no,location=no,toolbar=no');
	}	
</script>