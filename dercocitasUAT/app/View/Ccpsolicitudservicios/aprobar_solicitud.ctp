<!-- JS/CSS UTILIZADOS -->
<?php $this->layout = 'aventura';
	echo $this->Html->script('ccpsolicitudservicios/aprobar_solicitud.js'); 
?>

<!-- CONTENIDO DE LA PAGINA --> 
<?php echo $this->Session->flash(); ?>
<div id="flashMessage" class="message" style="display: none;"><?php echo __('GENERAL_SELECCIONAR_ITEM'); ?></div>
<h2><?php echo __('APROBAR_SOLICITUD_DE_SERVICIO'); ?></h2>

<br />
<!-- CREACION DEL FORMULARIO -->
<form id="aprobarSolicitud"  action="<?php echo $this->Html->url('/ccpsolicitudservicios/aprobarSolicitud/'.$solicitud['Ccpsolicitudservicio']['id'])?>" method="post">
<?php echo $this->Form->hidden('Ccpsolicitudservicio.id', array('value'=>$solicitud['Ccpsolicitudservicio']['id'])); ?>

<div class="span-10">
	<div id="reserva" class="span-10">
		<table>
			<tbody>
				<tr>
					<td class="etiqueta span-5"><?php echo __('Nro_ot'); ?></td>
					<td class="valor"><?php echo $solicitud['Ccpsolicitudservicio']['ot_numero']; ?></td>
				</tr>
				<tr>
					<td class="etiqueta span-5"><?php echo __('Servicio'); ?></td>
					<td class="valor span-5 last">
						<table id="tableros">
							<tbody><?php $tableros = explode('-',$solicitud['Ccpsolicitudservicio']['tablero_ids']);?>
								<?php foreach($ccptableros as $key => $tablero): ?>
								<tr>
									<?php if(in_array($key,$tableros)): ?>
										<td><?php echo $tablero; ?></td>
									<?php endif; ?>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td class="etiqueta span-5"><?php echo __('CCP_PEDIDO_REPUESTO'); ?></td>
					<td class="valor span-5 last">
						<?php echo $solicitud['Ccppedidorepuesto']['descripcion']?>
					</td>
				</tr>					
				<tr>
					<td class="etiqueta span-5"><?php echo __('Fecha_propuesta_entrega'); ?></td>
					<td class="valor span-5 last">
						<?php echo $this->Avhtml->date_dmY($solicitud['Ccpsolicitudservicio']['fecha_propuesta_entrega'])?>
					</td>
				</tr>				
				<tr>
					<td class="etiqueta span-5"><?php echo __('CCP_FECHA_CONFIRMADA_ENTREGA_ABP'); ?></td>
					<td class="valor span-5 last">
						<?php echo $this->Form->input('Ccpsolicitudservicio.fechacomfirmacionentrega',array('type'=>'text','class'=>'span-4 last required msg_error_1', 'div'=>false, 'label'=>false));?>
					</td>
				</tr>
				<tr>
					<td class="etiqueta span-5"><?php echo __('CCP_ASESOR'); ?></td>
					<td class="valor span-5 last"><?php echo $solicitud['Secperson']['nombreCompleto']; ?></td>
				</tr>
				<tr>
					<td class="etiqueta span-5"><?php echo __('CCP_FECHA_SOLICITUD'); ?></td>
					<td class="valor span-5 last"><?php echo $this->Avhtml->date_dmY($solicitud['Ccpsolicitudservicio']['fecha_emitido']); ?></td>
				</tr>				
			</tbody>
		</table>
	</div>
	<br/>
	<div style="text-align:center;" class="accion">
		<?php if(empty($this->request->data['guardado'])) echo $this->Form->submit(__('GENERAL_GUARDAR'), array('class'=>'guardar', 'div'=>false, 'label'=>false));?>
        <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar','onclick'=>'javascript:window.close()', 'div'=>false, 'label'=>false));?>
	</div>
</div>
</form>
<!-- FIN FORMULARIO -->
<?php echo $this->element('actualizar_padre') ?>