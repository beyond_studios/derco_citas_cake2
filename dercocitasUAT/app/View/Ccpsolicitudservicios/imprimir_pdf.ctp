<?php
App::import('Vendor','xtcpdf'); 
$fpdfreclamo = new XTCPDF();
//$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans'
//$tcpdf->Cell(0,14, "Hello World", 0,1,'L');
//echo $tcpdf->Output('filename.pdf', 'I');

	if($registros=true){
		/* se define la vista que contendra los parametros para mostrar el reporte en pdf */
	 	
		$alturafila=8;
		$alturafilamensaje=4;
		$alturafilacombinada=$alturafila*2;
		$tamañotextotitulo=14;
		$tamañotextocuerpo=10;
		$tamañotextomensaje=7;
		$fpdfreclamo->setup('P','mm','A4');	// 'L'andsCape(Horizaontal) y 'P'ortrait(Vertical)
	   	$fpdfreclamo->SetMargins(5,20,5);
		$fpdfreclamo->AddPage();	
		$fpdfreclamo->SetFont('Helvetica','',$tamañotextotitulo);
		
		//dibuja una linea horizontal
		$fpdfreclamo->cell(5);
		$fpdfreclamo->Cell(190,0,"",1,0,'C');
		$fpdfreclamo->Ln();
		
		//primera linea con Datos Informativos - ancho = 190
		//CAMBIAR TONIO
		$organizacion = 'organizacion';
		$direccionsucursal = 'sucursal';
		
		$fpdfreclamo->SetFont('','B');
		$fpdfreclamo->cell(5);
		$fpdfreclamo->Cell(190,$alturafila,"SOLICITUD DE SERVICIO",1,1,'C');
		
//		//tecera Linea
//		$fpdfreclamo->cell(5);
//		$fpdfreclamo->Cell(190,$alturafila,utf8_decode($organizacion),1,0,'L');
//		$fpdfreclamo->Ln();
//		
//		//cuarta linea
//		$fpdfreclamo->cell(5);
//		$fpdfreclamo->Cell(190,$alturafila,utf8_decode($direccionsucursal),1,0,'L');
		$fpdfreclamo->Ln();
		$fpdfreclamo->cell(190,$alturafila,null,0,1);
		
		// --- configuracion local
		$fpdfreclamo->SetFont('Helvetica','',12);
		$alturafila = 6;
		
		$fpdfreclamo->cell(5);
		$fpdfreclamo->Cell(190,$alturafila,'Datos de la OT','B',1);
		// ---
		$fpdfreclamo->Cell(80, $alturafila, 'OT:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, utf8_decode($solicitud['Ccpsolicitudservicio']['ot_numero']),0,1,'L', false);
		$fpdfreclamo->Cell(80, $alturafila, 'Cliente:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, utf8_decode($solicitud['Ccpsolicitudservicio']['ot_cliente']),0,1,'L', false);
		$fpdfreclamo->Cell(80, $alturafila, 'Asesor:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, utf8_decode($solicitud['Ccpsolicitudservicio']['ot_asesor']),0,1,'L', false);
		$fpdfreclamo->Cell(80, $alturafila, 'Placa:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, utf8_decode($solicitud['Ccpsolicitudservicio']['ot_placa']),0,1,'L', false);
		$fpdfreclamo->Cell(80, $alturafila, 'Fecha OT:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, utf8_decode($solicitud['Ccpsolicitudservicio']['ot_fecha_creacion']),0,1,'L', false);
		$fpdfreclamo->Cell(80, $alturafila, 'Fecha Entrega:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, utf8_decode($solicitud['Ccpsolicitudservicio']['ot_fecha_entrega']),0,1,'L', false);
		
		$fpdfreclamo->ln();
		$fpdfreclamo->cell(5);
		$fpdfreclamo->Cell(190,$alturafila,'Datos Presupuesto','B',1);
		$fpdfreclamo->Cell(80, $alturafila, 'Presupuesto:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, utf8_decode($solicitud['Ccpsolicitudservicio']['presupuesto_numero']),0,1,'L', false);
		$fpdfreclamo->Cell(80, $alturafila, 'Fecha Presupuesto Listo:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, utf8_decode(date('d-m-Y',strtotime($solicitud['Ccpsolicitudservicio']['fechapresupuestolisto']))),0,1,'L', false);
		$fpdfreclamo->Cell(80, $alturafila, 'Fecha Presupuesto Aprob. Compania:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, utf8_decode(date('d-m-Y',strtotime($solicitud['Ccpsolicitudservicio']['fechaaprobacioncompania']))),0,1,'L', false);
		$fpdfreclamo->Cell(80, $alturafila, 'Cant. Horas planchado:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, utf8_decode($solicitud['Ccpsolicitudservicio']['horas_planchado']),0,1,'L', false);
		$fpdfreclamo->Cell(80, $alturafila, 'Cant. Horas paños pintura:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, utf8_decode($solicitud['Ccpsolicitudservicio']['horas_panio']),0,1,'L', false);
		
		$fpdfreclamo->ln();
		$fpdfreclamo->cell(5);
		$fpdfreclamo->Cell(190,$alturafila,'Servicio','B',1);
		$fpdfreclamo->Cell(80, $alturafila, 'Tipo cliente:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, utf8_decode($solicitud['Ccptipocliente']['descripcion']),0,1,'L', false);
		$fpdfreclamo->Cell(80, $alturafila, 'Tipo servicio:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, utf8_decode($solicitud['Ccptiposervicio']['descripcion']),0,1,'L', false);
		$fpdfreclamo->Cell(80, $alturafila, 'Pedido repuesto:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, ($solicitud['Ccppedidorepuesto']['descripcion']),0,1,'L', false);
		$fpdfreclamo->Cell(80, $alturafila, 'Servicio:',0,0,'R', false); $fpdfreclamo->cell(1);
		$tableros = explode('-',$solicitud['Ccpsolicitudservicio']['tablero_ids']);
		$keytablero = 0; $cadenaTablero='';
		foreach($ccptableros as $key => $tablero){
			if(in_array($key,$tableros)){
				if($keytablero==0)$cadenaTablero .= $tablero;
				else $cadenaTablero .= ' , '.$tablero;
				$keytablero++;
			}
		}
		$fpdfreclamo->Cell(100, $alturafila, utf8_decode($cadenaTablero),0,1,'L', false);		
		//$fpdfreclamo->Cell(100, $alturafila, utf8_decode($solicitud['Ccpsolicitudservicio']['tablero_ids']),0,1,'L', false);
		
		$fpdfreclamo->Cell(80, $alturafila, 'Fecha entrega:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, utf8_decode($solicitud['Ccpsolicitudservicio']['fecha_propuesta_entrega']),0,1,'L', false);
		$fpdfreclamo->Cell(80, $alturafila, utf8_decode('Fecha Emision Solicitud:'),0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, (date('d-m-Y H:i:s',strtotime($solicitud['Ccpsolicitudservicio']['fecha_emitido']))),0,1,'L', false);
		$fpdfreclamo->Cell(80, $alturafila, 'Fecha y hora de aprobacion:',0,0,'R', false); $fpdfreclamo->cell(1);
		$fpdfreclamo->Cell(100, $alturafila, empty($solicitud['Ccpsolicitudservicio']['fecha_aprobacion'])?"":(date('d-m-Y H:i:s',strtotime($solicitud['Ccpsolicitudservicio']['fecha_aprobacion']))),0,1,'L', false);
		
		$fpdfreclamo->ln();
		$fpdfreclamo->cell(5);
		$fpdfreclamo->Cell(190,$alturafila,'Comentario','B',1);
		$fpdfreclamo->Cell(190, $alturafila, utf8_decode($solicitud['Ccptipocliente']['comentario_habilitar']),0,1,'L', false);
		
		//linea de cierre
		$fpdfreclamo->cell(5);
		$fpdfreclamo->Cell(190,0,"",1,0,'C');
		$fpdfreclamo->Ln();
	
		//muestra el reporte
	    echo $fpdfreclamo->fpdfOutput('solicitud.pdf', 'D');
	}else{?>
		<div style="margin: 0px; color: blue; text-align: center; font-size: 10pt;" id="flashMessage">
			<?php echo __('RECLAMO_MENSAJE_INFORMACION_NO_PDF')?><br>
		</div>
	<?php
	}?>
