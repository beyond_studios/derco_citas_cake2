<!-- JS/CSS UTILIZADOS -->
<?php $this->layout = 'aventura';
	echo($this->Html->script('maqsolicitudservicios/mostrar.js')); 
	echo($this->Html->script('maqsolicitudservicios/observar_solicitud.js')); 
?>
<!-- DIALOGOS A UTILIZAR -->

<!-- CONTENIDO DE LA PAGINA --> 
<div id="flashMessage" class="message" style="display: none;"><?php echo __('GENERAL_SELECCIONAR_ITEM'); ?></div>
<h2><?php echo __('HABILITAR_SOLICITUD_DE_SERVICIO'); ?></h2>
<br />
<!-- CREACION DEL FORMULARIO -->
<form id="observarForm"  action="<?php echo $this->Html->url('/ccpsolicitudservicios/habilitarSolicitud/'.$solicitud_id)?>" method="post">
<div class="span-23">
	<div id="reserva" class="span-11 prepend-1" style="float: left;">
		<fieldset>
			<legend style="font-weight: bold;"><?php echo __('Datos_Ot'); ?></legend>
			<table>
				<tbody>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Ot').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccpsolicitudservicio']['ot_numero']; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Cliente').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccpsolicitudservicio']['ot_cliente']; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Asesor').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccpsolicitudservicio']['ot_asesor']; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Placa').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccpsolicitudservicio']['ot_placa']; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Fecha_Ot').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccpsolicitudservicio']['ot_fecha_creacion']; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Fecha_entrega').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccpsolicitudservicio']['ot_fecha_entrega']; ?></td>
					</tr>
				</tbody>
			</table>
		</fieldset>
		<div id="presupuesto">
				<fieldset>
					<legend style="font-weight: bold;"><?php echo __('CCP_PRESUPUESTO'); ?></legend>
					<table>
						<tbody>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Presupuesto').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccpsolicitudservicio']['presupuesto_numero']; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('CCP_PRESUPUESTO_FECHA_LISTO').':'; ?></td>
						<td class="valor span-5 last"><?php echo (date('d-m-Y',strtotime($solicitud['Ccpsolicitudservicio']['fechapresupuestolisto']))); ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('CCP_PRESUPUESTO_APROBACION_COMPANIA').':'; ?></td>
						<td class="valor span-5 last"><?php echo (date('d-m-Y',strtotime($solicitud['Ccpsolicitudservicio']['fechaaprobacioncompania']))); ?></td>
					</tr>	
						</tbody>
					</table>
				</fieldset>
			</div>
		<div id="comentario1">
			<fieldset>
				<legend style="font-weight: bold;"><?php echo __('Comentario'); ?></legend>
				<TEXTAREA class=readonly NAME="data[ccpsolicitudservicios][comentario_emitido]" COLS=45 ROWS=1 readonly=readonly><?php echo $solicitud['Ccpsolicitudservicio']['comentario_emitido']; ?></TEXTAREA>
			</fieldset>
		</div>
	</div>
	
	<div id="tipo" class="span-11 last" style="float: left;">
		<fieldset>
			<legend style="font-weight: bold;"><?php echo __('Servicio'); ?></legend>
			<table id="contacto">
				<tbody>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Tipo_cliente').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccptipocliente']['descripcion']; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Tipo_servicio').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccptiposervicio']['descripcion']; ?></td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Pedido_repuestos').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccppedidorepuesto']['descripcion']; ?></td>
					</tr>
					
					<tr>
						<td class="etiqueta span-5"><?php echo __('Servicio').':'; ?></td>
						<td class="valor span-5 last">
							<table id="tableros">
								<tbody><?php $tableros = explode('-',$solicitud['Ccpsolicitudservicio']['tablero_ids']); ?>
									<?php foreach($ccptableros as $key => $tablero): ?>
									<tr>
										<?php if(in_array($key,$tableros)): ?>
											<td><?php echo $tablero; ?></td>
										<?php endif; ?>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td class="etiqueta span-5"><?php echo __('Fecha_propuesta_entrega').':'; ?></td>
						<td class="valor span-5 last"><?php echo $solicitud['Ccpsolicitudservicio']['fecha_propuesta_entrega']; ?></td>
					</tr>
				</tbody>
			</table>
		</fieldset>
		
		<?php  if(!empty($solicitud['Ccpsolicitudservicio']['comentario_observar'])): ?>
		<div id="comentario2">
			<fieldset>
				<legend style="font-weight: bold;"><?php echo __('Comentario-Observar'); ?></legend>
				<TEXTAREA class="readonly" NAME="data[tmp][comentario_observar]" COLS=45 ROWS=1 readonly=readonly><?php echo $solicitud['Ccpsolicitudservicio']['comentario_observar']; ?></TEXTAREA>
			</fieldset>
		</div>
		<?php endif; ?>
		
		<div id="comentario">
			<fieldset>
				<legend style="font-weight: bold;"><?php echo __('Comentario-Habilitar'); ?></legend>
				<TEXTAREA class="required" NAME="data[Ccpsolicitudservicio][comentario_habilitar]" COLS=45 ROWS=1><?php echo empty($this->request->data)?'':$this->request->data['Ccpsolicitudservicio']['comentario_habilitar']; ?></TEXTAREA>
			</fieldset>
		</div>
	</div>
	<div class="clear"></div>
	
	<div style="text-align:center;" class="accion">
        <?php if(empty($this->request->data['guardado'])) echo $this->Form->submit(__('GENERAL_GUARDAR'), array('class'=>'guardar', 'div'=>false, 'label'=>false));?>
        <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar','onclick'=>'javascript:window.close()', 'div'=>false, 'label'=>false));?>
	</div>
</div>
</form>
<!-- FIN FORMULARIO -->
<?php echo $this->element('actualizar_padre') ?>