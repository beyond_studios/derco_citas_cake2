<!-- JS/CSS UTILIZADOS -->
<?php $this->layout = 'aventura';
	echo $this->Html->script('ccpsolicitudservicios/ampliar_solicitud.js'); 
?>
<!-- DIALOGOS A UTILIZAR -->


<!-- CONTENIDO DE LA PAGINA --> 
<?php echo $this->Session->flash(); ?>
<div id="flashMessage" class="message" style="display: none;" align="center"><?php echo __('GENERAL_SELECCIONAR_ITEM'); ?></div>
<h2 align="center"><?php echo __('CCPSOLICITUD_SERVICIO_TITULO_AMPLIACION_OT'); ?></h2>

<br />
<form id="agregarForm" name="agregarForm" action="<?php echo $this->Html->url('/ccpsolicitudservicios/ampliarSolicitud/').$idsolicitudServicios;?>" method="post">
<table id="formularioEdicion" border="0" cellspacing="0" cellpadding="0" align="center">
    <tbody>
    	<?php echo $this->Form->hidden('Ccpsolicitudservicio.id',array('value'=>$idsolicitudServicios));?>
		<tr>
			<td class="etiqueta">
				<?php echo $this->Form->label('Ccpsolicitudservicio.comentario_ampliacion_antiguo',__('CCPSOLICITUD_SERVICIO_ETIQUETA_AMPLIACION_OT_HISTORIAL'));?>
            </td>
            <td class="valor">
                <?php echo $this->Xhtml->thisData('Ccpsolicitudservicio.comentario_ampliacion_antiguo');?>
            </td>
		</tr>
        <tr>
            <td class="etiqueta">
                <?php echo $this->Form->label('Ccpsolicitudservicio.comentario_ampliacion',__('CCPSOLICITUD_SERVICIO_TITULO_AMPLIACION_OT_COMENTARIO'));?>
            </td>
            <td class="valor">
                <?php echo $this->Form->input('Ccpsolicitudservicio.comentario_ampliacion', array('class'=>'required','type'=>'textarea','cols'=>'50', 'rows'=>'2', 'div'=>false, 'label'=>false));?>
     
            </td>
        </tr>
        <tr>
            <td colspan="2" class="accion">
                <?php echo $this->Form->submit(__('GENERAL_GUARDAR'), array('class'=>'guardar', 'div'=>false, 'label'=>false));?>
                <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar','onclick'=>'javascript:window.close()', 'div'=>false, 'label'=>false));?>
            </td>
        </tr>
    </tbody>
</table>

</form> 