<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	
	<!-- Framework CSS -->
		<base id="dtBasic" href="<?php echo Router::url('/'); ?>" permisorol="<?php //echo $datosLogeo['0']['Secrole']['name']; ?>" />

	<link href="https://fonts.googleapis.com/css?family=Hind+Siliguri:300,400,500,600,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	<?php
		echo $this->Html->script('jquery-1.3.2.min.js');
		echo $this->Html->script('jquery-validate/jquery.validate.js');
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');

		echo $this->Html->script('jquery-validate/localization/messages_es.js');
		echo $this->Html->script('general/forselenium.js');
		echo $this->Html->script('jquery-validate/mis.metodos.js');

		
		echo $this->Html->script('/dist/vendor/jquery.nanoscroller.js');
		echo $this->Html->css('/dist/css/main.css');
		echo $this->Html->script('/dist/js/main.js');
		echo $this->Html->css('print', 'stylesheet', array('media'=>'print'));

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
	?>
		
	<?php echo $scripts_for_layout; ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-K3CKC8L');</script>
    <!-- End Google Tag Manager-->
</head>

<body class='<?php echo isset($bodyClass)?$bodyClass:'backLogin' ?>'>
	<div class="logo">
		<a href="/">
            <?php echo $this->Html->image('/dist/images/logoDerco.png', array('alt' => 'Logo de la Empresa')); ?>
		</a>
	</div>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K3CKC8L"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript)-->
	<div class="barra">

		<div class="redes">
			<div class="facebook"><a href="#"><i class="fa fa-facebook fa-1x" aria-hidden="true"></i></a></div>
			<div class="twitter"><a href="#"><i class="fa fa-twitter fa-1x" aria-hidden="true"></i></a></div>
			<div class="youtube"><a href="#"><i class="fa fa-youtube-play fa-1x" aria-hidden="true"></i></a></div>
		</div>
	</div>
	
	<noscript>
		<div><?php __('estaPaginaRequiereTenerJavascriptActivado') ?></div>
	</noscript>
	<div class="wrapper">
		<div class="content">
			<div class="content_ex">
				<div class="base_content">
					
					<?php echo $this->Session->flash(); ?>
					<?php echo $this->Session->flash('auth'); ?>
						<?php echo $content_for_layout; ?>
						
				</div>
			</div>

		</div>
	</div>
<div class="footer">
		<p>© DERCO 2014, todos los derechos reservados   |   CASA MATRIZ: Av. Nicolás Aylón 2648 Ate
		<br>
Política de Privacidad | info@derco.com </p>
	</div>

</body>
</html>