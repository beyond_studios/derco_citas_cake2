<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title_for_layout; ?></title>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
<link href="https://fonts.googleapis.com/css?family=Hind+Siliguri:300,400,500,600,700" rel="stylesheet">

<?php echo $this->Html->charset('UTF-8'); ?>
<!--
<link rel="icon" href="<?php echo $this->webroot . 'favicon.ico'; ?>" type="image/x-icon" />
<link rel="shortcut icon" href="<?php echo $this->webroot . 'favicon.ico'; ?>" type="image/x-icon" />
-->
<base id="dtBasic" href="<?php echo Router::url('/'); ?>" permisorol="<?php //echo $datosLogeo['0']['Secrole']['name']; ?>" />


<?php

		//JS NECESARIOS #######################################################
		echo $this->Html->script('jquery-1.3.2.min.js');
		echo $this->Html->script('jquery-validate/jquery.validate.js');
		echo $this->Html->script('jquery-validate/lib/jquery.form.js');

		echo $this->Html->script('modulo_taller/jqgrid/jquery-ui-1.8.2.custom.min.js');
		echo $this->Html->script('modulo_taller/jqgrid/i18n/grid.locale-sp.js');
		echo $this->Html->script('modulo_taller/jqgrid/jquery.jqGrid.min.js');
		echo $this->Html->script('modulo_taller/popup.js');


		echo $this->Html->script('jquery-validate/localization/messages_es.js');
		echo $this->Html->script('general/forselenium.js');
		echo $this->Html->script('jquery-validate/mis.metodos.js');
        

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
?>
<!-- Url base para ajax -->
	<base id="dtBase" href="<?php echo Router::url('/'); ?>" permisorol="<?php //echo $datosLogeo['0']['Secrole']['name']; ?>" />
	<!-- <script type="text/javascript" src='/dist/js/jquery-3.2.1.min.js'></script> -->
	<link rel="stylesheet" href="/dist/vendor/nanoscroller.css">
	<script type="text/javascript" src="/dist/vendor/jquery.nanoscroller.js"></script>
	<link rel="stylesheet" href="dist/vendor/main.css">

<?php

		//CSS NECESARIOS #######################################################
//		echo $this->Html->css('modulo_taller/themes/redmond/jquery-ui-1.8.2.custom.css');
		echo $this->Html->css('modulo_taller/themes/blitzer/jquery.ui.all.css');
		echo $this->Html->css('modulo_taller/themes/ui.jqgrid.css');
		
		echo $this->Html->css('/dist/css/main.css');
		echo $this->Html->script('/dist/js/main.js');
		echo $this->Html->css('print', 'stylesheet', array('media'=>'print'));

		
?>
<?php echo $this->Html->script('modulo_taller/taller.js'); ?>
<?php echo $scripts_for_layout; ?>
<base href="<?php echo Router::url('/'); ?>" />
</head>

<body  class='<?php echo isset($bodyClass)?$bodyClass:'backLogin' ?>'>
	<div class="logo">
		<a href="index.html">
			<img src="dist/images/logoDerco.png">
		</a>
	</div>

	<div class="barra">
		<?php echo $this->element('menu_web_taller') ?>
		
		<div class="redes">
			<div class="facebook"><a href="#"><i class="fa fa-facebook fa-1x" aria-hidden="true"></i></a></div>
			<div class="twitter"><a href="#"><i class="fa fa-twitter fa-1x" aria-hidden="true"></i></a></div>
			<div class="youtube"><a href="#"><i class="fa fa-youtube-play fa-1x" aria-hidden="true"></i></a></div>
		</div>
	</div>

<div class="wrapper wrapperCenter">
	<div class="userInfo">
		<span class="info">Hola, <span><?php echo $username ?></span></span>
		<b style='font-weight:bold'>
		<?php 
			echo $this->Html->link(__('Cerrar Session',true),
				array('controller' => 'clientes', 'action' => 'logoutCliente', 'full_base' => false), array('class'=>'closeSession'));
		?>
		</b>
	</div>

	<div class="content">
		<div class="content_ex">
			<div class="base_content">
				<?php echo $content_for_layout; ?>
			</div>
		</div>

	</div>
</div>
<div class="footer">
		<p>© DERCO <?php echo date('Y')?>, todos los derechos reservados   |   CASA MATRIZ: Av. Nicolás Aylón 2648 Ate
		<br>
Política de Privacidad | info@derco.com </p>
	</div>

</body>
</html>