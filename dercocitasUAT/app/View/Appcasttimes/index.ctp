<?php echo $this->Html->script('appcasts/index.js'); ?>
		<?php echo $this->Session->flash();?>
		<br/>
			<h3 id="tituloTable"><?php echo(__('plantillaCronograma',true));?></h3>
		
		<div class="box">
		
			<div id="buscador" class="">
				<?php echo $this->element('buscador', array('elementos'=>$elementos,'url' => 'index')); ?>
			</div>
		</div>		
		<table cellpadding="0" cellspacing="0" class="table" >
			<thead>
				<tr>
	


				<th><?php echo $this->Paginator->sort('Appcast.id',__('codigo',true));?></th>
				<th><?php echo $this->Paginator->sort('Appcast.description',__('descripcion',true));?></th>
				<th><?php echo $this->Paginator->sort('Appcast.status',__('estado',true));?></th>
				<th class="actionsfijo"><?php echo __('Actions');?></th>
			
			</tr>			
			</thead>
			
			<tbody>
				<?php  foreach ($casts as $cast):?>
				<tr>
					<td><?php echo $cast['Appcast']['id']; ?>
				</td>
				<td>
					<?php echo $cast['Appcast']['description']; ?>
				</td>

				<td>
					<?php echo $cast['Appcast']['status'] == 'AC' ? 
									__('Enable',true)
								:
									($cast['Appcast']['status'] == 'DE'?
										__('Disable',true)
									:
										__('Limited',true))
								; ?>
				</td>
		
				<td class="actionsfijo">
				<?php echo $this->element('actionCronograma', 
								array('id'=>$cast['Appcast']['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>

<div id ="paging" class="span-18">

	    <?php echo $this->Paginator->options(array('url'=> array('buscador:'. isset($this->request->data['Buscar']['buscador']).'/valor:'. isset($this->request->data['Buscar']['valor']). '/desactivo:'. isset($this->request->data['Buscar']['desactivo'])  ))); ?>
		<?php echo $this->element('paginador'); ?>
</div>
<div class="clear"></div>