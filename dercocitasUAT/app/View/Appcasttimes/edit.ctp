<div class="span-8" >
	<?php 
        echo $this->Html->script('appcasttimes/edit.js',false);
	echo $this->Html->script('js-timepicker/jquery-ui-1.8.6.custom.min.js');
	echo $this->Html->script('js-timepicker/jquery-ui-timepicker-addon.js');
	echo $this->Html->script('js-timepicker/jquery.ui.datepicker-es.js');
	echo $this->Html->css('theme/redmond/jquery-ui-1.7.2.custom');
	?>
	
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-8" >
		<h3><?php echo(__('plantillaAgregar',true));?></h3>
	</div>
	<hr/>
	
	<?php echo $this->Form->create('Appcasttime');?>

		<?php echo $this->Form->hidden('appcast_id')?>

		<?php echo $this->Form->hidden('id')?>	

		<div class="span-8" >
				<div class="span-3" >
					<label><?php echo(__('horaInicioCronograma',true)) ?><span class="error"><?php echo " *";?>	</span>
					</label> 
				</div>
				<div class="span-5 last" >
					<?php echo $this->Form->input('inittime', array('label'=>false,'type'=>'text','class'=>'span-5','error'=>false));   
						  echo $this->Form->error('inittime', array('notEmpty' =>  __('empresaNombreNoVacio', true)), 
						  							array('class' => 'input text required error'));
					
					?>
				</div>
		</div>


		<div class="span-8" >
				<div class="span-3" >
					<label><?php echo(__('horaFinCronograma',true)) ?><span class="error"><?php echo " *";?>	</span>
					</label> 
				</div>
				<div class="span-5 last" >

		      <?php 
                    echo $this->Form->input('endtime', array('label'=>false,'type'=>'text','class'=>'span-5','error'=>false));   
		    echo $this->Form->error('endtime', array('notEmpty' =>  __('empresaNombreNoVacio', true)), 
						  							array('class' => 'input text required error'));
					
					?>
				</div>
		</div>	

  <div class="span-8" >
      <div class="span-3" >
           <label><?php echo(__('Nro Programado', true)) ?><span class="error"><?php echo " *"; ?>	</span>
           </label> 
      </div>
    <div class="span-5 last" >

        <?php
        echo $this->Form->input('programed', array('label' => false, 'type' => 'text', 'class' => 'span-5', 'error' => false));
        echo $this->Form->error('programed', array('notEmpty' => __('empresaNombreNoVacio', true)), array('class' => 'input text required error'));
        ?>
    </div>
  </div>
	


		<div id="rowLast" class="span-8" >
		</div>
		<br/>		
		<hr/>
		
		<div class=" span-8 botones" >
			<?php echo $this->Form->submit(__('Submit',true), array('div'=>false));	?>
			<?php echo $this->Form->button(__('Reset',true), array('type'=>'reset')); ?>				
			<?php echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>
	
	<?php echo $this->Form->end(); ?>
<?php //echo $this->element('sql_dump'); ?>
<?php echo $this->element('actualizar'); ?>
</div>
