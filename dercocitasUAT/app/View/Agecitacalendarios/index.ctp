<?php echo $this->Html->script('agecitacalendariodias/index.js'); ?>
		<?php echo $this->Session->flash();  ?>
		<br/>
			<h3 id="tituloTable"><?php echo __('Generaci&oacute;n de calendarios x Sucursal - Motivo de Servicio');?></h3>
		
		<div class="box">
			<div id="agregar" class="span-5" >
			<?php echo $this->element('agregar'); ?>
				&nbsp;
		    <?php $url = $this->Html->url(array('controller'=>'Appcasts','action'=>'indexplantilla')); 
					echo $this->Html->link($this->Html->image('add_cells.png', array('alt' => 'lista')),
						 'javascript:;',
						 array('onclick' => "addplantilla('".$url."')",'escape'=>false),
						 null); 
			?>
			&nbsp;
			<?php
				   echo $this->Html->link('Plantillas', 'javascript:;',array('onclick' => "addplantilla('".$url."')",'escape'=>false), null);; 
			?>			
			</div>
			
			<div id="buscador" class="">
				<?php echo $this->element('buscador', array('elementos'=>$elementos,'url' => 'index')); ?>		
			</div>
		</div>		
		<table cellpadding="0" cellspacing="0" class="table" >
			<thead>
			<tr>
                 
				<th><?php echo $this->Paginator->sort('Secorganization.name',__('organizacion',true));?></th>        
				<th><?php echo $this->Paginator->sort('Secproject.name',__('Sucursal',true));?></th>        
				<th><?php echo $this->Paginator->sort('Agemotivoservicio.description',__('Motivo servicio',true));?></th>        
				<th><?php echo $this->Paginator->sort('Agegrupo.description',__('Grupo',true));?></th>
				<th><?php echo $this->Paginator->sort('Agecitacalendario.maximo_citasprogramado',__('Nro de citas programadas'));?></th>       
				<th><?php echo $this->Paginator->sort(__('Calendario',true));?></th>         
				<th><?php echo $this->Paginator->sort('Agemotivoservicio.status',__('estado',true));?></th>
				<th class="actionsfijo"><?php  echo __('Actions');?></th>
			
			</tr>			
			</thead>
			
			<tbody>
				<?php  foreach ($agecitacalendarios as $agecitacalendario):?>
				<tr>
				<td>
					<?php 	echo $agecitacalendario['Secorganization']['name']; ?>	
				</td>
				<td>
					<?php 	echo $agecitacalendario['Secproject']['name']; ?>	
				</td>
				<td>
					<?php 	$url = $this->Html->url(array('controller'=>'Agecitacalendarios','action'=>'view',$agecitacalendario['Agecitacalendario']['id'])); 
								echo $this->Html->link($agecitacalendario['Agemotivoservicio']['description'], 'javascript:;',array('onclick' => "mostrar('".$url."')",'escape'=>false), null);; ?>	
				</td>
				<td>
					<?php 	echo $agecitacalendario['Agegrupo']['description']; ?>	
				</td>
				<td class="textc">
					<?php 	echo $agecitacalendario['Agecitacalendario']['maximo_citasprogramado']; 
					//echo $anioActual.'/'.$mesActual.'/'.$agecitacalendario['Agecitacalendario']['id'];?>	
				</td>
				<td class="textc">
					<?php 	if(!empty($agecitacalendario['Agecitacalendariodia']['year']))
								echo $agecitacalendario['Agecitacalendariodia']['year'];
							else 
								echo '-';						
					 ?>	
				</td>
				<td class="textc">
					<?php echo $agecitacalendario['Agecitacalendario']['status'] == 'AC' ? 
									__('Enable',true)
								:
									($agecitacalendario['Agecitacalendario']['status'] == 'DE'?
										__('Disable',true)
									:
										__('Limited',true))
								; ?>
				</td>
		
					<td class="actionsfijo">
					
					<?php 
					if($agecitacalendario['Agecitacalendario']['status']=='AC'){
						$url = $this->Html->url(array('controller'=>'agecitacalendariodias','action'=>'generarHorariosForm',$agecitacalendario['Agecitacalendario']['id']));
						$image = $this->Html->image('derco/defecto/calendario.gif', array('title'=>__('Generar Horario',true), "alt" => "mostrar", 'width'=>'19'));
						echo $this->Html->link($image, 'javascript:;',array('onclick' => "modificarCronograma('".$url."')",'escape'=>false), null, false);
						
						$url = $this->Html->url(array('controller'=>'Agecitacalendariodias','action'=>'modificarCalendarioGeneradoForm',$agecitacalendario['Agecitacalendario']['id']));
						$image = $this->Html->image('derco/defecto/modificar_calendario.png', array('title'=>__('Modificar Horario',true), "alt" => "mostrar", 'width'=>'19'));
						echo $this->Html->link($image, 'javascript:;',array('onclick' => "modificarCronograma('".$url."')",'escape'=>false), null, false);
						
						$url = $this->Html->url(array('controller'=>'agecitacalendariodias','action'=>'mostrarMes',$anio,$mes,$agecitacalendario['Agecitacalendario']['id']));
						$image = $this->Html->image('derco/defecto/generar_horario.png', array('title'=>__('Modificar Horario x Dia',true), "alt" => "Modificar Calendario"));
						echo $this->Html->link($image, 'javascript:;',array('onclick' => "modificarHorarios('$url')",'escape'=>false), null, false);
					}
	
						echo $this->element('actionDerco', array(
							'id'=>$agecitacalendario['Agecitacalendario']['id'], 
							'name'=>$agecitacalendario['Agemotivoservicio']['description'].' - '.$agecitacalendario['Agegrupo']['description'],
							'estado'=>$agecitacalendario['Agecitacalendario']['status']
						));
					?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>

<div id ="paging" class="span-18">
    <?php echo $this->element('paginador'); ?>
</div>
<div class="clear"></div>