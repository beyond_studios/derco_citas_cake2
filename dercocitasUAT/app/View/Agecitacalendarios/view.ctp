<table id="tituloVentana" border="0" cellspacing="0" cellpadding="0" align="center">
    <thead>
        <tr>
            <th colspan="2" class="titulo">
                <?php echo __('TALLER_CITACALENDAR_TITULO_MOSTRAR')?>
            </th>
        </tr>
    </thead>
</table>
<br/>
<table id="formularioEdicion" border="0" cellspacing="0" cellpadding="0" align="center">
    <tbody>
        <tr>
            <td class="etiqueta">
                <?php echo __('TALLER_CITACALENDAR_ETIQUETA_ORGANIZATION');?>
            </td>
            <td class="valor">
                <?php echo $organizationName;?>
            </td>
        </tr>
        <tr>
            <td class="etiqueta">
                <?php echo __('TALLER_CITACALENDAR_ETIQUETA_PROJECT');?>
            </td>
            <td class="valor">
                <?php echo $calendar['Secproject']['name'];?>
            </td>
        </tr>
		<tr>
			<td colspan="2"><hr /></td>
		</tr>
        <tr>
            <td class="etiqueta">
                <?php echo __('TALLER_CITACALENDAR_CAMPO_SERVICIO');?>
            </td>
            <td class="valor">
                <?php echo $calendar['Agemotivoservicio']['description'];?>
            </td>
        </tr>
        <tr>
            <td class="etiqueta">
                <?php echo __('TALGRUPOS_ETIQUETA_GRUPOS');?>
            </td>
            <td class="valor">
                <?php echo $calendar['Agegrupo']['description'];?>
            </td>
        </tr>		
        <tr>
            <td class="etiqueta">
                <?php echo __('TALLER_CITACALENDAR_TITULO_PROGRAMADAS');?>
            </td>
            <td class="valor">
                <?php echo $calendar['Agecitacalendario']['maximo_citasprogramado'];?>
            </td>
        </tr>		
        <tr>
            <td class="etiqueta">
                <?php echo __('TALLER_CITACALENDAR_CAMPO_ESTADO');?>
            </td>
            <td class="valor">
                <?php echo $calendar['Agecitacalendario']['status']; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="accion">
                <?php echo $this->Form->button(__('cerrar',true), array('class'=>'cerrar', 'type'=>'button','onClick' => 'javascript:window.close()')); ?>
            </td>
        </tr>
    </tbody>
</table>
<?php //echo $this->renderElement('actualizar_padre'); ?>
<?php echo $this->element('actualizar_ver'); ?>