<?php echo $this->element('dialog_message'); ?>
<?php echo $this->element('getBscVehicles'); ?>

<?php echo $this->Html->script('clientes/get_clientes.js'); ?>
<?php echo $this->Html->script('Agedetallecitas/agregar_cita.js'); ?>

<!-- DIALOGOS A UTILIZAR -->
<div id="dialog_cliente" title="Buscar cliente">
	<div id="containerclientes">
		<table id="clientes"></table> 
		<div id="clientes-pager"></div>
	</div>
</div>
<?php echo $this->Session->flash();?>
<table id="tituloVentana" border="0" cellspacing="0" cellpadding="0" align="center">
    <thead>
        <tr>
            <th colspan="2" class="titulo">
                <?php echo __('TALLER_TITULO_TALLER_CITA') ?>
            </th>
        </tr>
    </thead>
</table>

<ul class="acciones">
	<li>
		<?php echo $this->Xhtml->imageTextLink('derco/defecto/cerrar.gif', __('GENERAL_CERRAR'),
			'javascript:;',	array('onclick'=>'javascript:window.close()', 'escape'=>false), null, array('width'=>'16'))?>
    </li>
</ul>
<br/>

<div id="listaCaja">
	<fieldset style="height: 220px;">
		<legend><?php echo __('VENDEDOR_ETIQUETA_CONSULTA_DIAS_HABILES')?></legend>
		<?php echo $this->Form->create('Buscador', array('url'=>'actualizarNumeroCitas'));?>
		<?php //echo $this->Form->hidden('secprojectId', 'secproject_id')?>
		<div id="buscadorBasico">
			<table id="formularioEdicion" border="0" cellspacing="0" cellpadding="0" align="center" width="90%">
				<tbody>
					<tr>
		            	<td class="etiqueta">
		            		<?php echo __('AGE_REPORTE_FECHA_DESDE');?>
		            	</td>
						<td class="valor">
							<?php echo $this->Form->input('fechaInicial', array('class'=>'required msg_error_2 soloLectura','readonly'=>true ,'label'=>false, 'div'=>false,'style'=>'width:120px;'))?>
							<span class="error">*</span>
		            	</td>
		           </tr>
		           <tr>
		            	<td class="etiqueta">
		            		<?php echo __('AGE_REPORTE_FECHA_HASTA');?>
		            	</td>
						<td class="valor">
							<?php echo $this->Form->input('fechaFinal', array('class'=>'required msg_error_2 soloLectura','readonly'=>true, 'label'=>false, 'div'=>false,'style'=>'width:120px;'))?>
							<span class="error">*</span>
		            	</td>
					</tr>
					<tr>
		            	<td class="etiqueta">
		            		<?php echo __('TALLER_ETIQUETA_TALLER_SUCURSAL');?>
		            	</td>
						<td class="valor">
							<?php echo $this->Form->select('secproject_id', array($projects), array('empty'=>__('Seleccionar'), 'class'=>'required msg_error_1', 'style'=>'width:70%;'))?>
							<span class="error">*</span>
						</td>
					</tr>
					<tr>
						<td class="etiqueta">
		            		<?php echo __('AGE_GRUPO_DESCRIPCION');?>
		            	</td>
						<td class="valor">
							<?php echo $this->Form->select('agegrupo_id', array($grupos), array('empty'=>__('Seleccionar'), 'class'=>'required msg_error_1', 'style'=>'width:70%;'))?>
							<span class="error">*</span>
						</td>
		        	</tr>
					<tr>
						<td class="etiqueta">
		            		<?php echo __('AGE_MOTIVOSERVICIO_DESCRIPCION');?>
		            	</td>
						<td class="valor">
							<?php echo $this->Form->select('agemotivoservicio_id', array($servicios), array('empty'=>__('Seleccionar'), 'class'=>'required msg_error_1', 'style'=>'width:70%;'))?>
							<span class="error">*</span>
						</td>
						<td colspan=2 style="text-align:right;">
							<?php echo $this->Form->button(__('GENERAL_BUSCAR'), array('style'=>'width:80px', 'class'=>'buscar'));?>
						</td>
		        	</tr>
				</tbody>
			</table>
		</div>
		<?php echo $this->Form->end(); ?>
		<div id="programacion" style="text-align: center;"></div>
		<div id="mensajeFinal" title="Mensaje" style="text-align: center;">
			<span id="mensajeFinaltexto"></span>
		</div>
		<div id="dialog_confirm" title="Confirmar Datos de la Cita">
		<?php if(!empty($agecitacalendariodias)){?>
			<div style="width:400px; margin:0 auto 0 auto;">
		        <table id="listaPrincipal" cellpadding="0" cellspacing="0">
		            <thead>
		                <tr>
		                    <th><?php echo $idioma->etiqueta('TALCITACALENDAR_NUMERO_CITAS_INICIO')?></th>
		                   <!-- <th><?php //echo $idioma->etiqueta('TALCITACALENDAR_NUMERO_CITAS_FIN')?></th>-->
		                    <th><?php echo $idioma->etiqueta('TALCITACALENDAR_NUMERO_CITAS_PROGRAMADO')?></th>
		                    <th><?php echo $idioma->etiqueta('TALCITACALENDAR_NUMERO_CITAS_DISPONIBLE')?></th>
							<th style="width:90px"><?php echo $idioma->etiqueta('GENERAL_ACCIONES')?></th>
		                </tr>
		            </thead>
		            <tbody>
		            	<?php $fechaAnterior="";?>
		                <?php foreach ($agecitacalendariodias as $key=>$agecitacalendariodia): ?>
						<?php $class = (($key-1)%2 == 0) ? "par" : "impar";?>
						<?php $fecha=date('d-m-Y',strtotime($agecitacalendariodia['Agecitacalendariodia']['initDateTime']));
								if($fecha<>$fechaAnterior){?>
						<tr>
							<td colspan="5" align="center" style="font-weight:bolder;"><?php echo $fecha; ?></td>
						</tr>		
						<?php
								$fechaAnterior=$fecha;
								}						
						?>
						
	                    <tr class="<?php //echo $class; ?>">
	                    	<?php 
							$HoraInicial=date('H:i',strtotime($agecitacalendariodia['Agecitacalendariodia']['initDateTime']));
							$HoraFinal=date('H:i',strtotime($agecitacalendariodia['Agecitacalendariodia']['endDateTime']));
							?>
	                        <td class="centrado"><?php echo $HoraInicial;?></td>
	                      <!--  <td class="texto"><?php // echo $HoraFinal;?></td>-->
	                        <td class="centrado"><?php echo $agecitacalendariodia['Agecitacalendariodia']['programed']; ?></td>
	                        <td class="centrado"><?php echo $agecitacalendariodia['Agecitacalendariodia']['available']; ?></td>
	                        <td class="accion">
					            <?php
									if($agecitacalendariodia['Agecitacalendariodia']['available']>0){
										echo $this->Xhtml->imagelink('bajar.png', __('TALCITACALENDAR_NUMERO_DISMINUIR_CITAS'),
										'/agecitacalendariodia/disminuirDisponible/'.$agecitacalendariodia['Agecitacalendariodia']['id'].'/'.$secproject.'/'.$talcitacalendarId.'/'.$talgrupo.'/'.$fechaIN.'/'.$fechaOUT
										,null
										,$idioma->mensaje('TALCITACALENDAR_MENSAJE_DISMINUIR_CITAS').' '.$HoraInicial.' ?', 
						            	array('width'=>'16'));
									}
									
									?>
	                        </td>							
	                    </tr>
		                <?php endforeach; ?>
		            </tbody>
		        </table>
				</div>
			<?php }?>
			<?php echo $this->Form->end(); ?>
		</div>
	</fieldset>
</div> 