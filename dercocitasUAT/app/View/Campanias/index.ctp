<?php echo $this->Html->script('layouts/mijquery.js'); ?>
<?php echo $this->Html->script('Campanias/index.js'); ?>
		<?php echo $this->Session->flash();  ?>
		<br/>
			<h3 id="tituloTable"><?php echo __('Conoce nuestros campanias/Promociones');?></h3>
		
		<div class="box">
			<div id="agregar" class="span-5" >
			<?php 	
					$url = $this->Html->url(array("action"=>'add'));  
					echo $this->Html->link($this->Html->image('agregar.png', array('alt' => 'Agregar')),
									 'javascript:;',
									 array('onclick' => "add('".$url."')",'escape'=>false),
									 null); 
			?>
				&nbsp;
			<?php 
				echo $this->Html->link(__('Add', true), 'javascript:;',array('onclick' => "add('".$url."')")); 
			?>					
			</div>
			<div id="buscador" class="">&nbsp;
			</div>	
		</div>		
		<table cellpadding="0" cellspacing="0" class="table" >
			<thead>
			<tr>
                 <th><?php echo __('Imagen',true);?></th> 
				<th><?php echo $this->Paginator->sort('Campania.img',__('Archivo',true));?></th>  
				<th><?php echo $this->Paginator->sort('Campania.titulo',__('Titulo',true));?></th>  
				<th><?php echo $this->Paginator->sort('Campania.description',__('Descripcion',true));?></th>        
				<th><?php echo $this->Paginator->sort('Campania.status',__('Estado',true));?></th>        
				<th class="actionsfijo"><?php  echo __('Actions');?></th>
			
			</tr>			
			</thead>
			
			<tbody>
				<?php  foreach ($campanias as $campania):?>
				<tr>
					<td><?php 
						echo $this->Html->link(
								$this->Html->image("/img/Campanias/".$campania['Campania']['fileName'], array('WIDTH'=>$campania['Campania']['anchura'], 'HEIGHT'=>80, "alt" => "mostrar", 'width'=>'19')),
								$campania['Campania']['url'],
    							array('escape' => false)
						); 
					?>
					</td>
					<td><?php 	echo $campania['Campania']['fileName']; ?></td>
					<td><?php 	echo $campania['Campania']['titulo']; ?></td>
					<td><?php 	echo $campania['Campania']['description']; ?></td>
					<td class="textc">
						<?php echo $campania['Campania']['status'] == 'AC' ?__('Activo',true):__('Desactivo',true); ?>
					</td>
			
					<td class="actionsfijo">						
					<?php 
						if(in_array($campania['Campania']['status'], array('AC','DE','LI'))){
							echo $this->Html->link(
								$this->Html->image('derco/defecto/eliminar.png', array('title'=>__('eliminar',true), "alt" =>__('eliminar',true))), 
								array('action'=>'delete', $campania['Campania']['id']), 
								array('escape'=>false), 
								sprintf(__('estaSeguroEliminar %s?'), $campania['Campania']['description'])
							);
						}
					?>
					</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
		</table>

<div id ="paging" class="span-18">
    <?php echo $this->element('paginador'); ?>
</div>
<div class="clear"></div>