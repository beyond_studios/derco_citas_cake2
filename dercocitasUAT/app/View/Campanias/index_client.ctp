<table border="0" cellpadding="0" cellspacing="0" class="web">
	<tr>
		<td class="menuBar" style="height: 467px;">
			<?php echo $this->element('menu_web_taller') ?>
		</td>
		<td>
            <span class="tituloSeccion" style="width: 770px; margin-top: 2px;">
                <?php echo __('NUESTROS SERVICIOS');?>
            </span>
			<br/>
			<table id="formularioEdicion" class="" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
			<tbody>
				<tr>
					<tr>
		            	<td class="etiqueta">
		            		<?php echo __('REPORTE_VEHICULO_LISTA');?>
		            	</td>
						<td class="valor">
							
						</td>
						
					</tr>
				</tr>
			</tbody>
			</table>
			<hr/>
			<table id = "listaPrincipal">
				<thead>
					<tr>
						<th><?php echo __('Imagen')?></th>
	                    <th><?php echo __('descripcion')?></th>
	                    <th><?php echo __('estado')?></th>
	                    <th style="width:90px"><?php echo __('GENERAL_ACCIONES')?></th>
				    </tr>
				</thead>
				<tbody>
					<?php foreach ($campanias as $key=>$campania): $class = ($key%2==0) ? "par" : "impar"; ?>
				    <tr class="<?php echo $class; ?>">
				        <td class="texto"><?php echo $campania['Campania']['img']; ?></td>
				        <td class="texto"><?php echo $campania['Campania']['description']; ?></td>
				        <td class="texto"><?php echo $campania['Campania']['status']; ?></td>
				        <td class="accion">
					            <?php 
									echo $this->Html->image('derco/defecto/pedir_cita.png', array('border'=>'0', 
										'title'=>__('generarCita', true),
										'onClick'=>'checkCitations(\''.$campania['AgeclientesVehiculo']['id'].'\')'
									));
								?>
	                        </td>
				    </tr>
					<?php endforeach; ?>
				</tbody>
			</table>
 		</td>
	</tr>
</table>