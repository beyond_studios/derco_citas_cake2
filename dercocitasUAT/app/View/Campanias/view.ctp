<div class="login width100 titleCenter" style="text-align: left;">
					<h1>PROMOCIONES/CAMPAÑAS</h1>
		
		<div id="main">
			<div class="nano">
				<div class="overthrow nano-content description">
					<?php  foreach ($campanias as $campania):?>
						<div class='servicio'>
						
							<?php
									if(empty($campania['Campania']['url'])){
										echo  $this->Html->image("/img/Campanias/".$campania['Campania']['fileName'], array("alt" => "Campañas DERCO")); 	
									}else{
										echo $this->Html->link(
											$this->Html->image("/img/Campanias/".$campania['Campania']['fileName'], array("alt" => "Campañas DERCO",  "title"=>"Haz click y enterate más de nuestras campañas")), 
											$campania['Campania']['url'], 
											array('escape'=>false, "target"=>"_blank")
										);
									}
									?>
						
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>	

		<div class="logosFooter">
			<img src="/dist/images/basLogos.png">
		</div>
	</div>
