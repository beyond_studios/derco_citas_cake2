
<div class="span-9 botones" > 
<?php echo $this->Session->flash();?>
	<table cellpadding="0" cellspacing="0" class="table">
		<thead>
			<tr>
				<th colspan="2" scope="col">
					<?php echo __('Grupo');?>
				</th>
			</tr>
		</thead>
		<tbody>

			<tr>
		 		<td><label><?php echo __('description') ?></label> </td>
		 		<td><?php echo $agegrupo['Agegrupo']['description']; ?></td>
		 	</tr>


			<tr>
		 		<td><label><?php echo __('estado'); ?></label> </td>
		 		<td><?php echo $agegrupo['Agegrupo']['status'] == 'AC' ? 
							__('Enable',true)
						:
							($agegrupo['Agegrupo']['status'] == 'DE'?
								__('Disable',true)
							:
								__('Limited',true))
						; ?></td>
		 	</tr>
		</tbody>
	</table>	
	<br/>
	<?php echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
	<?php echo $this->Form->end();?>
	<?php echo $this->element('actualizar_ver'); ?>
</div>