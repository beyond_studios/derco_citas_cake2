<?php echo $this->Html->script('agetiposervicios/index.js'); ?>
<?php echo $this->Session->flash();?>
		<br/>
		<h3 id="tituloTable"><?php echo(__('AGE_TIPOSERVICIO_TITULO',true));?></h3>
		
		<div class="box">
			<div id="agregar" class="span-5" >
			<?php echo $this->element('agregar'); ?>	
			</div>
		
			<div id="buscador" class="">
				<?php echo $this->element('buscador', array('elementos'=>$elementos,'url' => 'index')); ?>
			</div>
		</div>	
			
		<table cellpadding="0" cellspacing="0" class="table" >
			<thead>
				<tr>
				<?php $this->Paginator->options(array('url' =>$this->passedArgs)) ;  
				?>
				<th><?php echo $this->Paginator->sort('Agemotivoservicio.description',__('AGE_MOTIVOSERVICIO_DESCRIPCION',true));?></th>
				<th><?php echo $this->Paginator->sort('Agetiposervicio.codigo_sap',__('GENERAL_CODIGO_SAP',true));?></th>
				<th><?php echo $this->Paginator->sort('Agetiposervicio.description',__('AGE_TIPOSERVICIO_TITULO',true));?></th>
				<th><?php echo __('AGE_TIPOSERVICIO_MOSTRAR_MANTENIMIENTO',TRUE);?></th>
				<th><?php echo __('estado',TRUE);?></th>
				<th class="actionsfijo"><?php echo __('Actions',true );?></th>
				</tr>			
			</thead>
			
			<tbody>
				<?php  foreach ($agetiposervicios as $agetiposervicio):?>
				<tr>
				<td>
					<?php 	
							echo $agetiposervicio['Agemotivoservicio']['description'];	
					?>	
				</td>
				<td>
					<?php 	
							echo $agetiposervicio['Agetiposervicio']['codigo_sap'];	
					?>	
				</td>
				<td>
					<?php 		
							$url = $this->Html->url(array('action'=>'view', $agetiposervicio['Agetiposervicio']['id']));
							echo $this->Html->link($agetiposervicio['Agetiposervicio']['description'], 'javascript:;',array('onclick' => "mostrar('".$url."')",'escape'=>false), null);	
					?>	
				</td>					
				<td class="textc"><?php echo $agetiposervicio['Agetiposervicio']['mostrar_mantenimiento'] == 1 ? 
									__('si',true):($agetiposervicio['Agetiposervicio']['status'] == 0?__('no',true)
									:__('Limited',true)); ?>
				</td>		
				<td class="textc">
				<?php echo $agetiposervicio['Agetiposervicio']['status'] == 'AC' ? 
									__('Enable',true):($agetiposervicio['Agetiposervicio']['status'] == 'DE'?__('Disable',true)
									:__('Limited',true)); ?>
				</td>
		
				<td class="actionsfijo">
				<?php echo $this->element('action', 
								array('id'=>$agetiposervicio['Agetiposervicio']['id'], 
										'name'=>$agetiposervicio['Agetiposervicio']['description'], 										
										'estado'=> $agetiposervicio['Agetiposervicio']['status'])); ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

<div id ="paging" class="span-18">
		<?php echo $this->element('paginador'); ?>
</div>
<div class="clear"></div>