
<div class="login centerAll small">
<?php echo $this->Session->flash(); ?>
<h1 class="titleCenter">INGRESO</h1>

<?php echo $this->Html->script('clientes/index'); ?>
<?php echo $this->Form->create('Cliente', array('class'=>'fomrLogin','url' => array('controller' => 'Clientes', 'action' =>'loginCliente')));  ?>
  
    <div class="formTable width70">
        <div id="camposLogin" class="groupFields">
            <div class="field" style='margin-bottom:20px'>
                <label style='padding-bottom:15px'><?php echo $this->Form->label(__('Número de Documento',true)); ?></label>
                <?php echo $this->Form->text('username',array('style'=>'text-align:center','placeholder' => 'Ingrese su N° de Documento')); ?>
                <div class="errorValidacion"></div>
            </div>
        </div>

        <div class="groupFields">
            <div class="field">
                <label style='padding-bottom:15px'><?php echo $this->Form->label(__('Contraseña',true)); ?></label>
                <?php echo $this->Form->text('password',array('style'=>'text-align:center','placeholder' => 'Ingrese su Contraseña', 'type'=>'password')); ?>
                <div class="errorValidacion"></div>
            </div>
        </div>
        <div class="groupFields">
            <div class="field">
                <?php echo $this->Form->submit(__('Ingresar',true),array('div' => false, 'class'=>'guinda')); ?>
            </div>
        </div>
        <div class="groupFields col2" style="margin-top: 20px;">
            <div class="field">
                <?php
                    $url =  $this->Html->url(array('action'=>'forgot_password'));
                    echo $this->Html->link(__('¿Olvidó su contraseña?', true),$url,array('class'=>'linksForm',));
                ?>
            </div>
            <div class="field">
                <?php
                    $url =  $this->Html->url(array('action'=>'addWebCliente'));
                    echo $this->Html->link(__('Quiero registrarme', true), $url,array('class'=>'color_guinda linksForm'));
                ?>
            </div>
        </div>
    </div>
        <?php
    echo $this->Form->end();
?>
</div>

