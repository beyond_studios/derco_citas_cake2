<?php echo $this->Html->script('messages/dialog_message.js'); ?>

<?php echo $this->Html->script('clientes/bandeja_reprogramar.js'); ?>

<div class="login width100 reform">
<h1 style="text-align: left;"><?php echo __('BANDEJA_REPROGRAMAR');?></h1>
	<div class="tableData styleTable2">
		<?php if(!empty($citas)) { ?>
		<div style="overflow: auto; max-height: 150px">
			<table id="formularioEdicion">
				<thead>
					<tr>
						<th><?php echo __('BANTRES_ETIQUETA_ID')?></th>
						<th><?php echo __('BANTRES_ETIQUETA_FECHA_HORA_CITA')?></th>
						<th><?php echo __('BANTRES_ETIQUETA_PLACA')?></th>
						<th><?php echo __('BANTRES_ETIQUETA_SERVICIO')?></th>
						<th><?php echo __('BANTRES_ETIQUETA_FECHA_HORA_REGISTRO')?></th>
						<th><?php echo __('estado')?></th>
						<th><?php echo __('Actions')?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($citas as $key=>$cita): ?>
					<?php $class = ($key%2 == 0) ? "par" : "impar"; ?>
					<tr class="<?php echo $class; ?>" onclick="f_cita('<?php echo $cita['Agedetallecita']['id']?>')" style="cursor: pointer;">
						<td><?php echo $cita['Agedetallecita']['otsap']; ?></td>
						<td><?php echo date('d/m/Y', strtotime($cita['Agedetallecita']['fechadecita'])).' '.date('H:i', strtotime($cita['Agedetallecita']['fechadecita'])); ?></td>
						<td><?php echo $cita['Agedetallecita']['placa']; ?></td>
						<td><?php echo $cita['Agemotivoservicio']['description']; ?></td>
						<td><?php echo date('d/m/Y', strtotime($cita['Agedetallecita']['fechaRegistro'])).' '.date('H:i', strtotime($cita['Agedetallecita']['fechaRegistro'])); ?></td>
						<td><?php echo $this->Xhtml->getStatus($cita['Agedetallecita']['estado']); ?></td>
						<td class='instead-hover'>
							<?php
							if(in_array($cita['Agedetallecita']['estado'],array('AC'))){
								
								echo $this->Html->link('<i class="fa fa-pencil"></i>', 
									array('controller'=>'Agedetallecitas','action'=>'reprogramarCitaClient', $cita['Agedetallecita']['id']), 
									array('escape'=>false, 'class'=>'c-editable'), 
									sprintf(__('Esta seguro de que desea reprogramar la cita del %s  [%s - %s], del vehiculo con placa %s ?', date('d-m-Y', strtotime(substr($citas[0]['Agecitacalendariodia']['initDateTime'], 0 , 10))), substr($citas[0]['Agecitacalendariodia']['initDateTime'], 11, 5), substr($citas[0]['Agecitacalendariodia']['endDateTime'], 11, 5), $cita['Agedetallecita']['placa']))
								);

								echo $this->Html->link(
									'<i class="fa fa-times"></i>',
									array('controller'=>'Agedetallecitas','action'=>'deleteClient', $cita['Agedetallecita']['id']), 
									array('escape'=>false,'class'=>'c-delete'), 
									sprintf(__('Esta seguro de que desea eliminar su cita del %s  [%s - %s], del vehiculo con placa %s ?', date('d-m-Y', strtotime(substr($citas[0]['Agecitacalendariodia']['initDateTime'], 0 , 10))), substr($citas[0]['Agecitacalendariodia']['initDateTime'], 11, 5), substr($citas[0]['Agecitacalendariodia']['endDateTime'], 11, 5), $cita['Agedetallecita']['placa']))
								);
								echo '&nbsp;';
								echo '&nbsp;';
								if(empty($cita['Agecitacalendariodia'])){
									$cita['Agecitacalendariodia']['initDateTime'] = $cita['Agedetallecita']['fechadecita'];
									$cita['Agecitacalendariodia']['endDateTime'] = $cita['Agedetallecita']['fechadecita'];
								}
								
							}
							?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<div class="titleBar" style="text-align: left;">
			<i class="fa fa-user" aria-hidden="true"></i><b>DETALLE DE LA CITA</b>
		</div>
			<div id="cita">
				<div style="padding:10px" id="flashMessage">
					<?php echo __('BANTRES_EN_ESTE_MOMENTO_NO_EXISTE_NINGUN_ELEMENTO_SELECCIONADO'); ?>
				</div>
			</div>
		<?php }else{ ?>
			<div style='padding:40px'>
				<h2>No hay citas que mostrar</h2>
			</div>
		<?php }?>
		<?php //echo $this->renderElement('mensaje_web'); ?>
	</div>
	<div class="logosFooter">
		<img src="dist/images/basLogos.png">
	</div>
</div>