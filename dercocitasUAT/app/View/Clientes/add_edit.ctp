<?php //echo $this->Html->script('Agedetallecitas/calendar.js');?>
<?php //echo $this->Html->script('jquery.js');?>
<?php //echo $this->Html->css('calendar.css');?>
<?php //echo $this->Html->script('Agedetallecitas/index.js'); ?>

<?php //echo $this->Html->script('modulo_taller/table_dinamic.js'); ?>
<?php 
	//  echo $this->Html->script('Agedetallecitas/set_estimate.js', false);
	//echo $this->Html->script('jquery.alerts/jquery.alerts.js',false);
	//echo $this->Html->css('/js/jquery.alerts/jquery.alerts.css');?>

<?php echo $this->Html->script('agedetallecitas/add_edit.js'); ?>
<?php echo $this->Html->script('clientes/get_clientes.js'); ?>
<?php echo $this->Html->script('clientes/set_estimate.js'); ?>

<!-- DIALOGOS A UTILIZAR -->
<div id="dialog_cliente" title="Buscar cliente">
	<div id="containerclientes">
		<table id="clientes"></table> 
		<div id="clientes-pager"></div>
	</div>
</div>

<div class="span-8-0" >
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-8-0" >
		<h3><?php echo (__('Cita de Taller',true));?></h3>
	</div>
	<hr/>
	
	<?php echo $this->Form->create('Agedetallecita');?>
	<?php 
			$image = $this->Html->image('cross.png', array('title'=>__('Cerrar',true), "alt" => "Cerrar"));
			echo $this->Html->link($image, 'javascript:;',array('onClick' => 'javascript:window.close()','escape'=>false), null, false);
	?>
	&nbsp;
	<?php 
			echo $this->Html->link('Cerrar', 'javascript:;',array('onClick' => 'javascript:window.close()'));
	?>
	<div><!--este div es la parte de arriba-->	
	<div class="span-25">
		<div class="span-9" >
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('apellidosNombres')); ?><span class="error"><?php echo " *";?></span>
				</label> 
			</div>
			<div class="span-5 last" >				
				<?php echo $this->Form->input('Cliente.apellidoPaterno', array('label'=>'','class'=>'span-4','div'=>false, 'label'=>false));
				?>	
				<?php echo $this->Html->image('/img/search_icon.gif', array('style' => 'cursor: pointer;', 'onclick' => "abrirDialog('dialog_cliente')")); ?>	
				<?php echo $this->Form->hidden('Cliente.id', array()) ?>
			</div>
		</div>		
		<div class="span-9" >
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('tipoDocumento')); ?><span class="error">
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('Cliente.documento_tipo', array('label'=>'','class'=>'span-4','disabled'=>'disabled','error'=>false)); 
				?>	
			</div>
		</div>			
		<div class="span-9" >
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('numeroDocumento')); ?>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('Cliente.documento_numero', array('label'=>'','class'=>'span-4','disabled'=>'disabled','error'=>false)); 
				?>	
			</div>
		</div>			
		<div class="span-9" >
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('distrito')); ?>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('Cliente.distrito', array('label'=>'','class'=>'span-4','disabled'=>'disabled','error'=>false)); 
				?>	
			</div>
		</div>			
		<div class="span-9" >
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('telefono')); ?>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('Cliente.telefono', array('label'=>'','class'=>'span-4','disabled'=>'disabled','error'=>false)); 
				?>	
			</div>
		</div>			
		<div class="span-9" >
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('celular')); ?>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('Cliente.celular', array('label'=>'','class'=>'span-4','disabled'=>'disabled','error'=>false)); 
				?>	
			</div>
		</div>		
		<div class="span-9" >
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('email')); ?>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('Cliente.email', array('label'=>'','class'=>'span-4','disabled'=>'disabled','error'=>false)); 
				?>	
			</div>
		</div>			
	</div>
			
	<div class="span-25-0">
		<div class="span-9" >
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('Placa')); ?><span class="error"><?php echo " *";?></span>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->select('placa',array($vehiculosclientes),array('class'=>'span-5','empty'=>'seleccione..'),false);
					  echo $this->Form->error('placa', array(															
													   	'maxLength' =>  __('Solo se pueden ingresar 60 carcteres'),
														'notEmpty' =>  __('empresaNombreNoVacio')							      
														), array('class' => 'input text required error'));				
				?>		
			</div>
		</div>		
		<div class="span-9" >
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('Marca')); ?><span class="error">
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('marca', array('label'=>'','class'=>'span-4','disabled'=>'disabled','error'=>false)); 
				?>	
			</div>
		</div>			
		<div class="span-9" >
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('Modelo')); ?>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('modelo', array('label'=>'','class'=>'span-4','disabled'=>'disabled','error'=>false)); 
				?>	
			</div>
		</div>				
	</div>
	</div><!--fin del div de arriba-->
	<div style="width: 800px;">&nbsp;</div><br /><br /><br /><br /><br /><br />
	<div><!--fin del div del medio-->
	<div class="span-25">
		<div class="span-9" >
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('Sucursal')); ?><span class="error"><?php echo " *";?></span>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->select('Secproject.id',array($secprojects),array('class'=>'span-5','empty'=>'seleccione..'),false);
					  echo $this->Form->error('maximo_citasprogramado', array(															
													   	'maxLength' =>  __('Solo se pueden ingresar 60 carcteres'),
														'notEmpty' =>  __('empresaNombreNoVacio')							      
														), array('class' => 'input text required error'));				
				?>		
			</div>
		</div>		
		<div class="span-9" >
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('Tipo de servicio')); ?><span class="error"><?php echo " *";?></span>
				</label> 
			</div>
			<div class="span-5 last" >
				
				<?php echo $this->Form->select('agemotivoservicio_id',array($agetiposervicios),array('class'=>'span-5','empty'=>'seleccione..'),false);
					  echo $this->Form->error('maximo_citasprogramado', array(															
													   	'maxLength' =>  __('Solo se pueden ingresar 60 carcteres'),
														'notEmpty' =>  __('empresaNombreNoVacio')							      
														), array('class' => 'input text required error'));				
				?>	
			</div>
		</div>				
	</div>		
	<div class="span-25-0">
		<div class="span-9" >
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('Motivo de Servicio')); ?><span class="error"><?php echo " *";?></span>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->select('agemotivoservicio_id',array($agemotivoservicios),array('class'=>'span-5','empty'=>'seleccione..'),false);
					  echo $this->Form->error('maximo_citasprogramado', array(															
													   	'maxLength' =>  __('Solo se pueden ingresar 60 carcteres'),
														'notEmpty' =>  __('empresaNombreNoVacio')							      
														), array('class' => 'input text required error'));				
				?>		
			</div>
		</div>		
		<div class="span-9" >
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('Tipo de Mantenimiento')); ?><span class="error"><?php echo " *";?></span>
				</label> 
			</div>
			<div class="span-5 last" >				
				<?php echo $this->Form->select('agemotivoservicio_id',array($agetipomantenimientos),array('class'=>'span-5','empty'=>'seleccione..'),false);
					  echo $this->Form->error('maximo_citasprogramado', array(															
													   	'maxLength' =>  __('Solo se pueden ingresar 60 carcteres'),
														'notEmpty' =>  __('empresaNombreNoVacio')							      
														), array('class' => 'input text required error'));				
				?>	
			</div>
		</div>				
	</div>		
	</div>	<!--fin del div del medio-->
	<div style="width: 800px;">
		<div class="span-8-0">
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('Servicios Solicitados')); ?>
				</label> 
			</div>
			<?php echo $this->Form->textarea('agemotivoservicio_id', array('style' => 'width: 580px; height:30px;'));?>
		</div>
		<div class="span-8-0">
			<div class="span-4" >
				<label><?php echo $this->Form->label(__('Fecha')); ?>
				</label> 
			</div>
			<?php 
				echo $this->Form->input('inicio', array('label' => '', 'class'=>'calendarSelectDate','div'=>false, 'label'=>false));
				echo "<div id='calendarDiv'></div>";
			?>			
			<?php echo $this->Form->submit(__('Cargar Horas',true), array('div'=>false));	?>
		</div>
	</div>
	<div><!--div de hiorarios por taller -->
		<fieldset>
			<legend>
				<label><?php echo $this->Form->label(__('Horarios por taller')); ?></label>
			</legend>	
				<p>Aqui va ir su contenido</p>
		</fieldset>	
	</div>	
	<div><!--div de consultas de dias habiles-->
		<fieldset>
			<legend>
				<label><?php echo $this->Form->label(__('Consultas de días Hábiles')); ?></label>
			</legend>		
			<div>	
				<?php
				echo $this->Form->label(__('Desde'));
				echo '&nbsp;';
				echo $this->Form->input('inicio', array('label' => '', 'class'=>'calendarSelectDate','div'=>false, 'label'=>false));
				echo "<div id='calendarDiv'></div>";
				echo '&nbsp;'; 
				echo $this->Form->label(__('Hasta'));
				echo '&nbsp;';
				echo $this->Form->input('fin', array('label' => '', 'class'=>'calendarSelectDate','div'=>false, 'label'=>false));
				echo "<div id='calendarDiv'></div>";	 
				echo '&nbsp;';
				echo $this->Form->submit(__('Buscar',true), array('div'=>false));
				?></div>
			<div>	
				<div class="span-25">
					<div class="span-9" >
						<div class="span-4" >
							<label><?php echo $this->Form->label(__('Sucursal')); ?><span class="error"><?php echo " *";?></span>
							</label> 
						</div>
						<div class="span-5 last" >
							<?php echo $this->Form->select('Secproject.id',array($secprojects),array('class'=>'span-5','empty'=>'seleccione..'),false);
								  echo $this->Form->error('maximo_citasprogramado', array(															
																   	'maxLength' =>  __('Solo se pueden ingresar 60 carcteres'),
																	'notEmpty' =>  __('empresaNombreNoVacio')							      
																	), array('class' => 'input text required error'));				
							?>		
						</div>
					</div>	
					<div class="span-9" >
						<div class="span-4" >
							<label><?php echo $this->Form->label(__('Motivo de Servicio')); ?><span class="error"><?php echo " *";?></span>
							</label> 
						</div>
						<div class="span-5 last" >
							<?php echo $this->Form->select('agemotivoservicio_id',array($agemotivoservicios),array('class'=>'span-5','empty'=>'seleccione..'),false);
								  echo $this->Form->error('maximo_citasprogramado', array(															
																   	'maxLength' =>  __('Solo se pueden ingresar 60 carcteres'),
																	'notEmpty' =>  __('empresaNombreNoVacio')							      
																	), array('class' => 'input text required error'));				
							?>		
						</div>
					</div>				
				</div>	
				<div class="span-25-1">
					<div class="span-9" >
						<div class="span-4" >
							<label><?php echo $this->Form->label(__('Dirección')); ?><span class="error"><?php echo " *";?></span>
							</label> 
						</div>
						<div class="span-5 last" >
							<?php echo $this->Form->input('Secproject.direccion', array('label'=>'','class'=>'span-4','error'=>false));		
							?>		
						</div>
					</div>				
				</div>	
			</div>
		</fieldset>	
	</div>	
		<br/>
		<hr/>	
	<?php echo $this->Form->end(); ?>

<?php echo $this->element('actualizar_ver'); ?>
</div>