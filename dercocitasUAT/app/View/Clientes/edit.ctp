
<div class="span-8" >	
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-20" >
		<h3 align="left"><?php echo __('Modificar Datos Personales');?></h3>
	</div>
	<hr/>
	
	<?php echo $this->Form->create('Cliente');?>
	<?php echo $this->Form->input('id', array('type'=>'hidden')); ?>
	
	<table border="0">
		<tr>
			<td>
		<div class="span-11" >
			<div class="span-3" >
				<label><?php echo (__('Código SAP',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('codigo_sap', array('label'=>false, 'class'=>'span-5','error'=>false));  
				 	  echo $this->Form->error('codigo_sap', array(															
													    'isUnique' =>  __('empresaCodigoUnico', true),
														'notEmpty' =>  __('empresaCodigoNoVacio', true),
														'maxLength' =>  __('empresaCodigoLongitud', true),
														'alphaNumeric' =>  __('empresaCodigoEspacios', true)													      
														), array('class' => 'input text required error'));		
						
				?>	
			</div>
		</div>
		
		
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo (__('Apellidos',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('apellidoPaterno', array('label'=>false, 'class'=>'span-5','error'=>false));  
				 	  echo $this->Form->error('apellidoPaterno', array(															
													    'isUnique' =>  __('empresaCodigoUnico', true),
														'notEmpty' =>  __('empresaCodigoNoVacio', true),
														'maxLength' =>  __('empresaCodigoLongitud', true),
														'alphaNumeric' =>  __('empresaCodigoEspacios', true)													      
														), array('class' => 'input text required error'));		
						
				?>	
			</div>
		</div>
		
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo (__('Nombres',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('nombres', array('label'=>false, 'class'=>'span-5','error'=>false));  
				 	  echo $this->Form->error('nombres', array(															
													    'isUnique' =>  __('empresaCodigoUnico', true),
														'notEmpty' =>  __('empresaCodigoNoVacio', true),
														'maxLength' =>  __('empresaCodigoLongitud', true),
														'alphaNumeric' =>  __('empresaCodigoEspacios', true)													      
														), array('class' => 'input text required error'));		
						
				?>	
			</div>
		</div>
		
		
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo (__('Tipo de Doc.',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('documento_tipo', array('label'=>false, 'class'=>'span-5','error'=>false));  
				 	  echo $this->Form->error('documetno_tipo', array(															
													    'isUnique' =>  __('empresaCodigoUnico', true),
														'notEmpty' =>  __('empresaCodigoNoVacio', true),
														'maxLength' =>  __('empresaCodigoLongitud', true),
														'alphaNumeric' =>  __('empresaCodigoEspacios', true)													      
														), array('class' => 'input text required error'));		
						
				?>	
			</div>
		</div>
		
				
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo (__('Número de Doc.',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('documento_numero', array('label'=>false, 'class'=>'span-5','error'=>false));  
				 	  echo $this->Form->error('documetno_numero', array(															
													    'isUnique' =>  __('empresaCodigoUnico', true),
														'notEmpty' =>  __('empresaCodigoNoVacio', true),
														'maxLength' =>  __('empresaCodigoLongitud', true),
														'alphaNumeric' =>  __('empresaCodigoEspacios', true)													      
														), array('class' => 'input text required error'));		
						
				?>	
			</div>
		</div>
		
		
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo (__('Email',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('email', array('label'=>false, 'class'=>'span-5','error'=>false));  
				 	  echo $this->Form->error('email', array(															
													    'isUnique' =>  __('empresaCodigoUnico', true),
														'notEmpty' =>  __('empresaCodigoNoVacio', true),
														'maxLength' =>  __('empresaCodigoLongitud', true),
														'alphaNumeric' =>  __('empresaCodigoEspacios', true)													      
														), array('class' => 'input text required error'));		
						
				?>	
			</div>
		</div>
		</td>
		
		<td>
			
			<div class="span-8" >
			<div class="span-3" >
				<label><?php echo (__('Dirección',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('direccion', array('label'=>false, 'class'=>'span-5','error'=>false));  
				 	  echo $this->Form->error('direccion', array(															
													    'isUnique' =>  __('empresaCodigoUnico', true),
														'notEmpty' =>  __('empresaCodigoNoVacio', true),
														'maxLength' =>  __('empresaCodigoLongitud', true),
														'alphaNumeric' =>  __('empresaCodigoEspacios', true)													      
														), array('class' => 'input text required error'));		
						
				?>	
		   	 </div>
		</div>
		
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo (__('Ciudad',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('ciudad', array('label'=>false, 'class'=>'span-5','error'=>false));  
				 	  echo $this->Form->error('ciudad', array(															
													    'isUnique' =>  __('empresaCodigoUnico', true),
														'notEmpty' =>  __('empresaCodigoNoVacio', true),
														'maxLength' =>  __('empresaCodigoLongitud', true),
														'alphaNumeric' =>  __('empresaCodigoEspacios', true)													      
														), array('class' => 'input text required error'));		
						
				?>	
			</div>
		</div>
		
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo (__('Distrito',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('distrito', array('label'=>false, 'class'=>'span-5','error'=>false));  
				 	  echo $this->Form->error('distrito', array(															
													    'isUnique' =>  __('empresaCodigoUnico', true),
														'notEmpty' =>  __('empresaCodigoNoVacio', true),
														'maxLength' =>  __('empresaCodigoLongitud', true),
														'alphaNumeric' =>  __('empresaCodigoEspacios', true)													      
														), array('class' => 'input text required error'));		
						
				?>	
			</div>
		</div>
		
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo (__('Teléfono',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('telefono', array('label'=>false, 'class'=>'span-5','error'=>false));  
				 	  echo $this->Form->error('telefono', array(															
													    'isUnique' =>  __('empresaCodigoUnico', true),
														'notEmpty' =>  __('empresaCodigoNoVacio', true),
														'maxLength' =>  __('empresaCodigoLongitud', true),
														'alphaNumeric' =>  __('empresaCodigoEspacios', true)													      
														), array('class' => 'input text required error'));		
						
				?>	
			</div>
		</div>
		
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo (__('Celular',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('celular', array('label'=>false, 'class'=>'span-5','error'=>false));  
				 	  echo $this->Form->error('celular', array(															
													    'isUnique' =>  __('empresaCodigoUnico', true),
														'notEmpty' =>  __('empresaCodigoNoVacio', true),
														'maxLength' =>  __('empresaCodigoLongitud', true),
														'alphaNumeric' =>  __('empresaCodigoEspacios', true)													      
														), array('class' => 'input text required error'));		
						
				?>	
			</div>
		</div>
		
		</td>
		
		</tr>
		</table>
		
		
		<table>
			<tr>
				<td>
			<div class="span-11" >
			<div class="span-3" >
				<label><?php echo (__('Número de Doc.',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('documento_numero', array('label'=>false, 'class'=>'span-5','error'=>false));  
				 	  echo $this->Form->error('documento_numero', array(															
													    'isUnique' =>  __('empresaCodigoUnico', true),
														'notEmpty' =>  __('empresaCodigoNoVacio', true),
														'maxLength' =>  __('empresaCodigoLongitud', true),
														'alphaNumeric' =>  __('empresaCodigoEspacios', true)													      
														), array('class' => 'input text required error'));		
						
				?>	
			</div>
		</div>
		
		
		<div class="span-11" >
			<div class="span-3" >
				<label><?php echo (__('Placa',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php 
				echo $this->Form->select('placa',$ageclientesvehiculos,array('label'=>false, 'class'=>'span-5','error'=>false));  				
				?>	
			</div>
		</div>
		
		
		</td>
		</tr>
		</table>
		
		
		</div>			
		
		<br/>
		<hr/>
		
		<div class=" span-8 botones" >
			<?php echo $this->Form->submit(__('Submit',true), array('div'=>false));	?>
			<?php echo $this->Form->button(__('Reset',true), array('type'=>'reset')); ?>				
			<?php echo $this->Form->button(__('Close',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>
	
	<?php echo $this->Form->end(); ?>

<?php echo $this->element('actualizar_ver'); ?>
</div>