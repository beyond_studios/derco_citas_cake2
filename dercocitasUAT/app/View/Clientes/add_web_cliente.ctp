<div class="login titleCenter small">
    <?php echo $this->Html->script('clientes/add_web_cliente'); ?>
	<?php echo $this->Session->flash();?>
                    <h1><?php echo (__('Registro',true));?></h1>
	<?php echo $this->Form->create('Cliente', array('class'=>'fomrLogin'));?>	
                    
						
						<div class="formTable leftAling">
							<div class="groupFields col2">
								<div class="field">
									<div>
										<label class='leftAling'><?php echo (__('CLIENTE_CAMPO_TIPO_DOCUMETO', true)) ?><span class="error"><?php echo " *"; ?></span></label>
									</div>
									<div>
										<?php $tipo = array('-1'=>__('Seleccione', TRUE), 'DN'=>__('DNI'), 'PA'=>__('Pasaporte'), 'RU'=>__('RUC')); ?>
                                        <?php
                                        //echo $this->Form->select('type', $tipo, null,array('label'=>'','class'=>'span-5','error'=>false), FALSE); 
                                        echo $this->Form->input('documento_tipo', array(
                                            'type' => 'select',
                                            'options' => $tipo,
                                            'label' => false,
                                            'class' => 'styled-select slate required"',
                                            'error' => false,
                                            'empty' => FALSE,
                                            'onChange'=>"validIpDoc();"
                                        ));
                                        ?> 
									</div>
								</div>
								<div class="field">
									<div>
										<label class='leftAling'><?php echo(__('Número de Documento', true)) ?><span class="error"><?php echo " *"; ?>	</span></label>
									</div>
									<div>
                                          <?php
                                                echo $this->Form->input('documento_numero', array('label' => false, 'placeholder'=>'Ingrese número', 'class' => 'span-4 required digits', 'error' => false));
                                            ?>
									</div>
								</div>
							</div>
							<div class="groupFields col2">
								<div class="field">
									<div>
										<label class='leftAling'>Marca del Vehículo:</label>
									</div>
									<div>
										<?php echo $this->Form->input('marca_id',array('id'=>'ClienteMarca', 'type'=>'select','empty'=>'Seleccione','options'=>$marcas, 'class'=>'styled-select slate', 'label'=>false)); ?>
									</div>
								</div>
								<div class="field">
									<div>
										<label class='leftAling'>Modelo:</label>
									</div>
									<div>
										<div id='ClienteModelo'></div>
									</div>
								</div>
							</div>
							<div class="groupFields">
								<div class="field">
									<div>
				                        <label class='leftAling'><?php echo (__('Placa',true)) ?></label><span class="error"></span>
									</div>
									<div>
											<?php echo $this->Form->input('placa', array('maxlength'=>6,'label'=>false,'placeholder'=>'Ingrese la placa de su vehículo', 'class'=>'required onlyPlaque','error'=>false,
															'style'=>'text-transform:uppercase;','onkeyup'=>'javascript:this.value=this.value.toUpperCase();'));  
				                            ?>	
									</div>
								</div>
							</div>
							<div class='bottom-actions'>
								<div class="groupFields centerAling">
									<div class="field">
                                        <?php echo $this->Form->submit(__('Registrarse',true), array('class'=>'guinda', 'div'=>false));	?>
                                        <?php echo $this->Form->button(__('Reset',true), array('class'=>'buttomReset','type'=>'reset', 'div'=>false)); ?>				
									</div>
								</div>
								<div class="groupFields">
									<div class="field" style="margin-top: 8px;">
                                        <?php echo $this->element('actualizar');?>
                                         <?php
                                            $url =  $this->Html->url(array('action'=>'login'));
                                            echo $this->Html->link(__('Ya soy usuario de Derco', true), $url,array('class'=>' linksForm'));
                                        ?>
									</div>
								</div>
							</div>
                        </div>
	                <?php echo $this->Form->end();?>
				</div>

<?php
$this->Js->get('#ClienteMarca')->event('change', 
$this->Js->request(array(
	'controller'=>'clientes',
	'action'=>'getModeloByMarca'
	), array(
	'update'=>'#ClienteModelo',
	'async' => true,
	'method' => 'post',
	'dataExpression'=>true,
	'data'=> $this->Js->serializeForm(array(
		'isForm' => true,
		'inline' => true
		))
	))
);
	if (class_exists('JsHelper') && method_exists($this->Js, 'writeBuffer')) echo $this->Js->writeBuffer();
?>

<script>
	$(function(){
		$('#ClienteMarca').trigger('change');
	});
</script>