<?php echo $this->element('dialog_message'); ?>
<?php echo $this->Html->script('clientes/taller_index.js'); ?>

<div class="login width100 reform">
<h1 style="text-align: left;">TALLER DE SERVICIOS</h1>
<div class="titleBar styleGriss" style="text-align: left;">
	<b> <?php echo __('TALLER_ETIQUETA_SEGUIMIENTO_VEHICULO');?></b>
</div>
<?php $url2 = $this->Html->url(array('controller'=>'Agedetallecitas','action'=>'setCitationClient' )); ?>
<?php echo $this->Form->create('Buscador', array('url'=>'tallerIndex', 'id'=>'buscadorForm','class'=>'fomrLogin', 'url2'=>$url2, 'type'=>'post'));?>
<?php echo $this->Form->hidden('Cliente.id', array('valur'=>$client_id))?>
	
	<div class="formTable leftAling ">
		<div class="groupFields" style='margin:0 -30px'>
			<div class="field" style="float:left;width: 50%;padding-right:30px">
				<div>
					<label class="leftAling">
						<?php echo __('REPORTE_VEHICULO_LISTA');?>
					</label>
				</div>
				<div>
					<select id="AgeclientesVehiculoId" class="msg_error_1 styled-select slate"  onchange="verificarVehicle(this);" name="data[AgeclientesVehiculo][id]">
						<option value=""><?php echo __('Seleccione', true) ?></option>
						<?php foreach($vehiculos as $value){
							echo "<option nro_citas=\"".$value['0']['nro_citas']."\" value=\"".$value['AgeclientesVehiculo']['id']."\">".utf8_encode($value['0']['label'])."</option>";
						} ?>
					</select>
				</div>
			</div>

			<div class="field" style="float:left;width: 30%;padding-right:30px">
				<div>
					<label class="leftAling">
						<?php echo __('placaNro');?>
					</label>
				</div>
				<div>
					<?php echo $this->Form->input('placa', array('label'=>false,'class'=>'','placeholder'=>'Ingrese número', 'div'=>false)); ?>
				</div>
			</div>
			<div class="field" style="float:left;width: 20%; text-aling:center">
				<div >
					<?php echo $this->Form->submit(__('BUSCAR'), array('style'=>'margin-top:20px;width:150px;','class'=>'buscar guinda'));?>
				</div>
			</div>
		</div>
		<div class="groupFields">

		</div>

	</div>
<?php echo $this->Form->end(); ?>

<div class="tableData">
		<table id = "listaPrincipal">
		<thead>
			<tr>
				<th style="text-align:center;color:#ffffff;"><?php echo __('TALLER_ETIQUETA_CODIGO')?></th>
				<th style="text-align:center;color:#ffffff;"><?php echo __('TALLER_ETIQUETA_MARCA')?></th>
				<th style="text-align:center;color:#ffffff;"><?php echo __('TALLER_ETIQUETA_MODELO')?></th>
				<th style="text-align:center;color:#ffffff;"><?php echo __('TALLER_ETIQUETA_PLACA')?></th>
				<th style="text-align:center;color:#ffffff;width:90px"><?php echo __('GENERAL_ACCIONES')?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($agevehiculos as $key=>$vehicle): $class = ($key%2==0) ? "par" : "impar"; ?>
			<tr class="<?php echo $class; ?>">
				<td class="texto"><?php echo $vehicle['AgeclientesVehiculo']['codigo_vehiculo']; ?></td>
				<td class="texto"><?php echo $vehicle['AgeclientesVehiculo']['marca']; ?></td>
				<td class="texto"><?php echo $vehicle['AgeclientesVehiculo']['modelo']; ?></td>
				<td class=centrado""><?php echo $vehicle['AgeclientesVehiculo']['placa']; ?></td>
				<td class="accion">
						<?php 
							if(empty($vehicle['AgeclientesVehiculo']['nro_citas']) || true){
								echo $this->Html->image('derco/defecto/pedir_cita.png', array('border'=>'0', 
									'title'=>__('generarCita', true),
									'onClick'=>'checkCitations(\''.$vehicle['AgeclientesVehiculo']['id'].'\')'
								));
							}
						?>
					</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>

<div class="logosFooter">
	<img src="dist/images/basLogos.png">
</div>
</div>
