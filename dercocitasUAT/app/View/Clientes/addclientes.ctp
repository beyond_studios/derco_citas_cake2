<div class="span-8" >
	<?php echo $this->Html->script('clientes/comun_clientes'); ?>
	<?php echo $this->Html->script('clientes/add_clientes'); ?>
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-8" >
		<h3><?php echo (__('Cliente',true));?></h3>
	</div>
	<hr/>
	
	<?php echo $this->Form->create('Cliente');?>
		<?php echo $this->Form->input('id', array('type' =>'hide')); ?>
        <div class="span-8" >
            <div class="span-3" >
                <label>
                    <?php echo (__('Tipo de Documento', true)) ?><span class="error"><?php echo " *"; ?>	</span>
                </label> 
            </div>
            <div class="span-5 last" >
                <?php $tipo = array(''=>__('', TRUE), 'DN'=>__('DNI', TRUE), 'PA'=>__('Pasaporte', TRUE), 'RU'=>__('RUC')); ?>
                <?php
                //echo $this->Form->select('type', $tipo, null,array('label'=>'','class'=>'span-5','error'=>false), FALSE); 
                echo $this->Form->input('documento_tipo', array(
                    'type' => 'select',
                    'options' => $tipo,
                    'label' => false,
                    'class' => 'span-5',
                    'error' => false,
                    'empty' => FALSE,
					'disabled'=>true,
                ));
                
                echo $this->Form->error('documento_tipo', array(
                    'isUnique' => __('empresaNombreUnico', true),
                    'notEmpty' => __('empresaNombreNoVacio', true),
                    'maxLength' => __('empresaNombreLongitud', true),
                        ), array('class' => 'input text required error'));
                ?> 
            </div>
        </div>
		
        <div class="span-8" >
            <div class="span-3" >
                <label>
                    <?php echo(__('CLIENTE_ETIQUETA_TIPO_DOCUMETO_NUMERO', true)) ?><span class="error"><?php echo " *"; ?>	</span>
                </label> 
            </div>
            <div class="span-5 last" >
                <?php
                echo $this->Form->input('documento_numero', array('label' => false, 'class' => 'span-5', 'error' => false,'disabled'=>true));
                echo $this->Form->error('documento_numero', array(
                    'isUnique' => __('empresaNombreUnico', true),
                    'notEmpty' => __('empresaNombreNoVacio', true),
                    'maxLength' => __('empresaNombreLongitud', true),
                        ), array('class' => 'input text required error'));
                ?>
            </div>
        </div>	

	    <div class="span-8" >
			<div class="span-3" >
				<label><?php echo (__('Placa',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
		 		<?php echo $this->Form->input('placa', array('label'=>false, 'class'=>'span-5','error'=>false,'disabled'=>true,));  
				 	  echo $this->Form->error('placa', array(															
													   'isUnique' =>  __('empresaCodigoUnico', true),
														'notEmpty' =>  __('empresaCodigoNoVacio', true),
														'maxLength' =>  __('empresaCodigoLongitud', true),
														'alphaNumeric' =>  __('empresaCodigoEspacios', true)													      
														), array('class' => 'input text required error'));		
						
				?>
			</div>
		</div>	

		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo (__('TIPO_DE_CLIENTE',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('str_cliente_tipo', array('label'=>false, 'class'=>'span-5','error'=>false,'disabled'=>true,));
						echo $this->Form->error('str_cliente_tipo', array(
							'isUnique' =>  __('empresaCodigoUnico', true),
							'notEmpty' =>  __('empresaCodigoNoVacio', true),
							'maxLength' =>  __('empresaCodigoLongitud', true),
							'alphaNumeric' =>  __('empresaCodigoEspacios', true)													      
							), array('class' => 'input text required error'));		
			
			?>
			</div>
		</div>

        <div class="span-8" >
            <div class="span-3" >
                <label><?php echo (__('Email', true)) ?></label><span class="error"><?php echo " *"; ?>	</span>
            </div>
            <div class="span-5 last" >
                <?php
                echo $this->Form->input('email', array('label' => false, 'class' => 'span-5', 'error' => false));
                echo $this->Form->error('email', array(
                    'isUnique' => __('empresaCodigoUnico', true),
                    'notEmpty' => __('empresaCodigoNoVacio', true),
                    'maxLength' => __('empresaCodigoLongitud', true),
                    'alphaNumeric' => __('empresaCodigoEspacios', true)
                        ), array('class' => 'input text required error'));
                ?>	
            </div>
        </div>	
                       
        <div class="span-8" >
            <div class="span-3" >
                <label><?php echo (__('Telefono', true)) ?></label><span class="error"><?php echo " *"; ?>	</span>
            </div>
            <div class="span-5 last" >
                <?php
                echo $this->Form->input('telefono', array('label' => false, 'class' => 'span-5', 'error' => false));
                echo $this->Form->error('telefono', array(
                    'isUnique' => __('empresaCodigoUnico', true),
                    'notEmpty' => __('empresaCodigoNoVacio', true),
                    'maxLength' => __('empresaCodigoLongitud', true),
                    'alphaNumeric' => __('empresaCodigoEspacios', true)
                        ), array('class' => 'input text required error'));
                ?>	
            </div>
        </div>	
 
        <div class="span-8" >
            <div class="span-3" >
                <label><?php echo (__('Celular', true)) ?></label><span class="error"><?php echo " *"; ?>	</span>
            </div>
            <div class="span-5 last" >
                <?php
                echo $this->Form->input('celular', array('label' => false, 'class' => 'span-5', 'error' => false));
                echo $this->Form->error('celular', array(
                    'isUnique' => __('empresaCodigoUnico', true),
                    'notEmpty' => __('empresaCodigoNoVacio', true),
                    'maxLength' => __('empresaCodigoLongitud', true),
                    'alphaNumeric' => __('empresaCodigoEspacios', true)
                        ), array('class' => 'input text required error'));
                ?>	
            </div>
        </div>	             

        <div id="rowLast" class="span-8" >
            <div class="span-3" >
                <label><?php echo (__('Usuario', true)) ?></label><span class="error"><?php echo " *"; ?>	</span>
            </div>
            <div class="span-5 last" >
                <?php
                echo $this->Form->input('username', array('label' => false, 'class' => 'span-5', 'error' => false,'disabled'=>true));
                echo $this->Form->error('username', array(
                    'isUnique' => __('empresaCodigoUnico', true),
                    'notEmpty' => __('empresaCodigoNoVacio', true),
                    'maxLength' => __('empresaCodigoLongitud', true),
                    'alphaNumeric' => __('empresaCodigoEspacios', true)
                        ), array('class' => 'input text required error'));
                ?>	
            </div>
        </div>	

		<br/>
		<hr/>
		
		<div class=" span-8 botones" >
			<?php if(!$guardadoExistoso){?>
				<?php echo $this->Html->image('loader.gif', array('id'=>'imgLoader','style' => 'display:none;'));?>
				<?php echo $this->Form->submit(__('Submit',true), array('id'=>'btnGuardar','div'=>false));	?>
				<?php echo $this->Form->button(__('Reset',true), array('id'=>'btnReset','type'=>'reset')); ?>
			<?php }?>				
			<?php echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>
	
	<?php echo $this->Form->end();?>

<?php echo $this->element('actualizar');?>
</div>