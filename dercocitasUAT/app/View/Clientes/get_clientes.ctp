<?php
	App::import('Vendor','json');
	$json = new Services_JSON;
	
	$responce->page = $this->Paginator->counter('%page%'); 
	$responce->total = $this->Paginator->counter('%pages%'); 
	$responce->records = $this->Paginator->counter('%count%'); 
	foreach($clientes as $key => $cliente){
		$responce->rows[$key]['id']=trim($cliente['Cliente']['id']);
		$responce->rows[$key]['cell'] = array(
			trim($cliente['Cliente']['id']),
			trim($cliente['Cliente']['documento_tipo']),
			trim($cliente['Cliente']['codigo_sap']),
			trim($cliente['Cliente']['documento_numero']),
			trim($cliente['Cliente']['nombres']),
			trim($cliente['Cliente']['distrito']),
			trim($cliente['Cliente']['telefono']),
			trim($cliente['Cliente']['celular']),
			trim($cliente['Cliente']['email'])
		);
	} 		
	echo  $json->encode($responce);