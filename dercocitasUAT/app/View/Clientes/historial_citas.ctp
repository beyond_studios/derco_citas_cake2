<?php echo $this->element('dialog_message'); ?>
<?php echo $this->Html->script('clientes/historial_citas.js'); ?>
<div class="login width100 reform">
		<h1 style="text-align: left;"><?php echo __('TALLER_ETIQUETA_HISTORIAL_CITAS_TALLER');?></h1>
					<div class="tableData styleTable2">
						<table id='formularioEdicion'>
							<thead>
								<tr>
									<th style="width: auto;"><?php echo __('BANTRES_ETIQUETA_ID')?></th>
									<th style="width: auto;"><?php echo __('BANTRES_ETIQUETA_FECHA_HORA_CITA')?></th>
									<th style="width: auto;"><?php echo __('BANTRES_ETIQUETA_PLACA')?></th>
									<th><?php echo __('BANTRES_ETIQUETA_SERVICIO')?></th>
									<th style="width: auto;"><?php echo __('BANTRES_ETIQUETA_FECHA_HORA_REGISTRO')?></th>
									<th style="width: auto;"><?php echo __('ESTADO')?></th>
								</tr>
							</thead>
							<tbody>
								 <?php foreach ($citas as $key=>$cita): ?>
									<?php $class = ($key%2 == 0) ? "par" : "impar"; ?>
									<tr class="<?php echo $class; ?>" onclick="f_cita('<?php echo $cita['Agedetallecita']['id']?>')" style="height: 25px; cursor: pointer;">
										<td class="centrado"><?php echo $cita['Agedetallecita']['otsap']; ?></td>
										<td class="centrado"><?php echo date('d/m/Y', strtotime($cita['Agedetallecita']['fechadecita'])).' '.date('H:i', strtotime($cita['Agedetallecita']['fechadecita'])); ?></td>
										<td class="texto" style="color: black;"><?php echo $cita['Agedetallecita']['placa']; ?></td>
										<td class="texto" style="color: black;"><?php echo $cita['Agemotivoservicio']['description']; ?></td>
										<td class="centrado"><?php echo date('d-m-Y', strtotime($cita['Agedetallecita']['fechaRegistro'])).' '.date('H:i', strtotime($cita['Agedetallecita']['fechaRegistro'])); ?></td>
										<td class="centrado"><?php echo $this->Xhtml->getStatus($cita['Agedetallecita']['estado']); ?></td>
									</tr>
									<?php endforeach; ?>
							</tbody>
						</table>
					</div>
						<?php if(!empty($citas)) { ?>
						<div class="titleBar" style="text-align: left;"><b>DETALLE DE LA CITA</b>
						</div>
							<div id="cita">
								<h2 style='padding:20px'>
									<?php echo __('BANTRES_EN_ESTE_MOMENTO_NO_EXISTE_NINGUN_ELEMENTO_SELECCIONADO'); ?>
								</h2>
							</div>
						<?php } ?>

					<div class="logosFooter">
						<img src="dist/images/basLogos.png">
					</div>
				</div>
