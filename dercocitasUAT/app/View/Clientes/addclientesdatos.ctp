<?php echo $this->Html->script('clientes/add_clientes_datos'); ?>
<div class="login  centerAll small">
    <?php echo $this->Session->flash();?>
    <h1><?php echo (__('Completa los datos',true));?></h1>
    <?php echo $this->Form->create('Cliente', array('class'=>'fomrLogin'));?>
    <div class="formTable leftAling">
        <div class="groupFields col2">
                <div class="field">
                    <div>
                        <label class="leftAling"> <?php echo (__('CLIENTE_CAMPO_TIPO_DOCUMETO', true)) ?>
                    </div>
                    <div>
                          <?php $tipo = array('DN'=>__('DNI', TRUE), 'PA'=>__('Pasaporte', TRUE), 'RU'=>__('RUC')); ?>
                        <?php
                        //echo $this->Form->select('type', $tipo, null,array('label'=>'','class'=>'span-5','error'=>false), FALSE); 
                        echo $this->Form->input('documento_tipo', array(
                            'type' => 'select',
                            'options' => $tipo,
                            'label' => false,
                            'class' => 'styled-select slate required',
                            'error' => false,
                            'empty' => FALSE,
                            'onChange'=>"validIpDoc();"
                        ));
                        
                        echo $this->Form->error('documento_tipo', array(
                            'isUnique' => __('empresaNombreUnico', true),
                            'notEmpty' => __('empresaNombreNoVacio', true),
                            'maxLength' => __('empresaNombreLongitud', true),
                                ), array('class' => 'input text required error'));
                        ?> 
                    </div>
                </div>
                <div class="field">
                    <div>
                        <label class="leftAling"> <?php echo(__('CLIENTE_ETIQUETA_TIPO_DOCUMETO_NUMERO', true)) ?>
                    </div>
                    <div>
                         <?php
                            echo $this->Form->input('documento_numero', array('label' => false,'placeholder'=>'Ingrese su nro de documento', 'class' => 'required', 'error' => false));
                            echo $this->Form->error('documento_numero', array(
                                'isUnique' => __('empresaNombreUnico', true),
                                'notEmpty' => __('empresaNombreNoVacio', true),
                                'maxLength' => __('empresaNombreLongitud', true),
                                    ), array('class' => 'input text required error'));
                            ?>
                    </div>
                </div>
            </div>
            <div class="groupFields col2">
                <div class="field">
                    <div>
                        <label class="leftAling"><?php echo (__('CLIENTE_ETIQUETA_CLIENTE_NOMBRES', true)) ?></label>
                    </div>
                    <div>
                    <?php
                        echo $this->Form->input('nombres', array('label' => false,'placeholder'=>'Ingrese sus nombres y apellidos', 'class' => 'onlyPlaque required', 'error' => false, 'minLength'=>4));
                        echo $this->Form->error('nombres', array(
                            'isUnique' => __('empresaCodigoUnico', true),
                            'notEmpty' => __('empresaCodigoNoVacio', true),
                            'maxLength' => __('empresaCodigoLongitud', true),
                            'alphaNumeric' => __('empresaCodigoEspacios', true)
                                ), array('class' => 'input text required error'));
                        ?>	
                    </div>
                </div>
                <div class="field">
                    <div>
                        <label class="leftAling">Tipo de cliente</label>
                    </div>
                    <div>
                        <input type="text" readonly='true' name="documento" placeholder="" value=""> 
                    </div>
                </div>
            </div>
            <div class="groupFields col2">
                <div class="field">
                    <div>
                        <label class="leftAling"><?php echo (__('Teléfono', true)) ?></label>
                    </div>
                    <div>
                        <?php
                            echo $this->Form->input('telefono', array('label' => false,'placeholder'=>'Ingrese su teléfono', 'class' => 'digits required', 'error' => false));
                            echo $this->Form->error('telefono', array(
                                'isUnique' => __('empresaCodigoUnico', true),
                                'notEmpty' => __('empresaCodigoNoVacio', true),
                                'maxLength' => __('empresaCodigoLongitud', true),
                                'alphaNumeric' => __('empresaCodigoEspacios', true)
                                    ), array('class' => 'input text required error'));
                            ?>	
                    </div>
                </div>
                <div class="field">
                    <div>
                        <label class="leftAling"><?php echo (__('Celular', true)) ?></label>
                    </div>
                    <div>
                        <?php
                            echo $this->Form->input('celular', array('label' => false,'placeholder'=>'Ingrese su celular',  'class' => 'span-5 digits required', 'error' => false));
                            echo $this->Form->error('celular', array(
                                'isUnique' => __('empresaCodigoUnico', true),
                                'notEmpty' => __('empresaCodigoNoVacio', true),
                                'maxLength' => __('empresaCodigoLongitud', true),
                                'alphaNumeric' => __('empresaCodigoEspacios', true)
                                    ), array('class' => 'input text required error'));
                            ?>	
                    </div>
                </div>
            </div>
            <div class="groupFields col2">
                <div class="field">
                    <div>
                        <label class="leftAling"><?php echo (__('E-mail', true)) ?></label>
                    </div>
                    <div>
                        <?php
                            echo $this->Form->input('email', array('label' => false,'placeholder'=>'Ingrese su email',  'class' => 'email required', 'error' => false));
                            echo $this->Form->error('email', array(
                                'isUnique' => __('empresaCodigoUnico', true),
                                'notEmpty' => __('empresaCodigoNoVacio', true),
                                'maxLength' => __('empresaCodigoLongitud', true),
                                'alphaNumeric' => __('empresaCodigoEspacios', true)
                                    ), array('class' => 'input text required error'));
                            ?>
                    </div>
                </div>
                <div class='field'>
                    <div>
                        <label class="leftAling"><?php echo (__('Dirección', true)) ?></label>
                    </div>
                    <div>
                       <?php
                            echo $this->Form->input('direccion', array('label' => false,'placeholder'=>'Ingrese su dirección',  'class' => 'span-5 onlyPlaque required', 'error' => false));
                            echo $this->Form->error('direccion', array(
                                'isUnique' => __('empresaCodigoUnico', true),
                                'notEmpty' => __('empresaCodigoNoVacio', true),
                                'maxLength' => __('empresaCodigoLongitud', true),
                                'alphaNumeric' => __('empresaCodigoEspacios', true)
                                    ), array('class' => 'input text required error'));
                            ?>	
                    </div>

                </div>
            </div>
            <div class="groupFields col2">
                <div class="field">
                    <div>
                        <label class="leftAling"><?php echo (__('Distrito', true)) ?></label>
                    </div>
                    <div>
                         <?php
                            echo $this->Form->input('distrito', array('label' => false,'placeholder'=>'Ingrese su distrito',  'class' => 'onlyPlaque required', 'error' => false));
                            echo $this->Form->error('distrito', array(
                                'isUnique' => __('empresaCodigoUnico', true),
                                'notEmpty' => __('empresaCodigoNoVacio', true),
                                'maxLength' => __('empresaCodigoLongitud', true),
                                'alphaNumeric' => __('empresaCodigoEspacios', true)
                                    ), array('class' => 'input text required error'));
                            ?>	
                    </div>
                </div>
                <div class='field'>
                    <div>
                        <label class="leftAling"><?php echo (__('Ciudad', true)) ?></label>
                    </div>
                    <div>
                        <?php
                            echo $this->Form->input('ciudad', array('label' => false,'placeholder'=>'Ingrese su ciudad',  'class' => 'onlyPlaque required', 'error' => false));
                            echo $this->Form->error('ciudad', array(
                                'isUnique' => __('empresaCodigoUnico', true),
                                'notEmpty' => __('empresaCodigoNoVacio', true),
                                'maxLength' => __('empresaCodigoLongitud', true),
                                'alphaNumeric' => __('empresaCodigoEspacios', true)
                                    ), array('class' => 'input text required error'));
                            ?>		
                    </div>

                </div>
            </div>
            <div class="groupFields col2">
                <div class="field">
                    <div>
                        <label class="leftAling"><?php echo (__('Marca de Vehículo', true)) ?></label>
                        
                    </div>
                    <div>
                        <?php
                            echo $this->Form->input('marca_id', array('type' => 'select','options' => $marcas,'empty'=>'Seleccione','label' => false, 'class' => 'styled-select slate required', 'error' => false));
                            echo $this->Form->error('marca_id', array(
                                'isUnique' => __('empresaCodigoUnico', true),
                                'notEmpty' => __('empresaCodigoNoVacio', true),
                                'maxLength' => __('empresaCodigoLongitud', true),
                                'alphaNumeric' => __('empresaCodigoEspacios', true)
                                    ), array('class' => 'input text required error'));
                            ?>	
                    </div>
                </div>
                <div class="field">
                    <div>
                        <label class="leftAling"><?php echo (__('Modelo', true)) ?></label>
                    </div>
                    <div>
                        <?php
                            echo $this->Form->input('modelo_id', array('type' => 'select','options' => $modelos,'empty'=>__('Seleccione',true),'label' => false, 'class' => 'styled-select slate  required', 'error' => false));
                            echo $this->Form->error('modelo_id', array(
                                'isUnique' => __('empresaCodigoUnico', true),
                                'notEmpty' => __('empresaCodigoNoVacio', true),
                                'maxLength' => __('empresaCodigoLongitud', true),
                                'alphaNumeric' => __('empresaCodigoEspacios', true)
                                    ), array('class' => 'input text required error'));
                            ?>	
                    </div>
                </div>
            </div>
            <div class="groupFields">
                <div class="field">
                    <div>
                        <label class="leftAling"><?php echo (__('Placa', true)) ?></label>
                    </div>
                    <div>
                         <?php
                            echo $this->Form->input('placa', array('maxlength'=>6, 'label' => false,'placeholder'=>'Ingrese su número de placa',  'class' => 'onlyPlaque required', 'error' => false));
                            echo $this->Form->error('placa', array(
                                'isUnique' => __('empresaCodigoUnico', true),
                                'notEmpty' => __('empresaCodigoNoVacio', true),
                                'maxLength' => __('empresaCodigoLongitud', true),
                                'alphaNumeric' => __('empresaCodigoEspacios', true)
                                    ), array('class' => 'input text required error'));
                            ?>	
                    </div>
                </div>
            </div>
            <?php if(!$is_register): ?>
            <div class='bottom-actions'>
                <div class="groupFields centerAling ">
                    <div class="field">
                        <?php echo $this->Form->submit(__('Submit',true), array('class'=>'guinda','id'=>'btnGuardar','div'=>false));	?>
                        <?php echo $this->Form->button(__('Reset',true), array('class'=>'buttomReset', 'id'=>'btnReset','type'=>'reset')); ?>
                    </div>
                </div>
            </div>
            <?php else: ?>
            <div class="groupFields ">
						<div class="field" style='text-align:center;padding-top:40px'>
                             <?php $url2 = $this->Html->url(array('controller'=>'clientes','action'=>'login'));
                    $where = 'window.location.href=\'' . $url2 . '\' ';
             ?>

			<?php echo $this->Form->button('Regresar al inicio', array('onclick'=>$where , 'type'=>'reset', 'div'=>false, 'class'=>'buttomReset'));	?>			
	    
            	</div>
					</div>
            <?php endif; ?>
           
    </div>
	<?php echo $this->Form->end();?>

</div>

<?php echo $this->element('actualizar');?>
</div>