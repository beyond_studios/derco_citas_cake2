<?php echo $this->element('dialog_message'); ?>
<?php echo $this->Html->script('clientes/actualizar_vehiculo'); ?>
<?php echo $this->Html->script('clientes/comun_clientes'); ?>

<?php echo $this->Session->flash();?>
<table id="tituloVentana" border="0" cellspacing="0" cellpadding="0" align="center">
    <thead>
        <tr>
            <th colspan="2" class="titulo">
                <?php echo __('ACTUALIZAR_VEHICULO') ?>
            </th>
        </tr>
    </thead>
</table>
<br />
<?php echo $this->Form->create('Cliente', array('url'=>'#', 'id'=>'clienteWebForm'));?>
<?php echo $this->Form->hidden('id', array())?>
<div id="listaCaja">
	<table id="formularioEdicion" width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tbody>
			<tr>
				<td class="etiqueta">
            		<?php echo __('GENERAL_CODIGO_SAP');?>
            	</td>
				<td class="valor"><?php echo $this->Form->input('codigo_sap', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
        	</tr>
			<tr>
				<td class="etiqueta">
            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_NOMBRES');?>
            	</td>
				<td class="valor"><?php echo $this->Form->input('nombres', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
        	</tr>
			<tr>
				<td class="etiqueta">
            		<?php echo __('CLIENTE_CAMPO_TIPO_DOCUMETO_NUMERO');?>
            	</td>
				<td class="valor"><?php echo $this->Form->input('documento_numero', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
        	</tr>
			<tr>
				<td class="etiqueta">
            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_CELULAR');?>
            	</td>
				<td class="valor"><?php echo $this->Form->input('celular', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
        	</tr>
			<tr>
				<td class="etiqueta">
            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_EMAIL');?>
            	</td>
				<td class="valor"><?php echo $this->Form->input('email', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
        	</tr>
			<tr>
				<td class="etiqueta">
            		<?php echo __('CLIENTE_ETIQUETA_PLACA');?>
            	</td>
				<td class="valor">
					<?php echo $this->Form->input('placa', array('label'=>false,'type'=>'select','error'=>false, 'style'=>'width:80%;','class'=>'required')); ?>
				</td>										
        	</tr>
			<tr>
				<td class="etiqueta">
            		<?php echo __('Marca');?>
            	</td>
				<td class="valor">
					<?php echo $this->Form->input('marca', array('label'=>false,'type'=>'select','error'=>false, 'style'=>'width:80%;','class'=>'required')); ?>
					<?php echo $this->Html->image('loader.gif', array('class' => 'hide')); ?>
				</td>										
        	</tr>
			<tr>
				<td class="etiqueta">
            		<?php echo __('Modelo');?>
            	</td>
				<td class="valor">
					<?php echo $this->Form->input('modelo_id',array('type'=>'select', 'options'=>$modelos, 'label' => false, 'empty'=>__('-- --',true), 'div' => false,'style'=>'width:80%;')); ?>
					<?php echo $this->Html->image('loader.gif', array('class' => 'hide')); ?>
				</td>										
        	</tr>
		</tbody>
	</table>
</div>
<br/>
<div align="center">				
	<?php echo $this->Form->button(__('ADMINISTRADOR_ACTUALIZAR'), array('id'=>'actualizarVehiculo'));?>
	&nbsp;
	<?php echo $this->Form->button(__('GENERAL_CERRAR'), array('style'=>'width:50px', 'type'=>'button','onClick' => 'javascript:window.close()'));?>			
</div>
<?php echo $this->Form->end(); ?>
					