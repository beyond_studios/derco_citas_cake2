<?php echo $this->Html->script('agetipomantenimientos/index.js'); ?>
		<?php echo $this->Session->flash();  ?>
		<br/>
			<h3 id="tituloTable"><?php echo __('AGE_TIPOMANTENIMIENTO_TITULO');?></h3>
		
		<div class="box">
			<div id="agregar" class="span-5" >
			<?php echo $this->element('agregar'); ?>	
			</div>
			
			<div id="buscador" class="">
				<?php echo $this->element('buscador', array('elementos'=>$elementos,'url' => 'index')); ?>
			</div>
		</div>		
		<table cellpadding="0" cellspacing="0" class="table" >
			<thead>
			<tr>
               
				<th><?php echo $this->Paginator->sort('Agemotivoservicio.description',__('AGE_MOTIVOSERVICIO_DESCRIPCION'));?></th>
				<th><?php echo $this->Paginator->sort('Agetipomantenimiento.description',__('AGE_TIPOMANTENIMIENTO_DESCRIPCION'));?></th>
				<th><?php echo $this->Paginator->sort('Agetipomantenimiento.tiposervicio',__('tipoServicio'));?></th>
				<th><?php echo $this->Paginator->sort('Agetipomantenimiento.status',__('estado',true));?></th>
				<th class="actionsfijo"><?php  echo __('Actions');?></th>
			
			</tr>			
			</thead>
			
			<tbody>
				<?php  foreach ($agetipomantenimientos as $agetipomantenimiento):?>
				<tr>
				<td>
					<?php 	
							echo $agetipomantenimiento['Agemotivoservicio']['description'];	
					?>	
				</td>
				<td>
					<?php 		
							$url = $this->Html->url(array('action'=>'view', $agetipomantenimiento['Agetipomantenimiento']['id']));
							echo $this->Html->link($agetipomantenimiento['Agetipomantenimiento']['description'], 'javascript:;',array('onclick' => "mostrar('".$url."')",'escape'=>false), null);	
					?>	
				</td>
				<td>
					<?php
						if(strlen(trim($agetipomantenimiento['Agetipomantenimiento']['tiposervicio']))>0)
							echo $tiposervicio[$agetipomantenimiento['Agetipomantenimiento']['tiposervicio']];
					?>
				</td>
				<td class="textc">
					<?php echo $agetipomantenimiento['Agetipomantenimiento']['status'] == 'AC' ? 
									__('Enable',true)
								:
									($agetipomantenimiento['Agetipomantenimiento']['status'] == 'DE'?
										__('Disable',true)
									:
										__('Limited',true))
								; ?>
				</td>
		
					<td class="actionsfijo">
				<?php echo $this->element('actionDerco', 
								array('id'=>$agetipomantenimiento['Agetipomantenimiento']['id'], 
										'name'=> $agetipomantenimiento['Agetipomantenimiento']['description'],
										'estado'=> $agetipomantenimiento['Agetipomantenimiento']['status'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>

<div id ="paging" class="span-18">
    <?php echo $this->element('paginador'); ?>
</div>
<div class="clear"></div>