<div class="span-8" >
	<?php echo $this->Html->script('agetipomantenimientos/add.js',false); ?>
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-8" >
		<h3><?php echo (__('AGE_TIPOMANTENIMIESTO_AGREGAR',true));?></h3>
	</div>
	<hr/>
	
	<?php echo $this->Form->create('Agetipomantenimiento');?>
			
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo (__('AGE_TIPOSERVICIO_DESCRIPCION',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->select('agemotivoservicio_id',$agemotivoservicios,array('class'=>'span-5','label'=>false, 'class'=>'span-5','error'=>false,'empty'=>__('seleccionar',true)));
				 	  echo $this->Form->error('agemotivoservicio_id', array(															
													       		'notEmpty' =>  __('GENERALES_VALIDACIONES_CAMPO_OBLIGATORIO', true),
																'numeric' =>  __('GENERALES_VALIDACIONES_SOLO_NUMEROS', true)													      
																), array('class' => 'input text required error'));
				?>
					
			</div>
		</div>
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo(__('descripcion',true)) ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('description', array('label'=>'','class'=>'span-5','error'=>false)); 
					  echo $this->Form->error('description', array(															
													   	'maxLength' =>  __('GENERALES_VALIDACIONES_LONGITUD_SESENTA'),
														'notEmpty' =>  __('GENERALES_VALIDACIONES_CAMPO_OBLIGATORIO'),
														'isUnique' =>  __('GENERALES_VALIDACIONES_DESCRIPCION_UNICO')											      
														), array('class' => 'input text required error'));				
				?>
			</div>
		</div>
		<div id="rowLast" class="span-8" >
			<div class="span-3" >
				<label><?php echo(__('tipoServicio',true)) ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->select('tiposervicio', $tiposervicio, array('label'=>false,'class'=>'span-5','error'=>false,'empty'=>__('seleccionar',true))); 
					  echo $this->Form->error('tiposervicio', array(															
													   	'maxLength' =>  __('GENERALES_VALIDACIONES_LONGITUD_SESENTA'),
														'notEmpty' =>  __('GENERALES_VALIDACIONES_CAMPO_OBLIGATORIO'),
														'isUnique' =>  __('GENERALES_VALIDACIONES_DESCRIPCION_UNICO')											      
														), array('class' => 'input text required error'));				
				?>
			</div>
		</div>
		<br/>
		<hr/>
		
		<div class=" span-8 botones" >
			<?php echo $this->Form->submit(__('Submit',true), array('div'=>false));	?>
			<?php echo $this->Form->button(__('Reset',true), array('type'=>'reset')); ?>				
			<?php echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>
	
	<?php echo $this->Form->end(); ?>

<?php echo $this->element('actualizar_ver'); ?>
</div>	