<?php //echo $html->script('secorganizations/view.js', false); ?>
<?php //echo $html->script('secpeople/index.js', false);  // id="titulo" ?>

<div class="span-9" > 
		<?php echo $this->Session->flash();?>
	<br/>
	<table cellpadding="0" cellspacing="0" class="table">
		<thead>
			<tr>
				<th colspan="2" scope="col">
					<?php echo __('AGE_TIPOMANTENIMIENTO_TITULO');?>
				</th>
			</tr>
		</thead>
		<tbody>

			<tr>
		 		<td><label><?php echo __('AGE_TIPOSERVICIO_DESCRIPCION') ?></label> </td>
		 		<td><?php echo $agetipomantenimiento['Agemotivoservicio']['description']; ?></td>
		 	</tr>
			<tr>
		 		<td><label><?php echo __('AGE_TIPOMANTENIMIENTO_TITULO') ?></label> </td>
		 		<td><?php echo $agetipomantenimiento['Agetipomantenimiento']['description']; ?></td>
		 	</tr>
			<tr>
		 		<td><label><?php echo __('tipoServicio') ?></label> </td>
		 		<td><?php if(strlen(trim($agetipomantenimiento['Agetipomantenimiento']['tiposervicio']))>0)
							echo $tiposervicio[$agetipomantenimiento['Agetipomantenimiento']['tiposervicio']]; ?></td>
		 	</tr>
			<tr>
		 		<td><label><?php echo __('estado'); ?></label> </td>
		 		<td><?php echo $agetipomantenimiento['Agetipomantenimiento']['status'] == 'AC' ? 
							__('Enable',true)
						:
							($agetipomantenimiento['Agetipomantenimiento']['status'] == 'DE'?
								__('Disable',true)
							:
								__('Limited',true))
						; ?></td>
		 	</tr>
		</tbody>
	</table>	
	<br/>
	<div class="span-9 botones" > 
	<?php echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
	<?php echo $this->Form->end();?>
	<?php echo $this->element('actualizar_ver'); ?>
	</div>
</div>