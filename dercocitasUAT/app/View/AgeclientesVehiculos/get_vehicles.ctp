<?php
App::import('Vendor','json');
$json = new Services_JSON;

$responce->page = $this->Paginator->counter('%page%'); 
$responce->total = $this->Paginator->counter('%pages%'); 
$responce->records = $this->Paginator->counter('%count%'); 
foreach($vehicles as $key => $value){
	$responce->rows[$key]['id']=trim($value['AgeclientesVehiculo']['id']);
	$responce->rows[$key]['cell'] = array(
		trim($value['AgeclientesVehiculo']['id']),
		
		trim($value['Cliente']['documento_tipo']),
		trim($value['Cliente']['codigo_sap']),
		trim($value['Cliente']['documento_numero']),
		trim($value['Cliente']['nombres']),
		trim($value['Cliente']['distrito']),
		trim($value['Cliente']['telefono']),
		trim($value['Cliente']['celular']),
		trim($value['Cliente']['email']),
		trim($value['Cliente']['cliente_tipo']),
		trim($value['Cliente']['str_cliente_tipo']),
		
		trim($value['AgeclientesVehiculo']['placa']),
		trim($value['AgeclientesVehiculo']['marca']),
		trim($value['AgeclientesVehiculo']['modelo']),
		trim($value['AgeclientesVehiculo']['codigo_vehiculo']),
		trim($value['AgeclientesVehiculo']['cliente_id']),
		trim($value['AgeclientesVehiculo']['ultimo_mant']),
		trim($value['AgeclientesVehiculo']['siguiente_mant']),
		
		trim($value['Marca']['id'])
	);
} 		
echo  $json->encode($responce);