<div class="span-8" >	
	<?php echo $this->Html->script('agemotivoservicios/add.js',false); ?>
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-8" >
		<h3><?php echo (__('AGE_MOTIVOSERVICIO_AGREGAR',true));?></h3>
	</div>
	<hr/>
	
	<?php echo $this->Form->create('Agemotivoservicio');?>
	
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo(__('GENERAL_CODIGO_SAP',true)) ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('codigo_sap', array('label'=>'','class'=>'span-5','error'=>false)); 
					  echo $this->Form->error('codigo_sap', array(															
													   	'maxLength' =>  __('GENERALES_VALIDACIONES_LONGITUD_CINCO'),
														'notEmpty' =>  __('GENERALES_VALIDACIONES_CAMPO_OBLIGATORIO')										      
														), array('class' => 'input text required error'));				
				?>
			</div>
		</div>
		
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo(__('descripcion',true)) ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('description', array('label'=>'','class'=>'span-5','error'=>false)); 
					  echo $this->Form->error('description', array(															
													   	'maxLength' =>  __('GENERALES_VALIDACIONES_LONGITUD_SESENTA'),
														'notEmpty' =>  __('GENERALES_VALIDACIONES_CAMPO_OBLIGATORIO'),
														'isUnique' =>  __('GENERALES_VALIDACIONES_DESCRIPCION_UNICO')											      
														), array('class' => 'input text required error'));				
				?>
			</div>
		</div>
		
		<div id="rowLast" class="span-8" >
			<div class="span-3" >
				<label><?php echo(__('mensajeMotivoServicio',true)) ?></label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('mensaje', array('label'=>'','type'=>'textarea','class'=>'span-5','style'=>'height: 50px;font-size: 1.2em;','error'=>false)); 
						echo $this->Form->error('mensaje', array(															
														'notEmpty' =>  __('GENERALES_VALIDACIONES_CAMPO_OBLIGATORIO')											      
														), array('class' => 'input text required error'));				
				?>
			</div>
		</div>
		
		<br/>
		<hr/>
		
		<div class=" span-8 botones" >
			<?php echo $this->Form->submit(__('Submit',true), array('div'=>false));	?>
			<?php echo $this->Form->button(__('Reset',true), array('type'=>'reset')); ?>				
			<?php echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>
	
	<?php echo $this->Form->end(); ?>

<?php echo $this->element('actualizar_ver'); ?>
</div>