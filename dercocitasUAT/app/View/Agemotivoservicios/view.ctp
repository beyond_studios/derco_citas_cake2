<?php //echo $html->script('secorganizations/view.js', false); ?>
<?php //echo $html->script('secpeople/index.js', false);  // id="titulo" ?>

		
<div class="span-9" > 
	<?php echo $this->Session->flash();?>
	<br/>
	<table cellpadding="0" cellspacing="0" class="table">
		<thead>
			<tr>
				<th colspan="2" scope="col">
					<?php echo __('AGE_MOTIVOSERVICIO_TITULO');?>
				</th>
			</tr>
		</thead>
		<tbody>

			<tr>
		 		<td><label><?php echo __('descripcion') ?></label> </td>
		 		<td><?php echo $agemotivoservicio['Agemotivoservicio']['description']; ?></td>
		 	</tr>

			<tr>
		 		<td><label><?php echo __('GENERAL_CODIGO_SAP') ?></label> </td>
		 		<td><?php echo $agemotivoservicio['Agemotivoservicio']['codigo_sap']; ?></td>
		 	</tr>

			<tr>
		 		<td><label><?php echo __('mensajeMotivoServicio') ?></label> </td>
		 		<td><?php echo $agemotivoservicio['Agemotivoservicio']['mensaje']; ?></td>
		 	</tr>

			<tr>
		 		<td><label><?php echo __('estado'); ?></label> </td>
		 		<td><?php echo $agemotivoservicio['Agemotivoservicio']['status'] == 'AC' ? 
							__('Enable',true)
						:
							($agemotivoservicio['Agemotivoservicio']['status'] == 'DE'?
								__('Disable',true)
							:
								__('Limited',true))
						; ?></td>
		 	</tr>
		</tbody>
	</table>	
	<br/>
	<div class="span-9 botones" > 
	<?php echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
	<?php echo $this->Form->end();?>
	<?php echo $this->element('actualizar_ver'); ?>
	</div>
</div>