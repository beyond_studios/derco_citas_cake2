<div class="span-8" >
<?php echo $this->Html->script('asesorservicios/add.js'); ?>
	<?php echo $this->Session->flash();?>
	<div id="titulo" class="span-8" >	
		<h3><?php echo __('ASESORSERVICIO_TITULO_AGREGAR');?></h3>
	</div><div class="clear"></div>
	<br/>
	
	<?php echo $this->Form->create('Asesorservicio');?>	
		<div id="rowLast" class="span-8" ><hr/>
			<div class="span-3" >
				<label><?php echo __('GENERAL_CODIGO_SAP') ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php 
					echo $this->Form->input('codigo_sap',array('type'=>'text','class'=>'span-5','label'=>'' ));
				?>
			</div>
		</div>		
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo $this->Form->label(__('organizacion')); ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >				
				<?php echo $this->Form->select('secorganization_id',array($secorganizations),array('class'=>'span-5', 'empty'=>'Selecione' )); 
				?>
				<?php echo $this->Js->get('#AsesorservicioSecorganizationId')->event('change',$this->Js->request(
				array('url' => array( 'action' => 'listrolPersonas'),'update' => 'AsesorservicioSecpersonId' )));?>							
			</div>
		</div>
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo __('sucursales') ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php 
					echo $this->Form->input('secproject_id',array('type'=>'select','class'=>'span-5','label'=>'' ));
					//debug($this->Form->input('secproject_id',array('type'=>'select','class'=>'span-5','label'=>'' )));
				?>
			</div>
		</div>
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo __('MOTIVO_DE_SERVICIO') ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php 
					echo $this->Form->input('agemotivoservicio_id',array('type'=>'select','class'=>'span-5','label'=>'' ));
				?>
			</div>
		</div>
		<div id="rowLast" class="span-8" >
			<div class="span-3" >
				<label><?php echo __('usuarios') ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php 
					echo $this->Form->input('secperson_id', array('type'=>'select','class'=>'span-5','label'=>'' ));
					
				?>
			</div>
		</div>
		
		<div id="rowLast" class=" span-8 botones" >
			<hr/>
			<?php echo $this->Form->submit(__('Guardar'), array('div'=>false));	?>			
			<?php echo $this->Form->button(__('Cerrar'), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>
	
	<?php echo $this->Form->end(); ?>
</div>
