
<div class="span-9 botones" > 
	<?php $this->Session->flash();?>
	<br/>
	<table cellpadding="0" cellspacing="0" class="table">
		<thead>
			<tr>
				<th colspan="2" scope="col">
					<?php  echo __('ASESORSERVICIO_TITULO_VIEW');?>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr>
		 		<td class="span-3" ><label><?php echo __('organizacion') ?></label></td>
		 		<td class="span-4 last"><?php echo $asesorservicio['Secorganization']['name']; ?></td>
			</tr>
			
			<tr>
		 		<td><label><?php echo __('sucursal') ?></label></td>
		 		<td><?php echo $asesorservicio['Secproject']['name']; ?>	
			</tr>
			<tr>
		 		<td><label><?php echo __('GENERAL_CODIGO_SAP') ?></label></td>
		 		<td><?php echo $asesorservicio['Asesorservicio']['codigo_sap']; ?>	
			</tr>
			<tr>
		 		<td><label><?php echo __('apellidoNombre') ?></label></td>
		 		<td><?php echo $asesorservicio['Secperson']['appaterno'].
													' '.$asesorservicio['Secperson']['apmaterno'].
													', '.$asesorservicio['Secperson']['firstname']; ?>	
			</tr>
			<tr>
		 		<td><label><?php echo __('MOTIVO_DE_SERVICIO') ?></label></td>
		 		<td><?php echo $asesorservicio['Agemotivoservicio']['description']; ?>	
			</tr>
			<tr>
		 		<td><label><?php echo __('usuario') ?></label></td>
		 		<td><?php echo $asesorservicio['Secperson']['username']; ?></td>
		 	</tr>
			<tr>
		 		<td><label><?php echo __('estado'); ?></label> </td>
		 		<td>
		 			<?php echo $asesorservicio['Asesorservicio']['status'] == 'AC' ? 
							__('Enable',true)
						:
							($asesorservicio['Asesorservicio']['status'] == 'DE'?
								__('Disable',true)
							:
								__('Limited',true))
						; ?>
		 		</td>
		 	</tr>
		</tbody>
	</table>
<br/>
	<div id="rowLast" class="span-9 botones" > 
	<?php echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
	<?php echo $this->Form->end();?>
	<?php echo $this->element('actualizar_ver'); ?>
	</div>
</div>