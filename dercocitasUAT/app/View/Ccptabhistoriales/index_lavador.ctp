<?php echo $this->Html->script('jqueryUI/jquery-ui-1.8.custom.min.js'); ?>
<?php echo $this->Html->css('theme/redmond/jquery-ui-1.7.2.custom.css'); ?> 
<?php echo $this->Html->css('modulo_taller/tooltip.css'); ?> 
<?php echo $this->Html->script('ccpsolicitudservicios/index.js'); ?>

<?php echo $this->Session->flash();  ?>
<br/><h3 id="tituloTable"><?php echo __('CCP_BANDEJA_LAVADOR');?></h3>

<!-- IMPLEMENTACION DEL BUSCADOR -->
<form id="principalForm" method="get" action="<?php echo $this->Html->url('indexLavador') ?>">
<div id="buscadorBasico">
	<div id="criteriosBusqueda">
		<table id="formBuscador" border="0" cellspacing="0" cellpadding="0" align="center" style="width:100%">
			<tbody>
	        	<tr>
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.placa', __('CCP_PLACA'));?></td>
	            	<td class="valor">
	            		<input type="text" size=15 name="placa" id="formBuscadorPlaca" value="<?php echo empty($data['placa'])?'':$data['placa']; ?>" size="10"/>
	            	</td>	

					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.nrocars', __('Nro_OT'));?></td>
	            	<td class="valor">
	            		<input type="text" size=15 name="nrocars" id="formBuscadorNrocars" value="<?php echo empty($data['nrocars'])?'':$data['nrocars']; ?>" size="10"/>
	            	</td>
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.chasis', __('CCP_CHASIS'));?></td>
					<td  class="valor">
						<input type="text" size="25" name="chasis" id="formBuscadorChasis" value="<?php echo empty($data['chasis'])?'':$data['chasis']; ?>" size="25"/>
					</td>
					<!-- buscador por estado -->
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.estadootccp_id', __('estadoLavado'));?></td>
	            	<td class="valor">
						<select id="formBuscadorEstadootccpId" value="<?php echo (!empty($data['estadootccp_id']) && isset($data['estadootccp_id']))?($data['estadootccp_id']=='7,7')?'':$data['estadootccp_id']:''; ?>" style="width:125px" name="estadootccp_id">
							<?php foreach($seguimientoestados as $id => $value): 
								if(empty($data['estadootccp_id'])) echo '<option value="'.$id.'">'.$value.'</option>';
								else{
									if($data['estadootccp_id'] != $id) echo '<option value="'.$id.'">'.$value.'</option>';
									elseif($data['estadootccp_id']=='7,7') echo '<option value="'.$id.'">'.$value.'</option>';
										else echo '<option selected="selected" value="'.$id.'">'.$value.'</option>';
								}
							endforeach; ?>
						</select>							
	            	</td>
					</td>
					<td>
						<?php 
						//Exportar para Excel
			    		$iconoExportarExcel = $this->Xhtml->image('derco/defecto/MicrosoftExcelDocument.gif', array(
			    			'title'=>__('REPORTE_EXPORTAR_EXCEL'), 
							'border'=>'0',
							'width'=>'16px',
							'height'=>'16px'
						));
						if(!empty($otsCars)){
						echo $this->Html->link($iconoExportarExcel, 'javascript:;', array(
								'onmouseover' => "return cambiarStatusMsg('".__('REPORTE_EXPORTAR_EXCEL')."');", 
								'onmouseout' => "return cambiarStatusMsg('');", 
								'onclick'=>"exportarExcel()", 'escape'=>false), null, false);
						}
						?>						
					</td>
					<td  class="valor">
						<div style="display:none;">
						<select id="formBuscadorCarsestadoId" value="<?php echo empty($data['carsestado_id'])?'':$data['carsestado_id']; ?>" style="width:125px" name="carsestado_id">
							<?php foreach($estadosOtCarsSelect as $id => $value): 
								if(empty($data['carsestado_id'])) echo '<option value="'.$id.'">'.$value.'</option>';
								else{
									if($data['carsestado_id'] != $id) echo '<option value="'.$id.'">'.utf8_encode($value).'</option>';
									else echo '<option selected="selected" value="'.$id.'">'.utf8_encode($value).'</option>';
								}
							endforeach; ?>
						</select>
						</div>					
					</td>
					<td><?php echo $this->Form->submit(__('Search'), array('style'=>'width:100px'));?></td>	
				</tr>
			</tbody>
		</table>
	</div>
</div>
</form>
<!-- FIN - IMPLEMENTACION DEL BUSCADOR -->
<?php //debug($this->layout); ?>
<div id="listaCaja">
	<table id="listaPrincipal" cellpadding="0" cellspacing="0">
		<thead>
		    <tr>
				<th><?php echo $this->Paginator->sort('Model.OT', __('Nro_Ot')) ?></th>
				<th><?php echo $this->Paginator->sort('Model.PLACA', __('CCP_PLACA')) ?></th>
				<th><?php echo $this->Paginator->sort('Model.MARCA', __('CCP_MARCA')) ?></th>
				<th style="width:100px;"><?php echo $this->Paginator->sort('Model.MODELO', __('CCP_MODELO')) ?></th>
				<th><?php echo __('CCP_COLOR')?></th>
				<th><?php echo $this->Paginator->sort('Model.NRO_SERIE', __('CCP_CHASIS')) ?></th>
				<th style="width:80px;"><?php echo __('CCP_ESTADO_CCP') ?></th>
				<th><?php echo __('estadoLavado') ?></th>
				<th><?php echo __('GENERAL_ACCIONES')?></th>
		    </tr>
		</thead>
		<tbody>
			<?php $i=0;?>
		    <?php foreach ($otsCars as $otCars): ?>
		    <tr>
		        <td class="texto">
		        	<?php $i++; ?>
		        	<?php 
						if($i>15){
							$onmouseover = "tooltipArriba";
						}elseif($i>10){
							$onmouseover = "tooltipMedio";
						} else{
							$onmouseover = "tooltipAbajo";
						}
					?>
					
		        	<div class="ToolText" href="#"><?php  echo $otCars['Talot']['ot_numero']; ?></div>
					<div class="<?php echo "tooltip $onmouseover" ?>">
						<span id='<?php echo $otCars['Talot']['ot_numero'].'TTP'?>' style="margin: 5px; background-color: red;">  
							<?php if(!empty($otCars['tooltip'])): ?>
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td class="detalles etiqueta" width="63px" style="border: hidden;"><?php echo __('OT_CAMPO_PROPIETARIO')?></td>
										<td class="detalles valor" width="285px" style="border: hidden;"><?php echo utf8_encode($otCars['tooltip']['Talot']['ot_cliente']); ?></td>
										<td class="detalles etiqueta" width="66px" style="border: hidden;"><?php echo __('OT_CAMPO_FECHA_ORDEN')?></td>
										<td class="detalles valor" width="100px" style="border: hidden;"><?php echo substr($otCars['tooltip']['Talot']['ot_fecha_creacion'],0,10); ?> <?php echo $otCars['tooltip']['Talot']['ot_hora_recibida']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_ETIQUETA_RESPONSABLE_ASESOR')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_nombre_asesor']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_ETIQUETA_FECHA_A_ENTREGA')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo substr($otCars['tooltip']['Talot']['ot_fecha_entrega'],0,10); ?> <?php echo $otCars['tooltip']['Talot']['ot_hora_prometida']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;">
											<?php echo __('OT_CAMPO_DESCRIPCION')?>
											<b style="color: yellow;"><?php echo utf8_encode($estadosOtCarsTooltip[$otCars['tooltip']['Talot']['ot_estado']]); ?></b>
										</td>
										<td class="detalles valor" colspan="3" style="padding-bottom: 5px;">
											<div class="div_textarea" style="padding: 2px; overflow: auto; height: 90px; width: 405px;">
												<?php echo trim(utf8_encode($otCars['tooltip']['Talot']['ot_descripcion'])); ?>
							            	</div>
										</td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_MARCA')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_marca']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_PLACA')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_placa']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_MODELO')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_modelo']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_NROCARS')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_numero']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_VERSION')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_version']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_NROCONO')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_numero_cono']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('Tipo_OT')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_tipo_ot']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('CCPSOLICITUD_TABLERO_ETIQUETA_CHASIS')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['chasis_vin']; ?></td>
									</tr>
									<tr style="border: hidden;">
										<td class="detalles etiqueta" style="border: hidden;"></td>
										<td class="detalles valor" style="border: hidden;">
											<?php //echo $this->Form->button(__('MAQUINARIAS_ENVIAR_A_ENTREGA'), array('onclick'=>'enviar_a_entrega('.$otCars['tooltip']['Talot']['id'].', "'.$otCars['tooltip']['Talot']['nrocars'].'")'));?>							
										</td>
									</tr>
								</tbody>
							</table>
							<?php endif; ?>
						</span>
					</div>
				</td>
		      	<td class="centrado"><?php echo utf8_encode($otCars['Talot']['ot_placa']); ?></td>
		        <td class="centrado"><?php echo utf8_encode($otCars['Talot']['ot_marca']); ?></td>
				<td class="centrado"><?php echo utf8_encode($otCars['Talot']['ot_modelo']); ?></td>
				<td class="centrado"><?php echo utf8_encode($otCars['Talot']['ot_color']); ?></td>
				<td class="centrado"><?php echo utf8_encode($otCars['Talot']['chasis_vin']); ?></td>
				<td class="centrado">
					<?php
					if(!empty($otCars['Ccpsolicitudservicio']['id'])){
						if($otCars['Ccptabestado']['id']==3){
							if($otCars['Ccpsolicitudservicioestado']['id']==3 || $otCars['Ccpsolicitudservicioestado']['id']==4 || $otCars['Ccpsolicitudservicioestado']['id']==5){
								echo empty($otCars['Ccptabestado'])?'':$otCars['Ccptabestado']['descripcion'].' - '.$otCars['Ccpsolicitudservicioestado']['descripcion'];
							}else echo empty($otCars['Ccptabestado'])?'':$otCars['Ccptabestado']['descripcion'];
						}else{
							if($otCars['Ccpsolicitudservicioestado']['id']==5){
								echo 'Con Solicitud - Rechazado';
							}else echo empty($otCars['Ccptabestado'])?'':$otCars['Ccptabestado']['descripcion'];
						}
					} else echo empty($otCars['Ccptabestado'])?'':$otCars['Ccptabestado']['descripcion'];
					?>
				</td>
				<td class="centrado"><?php echo utf8_encode($otCars['Talot']['segestado_descripcion']); ?></td>
		        <td class="accion">
		            <?php 
						//LAVADOR 
						if(($dtLog['role'] == 'Lavador CCP' && empty($otCars['Talot']['nro_seguimiento']))
							|| (($dtLog['role'] == 'Lavador CCP' && !empty($otCars['Talot']['nro_seguimiento'])) && ($otCars['Talot']['nro_seguimiento'] == $otCars['Talot']['nro_max_segimiento']))
						){
							echo $this->Xhtml->imagelink('enviar.gif', __('REGISTRAR_LAVADOS'),
			                	'javascript:;',array('onclick'=>"pasar_lavado('".$otCars['Talot']['ot_numero']."')"),
			                	null, array('width'=>'16'));
						}
					
						// 
						if($dtLog['role'] == 'Jefe de Taller' 
							&& !empty($otCars['Talot']['nro_seguimiento']) 
							&& empty($otCars['Talot']['secperson_coordinador_id'])
							&& empty($otCars['Talot']['secperson_jefetaller_id'])
						){
							echo $this->Xhtml->imagelink('atender.gif', __('APROBAR_JEFETALLER'),
			                	'javascript:;',array('onclick'=>"pasar_jefe('".$otCars['Talot']['seg_id']."')"),
			                	null, array('width'=>'16'));	
						}
						
						//verificar si fue aprobado por coordinador algunos
						if(($dtLog['role'] == 'Coordinador de Terceros') 
							&& ($otCars['Talot']['segestado_id']==1) 
							&& !empty($otCars['Talot']['nro_seguimiento']) 
							&& empty($otCars['Talot']['nro_coordinador_aprobado'])
							&& empty($otCars['Talot']['secperson_jefetaller_id'])
							){  
							echo $this->Xhtml->imagelink('derco/defecto/activar.gif', __('APROBAR_COORDINADOR'),
			                	'javascript:;',array('onclick'=>"pasar_coordinador('".$otCars['Talot']['seg_id']."')"),
			                	null, array('width'=>'16'));
						}		
					?>
		        </td>
		    </tr>
		    <?php endforeach; ?>
		</tbody>
	</table>
</div>
<div id ="paging" class="span-18">
    <?php echo $this->element('paginador'); ?>
</div>
<script type="text/javascript">
	//colorearTabla('listaPrincipal');
	
	function mostrar(id) {
	    var url = "<?php echo $this->Html->url('/ccpsolicitudservicios/mostrar')?>/"+id;
	    var w = window.open(url, 'mostrar', 
	        'scrollbars=yes,resizable=yes,width=940,height=480,top=115,left=200,status=no,location=no,toolbar=no');
	}
	
    function muestraOculta() {
		var id = (this.id).split('_')[1];
		
		$('contenidos_'+id).toggle();
		$('enlace_'+id).innerHTML = (!$('contenidos_'+id).visible()) ? 'Ocultar contenidos' : 'Mostrar contenidos';
	}
	
	function pasar_lavado(ot_numero){
	    var url = "<?php echo $this->Html->url('/ccptabhistoriales/pasarLavado')?>/"+ot_numero;
	    var w = window.open(url, 'pasarLavado', 
	        'scrollbars=yes,resizable=yes,width=400,height=300,top=500,left=800,status=no,location=no,toolbar=no');
		
	}
	function pasar_coordinador($seg_id){
	    var url = "<?php echo $this->Html->url('/ccptabhistoriales/pasarCoordinador')?>/"+$seg_id;
	    var w = window.open(url, 'pasarLavado', 
	        'scrollbars=yes,resizable=yes,width=400,height=300,top=500,left=800,status=no,location=no,toolbar=no');
		
	}
	function pasar_jefe($seg_id){
	    var url = "<?php echo $this->Html->url('/ccptabhistoriales/pasarJefe')?>/"+$seg_id;
	    var w = window.open(url, 'pasarLavado', 
	        'scrollbars=yes,resizable=yes,width=400,height=300,top=500,left=800,status=no,location=no,toolbar=no');
		
	}

	function exportarExcel(){
		var actionExel = "<?php echo $this->Html->url('/ccptabhistoriales/indexLavadorExportarExcel')?>";
		
	    var w = window.open('' , 'exportalExcel', 'scrollbars=yes,resizable=yes,width=100,height=100,status=no,location=no,toolbar=no');
		var actionBsc = $('#principalForm').attr('action');
		$('#principalForm').attr('action', actionExel);
		$('#principalForm').attr('target', 'exportalExcel');
		$('#principalForm').submit();
		$('#principalForm').attr('action', actionBsc);	
		$('#principalForm').attr('target', '');
			
	}
</script>