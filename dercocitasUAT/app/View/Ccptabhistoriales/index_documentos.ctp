<h3 id="tituloTable"><?php echo(__('CCP_ESTADO_DOCUMENTO',true));?></h3>

<!-- IMPLEMENTACION DEL BUSCADOR -->
<form id="f_bsc" method="get" action="<?php echo $this->Html->url('indexDocumentos') ?>">
<div id="buscadorBasico_" class="box">
	<div id="criteriosBusqueda">
		<table id="formBuscador" border="0" cellspacing="0" cellpadding="0" align="center" style="width:100%">
			<input type="hidden" size=15 name="click" id="formBuscadorClick" value="<?php echo empty($data['click'])?'':$data['click']; ?>" size="10"/>
			<tbody>
				<tr>
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.nrocars', __('Nro_OT'));?></td>
	            	<td class="valor">
	            		<input type="text" size=15 name="nrocars" id="formBuscadorNrocars" value="<?php echo empty($data['nrocars'])?'':$data['nrocars']; ?>" size="10"/>
	            	</td>
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.estadootccp_id', __('CCP_ESTADO_DOCUMENTO'));?></td>
	            	<td class="valor">
						<select id="formBuscadorEstadootccpId" value="<?php echo (!empty($data['estadodocumento_id']) && isset($data['estadodocumento_id']))?($data['estadodocumento_id']=='11,14,15')?'':$data['estadodocumento_id']:''; ?>" style="width:125px" name="estadodocumento_id">
							<?php foreach($estadoPorDocumento as $id => $value): 
								if(empty($data['estadodocumento_id'])) echo '<option value="'.$id.'">'.$value.'</option>';
								else{
									if($data['estadodocumento_id'] != $id) echo '<option value="'.$id.'">'.$value.'</option>';
									else echo '<option selected="selected" value="'.$id.'">'.$value.'</option>';
								}
							endforeach; ?>
						</select>							
	            	</td>
					<td style="width:5%"></td>
					<td><?php echo $this->Form->submit(__('Search'), array('class'=>'buscar'));?></td>
				</tr>
					<tr>
						<!-- buscador por placa -->
						<td class="etiqueta"><?php echo $this->Form->label('formBuscador.placa', __('CCP_PLACA'));?></td>
		            	<td class="valor">
		            		<input type="text" size=15 name="placa" id="formBuscadorPlaca" value="<?php echo empty($data['placa'])?'':$data['placa']; ?>" size="10"/>
		            	</td>
						
						<td class="etiqueta"><?php echo $this->Form->label('formBuscador.codasesor', __('Asesor'));?></td>
						<td  class="valor">
							<select id="formBuscadorCodasesor" value="<?php echo empty($data['codasesor'])?'':$data['codasesor']; ?>" style="width:125px" name="codasesor">
								<?php foreach($asesoresCars as $id => $value): 
									if(empty($data)) echo '<option value="'.$id.'">'.$value.'</option>';
									else{
										if($data['codasesor'] != $id) echo '<option value="'.$id.'">'.$value.'</option>';
										else echo '<option selected="selected" value="'.$id.'">'.$value.'</option>';
									}
								endforeach; ?>
							</select>					
						</td>						
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</form>
<!-- FIN - IMPLEMENTACION DEL BUSCADOR -->

<?php //debug($this->layout); ?>
<div id="listaCaja">
	<table id="listaPrincipal" cellpadding="0" cellspacing="0">
		<thead>
		    <tr>
				<th><?php echo $this->Paginator->sort('Talot.nrocars', __('Nro_Ot'))?></th>
				<th><?php echo $this->Paginator->sort('Talot.propietario', __('CCP_REPORTE_CLIENTE'))?></th>
				<th><?php echo $this->Paginator->sort('Talot.placa', __('CCP_PLACA'))?></th>
				<th><?php echo $this->Paginator->sort('Ccptabhistorialdocumento.comentario', __('CCP_COMENTARIO'))?></th>
				<th><?php echo __('GENERAL_ACCIONES')?></th>
		    </tr>
		</thead>
		<tbody>
			<?php $i=0;?>
		    <?php foreach ($otsCars as $otCars): ?>
		    <tr>
		        <td class="texto">
		        	<?php $i++; ?>
		        	<?php $onmouseover = ($i>10)?"javascript:this.className='tooltipArriba'":"javascript:this.className='tooltipAbajo'"; ?>
		        	<div id="<?php echo $otCars['Talot']['nrocars']; ?>" class="ToolText"  onMouseOver="<?php echo $onmouseover; ?>" onMouseOut="javascript:this.className='ToolText'">
		        		<?php  echo $otCars['Talot']['nrocars']; ?>
					</div>
				</td>
		        <td class="centrado"><?php echo utf8_encode($otCars['Talot']['propietario']); ?></td>
				<td class="centrado"><?php echo utf8_encode($otCars['Talot']['placa']); ?></td>
		        <td class="centrado"><?php echo utf8_encode($otCars['Ccptabhistorialdocumento']['comentario']); ?></td>

		        <td class="accion">
		            <?php 
							$url = $this->Html->url('/talactas/view/'.$otCars['Viewccptabhistorialdocumento']['ot_numero']);
							echo $this->Xhtml->imagelink('derco/defecto/mostrar.png', __('CCP_MOSTRAR_ACTA'),
								                	'javascript:;',array('onclick'=>"mostrar('".$url."')"),
								                	null, array('width'=>'16'));
							echo '&nbsp';
					?>
		        </td>
		    </tr>
		    <?php endforeach; ?>
		</tbody>
	</table>
</div>
<div id ="paging" class="span-18">
	<?php echo $this->element('paginador'); ?>
</div>
<script type="text/javascript">
	// create popups for each element with a title attribute
	colorearTabla('listaPrincipal');
	
	
	function mostrar(url) {
	    var w = window.open(url, 'mostrar', 
	        'scrollbars=yes,resizable=yes,width=940,height=480,top=115,left=200,status=no,location=no,toolbar=no');
	}
	
	
	function exportarExcel(){
			
		var estadoCCP = $('formBuscadorEstadootccpId').value;
		if(estadoCCP == '') estadoCCP = '0';
		
		//textos
		var placa = $('formBuscadorPlaca').value;
		if(placa == '') placa = '0';
		var numeroOT = $('formBuscadorNrocars').value;
		if(numeroOT == '') numeroOT = '0';
		var chasis = $('formBuscadorChasis').value;
		if(chasis == '') chasis = '0';
		
		var parametros=numeroOT+'/'+placa+'/'+estadoCCP+'/'+chasis;
		var url = "<?php echo $this->Html->url('/ccptabhistoriales/indexAsistenteExportarExcel/')?>"+parametros;
		
	    var w = window.open(url , 'exportalExcel', 
	        'scrollbars=yes,resizable=yes,width=100,height=100,status=no,location=no,toolbar=no');		
	}
</script>