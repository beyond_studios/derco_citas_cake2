<?php echo $this->Html->script('jqueryUI/jquery-ui-1.8.custom.min.js'); ?>
<?php echo $this->Html->css('theme/redmond/jquery-ui-1.7.2.custom.css'); ?> 
<?php echo $this->Html->css('modulo_taller/tooltip.css'); ?> 
<?php echo $this->Html->script('ccptabhistoriales/indexResponsableDeposito.js'); ?>
		
			<h3 id="tituloTable"><?php echo(__('CCP_BANDEJA_RESPONSABLE_DEPOSITO',true));?></h3>
<form id="f_bsc" method="get" action="<?php echo $this->Html->url('indexResponsableDeposito') ?>">
<div id="buscadorBasico_" class="box">
	<div id="criteriosBusqueda">
		<table id="formBuscador" border="0" cellspacing="0" cellpadding="0" align="center" style="width:100%">
			<input type="hidden" size=15 name="click" id="formBuscadorClick" value="<?php echo empty($data['click'])?'':$data['click']; ?>" size="10"/>
			<tbody>
				<tr>
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.nrocars', __('Nro_OT'));?></td>
	            	<td class="valor">
	            		<input type="text" size=15 name="nrocars" id="formBuscadorNrocars" value="<?php echo empty($data['nrocars'])?'':$data['nrocars']; ?>" size="10"/>
	            	</td>
					
					<!-- buscador por placa -->
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.placa', __('CCP_PLACA'));?></td>
	            	<td class="valor">
	            		<input type="text" size=15 name="placa" id="formBuscadorPlaca" value="<?php echo empty($data['placa'])?'':$data['placa']; ?>" size="10"/>
	            	</td>					
					
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.chasis', __('Chasis'));?></td>
	            	<td class="valor">
	            		<input type="text" size="25" name="chasis" id="formBuscadorChasis" value="<?php echo empty($data['chasis'])?'':$data['chasis']; ?>" size="25"/>
	            	</td>	
					<!-- buscador por estado -->
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.estadootccp_id', __('CCP_ESTADO_OT_CCP'));?></td>
	            	<td class="valor">
						<select id="formBuscadorEstadootccpId" value="<?php echo (!empty($data['estadootccp_id']) && isset($data['estadootccp_id']))?(($data['estadootccp_id']=='9,10,22')?'':$data['estadootccp_id']):''; ?>" style="width:125px" name="estadootccp_id">
							<?php foreach($estadosCcpSelect as $id => $value): 
								if(empty($data['estadootccp_id'])) echo '<option value="'.$id.'">'.$value.'</option>';
								else{
									if($data['estadootccp_id'] != $id) echo '<option value="'.$id.'">'.$value.'</option>';
									elseif($data['estadootccp_id']=='9,10,22') echo '<option value="'.$id.'">'.$value.'</option>';
										else echo '<option selected="selected" value="'.$id.'">'.$value.'</option>';
								}
							endforeach; ?>
						</select>							
	            	</td>					

					<td>
						<?php 
						//Exportar para Excel
			    		$iconoExportarExcel = $this->Xhtml->image('derco/defecto/MicrosoftExcelDocument.gif', array(
			    			'title'=>__('REPORTE_EXPORTAR_EXCEL'), 
							'border'=>'0',
							'width'=>'16px',
							'height'=>'16px'
						));
						if(!empty($otsCars)){
						echo $this->Html->link($iconoExportarExcel, 'javascript:;', array(
								'onmouseover' => "return cambiarStatusMsg('".__('REPORTE_EXPORTAR_EXCEL')."');", 
								'onmouseout' => "return cambiarStatusMsg('');", 
								'onclick'=>"exportarExcel()", 'escape'=>false), null, false);
						}
						?>						
					</td>	
					<td><?php echo $this->Form->submit(__('Search'), array('style'=>'width:100px'));?></td>			
				</tr>
			</tbody>
		</table>
	</div>
</div>
</form>		
<div id="listaCaja">	
	<table id="listaPrincipal" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
      
				<th><?php echo $this->Paginator->sort('Talot.nrocars',__('Nro_Ot',true));?></th>
				<th><?php echo $this->Paginator->sort('Talot.placa',__('CCP_PLACA',true));?></th>
				<th><?php echo $this->Paginator->sort('Talot.marca',__('CCP_MARCA',true));?></th>
				<th><?php echo $this->Paginator->sort('Talot.modelo',__('CCP_MODELO',true));?></th>
				<th><?php echo $this->Paginator->sort('',__('CCP_COLOR',true));?></th>
				<th><?php echo $this->Paginator->sort('Talot.chassis',__('CCP_CHASIS',true));?></th>
				<th><?php echo $this->Paginator->sort('',__('horasPlanchado',true));?></th>
				<th><?php echo $this->Paginator->sort('',__('horasPanio',true));?></th>
				<th><?php echo $this->Paginator->sort('',__('CCP_ESTADO_CCP',true));?></th>
				<th class="actionsfijo"><?php  echo __('Actions');?></th>
			
			</tr>			
		</thead>
		<tbody>
			<?php $i=0;?>
			<?php  foreach ($otsCars as $otCars):?>
			 <td class="texto">
	        	<?php $i++; ?>
	        	<?php 
					if($i>15){
						$onmouseover = "tooltipArriba";
					}elseif($i>10){
						$onmouseover = "tooltipMedio";
					} else{
						$onmouseover = "tooltipAbajo";
					}
				?>
				
	        	<div class="ToolText" href="#"><?php  echo $otCars['Talot']['ot_numero']; ?></div>
				<div class="<?php echo "tooltip $onmouseover" ?>">
					<?php if(!empty($otCars['tooltip'])): ?>
						<span style="margin: 3px;"> 
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td class="detalles etiqueta" width="63px" style="border: hidden;"><?php echo __('OT_CAMPO_PROPIETARIO')?></td>
										<td class="detalles valor" width="285px" style="border: hidden;"><?php echo utf8_encode($otCars['tooltip']['Talot']['ot_cliente']); ?></td>
										<td class="detalles etiqueta" width="66px" style="border: hidden;"><?php echo __('OT_CAMPO_FECHA_ORDEN')?></td>
										<td class="detalles valor" width="120px" style="border: hidden;"><?php echo substr($otCars['tooltip']['Talot']['ot_fecha_creacion'],0,10); ?> <?php echo $otCars['tooltip']['Talot']['ot_hora_recibida']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_ETIQUETA_RESPONSABLE_ASESOR')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_nombre_asesor']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_ETIQUETA_FECHA_A_ENTREGA')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo substr($otCars['tooltip']['Talot']['ot_fecha_entrega'],0,10); ?> <?php echo $otCars['tooltip']['Talot']['ot_hora_prometida']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;">
											<?php echo __('OT_CAMPO_DESCRIPCION')?>
											<b style="color: yellow;"><?php echo utf8_encode($estadosOtCarsTooltip[$otCars['tooltip']['Talot']['ot_estado']]); ?></b>
										</td>
										<td class="detalles valor" colspan="3" style="padding-bottom: 5px;">
											<div class="div_textarea" style="padding: 2px; overflow: auto; height: 90px; width: 405px;">
												<?php echo trim(utf8_encode($otCars['tooltip']['Talot']['ot_descripcion'])); ?>
							            	</div>
										</td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_MARCA')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_marca']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_PLACA')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_placa']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_MODELO')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_modelo']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_NROCARS')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_numero']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_VERSION')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_version']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_NROCONO')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_numero_cono']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('Tipo_OT')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_tipo_ot']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('CCPSOLICITUD_TABLERO_ETIQUETA_CHASIS')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['chasis_vin']; ?></td>
									</tr>
									<tr style="border: hidden;">
										<td class="detalles etiqueta" style="border: hidden;"></td>
										<td class="detalles valor" style="border: hidden;">
											<?php //echo $this->Form->button(__('MAQUINARIAS_ENVIAR_A_ENTREGA'), array('onclick'=>'enviar_a_entrega('.$otCars['tooltip']['Talot']['id'].', "'.$otCars['tooltip']['Talot']['nrocars'].'")'));?>							
										</td>
									</tr>
								</tbody>
							</table>
						</span>
					<?php endif; ?>
				</div>
			</td>
			<td class="centrado"><?php echo $otCars['Talot']['ot_placa']; ?></td>	
			<td><?php echo $otCars['Talot']['ot_marca']; ?></td>
			<td><?php echo $otCars['Talot']['ot_modelo']; ?></td>
			<td><?php echo $otCars['Talot']['color']; ?></td>
			<td class="centrado"><?php echo $otCars['Talot']['chasis_vin']; ?></td>
			<td class="centrado"><?php echo empty($otCars['Ccpsolicitudservicio'])?'':$otCars['Ccpsolicitudservicio']['horas_panio']; ?></td>
			<td class="centrado"><?php echo empty($otCars['Ccpsolicitudservicio'])?'':$otCars['Ccpsolicitudservicio']['horas_planchado']; ?></td>
			<td class="centrado">					
				<?php 
				if(!empty($otCars['Ccpsolicitudservicio']['id'])){
					if($otCars['Ccptabestado']['id']==3){
						if($otCars['Ccpsolicitudservicioestado']['id']==3 || $otCars['Ccpsolicitudservicioestado']['id']==4 || $otCars['Ccpsolicitudservicioestado']['id']==5){
							echo empty($otCars['Ccptabestado'])?'':$otCars['Ccptabestado']['descripcion'].' - '.$otCars['Ccpsolicitudservicioestado']['descripcion'];
						}else echo empty($otCars['Ccptabestado'])?'':$otCars['Ccptabestado']['descripcion'];
					}else{
						if($otCars['Ccpsolicitudservicioestado']['id']==5){
							echo 'Con Solicitud - Rechazado';
						}else echo empty($otCars['Ccptabestado'])?'':$otCars['Ccptabestado']['descripcion'];
					}
				} else echo empty($otCars['Ccptabestado'])?'':$otCars['Ccptabestado']['descripcion'];
				?>
			</td>
			<td class="actionsfijo">
			<?php  				
					if(!empty($otCars['Ccptabestado']) && isset($otCars['Ccptabestado'])){
						if($otCars['Ccptabestado']['id']==1||$otCars['Ccptabestado']['id']==13){
							$url = $this->Html->url(array('action'=>'enviarDeposito/'.$otCars['Talot']['ot_numero']));  
							echo $this->Html->link($this->Html->image('agenda.png', array('alt' => 'Ver', 'title'=>'CCP_PASAR_DEPOSITO')),
										 'javascript:;',
										 array('onclick' => "add('".$url."')",'escape'=>false),
										 null); 
							echo '&nbsp';
						}				
						if($otCars['Ccptabestado']['id']==2){
							$url = $this->Html->url(array('action'=>'reversar/'.$otCars['Talot']['ot_numero']));  
							echo $this->Html->link($this->Html->image('retomar.png', array('alt' => 'Ver', 'title'=>'CCP_REVERSAR_OT')),
										 'javascript:;',
										 array('onclick' => "add('".$url."')",'escape'=>false),
										 null); 
							echo '&nbsp';
						}	
					}
				//estas opciones paraceran solo cuando la ot tenga solicitud de servicio
				if(!empty($otCars['Ccpsolicitudservicio']['id'])){
					$url = $this->Html->url(array('controller'=>'ccpsolicitudservicios','action'=>'mostrar/'.$otCars['Ccpsolicitudservicio']['id']));  
					echo $this->Html->link($this->Html->image('mostrar1.png', array('alt' => 'Ver', 'title'=>'GENERAL_MOSTRAR')),
									 'javascript:;',
									 array('onclick' => "mostrar('".$url."')",'escape'=>false),
									 null); 
					echo '&nbsp';
					if($otCars['Ccptabestado']['id']==4){
						$url = $this->Html->url(array('action'=>'iniciarAbp/'.$otCars['Talot']['ot_numero']));  
						echo $this->Html->link($this->Html->image('enviar.gif', array('alt' => 'Ver', 'title'=>'CCP_INICIAR_ABP')),
									 'javascript:;',
									 array('onclick' => "add('".$url."')",'escape'=>false),
									 null); 
						echo '&nbsp';
					}
					if($otCars['Ccptabestado']['id']==8){
						$url = $this->Html->url(array('action'=>'reprogramar/'.$otCars['Talot']['ot_numero']));  
						echo $this->Html->link($this->Html->image('enviar.gif', array('alt' => 'Ver', 'title'=>'CCP_REPROGRAMAR')),
									 'javascript:;',
									 array('onclick' => "add('".$url."')",'escape'=>false),
									 null); 
						echo '&nbsp';
					}
					//agregado en la CCP SEGUNDA FASE
					//si la ot esta en estado Control de Calidad Asesor Aprobada => 10 puede pasar a pendiente de entrega						
					if($otCars['Ccptabestado']['id']==7||$otCars['Ccptabestado']['id']==22){
						$url = $this->Html->url(array('action'=>'pendienteEntrega/'.$otCars['Talot']['ot_numero']));  
						echo $this->Html->link($this->Html->image('suspender.png', array('alt' => 'Ver', 'title'=>'CCP_PENDIENTE_ENTREGA')),
									 'javascript:;',
									 array('onclick' => "add('".$url."')",'escape'=>false),
									 null); 										
						echo '&nbsp';
					}
					// si la ot esta en estado Pendiente Entrega 21 => pasa a Salida UNIDAD 22
					if($otCars['Ccptabestado']['id']==21){
						$url = $this->Html->url(array('action'=>'salidaUnidad/'.$otCars['Talot']['ot_numero']));  
						echo $this->Html->link($this->Html->image('siguiente.png', array('alt' => 'Ver', 'title'=>'CCP_SALIDA_UNIDAD')),
									 'javascript:;',
									 array('onclick' => "add('".$url."')",'escape'=>false),
									 null); 
						echo '&nbsp';
					}
				}					
			?>
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
</div>

<div id ="paging" class="span-18">    
    <?php echo $this->element('paginador'); ?>
</div>
<div class="clear"></div>
<script type="text/javascript">	
	
	function exportarExcel(){
		var actionExel = "<?php echo $this->Html->url('/ccptabhistoriales/indexResponsableDepositoExportarExcel')?>";
		
	    var w = window.open('' , 'exportalExcel', 'scrollbars=yes,resizable=yes,width=100,height=100,status=no,location=no,toolbar=no');
		var actionBsc = $('#f_bsc').attr('action');
		$('#f_bsc').attr('action', actionExel);
		$('#f_bsc').attr('target', 'exportalExcel');
		$('#f_bsc').submit();
		$('#f_bsc').attr('action', actionBsc);	
		$('#f_bsc').attr('target', '');
	}

</script>