<style>
	.titulo{
		background: #005f8c;
		color: #ffffff;
		font-weight:bolder;
		font-size:12pt;
		vertical-align:middle;
	}
	.subtitulo{
		font-weight:bolder;
		font-size:10pt;
		text-align:left;
		vertical-align:middle;
	}
</style>
<table id="listaPrincipalGarantia" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<td colspan="8" class="titulo" style="font-size:14pt;"><?php echo __('CCP_BANDEJA_LAVADOR');?></td>
				</tr>
				<tr></tr>
				<tr height="15">
					<td class="subtitulo"><?php echo __('Nro_OT').': ';?></td>
					<td class="subtitulo"><?php echo $data['nrocars']; ?></td>
					<td class="subtitulo"><?php echo __('CCP_PLACA').': ';?></td>
					<td class="subtitulo"><?php echo $data['placa']; ?></td>
					<td class="subtitulo"><?php echo __('CCP_CHASIS').': ';?></td>
					<td class="subtitulo"><?php echo $data['chasis']; ?></td>
				</tr>
				<tr>
					<td class="subtitulo"><?php echo __('estadoLavado').': ';?></td>
					<td class="subtitulo"><?php echo empty($estadoCCP)?'':$seguimientoestados[$estadoCCP]?></td>
				</tr>
				<tr></tr>
				<tr>
					<td width="50" class="titulo"><?php echo strtoupper(__('Nro_Ot'));?></td>
					<td width="75" class="titulo"><?php echo strtoupper(__('CCP_PLACA'));?></td>
					<td width="120" class="titulo"><?php echo strtoupper(__('CCP_MARCA'));?></td>
					<td width="150" class="titulo"><?php echo strtoupper(__('CCP_MODELO'));?></td>
					<td width="80" class="titulo"><?php echo strtoupper(__('CCP_COLOR'));?></td>
					<td width="60" class="titulo"><?php echo strtoupper(__('CCP_CHASIS'));?></td>
					<td width="70" class="titulo"><?php echo strtoupper(__('CCP_ESTADO_CCP'));?></td>
					<th width="70" class="titulo"><?php echo strtoupper(__('estadoLavado')); ?></th>
				</tr>
			</thead>
			<tbody>
			<?php if(!empty($reporte)) { ?>
			    <?php foreach($reporte as $item): ?>
			    <tr>
			        <td style="text-align:center;"><?php echo $item['Talot']['ot_numero'] ?></td>
			    	<td style="text-align:left;"><?php echo $item['Talot']['ot_placa'];?></td>
					<td style="text-align:center;"><?php echo $item['Talot']['ot_marca'];?></td>
					<td style="text-align:center;"><?php echo $item['Talot']['ot_modelo'];?></td>
					<td style="text-align:center;"><?php echo $item['Talot']['ot_color'];?></td>
					<td style="text-align:center;"><?php echo $item['Talot']['chasis_vin'];?></td>
					<td style="text-align:center;">
						<?php
						if(!empty($item['Ccpsolicitudservicio'])){
							if($item['Ccptabestado']['id']==3){
								if($item['Ccpsolicitudservicioestado']['id']==3 || $item['Ccpsolicitudservicioestado']['id']==4 || $item['Ccpsolicitudservicioestado']['id']==5){
									echo empty($item['Ccptabestado'])?'':$item['Ccptabestado']['descripcion'].' - '.$item['Ccpsolicitudservicioestado']['descripcion'];
								}else echo empty($item['Ccptabestado'])?'':$item['Ccptabestado']['descripcion'];
							}else{
								if($item['Ccpsolicitudservicioestado']['id']==5){
									echo 'Con Solicitud - Rechazado';
								}else echo empty($item['Ccptabestado'])?'':$item['Ccptabestado']['descripcion'];
							}
						} else echo empty($item['Ccptabestado'])?'':$item['Ccptabestado']['descripcion'];
						?>
					</td>
					<td style="text-align:center;"><?php echo utf8_encode($item['Talot']['segestado_descripcion']); ?></td>
			    </tr>
			    <?php endforeach; ?>
			<?php } ?>
			</tbody>
</table>

