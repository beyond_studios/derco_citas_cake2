<?php echo $this->Session->flash();  ?>
<br/><h3 id="tituloTable"><?php echo __('CCP_REPORTE_PROCESOS_CCP');?></h3>
<style>
	td{
		border-bottom:1px solid #DDDDEE;
	}
</style>
<!-- IMPLEMENTACION DEL BUSCADOR -->
<form id="principalForm" method="get" action="<?php echo $this->Html->url('reporte') ?>">
<div id="buscadorBasico_" class="box">
	<div id="criteriosBusqueda">
		<table id="formBuscador" border="0" cellspacing="0" cellpadding="0" align="center" style="width:100%">
			<tbody>
				<tr>
					<td class="etiqueta"><?php echo $this->Form->label('nrocars', __('Nro_OT'));?></td>
	            	<td class="valor">
						<input type="text" size=15 name="nrocars" id="formBuscadorNrocars" value="<?php echo empty($data['nrocars'])?'':$data['nrocars']; ?>" size="10"/>
	            	</td>
					
					<!-- buscador por placa -->
					<td class="etiqueta"><?php echo $this->Form->label('placa', __('CCP_PLACA'));?></td>
	            	<td class="valor">
	            		<input type="text" size=15 name="placa" id="formBuscadorPlaca" value="<?php echo empty($data['placa'])?'':$data['placa']; ?>" size="10"/>
	            	</td>					
					
					<!-- <td class="etiqueta"><?php //echo $this->Form->label('proceso_id', __('CCP_REPORTE_PROCESOS'));?></td>
					<td  class="valor"><?php //echo $this->Form->select('proceso_id',$estadosCcp,$proceso_id,array('style'=>'width:150px')); ?></td>	
					<!-- buscador por estado -->
					<!-- <td class="etiqueta"><?php //echo $this->Form->label('estadootccp_id', __('CCP_ESTADO_OT_CCP'));?></td>
	            	<td class="valor"><?php //echo $this->Form->select('estadootccp_id',$estadosCcp,$estadoccp_id,array('style'=>'width:150px')); ?></td>	-->				
					<td><?php echo $this->Form->submit(__('Search'), array('style'=>'width:100px'));?></td>		
					<td>
						<?php 
						//Exportar para Excel
			    		$iconoExportarExcel = $this->Xhtml->image('derco/defecto/MicrosoftExcelDocument.gif', array(
			    			'title'=>__('REPORTE_EXPORTAR_EXCEL'), 
							'border'=>'0',
							'width'=>'16px',
							'height'=>'16px'
						));
						if(!empty($reportes)){
						echo $this->Html->link($iconoExportarExcel, 'javascript:;', array(
								'onmouseover' => "return cambiarStatusMsg('".__('REPORTE_EXPORTAR_EXCEL')."');", 
								'onmouseout' => "return cambiarStatusMsg('');", 
								'onclick'=>"exportarExcel()", 'escape'=>false), null, false);
						}
						?>						
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
</form>
<!-- FIN - IMPLEMENTACION DEL BUSCADOR -->



<?php //debug($this->layout); ?>
<div id="listaCaja">
	<table id="listaPrincipal" cellpadding="0" cellspacing="0">
		<thead>
		    <tr>
				<th><?php echo __('CCP_REPORTE_OT') ?></th>
				<th><?php echo __('CCP_REPORTE_CLIENTE') ?></th>
				<th><?php echo __('CCP_REPORTE_PLACA') ?></th>
				<th><?php echo __('CCP_REPORTE_PROCESO') ?></th>
				<th><?php echo __('CCP_REPORTE_DESCRIPCION') ?></th>
				<th><?php echo __('CCP_REPORTE_RESPONSABLE') ?></th>
				<th><?php echo __('CCP_REPORTE_FECHA_INICIO') ?></th>
				<th><?php echo __('CCP_REPORTE_FECHA_FIN') ?></th>
				<th><?php echo __('CCP_REPORTE_DURACION') ?></th>
				<th><?php echo __('CCP_REPORTE_ESTADO')?></th>
		    </tr>
		</thead>
		<tbody>
		    <?php foreach ($reportes as $reporte): ?> 
		    <tr>
		        <td class="texto" rowspan="<?php echo count($reporte['Estados'])+1?>">
		        	<?php  echo $reporte['Talot']['ot_numero']; ?>
				</td>
		        <td class="texto" rowspan="<?php echo count($reporte['Estados'])+1?>"><?php echo utf8_encode($reporte['Talot']['ot_cliente']); ?></td>
		        <td class="texto" rowspan="<?php echo count($reporte['Estados'])+1?>"><?php echo ($reporte['Talot']['ot_placa']); ?></td>
			</tr>
			<?php foreach($reporte['Estados'] as $id => $item){ ?>
				<tr>
					<td class="texto" style="width:20px"><?php echo $item['proceso'];?></td>
			        <td class="texto"><?php echo $item['descripcion'];?></td>
					<td class="texto"><?php echo $item['responsable'];?></td>
					<td class="texto" style="width:120px;"><?php echo date('d-m-Y H:i',strtotime($item['fechaInicio']));?></td>
					<td class="texto" style="width:120px;"><?php echo date('d-m-Y H:i',strtotime($item['fechaFin']));?></td>
					<td class="texto"><?php echo $item['duracion'];?></td>
					<td class="texto"><?php echo $item['estado'];?></td>
			    </tr>
			<?php }?>
		    <?php endforeach; ?>
		</tbody>
	</table>
</div>

<script type="text/javascript">
	function exportarExcel(){
		var actionExel = "<?php echo $this->Html->url('/ccptabhistoriales/reporteExcel/')?>";
		
	    var w = window.open('' , 'exportalExcel', 'scrollbars=yes,resizable=yes,width=100,height=100,status=no,location=no,toolbar=no');
		var actionBsc = $('#principalForm').attr('action');
		$('#principalForm').attr('action', actionExel);
		$('#principalForm').attr('target', 'exportalExcel');
		$('#principalForm').submit();
		$('#principalForm').attr('action', actionBsc);	
		$('#principalForm').attr('target', '');
	}
</script>