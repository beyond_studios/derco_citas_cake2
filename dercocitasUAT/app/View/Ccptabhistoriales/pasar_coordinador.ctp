<!-- JS/CSS UTILIZADOS -->
<?php $this->Html->script('ccptabhistorial/enviar_deposito.js'); 

?>
<!-- CONTENIDO DE LA PAGINA --> 

<?php 	$optionsCombo=array("style"=>"width:120px","0"=>true);//cambiar el 0 por disablesd
		$optionsInput=array('readonly'=>false);
		$optionsCheckBox=false;
		$optionsInputFecha=array('size'=>10,'readonly'=>false);
		$optionsInputDisabled=array('readonly'=>true);
		$optionsInputMedio=array('size'=>30);

?>
<!-- CREACION DEL FORMULARIO -->	
<div id="flashMessage" class="message" style="display: none;"><?php echo __('GENERAL_SELECCIONAR_ITEM'); ?></div>
<h2><?php echo __('CCP_PASAR_LAVADO'); ?></h2>

<form id="Ccptabhistorial" action="<?php echo $this->Html->url('pasarCoordinador/').$seg_id?>" method="post">
	<?php echo $this->Form->hidden('Ccplavadoseguimiento.id',array('value'=>$seg_id)); ?>
<div class="span-9">
	<div style="text-align:center;">
		<table align="center">
			<tbody>
				<tr>
					<td colspan="2" class="centrado" style="font-weight:bolder;" ><?php  echo 'OT '.$ot_numero?></td>
				</tr>
				<tr>
					<td class="etiqueta span-3"><?php echo __('Comentario'); ?></td>
					<td class="valor span-6 last">
						<?php echo $this->Form->input('Ccplavadoseguimiento.comentario_coordinador', array('type'=>'textarea','cols'=>19,'rows'=>5, 'div'=>false, 'label'=>false)); ?>
						<div><label for="CcplavadoseguimientoComentarioCoordinador" generated="true" class="error" style="display: none;"></label></div>						
					</td>
				</tr>
	
			</tbody>
		</table>
	</div>
	<div style="text-align:center;" class="accion">
        <?php if(empty($guardado)) echo $this->Form->submit(__('GENERAL_GUARDAR'), array('class'=>'guardar', 'div'=>false, 'label'=>false));?>
        <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar','onclick'=>'javascript:window.close()', 'div'=>false, 'label'=>false));?>
	</div>	
</div>
</form>
<script type="text/javascript">
actualizarPadre();
function actualizarPadre() {
    var guardado = "<?php echo $guardado?>";
	if(guardado=="1"){
		window.opener.location.href = window.opener.location.href;
	} 
    
}

</script>