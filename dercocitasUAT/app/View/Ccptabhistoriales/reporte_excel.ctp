<style>
	.titulo{
		background: #005f8c;
		color: #ffffff;
		font-weight:bolder;
		font-size:12pt;
		vertical-align:middle;
	}
	.subtitulo{
		font-weight:bolder;
		font-size:10pt;
		text-align:left;
		vertical-align:middle;
	}
</style>
<table id="listaPrincipalGarantia" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<td colspan="10" class="titulo" style="font-size:14pt;"><?php echo __('CCP_REPORTE_PROCESOS_CCP');?></td>
				</tr>
				<tr height="15">
					<td class="subtitulo"><?php echo __('Nro_OT').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['nrocars'])?'':$data['nrocars'];?></td>
					<td class="subtitulo"><?php echo __('CCP_PLACA').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['placa'])?'':$data['placa'];?></td>

				</tr>
				<tr>
					<td width="50" class="titulo"><?php echo strtoupper(__('CCP_REPORTE_OT'));?></td>
					<td width="30" class="titulo"><?php echo strtoupper(__('CCP_REPORTE_CLIENTE'));?></td>
					<td width="75" class="titulo"><?php echo strtoupper(__('CCP_REPORTE_PLACA'));?></td>
					<td width="120" class="titulo"><?php echo strtoupper(__('CCP_REPORTE_PROCESO'));?></td>
					<td width="150" class="titulo"><?php echo strtoupper(__('CCP_REPORTE_DESCRIPCION'));?></td>
					<td width="80" class="titulo"><?php echo strtoupper(__('CCP_REPORTE_RESPONSABLE'));?></td>
					<td width="60" class="titulo"><?php echo strtoupper(__('CCP_REPORTE_FECHA_INICIO'));?></td>
					<td width="60" class="titulo"><?php echo strtoupper(__('CCP_REPORTE_FECHA_FIN'));?></td>
					<td width="70" class="titulo"><?php echo strtoupper(__('CCP_REPORTE_DURACION'));?></td>
					<td width="70" class="titulo"><?php echo strtoupper(__('CCP_REPORTE_ESTADO'));?></td>
				</tr>
			</thead>
			<tbody>
		<tbody>
		    <?php foreach ($reportes as $reporte): ?> 
		    <tr>
		        <td class="subtitulo" rowspan="<?php echo count($reporte['Estados'])+1?>">
		        	<?php  echo $reporte['Talot']['ot_numero']; ?>
				</td>
		        <td class="subtitulo" rowspan="<?php echo count($reporte['Estados'])+1?>"><?php echo utf8_encode($reporte['Talot']['ot_cliente']); ?></td>
		        <td class="subtitulo" rowspan="<?php echo count($reporte['Estados'])+1?>"><?php echo ($reporte['Talot']['ot_placa']); ?></td>
			</tr>
			<?php foreach($reporte['Estados'] as $id => $item){ ?>
				<tr>
					<td class="texto" style="width:20px"><?php echo $item['proceso'];?></td>
			        <td class="texto"><?php echo $item['descripcion'];?></td>
					<td class="texto"><?php echo $item['responsable'];?></td>
					<td class="texto" style="width:120px;"><?php echo date('d-m-Y H:i',strtotime($item['fechaInicio']));?></td>
					<td class="texto" style="width:120px;"><?php echo date('d-m-Y H:i',strtotime($item['fechaFin']));?></td>
					<td class="texto"><?php echo $item['duracion'];?></td>
					<td class="texto"><?php echo $item['estado'];?></td>
			    </tr>
			<?php }?>
		    <?php endforeach; ?>
		</tbody>
</table>

