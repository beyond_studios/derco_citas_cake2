<style>
	.titulo{
		background: #005f8c;
		color: #ffffff;
		font-weight:bolder;
		font-size:12pt;
		vertical-align:middle;
	}
	.subtitulo{
		font-weight:bolder;
		font-size:10pt;
		text-align:left;
		vertical-align:middle;
	}
</style>
<table id="listaPrincipalGarantia" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<td colspan="9" class="titulo" style="font-size:14pt;"><?php echo __('BANDEJA_LAVADOR_REPORTE');?></td>
				</tr>
				<tr></tr>
				<tr height="15">
					<td class="subtitulo"><?php echo __('fechaRegistro').': '.__('Desde');?></td>
					<td class="subtitulo"><?php echo $data['fechaIni']; ?></td>
	            	<td class="subtitulo"><?php echo __('Hasta');?></td>
					<td class="subtitulo"><?php echo $data['fechaFin']; ?></td>
					
	            	<td class="subtitulo"><?php echo __('Nro_OT');?></td>
					<td class="subtitulo"><?php echo $data['nrocars']; ?></td>
	            	<td class="subtitulo"><?php echo __('CCP_PLACA');?></td>
					<td class="subtitulo"><?php echo $data['placa']; ?></td>
				</tr>
				<tr>
					<td class="subtitulo"><?php echo __('estadoLavado').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['estadootccp_id'])?'':$seguimientoestados[$data['estadootccp_id']]?></td>
					<td class="subtitulo"><?php echo __('CCP_CHASIS');?></td>
					<td class="subtitulo"><?php echo $data['chasis']; ?></td>
				</tr>
				<tr></tr>
				<tr>
					<td width="50" class="titulo"><?php echo strtoupper(__('Nro_Ot'));?></td>
					<td width="75" class="titulo"><?php echo strtoupper(__('CCP_PLACA'));?></td>
					<td width="120" class="titulo"><?php echo strtoupper(__('CCP_MARCA'));?></td>
					<td width="150" class="titulo"><?php echo strtoupper(__('CCP_MODELO'));?></td>
					<td width="80" class="titulo"><?php echo strtoupper(__('CCP_COLOR'));?></td>
					<td width="60" class="titulo"><?php echo strtoupper(__('CCP_CHASIS'));?></td>
					<th width="60" class="titulo"><?php echo __('motivo') ?></th>
					<th width="60" class="titulo"><?php echo __('comentario') ?></th>
					<th width="60" class="titulo"><?php echo __('estadoLavado') ?></th>
				</tr>
			</thead>
			<tbody>
			<?php if(!empty($reporte)) { ?>
			    <?php foreach($reporte as $item): ?>
			    <tr>
			        <td style="text-align:center;"><?php echo $item['Talot']['ot_numero'] ?></td>
			    	<td style="text-align:left;"><?php echo $item['Talot']['ot_placa'];?></td>
					<td style="text-align:center;"><?php echo $item['Talot']['ot_marca'];?></td>
					<td style="text-align:center;"><?php echo $item['Talot']['ot_modelo'];?></td>
					<td style="text-align:center;"><?php echo $item['Talot']['ot_color'];?></td>
					<td style="text-align:center;"><?php echo $item['Talot']['chasis_vin'];?></td>
				
					<td class=""><?php echo utf8_encode($item['Talot']['segmotivo_descripcion']); ?></td>
					<td class=""><?php echo utf8_encode($item['Talot']['seg_comentario_lavado']); ?></td>
					<td class=""><?php echo utf8_encode($item['Talot']['segestado_descripcion']); ?></td>
			    </tr>
			    <?php endforeach; ?>
			<?php } ?>
			</tbody>
</table>