<!-- JS/CSS UTILIZADOS -->
<?php echo $this->Html->script('ccptabhistoriales/observar_file.js'); ?>

<!-- CONTENIDO DE LA PAGINA --> 

<?php 	$optionsCombo=array("style"=>"width:120px","0"=>true);//cambiar el 0 por disablesd
		$optionsInput=array('readonly'=>false);
		$optionsCheckBox=false;
		$optionsInputFecha=array('size'=>10,'readonly'=>false);
		$optionsInputDisabled=array('readonly'=>true);
		$optionsInputMedio=array('size'=>30);

?>
<!-- CREACION DEL FORMULARIO -->
<div class="span-8" >
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-8" >
		<h3><?php echo __('CCP_OBSERVAR_FILE');?></h3>
	</div>
</div>

<form id="Ccptabhistorial" action="<?php echo $this->Html->url('observarfile/').$ot_numero?>" method="post">
<?php echo $this->Form->hidden('Ccptabhistorialdocumento.ot_numero',array('value'=>$ot_numero)); ?>
<div class="span-12">
	<div style="text-align:center;">
		<table align="center">
			<tbody>
				<tr>
					<td colspan="2" class="centrado" style="font-weight:bolder;" ><?php  echo 'OT '.$ot_numero?></td>
				</tr>
				<tr>
					<td class="etiqueta span-3"><?php echo __('CCP_ESTADO_ACTUAL'); ?></td>
					<td class="valor span-9 last">
						<?php echo $this->Form->input('Ccptabhistorialdocumento.ccptabestado_anterior',array('type'=>'select','options'=>$estadoAnterior,'disabled'=>true,'class'=>'required readonly','size'=>'', 'readonly'=>true, 'div'=>false, 'label'=>false)); ?>
						<?php foreach($estadoAnterior as $id=>$item){?>
							<?php echo $this->Form->hidden('Ccptabhistorialdocumento.ccptabestado_anterior',array('value'=>$id)); ?>
						<?php }?>						
						<span class="campoObligatorio">*</span>
						<div><label for="CcptabhistorialCcptabestadoAnterior" generated="true" class="error" style="display: none;"></label></div>
					</td>
				</tr>
				<tr>
					<td class="etiqueta span-3"><?php echo __('CCP_ESTADO_SIGUIENTE'); ?></td>
					<td class="valor span-9 last">
						<?php echo $this->Form->input('Ccptabhistorialdocumento.ccptabestadodocumento_id',array('type'=>'select','options'=>$estadoPasarA,'disabled'=>true,'class'=>'required readonly','size'=>'', 'readonly'=>true, 'div'=>false, 'label'=>false)); ?>
						<?php foreach($estadoPasarA as $id=>$item){?>
							<?php echo $this->Form->hidden('Ccptabhistorialdocumento.ccptabestadodocumento_id',array('value'=>$id)); ?>
						<?php }?>
						<span class="campoObligatorio">*</span>
						<div><label for="CcptabhistorialdocumentoCcptabestadodocumentoId" generated="true" class="error" style="display: none;"></label></div>						
					</td>
				</tr>
				<tr>
			</tbody>
		</table>
		<table id="ccptabhistorial_table" align="center">	
			<thead>				
					<td class="etiqueta span-3"><?php echo __('Comentario'); ?></td>
					<td class="valor span-9 last">
						<?php echo $this->Form->textarea('Ccptabhistorialdocumento.comentario', array('class'=>'required','cols'=>30,'rows'=>3)); ?>
  						<?php echo $this->Xhtml->imageTextLink('plus.gif', null, 
										            'javascript:;',array('onclick'=>"agregar_observacion()", 'escape'=>false), null, array('width'=>'15'));?>						
						<span class="campoObligatorio">*</span>
						<div><label for="CcptabhistorialdocumentoComentario" generated="true" class="error" style="display: none;"></label></div>
					</td>
				</tr>
	
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
	<div style="text-align:center;" class="accion">
        <?php if(empty($guardado)) echo $this->Form->submit(__('GENERAL_GUARDAR'), array('class'=>'agregar', 'div'=>false, 'label'=>false));?>
        <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar','onclick'=>'javascript:window.close()', 'div'=>false, 'label'=>false));?>
	</div>	
</div>
</form>

	<fieldset>
		<legend><?php echo __('Historial_de_actas'); ?></legend>
		<table id="listaPrincipal">
			<thead>
				<tr>
					<!-- <th><?php echo __('ACTA_CLASIFICACION'); ?></th> -->
					<th><?php echo __('ACTA_FECHA'); ?></th>
					<th><?php echo __('ACTA_HORA'); ?></th>
					<th><?php echo __('CCP_TALACTAS_USUARIO'); ?></th>
					<th><?php echo __('ACTA_DESCRIPCION'); ?></th>
					
				</tr>
			</thead>
			<tbody>
				<?php foreach($actas as $acta): ?>
				<tr>
					<td><?php echo substr($acta['Talacta']['fechaUsuario'], 0,10); ?></td>
					<td><?php echo substr($acta['Talacta']['fechaUsuario'], 10,6); ?></td>
					<td width="100"><?php echo $acta[0]['Usuario'] ?></td>
					<td><?php echo $acta['Talacta']['descripcion'] ?></td>
				</tr>
				<?php endforeach; ?>
				<tr><td colspan=4>&nbsp;</td></tr>
			</tbody>	
		</table>	
	</fieldset>
<script type="text/javascript">
actualizarPadre();
function actualizarPadre() {
    var guardado = "<?php echo $guardado?>";
	if(guardado=="1"){
		window.opener.location.href = window.opener.location.href;
	} 
    
}

</script>