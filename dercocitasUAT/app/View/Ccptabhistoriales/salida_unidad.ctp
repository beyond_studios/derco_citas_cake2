<div class="span-8" >
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-8" >
		<h3><?php echo __('CCP_PASAR_SALIDA_UNIDAD');?></h3>
	</div>
	<hr/>	
	
	<?php echo $this->Form->create('Ccptabhistorial',array('url' => array('action' =>'salidaUnidad/'.$ot_numero)));?>
		<?php //echo $this->Form->hidden('Ccptabhistorial.ot_numero',array()); ?>	
		<?php echo $this->Form->input('ot_numero', array('type'=>'hidden')); ?>
		
		<div class="span-8" >
			<center>
				<h4 style="color: #000;"><label><?php  echo 'OT '.$ot_numero?>
				</label> </h4>
			</center>
		</div>	
		
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo(__('CCP_ESTADO_ACTUAL',true)) ?>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php 
					echo $this->Form->select('ccptabestado_anterior',$estadoAnterior,array(/*'disabled'=>true,*/'class'=>'required readonly','readonly'=>true,'empty'=>false),false);	
				?>
				<span class="error"><?php echo " *";?>	</span>
			</div>
		</div>	
	
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo(__('CCP_ESTADO_SIGUIENTE',true)) ?>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php 
					echo $this->Form->select('ccptabestado_id',$estadoPasarA,array(/*'disabled'=>true,*/'class'=>'required readonly','readonly'=>true,'empty'=>false),false);	
				?><span class="error"><?php echo " *";?>	</span>
			</div>
		</div>			
	
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo(__('Comentario',true)) ?>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('comentario',array('type'=>'textarea','label'=>false,'class'=>'span-5','div'=>false, 'style'=>'width:150px; height:50px;'));?>
			</div>
		</div>	
		
		<div style="text-align:center;" class="accion">
		<?php if(empty($guardado)) echo $this->Form->submit(__('GENERAL_GUARDAR'), array('class'=>'guardar' , 'div'=>false, 'label'=>false));?>
        <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar','onclick'=>'javascript:window.close()', 'div'=>false, 'label'=>false));?>
		
		</div>
	
	<?php echo $this->Form->end(); ?>

<?php echo $this->element('actualizar_ver'); ?>
</div>
<script type="text/javascript">
actualizarPadre();
function actualizarPadre() {
    var guardado = "<?php echo $guardado?>";
	if(guardado=="1"){
		window.opener.location.href = window.opener.location.href;
	} 
    
}

</script>	