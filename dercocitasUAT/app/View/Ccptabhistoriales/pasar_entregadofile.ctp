<!-- JS/CSS UTILIZADOS -->
<?php echo $this->Html->script('ccptabhistoriales/enviar_deposito.js'); ?>

<!-- CONTENIDO DE LA PAGINA --> 
<?php 	$optionsCombo=array("style"=>"width:120px","0"=>true);//cambiar el 0 por disablesd
		$optionsInput=array('readonly'=>false);
		$optionsCheckBox=false;
		$optionsInputFecha=array('size'=>10,'readonly'=>false);
		$optionsInputDisabled=array('readonly'=>true);
		$optionsInputMedio=array('size'=>30);

?>
<!-- CREACION DEL FORMULARIO -->	
<div class="span-8" >
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-8" >
		<h3><?php echo __('CCP_PASAR_ENTREGADO_FILE');?></h3>
	</div>
</div>

<form id="Ccptabhistorial" action="<?php echo $this->Html->url('pasarEntregadofile/').$ot_numero?>" method="post">
	<?php echo $this->Form->hidden('Ccptabhistorialdocumento.ot_numero',array('value'=>$ot_numero)); ?>
<div class="span-9">
	<div style="text-align:center;">
		<table align="center">
			<tbody>
				<tr>
					<td colspan="2" class="centrado" style="font-weight:bolder;" ><?php  echo 'OT '.$ot_numero?></td>
				</tr>
				<?php if(!empty($estadoAnterior) && isset($estadoAnterior)){ ?>
				<tr>
					<td class="etiqueta span-3"><?php echo __('CCP_ESTADO_ACTUAL'); ?></td>
					<td class="valor span-6 last">
						<?php  echo $this->Form->input('Ccptabhistorialdocumento.ccptabestado_anterior',array('options'=>$estadoAnterior,'disabled'=>true,'class'=>'required readonly','readonly'=>true, 'div'=>false, 'label'=>false)); ?>
						<?php foreach($estadoAnterior as $id=>$item){?>
							<?php echo $this->Form->hidden('Ccptabhistorialdocumento.ccptabestado_anterior',array('value'=>$id)); ?>
						<?php }?>						
						<span class="campoObligatorio">*</span>
						<div><label for="CcptabhistorialdocumentoCcptabestadoAnterior" generated="true" class="error" style="display: none;"></label></div>
					</td>
				</tr>
				<?php } ?>
				<tr>
					<td class="etiqueta span-3"><?php echo __('CCP_ESTADO_SIGUIENTE'); ?></td>
					<td class="valor span-6 last">
						<?php echo $this->Form->input('Ccptabhistorialdocumento.ccptabestadodocumento_id',array('options'=>$estadoPasarA,'disabled'=>true,'class'=>'required readonly','readonly'=>true,'div'=>false, 'label'=>false)); ?>
						<?php foreach($estadoPasarA as $id=>$item){?>
							<?php echo $this->Form->hidden('Ccptabhistorialdocumento.ccptabestadodocumento_id',array('value'=>$id)); ?>
						<?php }?>
						<span class="campoObligatorio">*</span>
						<div><label for="CcptabhistorialdocumentoCcptabestadodocumentoId" generated="true" class="error" style="display: none;"></label></div>						
					</td>
				</tr>
				<tr>
					<td class="etiqueta span-3"><?php echo __('Comentario'); ?></td>
					<td class="valor span-6 last">
						<?php echo $this->Form->textarea('Ccptabhistorialdocumento.comentario', array('cols'=>19,'rows'=>5)); ?>
						<div><label for="CcptabhistorialdocumentoComentario" generated="true" class="error" style="display: none;"></label></div>						
					</td>
				</tr>
	
			</tbody>
		</table>
	</div>
	<div style="text-align:center;" class="accion">
        <?php if(empty($guardado)) echo $this->Form->submit(__('GENERAL_GUARDAR'), array('class'=>'agregar', 'div'=>false, 'label'=>false));?>
        <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar','onclick'=>'javascript:window.close()', 'div'=>false, 'label'=>false));?>
	</div>	
</div>
</form>
<script type="text/javascript">
actualizarPadre();
function actualizarPadre() {
    var guardado = "<?php echo $guardado?>";
	if(guardado=="1"){
		window.opener.location.href = window.opener.location.href;
	} 
    
}

</script>