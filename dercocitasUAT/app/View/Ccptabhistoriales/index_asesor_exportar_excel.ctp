<style>
	.titulo{
		background: #005f8c;
		color: #ffffff;
		font-weight:bolder;
		font-size:12pt;
		vertical-align:middle;
	}
	.subtitulo{
		font-weight:bolder;
		font-size:10pt;
		text-align:left;
		vertical-align:middle;
	}
</style>

<table id="listaPrincipalGarantia" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<td colspan="9" class="titulo" style="font-size:14pt;"><?php echo __('CCP_BANDEJA_ASESOR_SERVICIO_CLIENTE');?></td>
				</tr>
				<tr height="15">
					<td class="subtitulo"><?php echo __('CCP_REPORTE_FECHA_SOLICTUD');?></td>
					<td class="subtitulo"><?php echo __('GARANTIAS_GENERAL_REPORTES_DESDE').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['fechaIni'])?'':$data['fechaIni']?></td>
					<td class="subtitulo"><?php echo __('GARANTIAS_GENERAL_REPORTES_HASTA').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['fechaFin'])?'':$data['fechaFin']?></td>
				</tr>
				<tr height="15">
					<td class="subtitulo"><?php echo __('CCP_REPORTE_FECHA_OT');?></td>
					<td class="subtitulo"><?php echo __('GARANTIAS_GENERAL_REPORTES_DESDE').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['fechaIniOt'])?'':$data['fechaIniOt']?></td>
					<td class="subtitulo"><?php echo __('GARANTIAS_GENERAL_REPORTES_HASTA').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['fechaFinOt'])?'':$data['fechaFinOt']?></td>
				</tr>	
				<tr height="15">
					<td class="subtitulo"><?php echo __('Asesor').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['asesor_id'])?'':$asesoresCars[$data['asesor_id']]?></td>
					<td class="subtitulo"><?php echo __('Tipo_servicio').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['ccptiposervicio_id'])?'':$ccptiposervicios[$data['ccptiposervicio_id']]?></td>
					<td class="subtitulo"><?php echo __('CCP_ESTADO_OT_CARS').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['carsestado_id'])?'':$estadosOtCarsTooltip[$data['carsestado_id']-1]?></td>
					<td class="subtitulo"><?php echo __('estadoSolicitud').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['ccpsolicitudservicioestado_id'])?'':$ccpsolicitudservicioestados[$data['ccpsolicitudservicioestado_id']]?></td>
				</tr>
				<tr height="15">
					<td class="subtitulo"><?php echo __('Nro_OT').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['nrocars'])?'':$data['nrocars'];?></td>
					<td class="subtitulo"><?php echo __('CCP_PLACA').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['placa'])?'':$data['placa'];?></td>
					<td class="subtitulo"><?php echo __('estadoPorPresupuesto').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['preestado_id'])?'':$estadoPorPresupuestos[$data['preestado_id']]?></td>
					<td class="subtitulo"><?php echo __('CCP_ESTADO_OT_CCP').': ';?></td>
					<td class="subtitulo"><?php echo empty($data['estadootccp_id'])?'':$estadosCcpSelect[$data['estadootccp_id']]?></td>
				</tr>
				<tr>
					<td width="50" class="titulo"><?php echo strtoupper(__('Nro_Ot'));?></td>
					<td width="30" class="titulo"><?php echo strtoupper(__('Cliente'));?></td>
					<td width="75" class="titulo"><?php echo strtoupper(__('CCP_PLACA'));?></td>
					<td width="120" class="titulo"><?php echo strtoupper(__('fechaAperturaOt'));?></td>
					<td width="150" class="titulo"><?php echo strtoupper(__('Fecha_aprobacion_presupuesto'));?></td>
					<td width="80" class="titulo"><?php echo strtoupper(__('Fecha_solicitud'));?></td>
					<td width="60" class="titulo"><?php echo strtoupper(__('Tipo_servicio'));?></td>
					<td width="60" class="titulo"><?php echo strtoupper(__('EstadoPres.'));?></td>
					<td width="70" class="titulo"><?php echo strtoupper(__('CCP_ESTADO_CCP'));?></td>
				</tr>
			</thead>
			<tbody>
			<?php if(!empty($reporte)) { ?>
			    <?php foreach($reporte as $item): ?>
			    <tr>
			        <td style="text-align:center;"><?php echo $item['Talot']['ot_numero'] ?></td>
			    	<td style="text-align:left;"><?php echo utf8_encode($item['Talot']['ot_cliente']);?></td>
					<td style="text-align:center;"><?php echo $item['Talot']['ot_placa'];?></td>
					<td style="text-align:center;"><?php echo substr($item['Talot']['ot_fecha_creacion'],0,10);?></td>
					<td style="text-align;">
						<?php $fecha = ''; 
							if(substr($item['Talot']['pre_fecha_aprobacion'],6,10) >= 2000) $fecha = substr($item['Talot']['pre_fecha_aprobacion'],0,10);
						echo $fecha; ?>
					</td>
			    	<td style="text-align:center;"><?php echo empty($item['Ccpsolicitudservicio'])?'':substr($item['Ccpsolicitudservicio']['fecha_emitido'],0,10); ?></td>					
					<td style="text-align:center;"><?php echo empty($item['Ccptiposervicio'])?'': utf8_encode($item['Ccptiposervicio']['descripcion']); ?></td>
					<td style="text-align:center;"><?php echo $estadoPorPresupuestos[$item['Talot']['ot_pre_estado']]; ?></td>
					<td style="text-align:center;">
						<?php
						if(!empty($item['Ccpsolicitudservicio'])){
							if($item['Ccptabestado']['id']==3){
								if($item['Ccpsolicitudservicioestado']['id']==3 || $item['Ccpsolicitudservicioestado']['id']==4 || $item['Ccpsolicitudservicioestado']['id']==5){
									echo empty($item['Ccptabestado'])?'':$item['Ccptabestado']['descripcion'].' - '.$item['Ccpsolicitudservicioestado']['descripcion'];
								}else echo empty($item['Ccptabestado'])?'':$item['Ccptabestado']['descripcion'];
							}else{
								if($item['Ccpsolicitudservicioestado']['id']==5){
									echo 'Con Solicitud - Rechazado';
								}else echo empty($item['Ccptabestado'])?'':$item['Ccptabestado']['descripcion'];
							}
						} else echo empty($item['Ccptabestado'])?'':$item['Ccptabestado']['descripcion'];
						?>
					</td>
			    </tr>
			    <?php endforeach; ?>
			<?php } ?>
			</tbody>
</table>

