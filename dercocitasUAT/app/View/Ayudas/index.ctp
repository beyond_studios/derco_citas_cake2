<?php echo $this->Html->script('layouts/mijquery.js'); ?>
<?php echo $this->Html->script('Ayudas/index.js'); ?>
		<?php echo $this->Session->flash();  ?>
		<br/>
			<h3 id="tituloTable"><?php echo __('DOCUMENTOS PDF DE AYUDA AL CLIENTE');?></h3>
		
		<div class="box">
			<div id="agregar" class="span-5" >
			<?php 	
					$url = $this->Html->url(array("action"=>'add'));  
					echo $this->Html->link($this->Html->image('agregar.png', array('alt' => 'Agregar')),
									 'javascript:;',
									 array('onclick' => "add('".$url."')",'escape'=>false),
									 null); 
			?>
				&nbsp;
			<?php 
				echo $this->Html->link(__('Add', true), 'javascript:;',array('onclick' => "add('".$url."')")); 
			?>					
			</div>
			<div id="buscador" class="">&nbsp;
			</div>	
		</div>		
		<table cellpadding="0" cellspacing="0" class="table" >
			<thead>
			<tr>
                <th><?php echo __('Documento',true);?></th> 
				<th style="width:50%;"><?php echo $this->Paginator->sort('Ayuda.titulo',__('Descripcion',true));?></th>        
				<th><?php echo $this->Paginator->sort('Ayuda.status',__('Estado',true));?></th>        
				<th class="actionsfijo"><?php  echo __('Actions');?></th>
			
			</tr>			
			</thead>
			
			<tbody>
				<?php  foreach ($ayudas as $ayuda):?>
				<tr>
					<td><?php 	echo $ayuda['Ayuda']['fileName']; ?></td>
					<td><?php 	echo $ayuda['Ayuda']['titulo']; ?></td>
					<td class="textc">
						<?php echo $ayuda['Ayuda']['status'] == 'AC' ?__('Activo',true):__('Desactivo',true); ?>
					</td>
			
					<td class="actionsfijo">						
					<?php 
						if(in_array($ayuda['Ayuda']['status'], array('AC','DE','LI'))){
							echo $this->Html->link(
								$this->Html->image('derco/defecto/eliminar.png', array('title'=>__('eliminar',true), "alt" =>__('eliminar',true))), 
								array('action'=>'delete', $ayuda['Ayuda']['id']), 
								array('escape'=>false), 
								sprintf(__('estaSeguroEliminar %s?'), $ayuda['Ayuda']['titulo'])
							);
						}
					?>
					</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
		</table>

<div id ="paging" class="span-18">
    <?php echo $this->element('paginador'); ?>
</div>
<div class="clear"></div>