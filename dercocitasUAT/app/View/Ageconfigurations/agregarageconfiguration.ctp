<style>
	a {
		text-decoration: none;
	}
	ul, li {
		margin:0pt 12px;
		margin-right: 0px;
	}
</style>
<br />
	<?php echo $this->Html->script('ageconfigurations/add.js'); ?>
	<?php echo $this->Session->flash();?>
	<div class="span-24" align="center">		
		<div class="span-24" >
			<h3 id="tituloTable"><?php echo __('AGE_CONFIGURACIONES');?></h3>
		</div>	
	<br/>
			<?php echo $this->Form->create('Ageconfiguration');?>		
		<div class="span-24" >	
			<div class="labelForm">
				<label >
					<?php echo __('organizacion') ?><span class="error"><?php echo " *";?>	</span>
				</label>	
			</div>	
			<div class="formInput">
				<?php echo $this->Form->input('secorganization_id',array('options'=>$secorganizations,'type'=>'select','class'=>'span-5','label' =>''));
				?>
				<?php echo $this->Js->get('#AgeconfigurationSecorganizationId')->event('change',$this->Js->request(
				array('url' => array( 'action' => 'listroles'),'update' => 'AgeconfigurationSecroleId' )));?>
			</div>			
										
		</div>
		<div class="span-24" >
			<div class="labelForm">
				<label >
					<?php echo __('AGE_CONFIGURACIONES_ETIQUETA_ROL') ?><span class="error"><?php echo " *";?>	</span>
				</label> 	
			</div>
			<div class="formInput">
					<?php echo $this->Form->input('secrole_id', array('label'=>'','class'=>'span-5')); ?>
			</div>			
		</div>			
		<div class="span-24" >
			<div class="labelForm">
				<label><?php echo __('AGE_CONFIGURACIONES_ETIQUETA_HORACORTECITAS') ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="formInput">
				<?php echo $this->Form->input('horacortecitas',array('type' =>'time','timeFormat'=>'24','label'=>'')); ?>
			</div>

		</div>
		<div class="span-24" >
			<div class="labelForm">
				<label><?php echo __('AGE_CONFIGURACIONES_ETIQUETA_CITASADICIONALES') ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="formInput">
				<?php echo $this->Form->input('citasadicionales',array('type' =>'text','class'=>'span-2','label'=>'')); ?>
			</div>

		</div>		
		<div class=" span-24 botones" >
			<hr/>
			<?php echo $this->Form->submit(__('guardar'), array('div'=>false));	?>			
			<?php echo $this->Form->button(__('cancelar'), array('type'=>'reset')); ?>
		</div>
	</div>	
	<?php echo $this->Form->end(); ?>



