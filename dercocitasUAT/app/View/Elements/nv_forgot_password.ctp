
<div class="overAll">
	<div class="modal">

	<?php echo $this->Html->script('clientes/add'); ?>
	<?php echo $this->Session->flash();?>
	<?php //echo $this->element('actualizar_padre');?>
	
		<div class="contentModal">
			<h1 class="leftAling"><?php echo (__('CAMBIAR CONTRASEÑA',true));?></h1>
			<P>
				Ingrese su Nro. documento y luego selecciones <span>ENVIAR,</span> en breves momentos le enviaremos un mensaje al <span>EMAIL</span> que ingresó al momento de registrarse. 
			</P>
	<?php echo $this->Form->create('Cliente', array('class'=>'formClave'));?>
				<div class="groupFields">
					<div class="field">
						<div>
							<label class="leftAling"><?php echo(__('Nro de Documento', true)) ?></label>
						</div>
						 <?php
							echo $this->Form->input('documento_numero', array('label' => false, 'class' => 'span-4', 'error' => false));
							echo $this->Form->error('documento_numero', array(
								'isUnique' => __('empresaNombreUnico', true),
								'notEmpty' => __('empresaNombreNoVacio', true),
								'maxLength' => __('empresaNombreLongitud', true),
									), array('class' => 'input text required error'));
							?>
					</div>
				</div>
				<div class='bottom-actions'>
					<div class="groupFields centerAling">
						<div class="field">
							<?php echo $this->Form->submit(__('Enviar',true), array('div'=>false, 'class'=>'guinda'));	?>			
							<?php  echo $this->Form->button(__('Cancelar',true), array('type'=>'button','class'=>'botonForm  buttomReset cerrarModal')); ?>
						</div>
					</div>
				</div>
	        <?php echo $this->Form->end();?>
		</div>
	</div>
</div>