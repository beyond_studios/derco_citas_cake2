<form class="normal-table formStyle1">
	<div class="formTable leftAling">
		<div class="groupFields col4">
			<div class="field">
					<?php if(empty($this->data['Cliente']['razonSocial'])){ ?>
					<div>
						<label class="leftAling"><?php echo __('TALLER_ETIQUETA_APELLIDOS_Y_NOMBRES');?>:</label>
					</div>
					<div>
						<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Cliente.nombres');?></label>
					</div>
					<?php } else { ?>
					<div>
						<label class="leftAling"><?php echo __('TALLER_ETIQUETA_RAZON_SOCIAL');?>:</label>
					</div>
					<div>
						<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Cliente.razonSocial');?></label>
					</div>
					<?php } ?>
			</div>

			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_TIPO_DOCUMENTO');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Cliente.documento_tipo')?></label>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_NUMERO_DOCUMENTO');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Cliente.documento_numero')?></label>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TIPO_DE_CLIENTE');?>:</label>
				</div>
				<div>
					<?php if($this->data['Cliente']['str_cliente_tipo'] == 'Corporativo'){?>
						<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Cliente.str_cliente_tipo')?></label>
					<?php }else{?>
						<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Cliente.str_cliente_tipo')?></label>
					<?php }?>
				</div>
			</div>
		</div>
		<div class="groupFields col4">
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_DISTRITO');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Cliente.distrito')?></label>
				</div>
			</div>

			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_TELEFONO');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Cliente.telefono')?></label>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_CELULAR');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Cliente.celular')?></label>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_CORREO');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Cliente.email')?></label>
				</div>
			</div>
		</div>
		<div class="groupFields col4">
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('AGE_MOTIVOSERVICIO_DESCRIPCION');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Agemotivoservicio.description');?></label>
				</div>
			</div>

			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('AGE_TIPOSERVICIO_DESCRIPCION');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Agetiposervicio.description');?></label>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('ORDEN_ESTANDAR_TITULO_AGREGAR_TIPO_MANTENIMIENTO');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Agedetallecita.tipoMantenimiento');?></label>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_OTROS_SERVICIOS');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Agedetallecita.otrosServicios');?></label>
				</div>
			</div>
		</div>
	</div>
	<div class="titleBar subTitleBar" style="text-align: left;">
		<i class="fa fa-wrench" aria-hidden="true"></i><b>DATOS DEL SERVICIO</b>
	</div>
	<div class="formTable leftAling">
		<div class="groupFields col4">
			<div class="field">
				<div>	
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_PROJECT');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Secproject.name');?></label>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling">Detalle</label>
				</div>
				<div>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_FECHA');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo date('d-m-Y H:i', strtotime($this->request->data['Agecitacalendariodia']['initDateTime']));?>
					</label>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('AGE_DETALLE_CITA_REFERENCIA');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Agedetallecita.otsap');?></label>
				</div>
			</div>
		</div>
	</div>
	<div class="titleBar subTitleBar" style="text-align: left;">
		<i class="fa fa-car" aria-hidden="true"></i><b><?php echo __('TALLER_ETIQUETA_DETALLE_VEHICULO');?></b>
	</div>
	<div class="formTable leftAling">
		<div class="groupFields col4">
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_MARCA');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Agedetallecita.marca');?></label>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_MODELO');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Agedetallecita.modelo');?></label>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_PLACA');?>:</label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Xhtml->thisData('Agedetallecita.placa');?></label>
				</div>
			</div>
		</div>
	</div>
</form>
	

<?php
/*
<table id="formularioEdicion" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tbody>
			<tr>
				<td class="etiqueta"><?php echo __('TALLER_ETIQUETA_DIRECCION');?></td>
				<td class="valor"><?php echo $this->Xhtml->thisData('Cliente.direccion');?></td>
			</tr>
			<tr>
				<td class="etiqueta"><?php echo __('AGE_CLIENTES_CIUDAD');?></td>
				<td class="valor"><?php echo $this->Xhtml->thisData('Cliente.ciudad')?></td>
			</tr>
			<?php if(!empty($this->data['Agedetallecita']['reschedulecomment'])): ?>
			<tr>
				<td class="etiqueta"><?php echo __('comentarioReprogramar');?></td>
				<td><textarea class="required" style="height: 50px;padding: 5px;width: 320px;" disabled="disabled"><?php echo trim($this->data['Agedetallecita']['reschedulecomment']); ?></textarea></td>
			</tr>
			<?php endif ?>
			<?php if(!empty($this->data['Agedetallecita']['deletecomment'])): ?>
			<tr>
				<td class="etiqueta"><?php echo __('comentarioEliminar');?></td>
				<td><textarea class="required" style="height: 50px;padding: 5px;width: 320px;" disabled="disabled"><?php echo trim($this->data['Agedetallecita']['deletecomment']); ?></textarea></td>
			</tr>	
			<?php endif ?>
		</tbody>
	</table>
<div class="borde_detalle">
	<table id="formularioEdicion" class="listaPrincipal" border="0" cellspacing="0" cellpadding="0" style="<?php echo $widthTable ?>">
        <caption style="font-weight: bold;"><?php echo __('TALLER_ETIQUETA_DETALLE_CITA_TALLER');?></caption>
        <thead>
	        <tr>
	        	<th style="width: 200px;"><?php echo __('TALLER_ETIQUETA_ORGANIZACION');?></th>
	        	<th></th>
	        	<th style="width: 120px;"></th>
				<th style="width: 120px;"></th>
	        </tr>
        </thead>
        <tbody>
	        <tr class="impar">
	            <td class="texto"><?php echo $organizationName;?></td>
	            <td class="texto"></td>
	            <td class="centrado"></td>
	       		<td class="centrado"></td>
		    </tr>
        </tbody>
	</table>
</div>*/
?>

<?php if ($servicios) { ?>
<div class="borde_detalle">
	<table id="formularioEdicion" class="listaPrincipal" border="0" cellspacing="0" cellpadding="0" style="<?php echo $widthTable ?>">
		<caption style="font-weight: bold;">
			<?php echo __('TALLER_ETIQUETA_DETALLE_SERVICIOS');?>
		</caption>
		<thead>
			<tr>
				<th width="30%"><?php echo __('TALLER_SERVICIO_CAMPO_CODIGO');?></th>
				<th width="50%"><?php echo __('TALLER_SERVICIO_CAMPO_DESCRIPCION');?></th>
				<th width="10%"><?php echo __('TALLER_SERVICIO_CAMPO_DURACION');?></th>
				<th width="10%"><?php echo __('TALLER_SERVICIO_CAMPO_UNIDAD');?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($servicios as $key=>$servicio): ?>
			<?php $class = ($key%2 == 0) ? "par" : "impar"; ?>
            <tr class="<?php echo $class; ?>">
				<td class="texto"><?php echo $servicio['Talservicio']['codigo'];?></td>
				<td class="texto"><?php echo $servicio['Talservicio']['descripcion'];?></td>
				<td class="centrado"><?php echo $servicio['Talservicio']['duracion'];?></td>
				<td class="centrado"><?php echo $servicio['Talservicio']['unidad'];?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>
<?php } ?>