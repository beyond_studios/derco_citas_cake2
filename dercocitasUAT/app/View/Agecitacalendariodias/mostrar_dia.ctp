<table id="tituloVentana" border="0" cellspacing="0" cellpadding="0" align="center">
    <thead>
        <tr>
            <th colspan="2" class="titulo">
                <?php echo $tallerName.' : '.$etiquetaFecha?>
            </th>
        </tr>
    </thead>
</table>

<ul class="acciones">
	<li>
        <?php echo $this->Xhtml->imageTextLink('agregar.png', __('GENERAL_AGREGAR'), 
            'javascript:;', array('onclick'=>"agregar()", 'escape'=>false),
			null, array('width'=>'16')) ?>
    </li>
</ul>

<table id="listaPrincipal" cellpadding="0" cellspacing="0" border="0">
    <thead>
        <tr>
            <th colspan="2"><?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_INTERVALO')?></th>
            <th rowspan="2"><?php echo __('TALLER_CITACALENDARDAY_CAMPO_PROGRAMED')?></th>
            <th rowspan="2"><?php echo __('TALLER_CITACALENDARDAY_CAMPO_AVAILABLE')?></th>
            <th rowspan="2" style="width:90px"><?php echo __('GENERAL_ACCIONES')?></th>
        </tr>
        <tr>    
            <th><?php echo __('TALLER_CITACALENDARDAY_CAMPO_INIT_DATE_TIME')?></th>
            <th><?php echo __('TALLER_CITACALENDARDAY_CAMPO_END_DATE_TIME')?></th>
        </tr>
    </thead>
	<tbody>
		<?php foreach ($horarios as $horario): 
			$initDateTime = date('H:i', strtotime($horario['Agecitacalendariodia']['initDateTime']));
			$endDateTime = date('H:i', strtotime($horario['Agecitacalendariodia']['endDateTime']));
		?>
    	<tr>
            <td class="centrado"><?php echo $initDateTime ?></td>
            <td class="centrado"><?php echo $endDateTime ?></td>
            <td class="centrado"><?php echo $horario['Agecitacalendariodia']['programed']; ?></td>
            <td class="centrado"><?php echo $horario['Agecitacalendariodia']['available']; ?></td>
       		<td class="accion">
            	<?php echo $this->Xhtml->imagelink('eliminar.png', __('GENERAL_ELIMINAR'),
					'/agecitacalendariodias/eliminar/'.$horario['Agecitacalendariodia']['id'], array('escape'=>false), 
					__('GENERAL_REGISTRO_CONFIRMAR_ELIMINADO').' '.$initDateTime.'-'.$endDateTime.' ?',
					array('width'=>'16'))?>
        	</td>
    	</tr>
    	<?php endforeach; ?>
        <tr>
            <td colspan="5" class="accion">
                <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar', 'onclick'=>'javascript:window.close()'));?>
            </td>
        </tr>        
</tbody>
</table>

<script type="text/javascript">
colorearTabla('listaPrincipal');

function agregar() {
    var url = "<?php echo $this->Html->url('/agecitacalendariodias/agregar/'.$anio.'/'.$mes.'/'.$dia.'/'.$talcitacalendar_id)?>";
    var w = window.open(url, 'agregar', 
        'scrollbars=yes,resizable=yes,width=450,height=250,top=100,left=200,status=no,location=no,toolbar=no');
}

</script>
