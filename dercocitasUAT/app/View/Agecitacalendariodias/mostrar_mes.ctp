
<table id="tituloVentana" border="0" cellspacing="0" cellpadding="0" align="center">
    <thead>
        <tr>
            <th colspan="2" class="titulo">
                <?php echo $tallerName.' : '.$calendarioMes['titulo']?>
            </th>
        </tr>
    </thead>
</table>                         

<ul class="acciones">
	<li>
		<?php echo $this->Xhtml->imageTextLink('derco/defecto/actualizar.png', __('GENERAL_ACTUALIZAR'),
			'javascript:;',	array('onclick'=>'actualizarPagina()','escape'=>false),
			null, array('width'=>'16'))?>
    </li>
</ul>

<?php echo $this->element('calendario_mes_barra_navegacion', 
							array('anterior'=>$anterior, 'siguiente'=>$siguiente)) ?>

<?php echo $this->element('calendario_mes', 
							array('calendarioMes'=>$calendarioMes)) ?>
							
<table align="center" class="leyenda">
<tbody>
	<tr>
		<td class="inactivo" style="width:20px">&nbsp;
			
		</td>
		<td>
			<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_DIA_INACTIVO')?>
		</td>
	</tr>
	<tr>
		<td class="inactivo actual" style="width:20px">&nbsp;
			
		</td>
		<td>
			<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_DIA_ACTUAL')?>
		</td>
	</tr>
	<tr>
		<td class="activo" style="width:20px">&nbsp;
			
		</td>
		<td>
			<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_DIA_ACTIVO')?>
		</td>
	</tr>
	<tr>
		<td class="feriado" style="width:20px">&nbsp;
			d
		</td>
		<td>
			<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_DIA_FERIADO')?>
		</td>
	</tr>
</tbody>
</table>



<script type="text/javascript">
	var calendarId = <?php echo $talcitacalendar_id?>;
/**
 *	Requerido para el elemento calendario_barra_navegacion
 */
function mostrarMes(anio, mes) {
	var url = "<?php echo $this->Html->url('/agecitacalendariodias/mostrarMes')?>"+ 
		"/" + anio + 
		"/" + mes + 
		"/" + calendarId;
	window.location.href = url;
	window.event.returnValue = false;// para que funcione tambien en IE6

}

/**
 *	Requerido para el elemento calendario_mes
 */
function mostrarDia(anio, mes, dia) {
	var url = "<?php echo $this->Html->url('/agecitacalendariodias/mostrarDia')?>"+ 
		"/" + anio + 
		"/" + mes + 
		"/" + dia +
		"/" + calendarId;
	var w = window.open(url, 'CalendarioDia', 'scrollbars=yes,resizable=yes,width=720,height=460,top=100,left=200,status=no,location=no,toolbar=no');
}

function actualizarPagina() {
	window.location.reload(true);
}

</script>