<div class="span-8">
	<?php echo $this->Html->script('appcalendariopersonales/mostrar_mes.js',false);?>
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-8" >
		<h4><?php echo /*$personName.*/'En taller de : '.$calendarioMes['titulo'];?></h4>
	</div>
	<hr/>
	<div class="span-8" >
		<?php echo $this->element('calendario_mes_barra_navegacion',array('anterior'=>$anterior, 'siguiente'=>$siguiente));?>
	</div>
	<div class="span-8" >
		<?php echo $this->element('calendario_mes',array('calendarioMes'=>$calendarioMes)); ?>
		<br/>
	</div>
	
	<div class="span-8" >
		<div class="span-2"><?php echo(__('leyenda',true));?> 
		</div>
		<div class="span-4 last">
						<table align="center" class="leyenda">
						<tbody>
							<tr>
								<td class="inactivo" style="width:20px">&nbsp;
									
								</td>
								<td>
									<?php echo(__('diaInactivo',true));?>
								</td>
							</tr>
							<tr>
								<td class="inactivo actual" style="width:20px">&nbsp;
									
								</td>
								<td>
									<?php echo(__('diaActual',true));?>
									
								</td>
							</tr>
							<tr>
								<td class="activo" style="width:20px">&nbsp;
									
								</td>
								<td>
									<?php echo(__('diaActivo',true));?>
									
								</td>
							</tr>
							<tr>
								<td class="feriado" style="width:20px">&nbsp;
									d
								</td>
								<td>
									<?php echo(__('diaFeriado',true));?>
									
								</td>
							</tr>
						</tbody>
						</table>
					
		</div>
	</div>

<?php echo $this->element('actualizar'); ?>
		<br/><br/>
		<div align="center" >
			<?php  echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>
</div>