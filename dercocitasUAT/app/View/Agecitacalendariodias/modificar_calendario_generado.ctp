<?php echo $this->Html->script('agecitacalendariodias/calendar.js');?>
<?php echo $this->Html->css('calendar.css');?>
<div class="span-8" >
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-8" >
		<h3><?php echo (__('Modificar Calendario Generado',true));?></h3>
	</div>
	<hr/>
	
	<?php echo $this->Form->create('Agecitacalendariodia');?>
	<?php echo $this->Form->hidden('Agecitacalendario.id'); ?>
	<?php echo $this->Form->input('id', array('type'=>'hidden')); ?>
			
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo 'A&ntilde;os'; ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >				
				<?php echo $this->Form->select('anio', array('2013'=>'2013', '2014'=>'2014', '2015'=>'2015', '2016'=>'2016','2017'=> '2017'),array('class'=>'span-5'),false);
				?>
			</div>
		</div>
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo 'Modificar Desde'; ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >				
				<?php echo $this->Form->input('desde', array('label' => '', 'class'=>'calendarSelectDate','error'=>false)); ?>
				<div id="calendarDiv"></div>
			</div>
		</div>
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo $this->Form->label(__('Lunes')); ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >				
				<?php echo $this->Form->select('lunes_cast_id',array($appcasts),array('class'=>'span-5'),false);				
				?>	
			</div>
		</div>
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo $this->Form->label(__('Martes')); ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >				
				<?php echo $this->Form->select('martes_cast_id',array($appcasts),array('class'=>'span-5'),false);				
				?>	
			</div>
		</div>
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo $this->Form->label(__('Miercoles')); ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >				
				<?php echo $this->Form->select('miercoles_cast_id',array($appcasts),array('class'=>'span-5'),false);				
				?>	
			</div>
		</div>
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo $this->Form->label(__('Jueves')); ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >				
				<?php echo $this->Form->select('jueves_cast_id',array($appcasts),array('class'=>'span-5'),false);				
				?>	
			</div>
		</div>
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo $this->Form->label(__('Viernes')); ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >				
				<?php echo $this->Form->select('viernes_cast_id',array($appcasts),array('class'=>'span-5'),false);				
				?>	
			</div>
		</div>
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo $this->Form->label(__('Plantilla Sabado')); ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >				
				<?php echo $this->Form->select('sabado_cast_id',array($appcasts),array('class'=>'span-5'),false);				
				?>	
			</div>
		</div>
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo $this->Form->label(__('Plantilla Domingo')); ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-5 last" >				
				<?php echo $this->Form->select('domingo_cast_id',array($appcasts),array('class'=>'span-5'),false);				
				?>	
			</div>
		</div>
		<br/>
		<hr/>
		
		<div class=" span-8 botones" >
			<?php echo $this->Form->submit(__('Submit',true), array('div'=>false));	?>
			<?php echo $this->Form->button(__('Reset',true), array('type'=>'reset')); ?>				
			<?php echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>
	
	<?php echo $this->Form->end(); ?>

<?php echo $this->element('actualizar'); ?>
</div>