<?php echo $this->Html->script('agecitacalendariodias/generar_horarios_form.js'); ?>

<table id="tituloVentana" border="0" cellspacing="0" cellpadding="0" align="center">
    <thead>
        <tr>
            <th colspan="2" class="titulo">
                <?php echo __('TALLER_CITACALENDARDAY_TITULO_GENERAR_HORARIOS') ?>
            </th>
        </tr>
    </thead>
</table>

<form id="principalForm" name="principalForm" action="<?php echo $this->Html->url('/agecitacalendariodias/generarHorariosForm')?>" method="post">
	<?php echo $this->Form->hidden('Agecitacalendario.id'); ?>
	<table align="center" border="0" cellpading="0" cellspacing="0">
		<tbody>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_ANIO_A_GENERAR') ?>    
				</td>
				<td>
					<?php echo $this->data['Agecitacalendario']['anio'];?>
					<?php echo $this->Form->hidden('Agecitacalendario.anio'); ?>
				</td>
			</tr>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_DIA_LUNES') ?>    
				</td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendariodia.lunes_cast_id',$casts,array('class'=>'required'),false); ?></td>
			</tr>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_DIA_MARTES') ?>    
				</td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendariodia.martes_cast_id',$casts,array('class'=>'required'),false); ?></td>
			</tr>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_DIA_MIERCOLES') ?>    
				</td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendariodia.miercoles_cast_id',$casts,array('class'=>'required'),false); ?></td>
			</tr>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_DIA_JUEVES') ?>    
				</td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendariodia.jueves_cast_id',$casts,array('class'=>'required'),false); ?></td>
			</tr>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_DIA_VIERNES') ?>    
				</td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendariodia.viernes_cast_id',$casts,array('class'=>'required'),false); ?></td>
			</tr>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_PLANTILLA_SABADO') ?>    
				</td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendariodia.sabado_cast_id',$casts,array('class'=>'required'),false); ?></td>
			</tr>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_PLANTILLA_DOMINGO') ?>    
				</td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendariodia.domingo_cast_id',$casts,array('class'=>'required'),false); ?></td>
			</tr>
			<tr>
	            <td colspan="2" class="accion">
	                <?php if(!$guardado) echo $this->Form->submit(__('GENERAL_GENERAR'), array('class'=>'guardar', 'div'=>false));?>
	                <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar', 'div'=>false, 'onclick'=>'javascript:window.close()'));?>
	            </td>
			</tr>
		</tbody>	
	</table>
</form>
<?php echo $this->element('actualizar_padre')?>