<div class="span-10">
<?php echo $this->Html->script('layouts/mijquery.js'); ?>
<?php echo $this->Html->script('appcalendariopersonales/mostrar_dia.js'); ?>

		<?php echo $this->Session->flash();?>
		<br/>
			<div id="titulo" class="span-10" >
				<h3><?php echo $etiquetaFecha;?></h3>
			</div>		
			<div id="agregar" class="span-5" ><!--$anio=null, $mes=null, $dia=null, $personId=null-->
			<?php //echo $this->element('agregar',array('url'=>'add'.'/'.$anio.'/'.$mes.'/'.$dia.'/'.$personId)); ?>	
			</div>
		<div>
		</div>

<table cellpadding="0" cellspacing="0" class="table" >
<thead>
	<tr>
        <th colspan="2"><?php echo(__('intervaloCronograma',true));?></th>
        <th rowspan="2" style="width:90px"><?php echo(__('Actions',true));?></th>
    </tr>
    <tr>
    	<th><?php echo(__('horaInicioCronograma',true));?></th>
    	<th><?php echo(__('horaFinCronograma',true));?></th>
    </tr>
</thead>
<tbody>
	<?php foreach ($castTimes as $castTime):
			$initDateTime = date('H:i', strtotime(str_replace("/","-",$castTime['Agecitacalendariodia']['initDateTime'])));
			$endDateTime = date('H:i', strtotime(str_replace("/","-",$castTime['Agecitacalendariodia']['endDateTime']))); ?>
    <tr>
    	<td class="actionsfijo">
        	<?php echo $initDateTime; ?>
        </td>
        <td class="actionsfijo">
        	<?php echo $endDateTime; ?>
        </td>
		<?php //$id,$anio,$mes,$dia,$personId $castTime['Apppersonalcalendar']['id'].'\'.$anio.'\'.$mes.'\'.$dia.'\'.$personId
			$id= $castTime['Agecitacalendariodia']['id'];
			$urlview = '';
			$urledit = '';
			$urldelete = $id.'/'.$anio.'/'.$mes.'/'.$dia/*.'/'.$personId*/;
		?>
		<td class="actionsfijo">
				<?php echo $this->element('actionPersonalizado', 
								array('id'=>$castTime['Agecitacalendariodia']['id'], 
										'name'=> $initDateTime.' - '.$endDateTime,
										'estado'=> $castTime['Agecitacalendariodia']['estado'],
										'urlview'=>$urlview,
										'urledit'=>$urledit,
										'urldelete'=>$urldelete)); ?>
			</td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
		
		<br/>
		<div align="center" >
			<?php  echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>
</div>