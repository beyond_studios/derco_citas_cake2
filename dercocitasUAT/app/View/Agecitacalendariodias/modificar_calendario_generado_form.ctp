<?php echo $this->Html->script('agecitacalendariodias/modificar_calendario_generado_form.js'); ?>

<table id="tituloVentana" border="0" cellspacing="0" cellpadding="0" align="center">
    <thead>
        <tr>
            <th colspan="2" class="titulo">
                <?php echo __('TALCITACALENDARDAYS_ETIQUETA_MODIFICAR_CALENDARIO_GENERADO') ?>
            </th>
        </tr>
    </thead>
</table>
<br/>
<form id="modificarForm" name="modificarForm" action="<?php echo $this->Html->url('/agecitacalendariodias/modificarCalendarioGeneradoForm/').$calendar?>" method="post">
	<?php echo $this->Form->hidden('Agecitacalendario.id',array('value'=>$calendar)); ?>
	<table id="formularioEdicion" align="center" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALCITACALENDARDAYS_ETIQUETA_CALENDARIO_GENERADO') ?>    
				</td>
				<td>
					<?php if(!empty($aniosGenerados)) { ?>
						<?php echo $this->Form->select('Agecitacalendario.anio',$aniosGenerados,array('class'=>'required'),false); ?>
                	<?php } else { ?>
                		<?php echo __('TALCITACALENDARDAYS_ETIQUETA_MODIFICAR_CALENDARIO_NO_EXISTEN_CALENDARIO'); ?>
                	<?php } ?>
				</td>
			</tr>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALCITACALENDARDAYS_ETIQUETA_MODIFICAR_CALENDARIO_DESDE') ?>    
				</td>
				<td>
	                <?php echo $this->Form->input('Fecha.desde', array('class'=>'required', 'label'=>false, 'div'=>false))?>
				</td>
			</tr>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_DIA_LUNES') ?>    
				</td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendariodia.lunes_cast_id',$casts,array('class'=>'required'),false); ?></td>
			</tr>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_DIA_MARTES') ?>    
				</td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendariodia.martes_cast_id',$casts,array('class'=>'required'),false); ?></td>
			</tr>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_DIA_MIERCOLES') ?>    
				</td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendariodia.miercoles_cast_id',$casts,array('class'=>'required'),false); ?></td>
			</tr>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_DIA_JUEVES') ?>    
				</td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendariodia.jueves_cast_id',$casts,array('class'=>'required'),false); ?></td>
			</tr>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_DIA_VIERNES') ?>    
				</td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendariodia.viernes_cast_id',$casts,array('class'=>'required'),false); ?></td>
			</tr>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_PLANTILLA_SABADO') ?>    
				</td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendariodia.sabado_cast_id',$casts,array('class'=>'required'),false); ?></td>
			</tr>
			<tr>
				<td class="etiqueta">
					<?php echo __('TALLER_CITACALENDARDAY_ETIQUETA_PLANTILLA_DOMINGO') ?>    
				</td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendariodia.domingo_cast_id',$casts,array('class'=>'required'),false); ?></td>
			</tr>
			<tr>
	            <td colspan="2" class="accion">
	                <?php echo $this->Form->submit(__('GENERAL_GENERAR'), array('class'=>'guardar', 'div'=>false));?>
	                <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar', 'div'=>false, 'onclick'=>'javascript:window.close()'));?>
	            </td>
			</tr>
		</tbody>
	</table>
</form>
<?php echo $this->element('actualizar_padre'); ?>