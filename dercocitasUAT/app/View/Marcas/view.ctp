<div class="span-9 botones" > 
<?php echo $this->Session->flash();?>
	<table cellpadding="0" cellspacing="0" class="table">
		<thead>
			<tr>
				<th colspan="2" scope="col">
					<?php echo __('Marca');?>
				</th>
			</tr>
		</thead>
		<tbody>

			<tr>
		 		<td><label><?php echo __('Codigo SAP') ?></label> </td>
		 		<td><?php echo $marca['Marca']['codigo']; ?></td>
		 	</tr>

			<tr>
		 		<td><label><?php echo __('descripcion') ?></label> </td>
		 		<td><?php echo $marca['Marca']['description']; ?></td>
		 	</tr>

			<tr>
		 		<td><label><?php echo __('Pagina Web') ?></label> </td>
		 		<td><?php echo $marca['Marca']['portal']; ?></td>
		 	</tr>

			<tr>
		 		<td><label><?php echo __('estado'); ?></label> </td>
		 		<td><?php echo $marca['Marca']['status'] == 'AC' ? 
							__('Enable',true)
						:
							($marca['Marca']['status'] == 'DE'?
								__('Disable',true)
							:
								__('Limited',true))
						; ?></td>
		 	</tr>
		</tbody>
	</table>	
	<br/>
	<?php echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
	<?php echo $this->Form->end();?>
	<?php echo $this->element('actualizar_ver'); ?>
</div>