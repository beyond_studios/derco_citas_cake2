<?php echo $this->Html->script('marcas/index.js'); ?>
		<?php echo $this->Session->flash();  ?>
		<br/>
			<h3 id="tituloTable"><?php echo __('Marcas');?></h3>
		
		<div class="box">
			<div id="agregar" class="span-5" >
			<?php echo $this->element('agregar'); ?>	
			</div>
			
			<div id="buscador" class="">
				<?php echo $this->element('buscador', array('elementos'=>$elementos,'url' => 'index')); ?>
			</div>
		</div>		
		<table cellpadding="0" cellspacing="0" class="table" >
			<thead>
			<tr>
         
				<th><?php echo $this->Paginator->sort('Marca.codigo',__('Codigo SAP',true));?></th>
				<th><?php echo $this->Paginator->sort('Marca.description',__('Marca'));?></th>        
				<th><?php echo $this->Paginator->sort('Marca.portal',__('Pagina Web',true));?></th>    
				<th><?php echo $this->Paginator->sort('Marca.status',__('estado',true));?></th>
				<th class="actionsfijo"><?php  echo __('Actions');?></th>
			
			</tr>			
			</thead>
			
			<tbody>
				<?php  foreach ($marcas as $marca):?>
				<tr>
				<td>
					<?php echo $marca['Marca']['codigo']; ?>
				</td>
				<td>
					<?php //echo $marca['Marca']['description']; ?>
					<?php 		
							$url = $this->Html->url(array('action'=>'view', $marca['Marca']['id']));
							echo $this->Html->link($marca['Marca']['description'], 'javascript:;',array('onclick' => "mostrar('".$url."')",'escape'=>false), null);	
					?>	
				</td>
				<td>
					<?php echo $marca['Marca']['portal']; ?>
				</td>

				<td class="textc">
					<?php echo $marca['Marca']['status'] == 'AC' ? 
									__('Enable',true)
								:
									($marca['Marca']['status'] == 'DE'?
										__('Disable',true)
									:
										__('Limited',true))
								; ?>
				</td>
		
					<td class="actionsfijo">
				<?php echo $this->element('actionDerco', 
								array('id'=>$marca['Marca']['id'], 
										'description'=> $marca['Marca']['description'],
										'portal'=> $marca['Marca']['portal'],
										'estado'=> $marca['Marca']['status'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>

<div id ="paging" class="span-18">
    <?php echo $this->element('paginador'); ?>
</div>
<div class="clear"></div>