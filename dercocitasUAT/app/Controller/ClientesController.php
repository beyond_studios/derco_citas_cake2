<?php
class ClientesController extends AppController {
	public $name='Clientes';
	/**
    public function beforeFilter() {
        parent::beforeFilter();
    }
	**/
	public function indexCallCenter(){
		//BORRAMOS LAS VARIABLES DE SESSION DEL BUSCADOR PRIMER INGRESO
		@$this->setInitSessionConditions();
		//FORMAMOS LA DATA SI SE ESTA ENVIANDO LOS DATOS POR PAGINADOR
		if($this->request->is('get')){
			//RECUPERAMOS LAS CONDICIONES DE BUSQUEDA ALMACENADOS EN SESSION
			$bscCnd = $this->getSessionConditions();
			$this->request->data['bsc']['nombres'] = empty($bscCnd['bsc']['nombres'])?"":$bscCnd['bsc']['nombres'];
			$this->request->data['bsc']['placa'] = empty($bscCnd['bsc']['placa'])?"":$bscCnd['bsc']['placa'];
			$this->request->data['bsc']['std'] = empty($bscCnd['bsc']['std'])?"":$bscCnd['bsc']['std'];
		}
	
		//GUARDAMOS LAS CONDICIONES EN SESSION
		@$this->setSessionConditions($this->request->data);
		$cnd = $this->Cliente->getConditionsBuscador($this->request->data, $this->_getDtLg());

		// enviamos los datos para el buscador
		$std = $this->Cliente->stdClient;
		$this->set('std',$std);
		
		$this->paginate = array(
			'limit' => 10, 
			'page' => 1,
			'order' => array ('Cliente.id' => 'asc'),
			'conditions' => $cnd
		);
		
		$this->set('clientes',$this->paginate('Cliente'));	
	}

	public function actualizarClienteSap($id,$placa = null) {
		$this->layout='modulo_taller'.DS.'default_grid';
		$cliente = $this->Cliente->findById($id);
		$param = array('documento'=>$cliente['Cliente']['documento_tipo'],'numeroDocumento'=>$cliente['Cliente']['documento_numero'],
						'placa'=>(isset($placa))?$placa:$cliente['Cliente']['username']);

		$clienteWebService = $this->Session->read('dataWebService');
		if(!empty($clienteWebService['CLIENTE']) && isset($clienteWebService['CLIENTE'])){
			if($cliente['Cliente']['codigo_sap'] != $clienteWebService['CLIENTE']['IdCliente']){
				$clienteWebService = $this->requestAction('webservicessaps/getConsultaClientePlaca',$param);
				$this->Session->write('dataWebService',$clienteWebService);
			}
		}else{
			$clienteWebService = $this->requestAction('webservicessaps/getConsultaClientePlaca',$param);
			$this->Session->write('dataWebService',$clienteWebService);
		}
		$cliente['Clientesap'] = $clienteWebService['CLIENTE'];
		$cliente['Vehiculo'] = $clienteWebService['VEHICULO'];
		$placas = array();
		foreach($cliente['AgeclientesVehiculo'] as $id => $item){
			$placas[$item['placa']] = $item['placa'];
		}
		//llenamos el tipo de cliente con su desccripcion
		$cliente['Cliente']['str_cliente_tipo'] = $this->Cliente->getStrTipoCliente($cliente['Cliente']['cliente_tipo']);
		$cliente['Clientesap']['StrTipoCliente'] = $this->Cliente->getStrTipoCliente($cliente['Clientesap']['TipoCliente']);
		
		$this->set('placas',$placas);
		$this->request->data = $cliente;
	}
					
	public function updateClienteWebFromSap($id,$placa = null){
		$this->layout = 'ajax';
		$this->autoRender = false;
		Configure::write('debug',0);
		$this->loadModel('AgeclientesVehiculo');
		$responseAjax = array('Success'=>false,'Mensaje'=>__('MENSAJE_NUMERO_TELEFONICO'));
		$clienteWebService = $this->Session->read('dataWebService');
		if(!empty($clienteWebService['Estado']) && isset($clienteWebService['Estado'])){
			if($clienteWebService['Estado'] == 0 || $clienteWebService['Estado'] == 1){
				$cliente = $this->Cliente->convertClienteSapOnClient($clienteWebService);
				$agevehiculocliente = $this->Cliente->convertClienteSapOnVehiculo($clienteWebService);
				$cliente['Cliente']['id'] = $id;
				unset($cliente['Cliente']['username']);
				unset($cliente['Cliente']['password']);
				$this->Cliente->begin();
				if($this->Cliente->save($cliente)){
					$flagagevehiculocliente = $this->AgeclientesVehiculo->find('first',array('conditions'=>array('AgeclientesVehiculo.cliente_id'=>$id,
																					'AgeclientesVehiculo.placa'=>$placa,
																					'AgeclientesVehiculo.estado'=>'AC')));
					if(!empty($flagagevehiculocliente) && isset($flagagevehiculocliente)){
						$agevehiculocliente['id'] = $flagagevehiculocliente['AgeclientesVehiculo']['id'];
						if($this->AgeclientesVehiculo->save($agevehiculocliente)){
							$this->Cliente->commit();
							$responseAjax = array('Success'=>true,'Mensaje'=>__('MENSAJE_DATOS_CLIENTES_ACTUALIZADOS_CORRECTAMENTE'),'data'=>$cliente);
						}else $this->Cliente->rollback();
					}else{
						$this->Cliente->commit();
						$responseAjax = array('Success'=>true,'Mensaje'=>__('MENSAJE_DATOS_CLIENTES_ACTUALIZADOS_CORRECTAMENTE'),'data'=>$cliente);
					}
					$this->Session->delete('dataWebService');
				}else $this->Cliente->rollback();
			}
		}
		echo json_encode($responseAjax);
	}

	public function clienteweb(){
		$this->layout='clientewebindex';          
		$clientes = $this->paginate('Cliente');
//		var_dump($clientes);
		$this->set('clientes', $clientes);
	}
	   
	public function login() {
		$this->layout='inicioDerco02';
		$this->set('bodyClass','backLogin');
		
//		$this->layout=false;
	}

	#Mauricio
	public function getModeloByMarca(){
		$this->layout = 'modulo_taller'.DS.'vacio';
		
		$marca_id = $this->request->data['Cliente']['marca_id'];
		$this->loadModel('Modelo');
		$modelos = $this->Modelo->find('list', array(
			'fields'=>array('Modelo.description'),
			'conditions' => array('Modelo.marca_id' => $marca_id, 'Modelo.status'=>'AC'),
		));
 
		$this->set('modelos',$modelos);
	}
	#Mauricio -end 
	
	public function addWebCliente() {
		$this->layout='inicioDerco02';
		$this->set('bodyClass', 'backRegister');
		# Mauricio 
		$this->loadModel('Marca');
		$this->loadModel('Modelo');
		
		
		$d = $this->Marca->find('list',array('fields'=>array('Marca.description'),'conditions'=>array('Marca.status'=>'AC')));
		$this->set('marcas', $d);
		#Mauricio end
		if(!empty($this->request->data)){
			$data = $this->request->data;

			$param = array('documento'=>$data['Cliente']['modelo_id'],'documento'=>$data['Cliente']['marca_id'],'documento'=>$data['Cliente']['documento_tipo'],'numeroDocumento'=>$data['Cliente']['documento_numero'],
							'placa'=>$data['Cliente']['placa']);
//			var_dump($this->Cliente->existsAcessNew($data['Cliente']['documento_numero'],$data['Cliente']['placa']));
//			die;
			if($this->Cliente->existsAcessNew($data['Cliente']['documento_numero'],$data['Cliente']['placa'])){
				$this->Session->setFlash(__('ClienteYaCuentaConAccesos', true),'flash_success');
				return;
			}
			
			/** ONLY TEST - START*/

			$dataWebService = array();
			//2017-001 se comenta el llamado al web service ahora se cambia de metodo
			//$dataWebService = $this->requestAction('webservicessaps/getConsultaClientePlaca',$param);
			$dataWebService = $this->requestAction('webservicessaps/getConsultaClientePlacaNew',$param);
			
			if(!empty($dataWebService['faultcode']) && isset($dataWebService['faultcode'])){
				$this->Session->setFlash('Error Servidor: '.$dataWebService['Servidor'],'flash_success');
				return;
			}
//			var_dump($dataWebService); 
//			var_dump('temino'); die;

			$response = $this->Cliente->procesar($dataWebService,$data);
			if($response['Estado']=='CLIENTEVEHICULOCONACCESSOS'){
				$this->Session->setFlash(__('ClienteYaCuentaConAccesos', true),'flash_success');
				return;
			}elseif($response['Estado']=='CLIENTEVEHICULOCSINACCESSONEW'){
				//utilizar $dataWebService
				$this->Session->write('Cliente',$response['dataWeb']);
				$this->Session->write('DataWebService',$response['dataWebService']);
				$this->redirect('/Clientes/addclientes');
			}if($response['Estado']=='CLIENTEVEHICULOREGISTRADOONLYSAP'){
				//utilizar $dataWebService
				$this->Session->write('<Cliente></Cliente>',$response['dataWeb']);
				$this->Session->write('DataWebService',$response['dataWebService']);
				$this->redirect('/Clientes/addclientes');
			}elseif($response['Estado']=='CLIENTEREGISTRADOONLYSAP'){
				//utilizar $dataWebService
				$this->Session->write('Cliente',$response['dataWeb']);
				$this->Session->write('DataWebService',$response['dataWebService']);
				$this->redirect('/Clientes/addclientesdato');
			}elseif($response['Estado']=='CLIENTENOEXISTE'){
				$this->Session->write('Cliente',$response['dataWeb']);
				$this->redirect('/Clientes/addclientesdatos');
			}
		}
	}

	public function addclientes() {
		$this->layout = 'contenido';
		$this->set('guardadoExistoso',false);
		if(!empty($this->request->data)){
			$dataWebService = $this->Session->read('DataWebService');
			$cliente = $this->Cliente->selectDataClient($this->request->data,$dataWebService,0);
			$this->request->data = $cliente;
			$dataSource = $this->Cliente->getDataSource();
			$dataSource->begin();
			$this->Cliente->create();
			$respons = $this->Cliente->saveCliente($cliente);
			if($respons['Estado']=='Success'){
				$dataSource->commit();
				$this->set('guardadoExistoso',true);
				$this->Session->setFlash($respons['Msg'],'flash_success');
				$this->Session->delete('DataWebService');
				$this->Session->delete('Cliente');
				//$this->Session->write('actualizarPadre', true);
			} else {
				$dataSource->rollback();
				$this->Session->setFlash($respons['Msg'],'flash_failure');
			}
		}else{
			$data = $this->Session->read('Cliente');
			$data['Cliente']['placa'] = $data['Cliente']['username'];
			$this->request->data =$data;
		}
	} 

	public function addclientesdato() {
		$this->layout = 'contenido';
		$this->loadModel('Marca');
		$this->loadModel('Modelo');
		$this->set('guardadoExistoso',false);
		
		if(!empty($this->request->data)){
			$dataWebService = $this->Session->read('DataWebService');
			
			if(!empty($this->request->data['Cliente']['marca_id'])){
				$marca = $this->Marca->find('first',array('fields'=>array('Marca.description'),'conditions'=>array('Marca.status'=>'AC', 'Marca.id'=>$this->request->data['Cliente']['marca_id']), 'recursive'=>-1));
				if(!empty($marca['Marca']['description'])){
					$this->request->data['Cliente']['marca'] = $marca['Marca']['description'];	
				}
			}else{
				$this->request->data['Cliente']['marca'] = "";
			}
			
			$cliente = $this->Cliente->selectDataClient($this->request->data,$dataWebService,1);
			if(!empty($this->request->data['Cliente']['modelo_id'])){
				$modelId_tmp = $this->request->data['Cliente']['modelo_id'];
				$this->request->data = $cliente;
				$this->request->data['Cliente']['modelo_id'] = $modelId_tmp;
				$cliente['Cliente']['modelo_id'] = $modelId_tmp;
			}else{
				$this->request->data = $cliente;
			}
			
			$dataSource = $this->Cliente->getDataSource();
			$dataSource->begin();
			$this->Cliente->create();
			$respons = $this->Cliente->saveCliente($cliente);
			if($respons['Estado']=='Success'){
				/** ENVIAMOS EL MENSAJE DE BIENVENIDA **/
				$emails = trim($cliente['Cliente']['email']);  
				if($this->sendMailClient($emails,$cliente['Cliente']['nombres'], $cliente['Cliente']['documento_numero'], $respons['password_new'])){
					$dataSource->commit();
					$this->set('guardadoExistoso',true);
					$this->Session->setFlash("<b>SE LE ENVIO UN MENSAJE AL EMAIL:<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$emails<br/><br/><i>El mensaje  puede demorar unos minutos en ser enviado, en caso de presentar inconvenientes por favor comunicarse con el +511-7135050 para validar la creación de su usuario telef&oacute;nicamente.</i>",'flash_success');
					$this->Session->delete('DataWebService');
					$this->Session->delete('Cliente');
				}else{
					$this->Cliente->rollback();
					$this->Session->setFlash("EL CORREO NO PUDO SER ENVIADO, POR FAVOR COMUNICARSE CON EL CALL CENTER DERCO EN EL TELEFONO 7135050",'flash_failure');
				}
			} else {
				$dataSource->rollback();
				$this->Session->setFlash($respons['Msg'],'flash_failure');
			}
		}else{
			$data = $this->Session->read('Cliente');
			$data['Cliente']['placa'] = $data['Cliente']['placa'];
			$this->request->data =$data; 
		}
		
		/** cargamos los datos de model0 **/
		if(!empty($this->request->data['Cliente']['marca'])){
			$marca = $this->Marca->find('first',array('fields'=>array('Marca.id'),'conditions'=>array('Marca.status'=>'AC', 'Marca.description'=>sprintf("%s", trim($this->request->data['Cliente']['marca']))), 'recursive'=>-1));
			if(!empty($marca)){
				$this->request->data['Cliente']['marca_id'] = $marca['Marca']['id'];	
			}	
		}
		
		if(!empty($this->request->data['Cliente']['marca_id'])){
			$modelos = $this->Modelo->find('list',array('fields'=>array('Modelo.id','Modelo.description'),'conditions'=>array('Modelo.status'=>'AC', 'Modelo.marca_id'=>$this->request->data['Cliente']['marca_id']),'recursive'=>-1));	
		}
		
		$marcas = $this->Marca->find('list',array('fields'=>array('Marca.id','Marca.description'),'conditions'=>array('Marca.status'=>'AC'),'recursive'=>-1));
		$this->set('marcas',$marcas);
		$this->set('modelos',empty($modelos)?array():$modelos);
	}

	public function addclientesdatos() {
		$this->layout = 'inicioDerco02';
		$this->loadModel('Marca');
		$this->loadModel('Modelo');
		$this->set('is_register', false);
		$this->set('marcas',$this->Marca->getAllMarcasForRegistroClientes());
		$this->set('guardadoExistoso',false);
		if(!empty($this->request->data)){
			$dataWebService = $this->Session->read('DataWebService');
			
			if(!empty($this->request->data['Cliente']['marca_id'])){
				$marca = $this->Marca->find('first',array('fields'=>array('Marca.description'),'conditions'=>array('Marca.status'=>'AC', 'Marca.id'=>$this->request->data['Cliente']['marca_id']), 'recursive'=>-1));
				if(!empty($marca['Marca']['description'])){
					$this->request->data['Cliente']['marca'] = $marca['Marca']['description'];	
				}
			}else{
				$this->request->data['Cliente']['marca'] = "";
			}
			
			$cliente = $this->Cliente->selectDataClient($this->request->data,$dataWebService,2);
			if(!empty($this->request->data['Cliente']['modelo_id'])){
				$modelId_tmp = $this->request->data['Cliente']['modelo_id'];
				$this->request->data = $cliente;
				$this->request->data['Cliente']['modelo_id'] = $modelId_tmp;
				$cliente['Cliente']['modelo_id'] = $modelId_tmp;
			}else{
				$this->request->data = $cliente;
			}
			
			$dataSource = $this->Cliente->getDataSource();
			$dataSource->begin();
			$this->Cliente->create();
			$respons = $this->Cliente->saveCliente($cliente);
			if($respons['Estado']=='Success'){
				/** ENVIAMOS EL MENSAJE DE BIENVENIDA **/
				$emails = trim($cliente['Cliente']['email']);  
				if($this->sendMailClient($emails,$cliente['Cliente']['nombres'], $cliente['Cliente']['documento_numero'], $respons['password_new'])){
					$dataSource->commit();
					$this->set('guardadoExistoso',true);
					$this->request->data = $cliente;
					$this->Session->setFlash("<b>TUS DATOS HAN SIDO REGISTRADOS CORRECTAMENTE. SE LE ENVIO UN MENSAJE AL EMAIL:</b><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$emails<br/><br/><i>El mensaje  puede demorar unos minutos en ser enviado, en caso de presentar inconvenientes por favor comunicarse con el +511-7135050 para validar la creación de su usuario telef&oacute;nicamente.</i>",'flash_success');
					$this->Session->delete('DataWebService');
					$this->Session->delete('Cliente');
					$this->set('is_register', true);
				}else{
					$this->Cliente->rollback();
					$this->Session->setFlash("EL CORREO NO PUDO SER ENVIADO, POR FAVOR COMUNICARSE CON EL CALL CENTER DERCO EN EL TELEFONO 7135050",'flash_failure');
				}
			} else {
				$dataSource->rollback();
				$this->Session->setFlash($respons['Msg'],'flash_failure');
			}
		}else{
			$data = $this->Session->read('Cliente');
			$this->request->data = $data;
//			var_dump($this->data); 
		}
		
		/** cargamos los datos de model0 **/
		if(!empty($this->request->data['Cliente']['marca'])){
			$marca = $this->Marca->find('first',array('fields'=>array('Marca.id'),'conditions'=>array('Marca.status'=>'AC', 'Marca.description'=>sprintf("%s", trim($this->request->data['Cliente']['marca']))), 'recursive'=>-1));
			if(!empty($marca)){
				$this->request->data['Cliente']['marca_id'] = $marca['Marca']['id'];	
			}	
		}
		
		if(!empty($this->request->data['Cliente']['marca_id'])){
			$modelos = $this->Modelo->find('list',array('fields'=>array('Modelo.id','Modelo.description'),'conditions'=>array('Modelo.status'=>'AC', 'Modelo.marca_id'=>$this->request->data['Cliente']['marca_id']),'recursive'=>-1));	
		}
		
		$marcas = $this->Marca->find('list',array('fields'=>array('Marca.id','Marca.description'),'conditions'=>array('Marca.status'=>'AC'),'recursive'=>-1));
		$this->set('marcas',$marcas);
		$this->set('modelos',empty($modelos)?array():$modelos);
	}
	
	/**AUTHOR: VENTURA RUEDA JOSE ANTONIO
	 * AGREGA UN NUEVO CLIENTE
	 * @return 
	 */
    public function clienteadd() {
    	$this->layout = 'contenido';
		$this->set('guardadoExistoso',false);
		if(!empty($this->request->data)){
			$this->request->data['Cliente']['documento_tipo'] = 'DN';  /** SIEMPRE SERA DNI **/
			$data = $this->request->data;
			
			/** VALIDAMOS SI NUESTRO CLIENTE SE ENCUENTRA REGISTRADO EN MYSQL 
			 *  FORMA ANTIGUA 
			 *  	placa => USUARIO
			 *  	password = documento_numero => CONTRASEÑA
			 *  FORMA NUEVA
			 *  	documento_numero = USUARIO
			 *  	password_new = CONTRASEÑA (AUTOGENERADO POR EL SISTEMA)
			 */
			$client_db = $this->Cliente->find('first', array(
				'conditions'=>array(
					'documento_numero'=>$data['Cliente']['documento_numero'],
					'estado'=>'AC'
				),
				'recursive'=>'-1'
			));
			
			if(!empty($client_db)){
				$this->Session->setFlash(__('ClienteYaCuentaConAccesos', true),'flash_success');
				return;
			}
			
			/** GUARDAMOS NUESTRO CLIENTE EN EL SISTEMA WEB MYSQL **/
			$this->Cliente->begin();	
			$rpt = $this->Cliente->clienteAdd($this->data);
			if($rpt['0']){
				if(!empty($rpt['password'])){
					/** ENVIAMOS EL MENSAJE DE BIENVENIDA **/
					$emails = $data['Cliente']['email'];  
					if($this->sendMailClient($emails,$data['Cliente']['nombres'], $data['Cliente']['documento_numero'], $rpt['password'])){
						$this->Cliente->commit();
						$this->set('guardadoExistoso', true);
						$this->Session->write('actualizarPadre', true);
						return;
					}else{
						$this->Cliente->rollback();
						$this->Session->setFlash("EL CORREO NO PUDO SER ENVIADO, POR FAVOR COMUNICARSE CON EL CALL CENTER DERCO EN EL TELEFONO 7135050",'flash_failure');
					}
				}else{
					$this->Cliente->commit();
					$this->set('guardadoExistoso', true);
					$this->Session->write('actualizarPadre', true);
					return;
				}
			} else {
				$this->Cliente->rollback();
				$this->Session->setFlash($rpt['1'],'flash_failure');
				return;
			}
			
			/** CONSULTAMOS SI EL CLIENTE SE ENCUENTRA REGISTRADO EN SAP **/	
			$dataWebService = array();
			$param = array(
				'documento'=>$data['Cliente']['documento_tipo'],
				'numeroDocumento'=>$data['Cliente']['documento_numero'],
				'placa'=>""
			);
			
			$dataWebService = $this->requestAction('webservicessaps/getConsultaClientePlaca',$param);
			if(!empty($dataWebService['faultcode']) && isset($dataWebService['faultcode'])){
				$this->Session->setFlash('Error Servidor: '.$dataWebService['Servidor'],'flash_success');
				return;
			}
			
			$response = $this->Cliente->procesar($dataWebService,$data);
			if($response['Estado']=='CLIENTENOEXISTE'){
				$this->Session->setFlash("CLIENTE GUARDADO, PERO NO SE ENCUENTRA REGISTRADO EN SAP", 'flash_success');
				return;
			}
			
			$this->Session->setFlash("CLIENTE GUARDADO SATISFACTORIAMENTE SE ENVIO SU CONTRASEÑA A SU EMAIL",'flash_success');
		}
	}
	
    public function edit($id=true)
	{
		if(!$id && empty($this->request->data))
		{
			$this->Session->setflash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
			//$this->redirect(array('action'=>'index'));
		}
		
		$estadoActivo = 'AC';
		$estadoDesactivo = 'DE';

		if (empty($this->request->data['Cliente'])) 
		{
		     $this->request->data = $this->Cliente->read(null, $id);
		} 
		else 
		{
			      $id = $this->request->data['Cliente']['id'];
				  $nuevoEstado = $this->request->data['Cliente']['estado'];
				    	
			    if ($this->Cliente->save($this->request->data['Cliente'])) 
			    {			
					$this->Session->setFlash(__('GENERAL_REGISTRO_MODIFICADO', true),'flash_success');	
					$this->Session->write('actualizarPadre', true);	
				    $this->Session->write($this->redirect(array('action'=>'edit',$this->request->data['Cliente']['id'])));
				    //$this->Session->write($this->redirect(array('action'=>'view',$this->Agegrupo->getInsertID())));					
			    } 
			    else 
			    {
					$this->Session->setFlash(__('GENERAL_REGISTRO_NO_FUE_ACTUALIZADO', true),'flash_failure');
	            }
		
	     }

       $ageclientesvehiculos = $this->Cliente->AgeclientesVehiculo->find('all',array('fields'=>'AgeclientesVehiculo.placa'));
	   $this->set('ageclientesvehiculos',$ageclientesvehiculos);

	}


/* ################################## JAVASCRIPT  ############################################ */	
	/** RECUPERA TODOS LOS ACTORES JQUERY GRID
	 * @AUTOR: CHIUYARI VERAMENDI, MIGUEL ANGEL
	 */
	function getClientes(){
		configure::write('debug',0);
		app::import('Vendor','json');
		$json = new Services_JSON;
		
		$this->layout = 'ajax';
		
		$filters =  null;
		if($this->params['url']['_search'] == 'true'){
			$filters = $json->decode($this->params['url']['filters']);
			$filters = $this->Cliente->TransfObjectToArray($filters);
		}
		
		$cndGrid = $this->Cliente->conditionsBuscadorGrid($this->params['url'], $filters, $this->_getDtLg());
		
		$this->paginate = array(
			'page'=>$this->params['url']['page'],
			'limit'=>$this->params['url']['rows'],
			'order'=>array($this->params['url']['sidx']=>$this->params['url']['sord']),
			'conditions'=>array('1'=>$cndGrid)
		);
		
		$this->set('clientes',$this->paginate());
	}
	
	/** GUARDA UN ACTOR JQUERY GRID
	 * @AUTOR: CHIUYARI VERAMENDI, MIGUEL ANGEL
	 */
	function setCliente(){
		configure::write('debug',0);
		$this->layout = false;
		
		App::import('Vendor','json');
		$json = new Services_JSON;
		
		$this->Cliente->begin();
		$client['Cliente'] = $this->params['form'];
		$rpt = $this->Cliente->setClient($client);
		
		if(empty($rpt['0'])) $this->Cliente->rollback();
		else $this->Cliente->commit();
		
		$responce->susses = $rpt['0'];
		$responce->errors = $rpt['0']?'':$rpt['errors'];
		echo $json->encode($responce);
		$this->autoRender = false;
	}
	
	
	/**MIGRADO POR: VENTURA RUEDA,JOSE ANTONIO
	 * FECHA: 2013-03-18
     * Recibe los parámetros de login los valida contra la base de datos.
     * Si la validación es correcta, se crea la sesión del usuario(cliente).
     */
    function loginCliente() {
	Configure::write('debug',2);
        $estadoCliente = 'CL';
		if($this->request->is('post')){			
			$rpt = $this->Cliente->setLogin($this->request->data);
			if($rpt[0]){
				unset($rpt['cliente']['Cliente']['password_real']);
				unset($rpt['cliente']['Cliente']['password_new']);
				unset($rpt['cliente']['Cliente']['password_url']);
				
				 // se guardan los datos del cliente en la sesion
				$this->Session->write('loginCliente', $rpt['cliente']);
				$this->redirect('/clientes/tallerIndex');
			}
			else{
				$this->Session->setFlash('<span class="error">El usuario o la contraseña no es válida, por favor verifique sus datos</span>');
				$this->redirect('/clientes/login');
			}
        }
    }
	
	/**MIGRADO POR: VENTURA RUEDA,JOSE ANTONIO
	 * FECHA: 2013-03-18
	 * Permite listar los mensajes enviados al cliente.
	 * @param $dermarca_id Object[optional] usado para la  cabecera_web
	 */
	function tallerIndex() {
        $this->pageTitle = __('TALLER_MIS_COMUNICACIONES');
		$this->layout = 'modulo_taller'.DS.'web_cliente';
		$this->set('bodyClass','backTalleres');
		$this->loadModel('Secconfiguration');
		$this->loadModel('AgeclientesVehiculo');
		$this->loadModel('Agedetallecita');
		
		//obligatorio para todas las vistas de la web del cliente
		$clientSession = $this->verifySessionClient();
		$this->request->data['ClienteLogin'] = $clientSession['Cliente'];
		
		// recuperamos la lista de vehiculos del cliente
		$vehiculos = $this->AgeclientesVehiculo->getVehileClient($clientSession['Cliente']['id']);
//		debug($vehiculos);
		$this->set('vehiculos', $vehiculos);
		$this->set('client_id', $clientSession['Cliente']['id']);
		$this->set('tallerIndex',true);
		if($this->request->is('post')){
			$this->paginate = array(
		        'AgeclientesVehiculo'=>array(
			        'conditions' => array(
						'AgeclientesVehiculo.placa LIKE' => '%'.trim($this->request->data['Buscador']['placa']).'%',
						'AgeclientesVehiculo.cliente_id'=>$clientSession['Cliente']['id'],
						'AgeclientesVehiculo.estado'=>'AC'
					),
					'order'=>array('AgeclientesVehiculo.place'=>'ASC'),
			        'limit' => 30
			    )
		    );
			
			$agevehiculos = $this->paginate('AgeclientesVehiculo');
			foreach($agevehiculos AS $key => $value){
				$nroCita = $this->AgeclientesVehiculo->getVehileClient($clientSession['Cliente']['id'], "AgeclientesVehiculo.id = '".$value['AgeclientesVehiculo']['id']."'");
				$agevehiculos[$key]['AgeclientesVehiculo']['nro_citas'] = $nroCita[0][0]['nro_citas'];
			}
			$this->set('agevehiculos', $agevehiculos);
		}
		
		if($this->request->is('get')){
			$this->paginate = array(
		        'AgeclientesVehiculo'=>array(
			        'conditions' => array("1=2"),
					'order'=>array('AgeclientesVehiculo.place'=>'ASC'),
			        'limit' => 30
			    )
		    );
			$this->set('agevehiculos', array());
		}
		
	}
	
	/**MIGRADO POR: VENTURA RUEDA,JOSE ANTONIO
	 * FECHA: 2013-03-18
	 * Envia los datos del ccliente a la vista
	 * @param $dermarca_id Object[optional] usado para la  cabecera_web
	 */
	function cargarDatosClienteWeb($clientSession) {
		$personaNatural = 'DN';
		//obteniendo datos segun tipo de persona
		if(!empty($clientSession['Cliente'])){
			if($clientSession['Cliente']['documento_tipo'] == $personaNatural) {
				$clientSession['Cliente']['personaNatural'] = true;
			} else {
				$clientSession['Cliente']['personaNatural'] = false;
			}
			 $nombreMostrar = (!empty($clientSession['Cliente']['nombres']))?$clientSession['Cliente']['nombres']:'';
			 $nombreMostrar = $nombreMostrar.' '.((!empty($clientSession['Cliente']['apellidoPaterno']))?$clientSession['Cliente']['apellidoPaterno']:'');
			 $nombreMostrar = $nombreMostrar.' '.((!empty($clientSession['Cliente']['apellidoMaterno']))?$clientSession['Cliente']['apellidoMaterno']:'');
			 $clientSession['Cliente']['nombreMostrar'] = $nombreMostrar;
		}

		return $clientSession;
	}
	
	/**
	 * @author: Ronald Tineo
	 * @return datos de session si aun es valida 
	 */
	function verifySessionClient(){
		//validando si existe algun cliente logueado
		$clientSession = $this->Session->read('loginCliente');
		if(empty($clientSession)) {
			$this->Session->write('msg_web', __('WEBSOLICITUD_TIEMPO_SESION_EXPIRADO'));
			$this->redirect('/clientes/login');
			return;
		}
		//obtenemos los datos del cliente web
		$clientSession = $this->cargarDatosClienteWeb($clientSession);
		return 	$clientSession;
	}
	
	/**
	 * @author: Ronald Tineo
	 * @return datos de session si aun es valida 
	 */
	function verifySessionClientAjax(){
		$this->layout = 'ajax';
		$this->autoRender = false;
		$responseAjax = array('Estado'=>true);
		//validando si existe algun cliente logueado
		$clientSession = $this->Session->read('loginCliente');
		if(empty($clientSession)) {
			$responseAjax = array('Estado'=>false);
		}
		echo json_encode($responseAjax);
	}

	/**
	 * @author: Ronald Tineo
	 * @return elimina las datos de sessio del cliente 
	 */
	function logoutCliente(){
		$this->Session->delete('loginCliente');
		$this->redirect('/clientes/login');
	}
	
	public function forgot_password() {
		$this->layout='inicioDerco02';
					
		if(!empty($this->request->data)){
			$this->request->data['Cliente']['documento_tipo'] = 'DN';  /** SIEMPRE SERA DNI **/
			$data = $this->request->data;
			
			/** VALIDAMOS SI NUESTRO CLIENTE SE ENCUENTRA REGISTRADO EN MYSQL 
			 *  FORMA ANTIGUA 
			 *  	placa => USUARIO
			 *  	password = documento_numero => CONTRASEÑA
			 *  FORMA NUEVA
			 *  	documento_numero = USUARIO
			 *  	password_new = CONTRASEÑA (AUTOGENERADO POR EL SISTEMA)
			 *  	password_new = password temporal para reiniciar contraseña
			 */
			$client_db = $this->Cliente->find('first', array(
				'conditions'=>array(
					'documento_numero'=>$data['Cliente']['documento_numero'],
					'estado'=>'AC'
				),
				'recursive'=>'-1'
			));
			
			if(empty($client_db)){
				$this->Session->setFlash('<span class="error">VERIFIQUE SU NÚMERO DE DOCUMENTO</span>','flash_failure');
				return;
			}
			
			$data['Cliente']['id'] = $client_db['Cliente']['id'];
			
			/** creamos una contrasea temporal (password_url) **/
			$data['Cliente']['password_url'] = md5($this->Cliente->makePassword());
//			debug($data);
			
			$this->Cliente->begin();	
			if($this->Cliente->save($data)){
				/** ENVIAMOS EL MENSAJE DE BIENVENIDA **/
				$emails = $client_db['Cliente']['email']; // = 'jose.antonio.ventura.rueda@gmail.com';
				$password_url = Router::url(array('controller'=>'clientes', 'action'=>'password_new', 'd'=>$data['Cliente']['password_url']), true);
			  
				if($this->sendMailClientForgotPassword($emails,$client_db['Cliente']['nombres'], $client_db['Cliente']['documento_numero'], $password_url)){
					$this->Cliente->commit();
					$this->set('guardadoExistoso', true);
					$this->Session->setFlash("SE LE ENVIO UN MENSAJE AL EMAIL:<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$emails<br/><br/><i>El mensaje puede demorar unos minutos en ser enviado, en caso de presentar inconvenientes por favor comunicarse con el +511-7135050 para agendar su cita telefónicamente</i>",'flash_success');
					return;
				}else{
					$this->Cliente->rollback();
					$this->Session->setFlash("EL CORREO NO PUDO SER ENVIADO, POR FAVOR COMUNICARSE CON EL CALL CENTER DERCO EN EL TELEFONO 7135050",'flash_failure');
				}
				
			} else {
				$this->Cliente->rollback();
				$this->Session->setFlash("ERROR AL REINICIAR SU CONTRASEÑA, POR FAVOR INTENTE NUEVAMENTE",'flash_failure');
				return;
			}
		}
	}
	
	public function password_new() {
		$d = $this->request->params['named']['d'];
		$this->layout='contenido';
		if(!empty($d)){
			/** VALIDAMOS SI NUESTRO CLIENTE SE ENCUENTRA REGISTRADO EN MYSQL  */
			$client_db = $this->Cliente->find('first', array(
				'conditions'=>array(
					'password_url'=>$d,
					'estado'=>'AC'
				),
				'recursive'=>'-1'
			));
			
			if(empty($client_db)){
				$this->Session->setFlash(__('EL URL YA FUE UTILIZADA', true),'flash_failure');
				return;
			}
			
			$data['Cliente']['id'] = $client_db['Cliente']['id'];
			$password_new = $this->Cliente->makePassword();
			$data['Cliente']['password_new'] = md5($password_new);
			$data['Cliente']['password_url'] = NULL;
			
			$this->Cliente->begin();	
			if($this->Cliente->save($data)){
				/** ENVIAMOS EL MENSAJE DE BIENVENIDA **/
				$emails = $client_db['Cliente']['email'];  
				if($this->sendMailClient($emails,$client_db['Cliente']['nombres'], $client_db['Cliente']['documento_numero'], $password_new)){
					$this->Cliente->commit();
					$this->Session->setFlash("SU NUEVA CONTRASENA FUE ENVIADA AL EMAIL:<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$emails<br/><br/><i>El mensaje puede demorar unos minutos en ser enviado, en caso de presentar inconvenientes por favor comunicarse con el +511-7135050 para asistirlo telefónicamente</i>",'flash_success');
					return;
				}else{
					$this->Cliente->rollback();
					$this->Session->setFlash("EL CORREO NO PUDO SER ENVIADO, POR FAVOR COMUNICARSE CON EL CALL CENTER DERCO EN EL TELEFONO 7135050",'flash_failure');
				}
			} else {
				$this->Cliente->rollback();
				$this->Session->setFlash("ERROR AL GENERAR SU NUEVA CONTRASEÑA",'flash_failure');
				return;
			
			}
		}
	}
	/**
	 * @author: Ronald Tineo
	 * @return: historial citas de la web del cliente
	 */
	function historialCitas(){
		$this->pageTitle = __('TALLER_HISTORIAL_CITAS_TALLER');
		$this->layout = 'modulo_taller'.DS.'web_cliente';
		$this->set('historialCitas',true);
		
		$this->loadModel('Agedetallecita');
		
		//obligatorio para todas las vistas de la web del cliente
		$clientSession = $this->verifySessionClient();
		$this->request->data['ClienteLogin'] = $clientSession['Cliente'];
		
		// cargando datos de la marca y modelo
		//$this->cargarDatosMarcaModelo($dermarca_id);
		
		// se obtiene el historial de citas

		$citas = $this->Agedetallecita->getDetalleCitasPorCliente($clientSession['Cliente']['id']);
		if(empty($citas)) {
			$this->Session->write('msg_web', __('BANTRES_NO_EXISTEN_CITAS_TALLER_REGISTRADAS'));
		}
		
		$this->set('citas', $citas);
		$this->set('cita_id', isset($cita_id) && !empty($cita_id) ? $cita_id : 0);
	}
	
	/**
	 * @author: Ronald Tineo
	 * @return: lista los datos a actualizar
	 */
	function actualizarVehiculo($id){
		$this->pageTitle = __('ACTUALIZAR_VEHICULO');
		$this->layout='modulo_taller'.DS.'default_grid';
		$this->loadModel('Marca');
		$this->loadModel('Modelo');
		$this->loadModel('AgeclientesVehiculo');
		
		$cliente = $this->Cliente->find('first',array('conditions'=>array('Cliente.id'=>$id),'recursive'=>0));
		$placas = $this->AgeclientesVehiculo->find('list', array('fields'=>array('id', 'placa'), 'conditions'=>array('cliente_id'=>$id,'estado'=>'AC'), 'recursive'=>-1));
		$marcas = $this->Marca->find('list',array('fields'=>array('Marca.id','Marca.description'),'conditions'=>array('Marca.status'=>'AC'),'recursive'=>-1));
		$this->request->data = $cliente;
					
		$clienteVehicle = $this->AgeclientesVehiculo->find('first', array(
			'conditions'=>array('estado'=>'AC', 'cliente_id'=>$id), 'order'=>array('AgeclientesVehiculo.id'=>'ASC'), 'recursive'=>-1
		));
		if(!empty($clienteVehicle)){
			$this->request->data['Cliente']['placa'] = $clienteVehicle['AgeclientesVehiculo']['id'];
			$marca = $clienteVehicle['AgeclientesVehiculo']['marca'];
			$this->request->data['Cliente']['modelo_id'] = $clienteVehicle['AgeclientesVehiculo']['modelo_id'];
			
			/** Si no es entero nos envia la descripcion de la marca */
			$this->loadModel('Marca');
			$marca = $this->Marca->find('first',array('fields'=>array('Marca.id'),'conditions'=>array('Marca.status'=>'AC', 'Marca.description'=>sprintf("%s", $marca)), 'recursive'=>-1));
			$marcaId = empty($marca)?0:$marca['Marca']['id'];
			
			$this->request->data['Cliente']['marca'] = $marcaId;
			$modelos = $this->Modelo->find('list',array('fields'=>array('Modelo.id','Modelo.description'),'conditions'=>array('Modelo.status'=>'AC', 'Modelo.marca_id'=>$marcaId),'recursive'=>-1));
		}
			
		$this->set('placas', $placas);
		$this->set('marcas', $marcas);
		$this->set('modelos', empty($modelos)?array():$modelos);
	}

	/**
	 * @author: Ronald Tineo
	 * @return: Actualizar el vehiculo por medio de ajax
	 */
	function actualizarVehiculoJson($idageclientevehiculo,$marca, $modeloId = null, $allIds = 0){
		configure::write('debug',1);
		$this->layout = false;
		$this->loadModel('AgeclientesVehiculo');
		$this->loadModel('Marca');
		$responce = array('Success'=>false,'Mensaje'=>__('MENSAJE_NUMERO_TELEFONICO',true));
		
		
		if(!empty($allIds)){
			$marca = $this->Marca->find('first',array('conditions'=>array('id'=>$marca)));
			$marca = $marca['Marca']['description'];
		}
		
		$this->AgeclientesVehiculo->begin();
		$ageclientevehiculo['AgeclientesVehiculo']['id'] = $idageclientevehiculo;
		$ageclientevehiculo['AgeclientesVehiculo']['marca'] = $marca;
		$ageclientevehiculo['AgeclientesVehiculo']['modelo_id'] = $modeloId;
		if($this->AgeclientesVehiculo->save($ageclientevehiculo)){
			$this->AgeclientesVehiculo->commit();
			$responce = array('Success'=>true,'Mensaje'=>'Actualizado Correctamente');
		}else{
			$this->AgeclientesVehiculo->rollback();
		}

		echo json_encode($responce);
		$this->autoRender = false;
	}
	
	/**
	 * @author: Ronald Tineo
	 * @return: Bandeja para reprogramar y Eliminar las citas
	 */
	function bandejaReprogramar(){
		$this->pageTitle = __('BANDEJA_REPROGRAMAR');
		$this->layout = 'modulo_taller'.DS.'web_cliente';
		$this->set('bandejaReprogramar',true);
		
		$this->loadModel('Agedetallecita');
		
		//obligatorio para todas las vistas de la web del cliente
		$clientSession = $this->verifySessionClient();
		$this->request->data['ClienteLogin'] = $clientSession['Cliente'];
		
		$citas = $this->Agedetallecita->getDetalleCitasReprogramarPorCliente($clientSession['Cliente']['id']);
//		debug($citas); die;
		if(empty($citas)) {
			$this->Session->write('msg_web', __('BANTRES_NO_EXISTEN_CITAS_TALLER_REGISTRADAS'));
		}
		
		$this->set('citas', $citas);
		$this->set('cita_id', isset($cita_id) && !empty($cita_id) ? $cita_id : 0);
	}
	
	/**
	 * @author: Ronald Tineo
	 * @return: Muestra los datos del cliente
	 */
	public function mostrarCliente(){
		$this->set('bodyClass','backRegister');
		$this->pageTitle = __('MOSTRAR_CLIENTE');
		$this->layout = 'modulo_taller'.DS.'web_cliente';
		$this->set('mostrarCliente',true);
		
		$clientSession = $this->verifySessionClient();
		
		if($this->request->data){
			$dt = $this->request->data;
			$this->Cliente->begin();	
			
			if($this->Cliente->save($dt)){
//				$this->Cliente->commit();
//				$this->Session->setFlash("SUS DATOS HAN SIDO ACTUALIZADOS", 'flash_success');
				/** ENVIAMOS EL MENSAJE DE BIENVENIDA **/
				$emails = $dt['Cliente']['email']; 
				if($this->sendMailClientUpdate($emails,$clientSession['Cliente']['nombres'])){
					$this->Cliente->commit();
					$this->Session->setFlash("SUS DATOS HAN SIDO ACTUALIZADOS, SE LE ENVIO UN MENSAJE AL EMAIL:&nbsp;&nbsp;$emails<br/><br/><i>El mensaje puede demorar unos minutos en ser enviado, en caso de presentar inconvenientes por favor comunicarse con el +511-7135050</i>",'flash_success');
					
					$this->Session->setFlash("SUS DATOS HAN SIDO ACTUALIZADOS", 'flash_success');
				}else{
					$this->Cliente->rollback();
					$this->Session->setFlash("EL CORREO NO PUDO SER ENVIADO, VERIFIQUE SU EMAIL",'flash_failure');
				}
			} else {
				$this->Cliente->rollback();
				$this->Session->setFlash("ERROR AL ACTUAZAR SUS DATOS, INTENTE NUEVAMENTE",'flash_failure');
			}
		}

		$clientSession = $this->Cliente->find('first', array('conditions'=>array('id'=>$clientSession['Cliente']['id']), 'recursive'=>'-1'));
		$this->request->data['ClienteLogin'] = $clientSession['Cliente'];
		$this->request->data['Cliente'] = $clientSession['Cliente'];
		if(!empty($dt)){
			$this->request->data['Cliente'] = array_merge($this->request->data['Cliente'], $dt['Cliente']);
		}			
	} 
}
