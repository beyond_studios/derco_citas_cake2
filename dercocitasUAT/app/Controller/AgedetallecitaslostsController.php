<?php
class AgedetallecitaslostsController extends AppController
{
	public $name = 'Agedetallecitaslosts';	
	public $helpers = array('Html', 'Form', 'Js'); //,'Excel','Numerosaletras', 'Xmlexcel');
	
    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow();
		$this->Agedetallecitaslost->status = array(
			'AC'=>__('Programado', true),
			//'DE'=>__('Desactivo'),
			'RE'=>__('Reprogramado'),
			'EL'=>__('Eliminado'),
			//'PR'=>__('Programado')
		);
		
		$this->Agedetallecitaslost->statusSap = array(
			''=>__('enviado', true),
			'1'=>__('enviado', true),
			'0'=>__('noEnviado', true)
		);
		
		$this->Agedetallecitaslost->tiposervicio = array(
			0=>__('Mantenimiento Menor', true),
			1=>__('Mantenimiento Mayor'),
		);
    }
	
	public function index(){
		$this->loadModel('Secproject');
		$this->loadModel('Agedetallecitaslostsmotive');
		
		//BORRAMOS LAS VARIABLES DE SESSION DEL BUSCADOR PRIMER INGRESO
		@$this->setInitSessionConditions();
		
//		debug($this->request->data); die;
		//FORMAMOS LA DATA SI SE ESTA ENVIANDO LOS DATOS POR PAGINADOR
		if($this->request->is('get')){
			//RECUPERAMOS LAS CONDICIONES DE BUSQUEDA ALMACENADOS EN SESSION
			$dt = $this->getSessionConditions();
			$this->request->data['bsc']= empty($dt)?array():$dt['bsc'];
		}
		
		if(!empty($this->request->data['bsc'])){
			$dt = $this->request->data['bsc'];
			
			if(!empty($dt['secproject_id'])){
				$this->loadModel('Agemotivoservicio');
				// motivo de servicio
				$agemotivoservicios = $this->Agemotivoservicio->getMotivoServicio($dt['secproject_id']);
				
				if(!empty($agemotivoservicios)){
					foreach($agemotivoservicios as $key => $value){
						$motivos_tmp[$value['Agemotivoservicio']['id']] = $value['Agemotivoservicio']['description'];
					}
					$agemotivoservicios = $motivos_tmp;
				}
			}
			
			if(!empty($dt['agemotivoservicio_id'])){
				// agetipomantenimientos/getTipoMantenimientoJson/2
				$this->loadModel('Agetipomantenimiento');
				$agetipomantenimientos = $this->Agetipomantenimiento->find('list',array(
					'fields'=>array('id', 'description'),
					'conditions'=>array('agemotivoservicio_id'=>$dt['agemotivoservicio_id'], 'status'=>'AC'),
					'recursive'=>-1
				));
		
				// agetiposervicios/getTiposerviciosJson/2
				$this->loadModel('Agetiposervicio');
				$agetiposervicios = $this->Agetiposervicio->find('list', array(
					'fields'=>array('id', 'description'),
					'conditions'=>array('1'=>empty($motivoId)?"1=1":"Agetiposervicio.agemotivoservicio_id = $motivoId", 'Agetiposervicio.status'=>'AC'),
					'recursive'=>-1
				));
			}	
		}
		
		//GUARDAMOS LAS CONDICIONES EN SESSION
		@$this->setSessionConditions($this->request->data);
		$cnd = $this->Agedetallecitaslost->getConditionsBuscador($this->request->data, $this->_getDtLg());
				
		$this->paginate = array('limit' => 10,
			'page' => 1,
			'order' => array ('Agedetallecitaslost.fechaRegistro' => 'ASC'),
			'conditions' => $cnd
		);
		
		$agedetallecitaslosts=$this->paginate('Agedetallecitaslost');
		$talleres = $this->Secproject->find('list',array('fields'=>array('id', 'name'),'conditions'=>array('status'=>'AC'), 'recursive'=>-1));
		
		$this->set('agedetallecitaslosts',$agedetallecitaslosts);
		$this->set('talleres',$talleres);
		$this->set('motivonocitas', $this->Agedetallecitaslostsmotive->find('list', array('conditions'=>array('status'=>'AC'))));		
		$this->set('agemotivoservicios', empty($agemotivoservicios)?array():$agemotivoservicios);
		$this->set('agetipomantenimientos', empty($agetipomantenimientos)?array():$agetipomantenimientos);
		$this->set('agetiposervicios', empty($agetiposervicios)?array():$agetiposervicios);		
	}	

	//elimina/reprograma citas desde la web...
	function delete($detallecita_id = 0){
		$this->layout = 'modulo_taller'.DS.'default_grid';
		
		$this->set('detallecita_id',$detallecita_id);
		if (!$detallecita_id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}
		
		$this->loadModel('Agecitacalendariodia');
		
		if($this->request->is('post')){
			$dt_original = $this->request->data;
			$this->set('dt_original',$dt_original);
			
			$this->Agedetallecitaslost->begin();
			$rpt = $this->Agedetallecitaslost->setEliminar($this->request->data, $this->_getDtLg());
			
			if($rpt[0]){
				$this->Agedetallecitaslost->commit();
				$this->Session->write('actualizarPadre', true);
				$this->redirect(array('action'=>'mostrarCita', $detallecita_id));
			}else{
				$this->Agedetallecitaslost->rollback();
			}
			
			$this->Session->setFlash($rpt[1]);
		}
		$this->mostrarCitaTaller($detallecita_id);

	}
	
	/**
	 * 
	 * @param object $detalle_cita [optional]
	 * @return: elimina una cita desde la web del cliente 
	 */
	public function deleteClient($detallecita_id = 0){
		$this->layout = 'modulo_taller'.DS.'web_cliente';
		$this->set('bandejaReprogramar',true);
		// validando si existe algun cliente logueado
		$clientSession = $this->requestAction('clientes/verifySessionClient');
		
		$this->set('detallecita_id',$detallecita_id);
		if (!$detallecita_id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}
		
		$this->loadModel('Agecitacalendariodia');
		
		if($this->request->is('post')){
			$dt_original = $this->request->data;
			$this->set('dt_original',$dt_original);
			
			$this->Agedetallecitaslost->begin();
			$rpt = $this->Agedetallecitaslost->setEliminar($this->request->data, $this->_getDtLg());
			
			if($rpt[0]){
				$this->Agedetallecitaslost->commit();
				$this->Session->write('actualizarPadre', true);
				$this->redirect(array('controller'=>'Clientes','action'=>'bandejaReprogramar'));
			}else{
				$this->Agedetallecitaslost->rollback();
			}
			
			$this->Session->setFlash($rpt[1]);
		}
		$this->mostrarCitaTaller($detallecita_id,null,$clientSession);
	}
	
	/**@author: ronald Tineo Santamaria
	 * 
	 * @param object $detallecitaId [optional]
	 * @return AGREGA LA CITA MEDIANTE AJAX
	 */
	function agregarCitaAjax($detallecitaId=null){
		$this->layout = 'ajax';
		$this->autoRender = false;
		$responseAjax = array('Success'=>false,'Mensaje'=>__('MENSAJE_NUMERO_TELEFONICO'));
		Configure::write('debug',0);

		if($this->request->data){
			$this->Agedetallecitaslost->begin();
			$rpt = $this->Agedetallecitaslost->agregarCita($this->request->data, $this->_getDtLg(), $dtAud = null);
			
			if($rpt[0]){
				$this->Agedetallecitaslost->commit();
				$this->Session->write('actualizarPadre', true);
				$responseAjax = array('Success'=>true,'Mensaje'=>'El Registro ha sido guardado', 'id'=>$rpt['id']);
			}else{
				$this->Agedetallecitaslost->rollback();
				$responseAjax = array('Success'=>false,'Mensaje'=>$rpt[1]);
			}
		}
		echo json_encode($responseAjax);
	}
	
	/** ############################################################################
	 * ###################### ACCIONES MODIFICADAS #################################
	 * ########## AUTOR: VENTURA RUEDA, JOSE ANTONIO ###############################
	 * */
	
	/**GENERA UNA NUEVA CITA DE CLIENTE
	 * AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 */
	function agregarCita() {
		$this->layout = 'modulo_taller'.DS.'default_grid';
		
		$this->loadModel('Agecitacalendario');
		$this->loadModel('Cliente');
		$this->loadModel('Agemotivoservicio');
		$this->loadModel('Secproject');
		$this->loadModel('Agedetallecitaslostsmotive');
		
		$talleres = $this->Secproject->find('all',array('conditions'=>array('status'=>'AC'), 'recursive'=>-1));
		$this->set('talleres',$talleres);
		$this->set('isPost',$this->request->is('post'));
		
		if($this->request->is('post')){
			$dt = $this->request->data;
			$this->loadModel('Marca');
			$this->loadModel('Secproject');
			$this->loadModel('Agemotivoservicio');
			$this->loadModel('Agetipomantenimiento');
			$this->loadModel('Agetiposervicio');
			
			// recuperamos las marcas
			$marcas = $this->Marca->getMarcasClient($dt['Agedetallecitaslost']['cliente_id']);	
				
			$secprojectsArray = $this->Secproject->getSecprojecsMarca(0, $dt['Agedetallecitaslost']['marca']);
			foreach($secprojectsArray as $value)
				$secprojects[$value['Secproject']['id']] = $value['Secproject']['name'];
			
			//recuperamos los motivos de servicio
			$motivoservicios = $this->Agemotivoservicio->getMotivoServicio($dt['Secproject']['id']);

			// recuperamos los tipo de mantenimiento
			$tipomantenimientos = $this->Agetipomantenimiento->find('list',array(
				'conditions'=>array('agemotivoservicio_id'=>$dt['Agemotivoservicio']['id'], 'status'=>'AC'),
				'recursive'=>-1
			));
			
			$tiposervicios = $this->Agetiposervicio->find('all', array(
				'conditions'=>array("Agetiposervicio.agemotivoservicio_id"=>$dt['Agemotivoservicio']['id'], 'Agetiposervicio.status'=>'AC'),
				'recursive'=>-1
			));
			
			$this->set('marcas',$marcas);
			$this->set('secprojects',$secprojects);
			$this->set('motivoservicios',$motivoservicios);
			$this->set('tipomantenimientos',$tipomantenimientos);
			$this->set('tiposervicios',$tiposervicios);
			
		}
		
		if($this->request->is('get')){
			$this->set('motivoservicios',array());
			$this->set('tipomantenimientos', array());
			$this->set('secprojects', array());
			$this->set('marcas', array());
			$this->set('tiposervicios',array());
			$this->set('horarios',0);
		}
		
		$this->set('motivonocitas', $this->Agedetallecitaslostsmotive->getList(array('status'=>'AC')));
	}
	
	/**AUTOR VENTURA RUEDA, JOSE ANTONIO
	 * TRAIDO DESDE: derwebsolicitudes
	 * @param object $calendarId
	 * @param object $fechaInicial
	 * @param object $fechaFinal
	 * @param object $marcaApoyo
	 * @return 
	 */
	function listarProgramacionCitaTaller() {
		$this->layout = 'ajax';
		
		$this->loadModel('Agecitacalendario');
		$this->loadModel('Secproject');
		
		$calendarId = $this->request->data['Buscador']['agecitacalendario_id'];
		$fechaInicial = $this->request->data['Buscador']['fechaInicial'];
		$fechaFinal = $this->request->data['Buscador']['fechaFinal'];
		
		
		// se obtienen el taller y los rangos de fecha
		$fechaActual = date('Ymd', strtotime($this->Agedetallecitaslost->fechaHoraActual()));
		$fechaIni = date('Ymd', strtotime($fechaInicial));
		$fechaFin = date('Ymd', strtotime($fechaFinal));
		
		// se obtiene la programacion de cita de taller
		$fechas = (!isset($fechas)) ? $this->Agecitacalendario->obtenerProgramacionCitaTaller($calendarId, $fechaInicial, $fechaFinal) : array();
		
		$this->set('fechas', isset($fechas) && !empty($fechas) ? $fechas : array());
	}
	
	/**MUESTRA LA CITA
	 * autor: VENTURA RUEDA, JOSE ANTONIO
	 * @param object $taldetallecita_id
	 * @return 
	 */
	function mostrarCita($taldetallecita_id) {
		$this->layout = 'modulo_taller'.DS.'default_grid';
		$this->mostrarCitaTaller($taldetallecita_id);
	}

	/*
	 * Muestra la cita de taller generada por el cliente.
	 * @return 
	 * @param $taldetallecita_id Object
	 * @param $dermarca_id Object
	 * @param $dercliente_id Object
	 */
	function mostrarCitaTaller($taldetallecita_id, $dermarca_id=null,$clienteWeb = null) {
		$this->loadModel('Secorganization');
		$this->loadModel('Agedetallecitaslost');
		$this->loadModel('Secproject');
		$this->loadModel('Cliente');
		$this->loadModel('Marca');
		
		$estadoActivo = 'AC';
		
		// obtenemos los datos de la cita de taller
		$dt = $this->Agedetallecitaslost->findById($taldetallecita_id);
		// obteniendo los datos del cliente
		$cliente = $this->Cliente->findById($dt['Cliente']['id']);
		$dt['Cliente'] = $cliente['Cliente'];
		
		// obteniendo el nombre de la organizacion de la sucursal
		$organizationName = $this->Secorganization->obtenerName($dt['Secproject']['secorganization_id']);
		
		// obteniendo los datos de los servicios solicitados
		$servicios = array();
		
		$this->set('servicios', $servicios);
		$this->set('organizationName', $organizationName);
		$this->set('dercliente_id', $dt['Cliente']['id']);
		
		//Solo para la web del cliente
		$this->set('tallerVehiculos', true);// para marcar la opcion seleccionada
		$this->request->data = $dt;
		$this->set('motivonocitas', $this->Agedetallecitaslost->motivonocitas);
	}
	
	
	/**UTOR: VENTURA RUEDA, JOSE ANTNIO
	 * FECHA: 2013-03-20
	 * @return 
	 */
	function getIndexExel(){
		$this->loadModel('Cliente');
		//BORRAMOS LAS VARIABLES DE SESSION DEL BUSCADOR PRIMER INGRESO
		@$this->setInitSessionConditions();
		//FORMAMOS LA DATA SI SE ESTA ENVIANDO LOS DATOS POR PAGINADOR
		if($this->request->is('get')){
			//RECUPERAMOS LAS CONDICIONES DE BUSQUEDA ALMACENADOS EN SESSION
			$bscCnd = $this->getSessionConditions();
			$this->request->data['bsc']['vlr'] = (trim(isset($bscCnd['bsc']['vlr'])?$bscCnd['bsc']['vlr']:'') == '')?'':$bscCnd['bsc']['vlr'];
			$this->request->data['bsc']['f_ini'] = empty($bscCnd['bsc']['f_ini'])?'':$bscCnd['bsc']['f_ini'];
			$this->request->data['bsc']['f_fin'] = empty($bscCnd['bsc']['f_fin'])?'':$bscCnd['bsc']['f_fin'];
		}
	
		//GUARDAMOS LAS CONDICIONES EN SESSION
		@$this->setSessionConditions($this->request->data);
		$cnd = $this->Agedetallecitaslost->getConditionsBuscador($this->request->data, $this->_getDtLg());

		$agedetallecitaslostslosts=$this->Agedetallecitaslost->find('all',array(
			'conditions'=>$cnd,
			'order' => array ('Agedetallecitaslost.fechaRegistro' => 'ASC','Agedetallecitaslost.horaRegistro' => 'ASC')
		));

		// se obtiene el caledario  de los registros obtenidos
		foreach($agedetallecitaslostslosts as $key => $row) {	
			$this->Agedetallecitaslost->Agecitacalendariodia->Agecitacalendario->recursive = -1;	
			$agecitacalendarios = $this->Agedetallecitaslost->Agecitacalendariodia->Agecitacalendario->find('first', array(
				'conditions' => array('Agecitacalendario.status' =>'AC','Agecitacalendario.id' => $row['Agecitacalendariodia']['agecitacalendario_id'])
			));
			$agedetallecitaslostslosts[$key]['Agecitacalendario'] = $agecitacalendarios['Agecitacalendario'];
			$agedetallecitaslostslosts[$key]['Cliente']['str_cliente_tipo'] = $this->Cliente->getStrTipoCliente($agedetallecitaslostslosts[$key]['Cliente']['cliente_tipo']);
		}
		
		// se obtiene el grupo de los registros obtenidos
		foreach($agedetallecitaslostslosts as $key => $row) {	
			$this->Agedetallecitaslost->Agecitacalendariodia->Agecitacalendario->Agegrupo->recursive = -1;	
			$agegrupos = $this->Agedetallecitaslost->Agecitacalendariodia->Agecitacalendario->Agegrupo->find('first', array(
				'conditions' => array('Agegrupo.status' =>'AC','Agegrupo.id' => $row['Agecitacalendario']['agegrupo_id'])
			));
			$agedetallecitaslostslosts[$key]['Agegrupo'] = $agegrupos['Agegrupo'];
		}
		
		// se obtiene lña sucursal  de los Sucursal obtenidos
		foreach($agedetallecitaslostslosts as $key => $row) {	
			$this->Agedetallecitaslost->Agecitacalendariodia->Agecitacalendario->Secproject->recursive = -1;	
			$secprojects = $this->Agedetallecitaslost->Agecitacalendariodia->Agecitacalendario->Secproject->find('first', array(
				'conditions' => array('Secproject.status' =>'AC','Secproject.id' => $row['Agecitacalendario']['secproject_id'])
			));
			$agedetallecitaslostslosts[$key]['Secproject'] = $secprojects['Secproject'];
		}
		
		// se obtiene el servicio de los Sucursal obtenidos
		foreach($agedetallecitaslostslosts as $key => $row) {	
			$this->Agedetallecitaslost->Agecitacalendariodia->Agecitacalendario->Agemotivoservicio->recursive = -1;	
			$agemotivoservicios = $this->Agedetallecitaslost->Agecitacalendariodia->Agecitacalendario->Agemotivoservicio->find('first', array(
				'conditions' => array('Agemotivoservicio.status' =>'AC','Agemotivoservicio.id' => $row['Agecitacalendario']['agemotivoservicio_id'])
			));
			$agedetallecitaslostslosts[$key]['Agemotivoservicio'] = $agemotivoservicios['Agemotivoservicio'];
		}
		
		$this->set('f_campo',array('Agedetallecitaslost.fechadecita'=>__('cita'), 'Agedetallecitaslost.fechaRegistro'=>__('creacion')));	
		$this->set('std', $this->Agedetallecitaslost->status);
		$this->set('crt', array('Cliente.nombres'=>__('Cliente'), 'Agedetallecitaslost.placa'=>__('placa')));
		$this->set('dt', $this->request->data);
		$this->set('agedetallecitaslostslosts',$agedetallecitaslostslosts);	
		$this->set('statusSap', $this->Agedetallecitaslost->statusSap);
		
		//IMPRESION EN EXEL
		$this->layout = 'modulo_taller'.DS.'excel';
		header('Content-Type: text/html');
		header("Content-Disposition: attachment; filename=Historial_de_Citas.xls");
		
		set_time_limit(1200);
		ini_set('memory_limit', '512M');
		$fecha = date('d/m/Y');
        $this->set('fecha', $fecha);
	}
	
	
	
	function ajaxCountTipoServicios($secproject_id, $motivoservicio_id, $fechatoday){
		configure::write('debug', 0);
		$this->layout = 'ajax';
		$this->autoRender = false;
		$fechatoday = date('Y-m-d',strtotime($fechatoday));
		$result = $this->Agedetallecitaslost->getCountTipoServicios($secproject_id, $motivoservicio_id,$fechatoday);
		$json = array(0=>array('descripcion'=>$this->Agedetallecitaslost->tiposervicio[0],'cantidad'=>0),1=>array('descripcion'=>$this->Agedetallecitaslost->tiposervicio[1],'cantidad'=>0));
		if(!empty($result) && isset($result)){
			foreach($result as $id => $item){
				if($item['Agetipomantenimiento']['tiposervicio']==0){
					$json[0]['cantidad'] = $item[0]['cantidad'];
				}
				if($item['Agetipomantenimiento']['tiposervicio']==1){
					$json[1]['cantidad'] = $item[0]['cantidad'];
				}
			}
		}
		echo json_encode($json);
	}
	
	function ajaxGetMensajeTipoServicios($motivoservicio_id){
		//configure::write('debug', 0);
		$this->layout = 'ajax';
		$this->autoRender = false;
		$this->loadModel('Agemotivoservicio');
		$this->Agemotivoservicio->recursive = -1;
		$result = $this->Agemotivoservicio->findById($motivoservicio_id);
		$json = array('mensaje'=>empty($result['Agemotivoservicio']['mensaje'])?'':$result['Agemotivoservicio']['mensaje']);
		echo json_encode($json);
	}
}
?>