<?php
class ChevroletsController extends AppController {

	public $name = 'Chevrolets';
	public $helpers = array('Html', 'Form', 'Avhtml');
	
    public function beforeFilter() {
        parent::beforeFilter();

    }
	
	
	public function validCar($placa){ 
        configure::write('debug',0);
		$this->layout = 'ajax';
		
		$validCar = $this->Chevrolet->validCar($field='placa', $placa);
		echo json_encode($validCar);
		$this->autoRender = false;
	}
	
	public function index(){
		//BORRAMOS LAS VARIABLES DE SESSION DEL BUSCADOR PRIMER INGRESO
		@$this->setInitSessionConditions();
		//FORMAMOS LA DATA SI SE ESTA ENVIANDO LOS DATOS POR PAGINADOR
		if($this->request->is('get')){
			//RECUPERAMOS LAS CONDICIONES DE BUSQUEDA ALMACENADOS EN SESSION
			$bscCnd = $this->getSessionConditions();
			$fechaActual = $this->Chevrolet->getDateFormatView($this->Chevrolet->fechaHoraActual());
			$this->request->data['bsc']['crt'] = empty($bscCnd['bsc']['crt'])?'':$bscCnd['bsc']['crt'];
			$this->request->data['bsc']['vlr'] = (trim(isset($bscCnd['bsc']['vlr'])?$bscCnd['bsc']['vlr']:'') == '')?'':$bscCnd['bsc']['vlr'];
			$this->request->data['bsc']['std'] = empty($bscCnd['bsc']['std'])?'':$bscCnd['bsc']['std'];
			$this->request->data['bsc']['f_campo'] = empty($bscCnd['bsc']['f_campo'])?'Chevrolet.fecha_base':$bscCnd['bsc']['f_campo'];
			$this->request->data['bsc']['f_ini'] = empty($bscCnd['bsc']['f_ini'])?'':$bscCnd['bsc']['f_ini'];
			$this->request->data['bsc']['f_fin'] = empty($bscCnd['bsc']['f_fin'])?'':$bscCnd['bsc']['f_fin'];
		}
		
		// INICIALIZACION POR DEFECTO
   		if(!isset($this->request->data['bsc']['f_campo'])) $this->request->data['bsc']['f_campo'] = 'Chevrolet.fecha_base';
		
		//GUARDAMOS LAS CONDICIONES EN SESSION
		@$this->setSessionConditions($this->request->data);
		$f_campo = array('Chevrolet.fecha_base'=>__('Fecha Base'));
		
		$cnd = $this->Chevrolet->getConditionsBuscador($this->request->data, $this->_getDtLg());
		
		// DATOS PARA LA BUSQUEDA
		$this->set('std', $this->Chevrolet->status);
		$this->set('crt', array('vim'=>__('VIN'), 'placa'=>__('Placa')));
		$this->set('f_campo',$f_campo);
		
		$this->paginate = array('limit' => 5,
			'page' => 1,
			'order' => array ('Chevrolet.fecha_base' => 'ASC'),
			'conditions' => $cnd
		);
		
		$chevrolets=$this->paginate('Chevrolet');
			
		// se obtiene el caledario  de los registros obtenidos
		foreach($chevrolets as $key => $row) {	
			$chevrolet = $this->Chevrolet->validCar('id', $row['Chevrolet']['id']);
			$chevrolets[$key]['Chevrolet']['msg'] = $chevrolet['msg'];
		}
		
		$msg = null;
		if(!empty($this->request->data['bsc']['f_ini'])){
			$msg = $this->Chevrolet->validFecha($this->request->data['bsc']['f_ini']);
		}
		
		$this->set('msg',$msg);
		$this->set('chevrolets',$chevrolets);				
	}	
}	
?>