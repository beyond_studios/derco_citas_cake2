<?php
class ServiciosController extends AppController {
	public $name = 'Servicios';
    public $helpers = array('Html', 'Xhtml', 'Form', 'Idioma');
	var $components = array('RequestHandler', 'Imagenes');
	
	public function index() {
		$this->layout = "default_02";
		$this->paginate = array(
			'limit' => 4, 
			'page' => 1,
			'order' => array ('Servicio.id' => 'desc'),
			'conditions'=>array('Servicio.status'=>'AC')
		);
		
		$this->set('servicios',$this->paginate('Servicio'));	
	}
	
	public function add() {
		$this->pageTitle = __('NUEVO SERVICIO');
		$this->layout='modulo_taller'.DS.'default_grid';
		
		$this->loadModel('Agemotivoservicio');
		
		if(!empty($this->request->data)){
			$dt = $this->request->data;
			
			/* GUARDAMOS LOS DATOS ENVIADOS POR EL FORMULARIO */
			$this->Servicio->begin();
			
			$dt['Servicio']['fileName'] = $this->Imagenes->subirImg($dt['Servicio']['img'], 'img/Servicios/', 0);
			
			if((isset($dt['Servicio']['img'])) && ($dt['Servicio']['img'] !='') && !empty($dt['Servicio']['fileName'])){
				//guardamos el archivo
				$dt['Servicio']['extension'] = $dt['Servicio']['img']['type'];
				$dt['Servicio']['binario'] = '';
				
				if(!$this->Servicio->save($dt['Servicio'])){
					$this->Servicio->rollback();
					$this->Session->setFlash($rpt['error'], 'flash_failure');
				}else{
					$this->Session->write('actualizarPadre', true);
					$this->Servicio->commit();
					$this->Session->setFlash(__('datosGuardados'), 'flash_success');
				}
			}else{
				$this->Session->setFlash("VERIFIQUE LA IMAGEN ENVIADA", 'flash_failure');
			}
		}
	}
	
	function delete($servicioId = null){
		$this->layout = 'modulo_taller'.DS.'default_grid';
		
		if (!$servicioId) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}else{
			$servicio['Servicio']['id'] = $servicioId;
			$servicio['Servicio']['status'] = 'EL';
			
			$this->Servicio->begin();
			if($this->Servicio->save($servicio)){
				$this->Servicio->commit();
				$this->Session->setFlash("SERVICIO ELIMINADO");
			}else{
				$this->Servicio->rollback();
				$this->Session->setFlash("NO SE PUDO ELIMINAR EL SERVICIO");
			}
		}
		
		$this->redirect(array('action'=>'index'));
	}
	
	public function view(){
		$this->set('bodyClass','backRegister');
		$this->layout = 'modulo_taller'.DS.'web_cliente';
		$servicios = $this->Servicio->find('all', array(
			'order'=>array('id'=>'desc'),
			'conditions'=>array('status'=>'AC'),
			'recursive'=>-1
		));
		
		$this->set('servicios' ,$servicios);
	}
}