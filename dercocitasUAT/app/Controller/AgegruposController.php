<?php 
 class AgegruposController extends AppController
 {
     public $name = 'Agegrupos';
     public $helpers = array('Html', 'Form');

    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow();
    }

     public function index()
     {

        $this->Agegrupo->recursive = 0;
		$elementos = array('Agegrupo.description'=>__('AGE_GRUPOS_DESCRIPCION',true));
		$this->set('elementos',$elementos);			
		
		if(!empty($this->params['named']['valor']) || !empty($this->params['named']['desactivo']))
		{
			$this->request->data['Buscar']['buscador'] = $this->params['named']['buscador'];
			$this->request->data['Buscar']['valor'] = $this->params['named']['valor'];
			$this->request->data['Buscar']['desactivo'] = $this->params['named']['desactivo'];
		}
		
		$valorDeBusqueda = isset($this->request->data['Buscar']['valor'])?trim($this->request->data['Buscar']['valor']):null;
		$conditions = !empty($valorDeBusqueda)?
						array($this->request->data['Buscar']['buscador'].' LIKE'=>'%'.trim($this->request->data['Buscar']['valor']).'%'):
						array();		
		
		$conditionsActivos = (!empty($this->request->data['Buscar']['desactivo']) == 1) ?
								array('Agegrupo.status'=>'DE') :
								array('Agegrupo.status'=>'AC');
		
		$conditions = $conditions + $conditionsActivos;
		
		$fields = array('Agegrupo.id','Agegrupo.description','Agegrupo.status');
			
		$this->paginate = array('limit' => 10,
								'page' => 1,
								'order' => array ('Agegrupo.description' => 'asc'),
								'conditions' => $conditions,
								'fields' => $fields
								); // Take care with order
			
		
		$agegrupos = $this->paginate('Agegrupo');

        foreach ($agegrupos as $key => $row) 
        {
               $marca = $this->Agegrupo->AgegruposMarca->find('first', array(
				'conditions' => array('AgegruposMarca.agegrupo_id' => $row['Agegrupo']['id'])
			    ));
			if(!empty($marca))
				$agegrupos[$key]['AgegruposMarca'] = $marca['AgegruposMarca'];
			//pr($marca);
        }
     

		$this->set('agegrupos',$agegrupos);

     }  

     
     public function add()
     {
     	$this->layout = 'contenido';
		if (!empty($this->request->data)) 
		{
			$this->Agegrupo->create();
			if ($this->Agegrupo->save($this->request->data)) 
			{
				$this->Session->setFlash(__('GENERAL_REGISTRO_AGREGADO', true),'flash_success');
				$this->Session->write('actualizarPadre', true);
				$this->Session->write($this->redirect(array('action'=>'view',$this->Agegrupo->getInsertID())));	
			} 
			else 
			{
				$this->Session->setFlash(__('GENERAL_ERROR_GRABACION', true),'flash_failure');
			}
		}
     }

     
	 function view($id=true)
	 {
	 	$this->layout = 'contenido';
		if(!$id)
		{
			$this->Session->setFlash(__('GENERAL_REGISTRO_AGREGADO',true),'flash_failure');
			$this->redirect(array('action'=>'index'));
		}
       
	    $this->set('agegrupo', $this->Agegrupo->read(null, $id));
	 }
	 
	 
	 
	 function edit($id=true)
	 {	
		$this->layout = 'contenido';  
	    if(!$id && empty($this->request->data)) 
	    {
			$this->Session->setflash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
			$this->redirect(array('action'=>'index'));
		}
      
        $estadoActivo = 'AC';
		$estadoDesactivo = 'DE';

		if (empty($this->request->data['Agegrupo'])) 
		{
		     $this->request->data = $this->Agegrupo->read(null, $id);
		} 
		else 
		{
			      $id = $this->request->data['Agegrupo']['id'];
				  $nuevoEstado = $this->request->data['Agegrupo']['status'];
				    	
			    if ($this->Agegrupo->save($this->request->data['Agegrupo'])) 
			    {			
					$this->Session->setFlash(__('GENERAL_REGISTRO_MODIFICADO', true),'flash_success');	
					$this->Session->write('actualizarPadre', true);	
				    $this->Session->write($this->redirect(array('action'=>'view',$this->request->data['Agegrupo']['id'])));
				    //$this->Session->write($this->redirect(array('action'=>'view',$this->Agegrupo->getInsertID())));					
			    } 
			    else 
			    {
					$this->Session->setFlash(__('GENERAL_REGISTRO_NO_FUE_ACTUALIZADO', true),'flash_failure');
	            }
		
	     }
		    
	 }

     
     function delete($id = null) 
     {
		$estadoEliminado = 'EL';
		$estadoActivo = 'AC';
		if (!$id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}else{
			//Si existen marcas asociados no se peuden eliminar---Miguel Chiuyari
			$existMarca = $this->Agegrupo->AgegruposMarca->find('count', array('conditions' => array('AgegruposMarca.status' => 'AC', 'AgegruposMarca.agegrupo_id'=>$id)));
			if($existMarca)
				{
				$this->Session->setFlash(__('GENERALES_REGISTRO_ASOCIADO',true),'flash_failure');				
				}
				else{					
					$this->request->data['Agegrupo']['id'] = $id;
					$this->request->data['Agegrupo']['status'] = $estadoEliminado;
					if ($this->Agegrupo->save($this->request->data['Agegrupo'])) {
						$this->Session->setFlash(__('GENERAL_REGISTRO_ELIMINADO', true),'flash_success');	
					} else {
						$this->Session->setFlash(__('GENERAL_REGISTRO_ACTIVADO', true),'flash_failure');
					}
				}
			}
			$this->redirect(array('action'=>'index'));
	}

    
    function asignar($agegrupoId) 
    {
		$this->set('agegrupoId', $agegrupoId);
		$this->layout = 'contenidocmb';
        
        if (!$agegrupoId && empty($this->request->data)) {
            $this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
            $this->redirect(array('action'=>'index'));
        }


        //MODELOS UTILIZADOS
        $this->loadModel('AgegruposMarca');
        
        if (!empty($this->request->data)) {
            $this->AgegruposMarca->begin();
            $rpt = $this->AgegruposMarca->setMarcas($agegrupoId, $this->request->data);
            
            if ($rpt[0]){
                $this->AgegruposMarca->commit();
                $this->Session->setFlash(__('GENERAL_REGISTRO_AGREGADO'),'flash_success');
                $this->Session->write('actualizarPadre', true);
            } else {
                $this->AgegruposMarca->rollback();
                $this->Session->setFlash(__('GENERAL_ERROR_GRABACION'),'flash_failure');
            }
        }


        if (empty($this->request->data)) {
            $marcasAgegrupo_db= $this->AgegruposMarca->find('list',array(
                'fields'=>array('AgegruposMarca.marca_id', 'AgegruposMarca.marca_id'),
                'conditions'=>array('AgegruposMarca.agegrupo_id'=>$agegrupoId, 'AgegruposMarca.status'=>'AC'),
                'recursive'=>-1
            ));
            $this->set('marcasAgegrupo_db',$marcasAgegrupo_db);
        }


        $agegrupo = $this->Agegrupo->find('first',array(
            'conditions' => array(
                'Agegrupo.status' => 'AC',
                'Agegrupo.id' => $agegrupoId)
        ));

        $marcas = $this->Agegrupo->Marca->find('list',array(
            'fields'=>array('id','description'),
            'conditions' => array('Marca.status' => 'AC')
        ));


        $this->set(compact('agegrupo','marcas'));
		

	}


       
 }
?>