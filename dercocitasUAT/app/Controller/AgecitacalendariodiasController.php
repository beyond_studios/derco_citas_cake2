<?php
App::uses('AppController', 'Controller');
class AgecitacalendariodiasController extends AppController {
	public $name = 'Agecitacalendariodias';
	
    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow();
        $this->loadModel('Appcast');
        $this->loadModel('Appcasttime');
    }

	/**
     * Permite generar el numero de citas programadas por calendario.
     */    
	function _generarhorario($id=true) 
	{
		$this->layout = 'contenido';
		
		if (!empty($this->request->data)) {
			
			//Generan los registros segun los calendarios...
			$agecitacalendario_id=$this->request->data['Agecitacalendario']['id'];
			$lunesid=$this->request->data['Agecitacalendariodia']['lunes_cast_id'];
			$martesid=$this->request->data['Agecitacalendariodia']['martes_cast_id'];
			$miercolesid=$this->request->data['Agecitacalendariodia']['miercoles_cast_id'];
			$juevesid=$this->request->data['Agecitacalendariodia']['jueves_cast_id'];
			$viernesid=$this->request->data['Agecitacalendariodia']['viernes_cast_id'];
			$sabadoid=$this->request->data['Agecitacalendariodia']['sabado_cast_id'];
			$domingoid=$this->request->data['Agecitacalendariodia']['domingo_cast_id'];
			$anios=$this->Agecitacalendariodia->getAño();
			$anio=$anios['0']['0']['year'];
			
			//for para generar calendario x un año
			for($mes=1; $mes<=12; $mes++) {
			$primeraFecha = getDate(mktime(0, 0, 0, $mes, 1, $anio));
			$ultimaFecha = getDate(mktime(0, 0, 0, $mes+1, 0, $anio));
		
			$dia = 1;
			$fecha = $primeraFecha;
			
			while($fecha['0']<=$ultimaFecha['0']) {
				$diaSemana = $fecha['wday'];
				if($diaSemana==0) {
					// caso: domingo
					$cast_id = $domingoid;
				}
				if($diaSemana==1){
					//caso lunes
					$cast_id = $lunesid;
				}
				if($diaSemana==2){
					//caso martes
					$cast_id = $martesid;
				}
				if($diaSemana==3){
					//caso miercoles
					$cast_id = $miercolesid;
				}
				if($diaSemana==4){
					//caso jeuves
					$cast_id = $juevesid;
				}
				if($diaSemana==5){
					//caso viernes
					$cast_id = $viernesid;
				}
				if($diaSemana==6) {
					// caso: sabado
					$cast_id = $sabadoid;
				} 
				
				$castTimes = $this->Appcasttime->find('all',array('conditions'=>array(
					'Appcasttime.appcast_id' => $cast_id,
					'Appcasttime.status' => 'AC'
				)));
				
				// agregar
				foreach($castTimes as $castTime) {
					$this->Agecitacalendariodia->create();
					$fechaYmd = date('Y-m-d', mktime(0, 0, 0, $mes, $dia, $anio));
					$this->request->data['Agecitacalendariodia']['initDateTime'] = $fechaYmd.' '.$castTime['Appcasttime']['initTime'];
					$this->request->data['Agecitacalendariodia']['endDateTime'] = $fechaYmd.' '.$castTime['Appcasttime']['endTime'];
					$this->request->data['Agecitacalendariodia']['programed'] = $castTime['Appcasttime']['programed'];
					$this->request->data['Agecitacalendariodia']['available'] = $castTime['Appcasttime']['programed'];
					$this->request->data['Agecitacalendariodia']['agecitacalendario_id'] = $agecitacalendario_id;

					if ($this->Agecitacalendariodia->save($this->request->data['Agecitacalendariodia'])) {
						// nada 
					} else {
						$this->Session->setFlash(__('plantillaNoGuardado'),'flash_failure');
					}
					
				}
				// incremento
				$dia++;
				$fecha = getDate(mktime(0, 0, 0, $mes, $dia, $anio));
			}// end while
		}
		$this->Session->setFlash(__('plantillaGuardado'),'flash_success');
		$this->Session->write('actualizarPadre',true);	
		//$this->Session->write($this->redirect(array('action'=>'view',$this->Agecitacalendariodia->getInsertID())));
		}
				
		$appcasttimes = $this->Appcast->Appcasttime->find('all', array(
			'conditions' => array('Appcasttime.status' => 'AC'),
				'fields' => array('Appcast.description')
		));
		foreach($appcasttimes as $key => $row) {
			$appcasts[$key] = $appcasttimes[$key]['Appcast']['description'];
		}
	
		$años = $this->Agecitacalendariodia->getAño();
		$año['Año']=$años['0']['0'];
							
		$this->request->data['Agecitacalendario']['id']=$id;
		$agecitacalendario=$this->request->data['Agecitacalendario']['id'];

		$this->set(compact('año','appcasts','$agecitacalendario'));
	}

	function hayHorario($anio=null, $mes=null, $dia=null, $agecitacalendario_id=null) {
		$fecha = date('Y-m-d', mktime(0, 0, 0, $mes, $dia, $anio));
        $condicion = array(
			'agecitacalendario_id'=>$agecitacalendario_id,
			'DATE(initdatetime)'=>$fecha,
			'status'=>'AC'
		);
        $workSchedules = $this->Agecitacalendariodia->find('all',array('conditions'=>$condicion));
		if (empty($workSchedules)) {
			return false;
		} else {
			return true;
		}
	}
	
	public function modificarCalendarioGenerado($id=null)
	{
		$this->layout = 'contenido';
		
		if (!empty($this->request->data)) {
			
			//Generan los registros segun los calendarios...
			$agecitacalendario_id=$this->request->data['Agecitacalendario']['id'];
			$lunesid=$this->request->data['Agecitacalendariodia']['lunes_cast_id'];
			$martesid=$this->request->data['Agecitacalendariodia']['martes_cast_id'];
			$miercolesid=$this->request->data['Agecitacalendariodia']['miercoles_cast_id'];
			$juevesid=$this->request->data['Agecitacalendariodia']['jueves_cast_id'];
			$viernesid=$this->request->data['Agecitacalendariodia']['viernes_cast_id'];
			$sabadoid=$this->request->data['Agecitacalendariodia']['sabado_cast_id'];
			$domingoid=$this->request->data['Agecitacalendariodia']['domingo_cast_id'];
			$desde=$this->request->data['Agecitacalendariodia']['desde'];
			$anio=$this->request->data['Agecitacalendariodia']['anio'];
			pr($this->request->data);
			
			$mesInicial = date('n', strtotime($desde));
			$diaInicial = date('d', strtotime($desde));
			//for para generar calendario x un año
			for($mes=$mesInicial; $mes<=12; $mes++) {
			$primeraFecha = getDate(mktime(0, 0, 0, $mes, 1, $anio));
			$ultimaFecha = getDate(mktime(0, 0, 0, $mes+1, 0, $anio));
			
			$dia=($mes==$mesInicial)?$diaInicial:1;
			//$dia = $diaInicial;
			$fecha = $primeraFecha;
			
			while($fecha['0']<=$ultimaFecha['0']) {
				$diaSemana = $fecha['wday'];
				if($diaSemana==0) {
					// caso: domingo
					$cast_id = $domingoid;
				}
				if($diaSemana==1){
					//caso lunes
					$cast_id = $lunesid;
				}
				if($diaSemana==2){
					//caso martes
					$cast_id = $martesid;
				}
				if($diaSemana==3){
					//caso miercoles
					$cast_id = $miercolesid;
				}
				if($diaSemana==4){
					//caso jeuves
					$cast_id = $juevesid;
				}
				if($diaSemana==5){
					//caso viernes
					$cast_id = $viernesid;
				}
				if($diaSemana==6) {
					// caso: sabado
					$cast_id = $sabadoid;
				} 
				
				// buscar
				$condicion = array(
					'appcast_id' => $cast_id,
					'Appcasttime.status' => 'AC'
				);
				$castTimes = $this->Appcasttime->find('all',array('conditions'=>array($condicion)));
				// agregar
				foreach($castTimes as $castTime) {
					$this->Agecitacalendariodia->create();
					$fechaYmd = date('Y-m-d', mktime(0, 0, 0, $mes, $dia, $anio));
					$this->request->data['agecitacalendariodia']['initDateTime'] = $fechaYmd.' '.$castTime['Appcasttime']['initTime'];
					$this->request->data['agecitacalendariodia']['endDateTime'] = $fechaYmd.' '.$castTime['Appcasttime']['endTime'];
					$this->request->data['agecitacalendariodia']['programed'] = $castTime['Appcasttime']['programed'];
					$this->request->data['agecitacalendariodia']['available'] = $castTime['Appcasttime']['programed'];
					$this->request->data['agecitacalendariodia']['agecitacalendario_id'] = $agecitacalendario_id;

					if ($this->Agecitacalendariodia->save($this->request->data['agecitacalendariodia'])) {
						// nada 
					} else {
						return false;
					}
				}
				// incremento
				$dia++;
				$fecha = getDate(mktime(0, 0, 0, $mes, $dia, $anio));
			}// end while
		}// end for
		$this->Session->setFlash(__('plantillaGuardado'),'flash_success');
		$this->Session->write('actualizarPadre',true);	
		}
				
		$appcasttimes = $this->Appcast->Appcasttime->find('all', array(
			'conditions' => array('Appcasttime.status' => 'AC'),
				'fields' => array('Appcast.description')
		));
		foreach($appcasttimes as $key => $row) {
			$appcasts[$key] = $appcasttimes[$key]['Appcast']['description'];
		}
							
		$this->request->data['Agecitacalendario']['id']=$id;
		$agecitacalendario=$this->request->data['Agecitacalendario']['id'];

		$this->set(compact('appcasts','$agecitacalendario'));
		
	}
	
	
	/** ############################################################################
	 * ###################### ACCIONES MODIFICADAS #################################
	 * ########## AUTOR: VENTURA RUEDA, JOSE ANTONIO ###############################
	 * */
	/**
	 * MIGRADO POR: VENTURA RUEDA JOSE ANTONIO
	 * correo: jose.antonio.ventura.rueda@hotmail.com
	 */
	function modificarCalendarioGeneradoForm($idCalendar=null) {
		$this->pageTitle = __('CITAS_TALLER_MODIFICAR_CALENDARIO_GENERADO');
		$this->layout = 'modulo_taller'.DS.'default_grid';
		
		$this->loadModel('Agecitacalendario');
		$this->loadModel('Appcast');
		$this->loadModel('Agedetallecita');
		
		$estadoActivo = 'AC';
		// se establece la fecha y hora actual como defecto
		$this->request->data['Fecha']['desde'] =(empty($this->request->data['Fecha']['desde']))?date('d-m-Y', strtotime($this->Agecitacalendario->fechaHoraActual())):date('d-m-Y',strtotime($this->request->data['Fecha']['desde']));
		
		// se obtienen los anios de los calendarios generados
		$aniosGenerados = $this->Agecitacalendario->obtenerAniosGeneradoPorCitaCalendar($idCalendar);
		if(!empty($aniosGenerados)) {
			// se obtienen las plantillas de los calendarios
			$condicion = array('Appcast.status'=>$estadoActivo);
			$casts = $this->Appcast->generateLista();
			if (!$casts) {
				$this->Session->setFlash(__('TALCITACALENDARDAYS_ETIQUETA_MODIFICAR_CALENDARIO_NO_EXISTEN_PLANTILLA'));
			}
		} else {
			$this->Session->setFlash(__('TALCITACALENDARDAYS_ETIQUETA_MODIFICAR_CALENDARIO_NO_EXISTEN_CALENDARIO'));
		}
		$this->set('casts', !empty($casts) ? $casts : array());
		$this->set('calendar', !empty($idCalendar) ? $idCalendar :null);
		$this->set('aniosGenerados', !empty($aniosGenerados) ? $aniosGenerados : array());
		
		if(!empty($this->request->data['Agecitacalendario']) && !empty($this->request->data['Agecitacalendariodia'])){
			$talcitacalendar_id = $this->request->data['Agecitacalendario']['id'];
			//obteniendo las plantillas
			//se comento el diario cast id porque se va IMPLEMENTAR UNO DIARIO que comprenda de lunes a viernes,
			//$diarioCastId = $this->request->data['Agecitacalendariodia']['diario_cast_id'];
			$lunesCastId = $this->request->data['Agecitacalendariodia']['lunes_cast_id'];
			$martesCastId = $this->request->data['Agecitacalendariodia']['martes_cast_id'];
			$miercolesCastId = $this->request->data['Agecitacalendariodia']['miercoles_cast_id'];
			$juevesCastId = $this->request->data['Agecitacalendariodia']['jueves_cast_id'];
			$viernesCastId = $this->request->data['Agecitacalendariodia']['viernes_cast_id'];
			
			$sabadoCastId = $this->request->data['Agecitacalendariodia']['sabado_cast_id'];
			$domingoCastId = $this->request->data['Agecitacalendariodia']['domingo_cast_id'];
			$anio = $this->request->data['Agecitacalendario']['anio'];
			$mes=date('m',strtotime($this->request->data['Fecha']['desde']));
			$dia=date('d',strtotime($this->request->data['Fecha']['desde']));
			$this->Agecitacalendario->unbindModel(array('belongsTo'=>array('Secproject'),'hasMany'=>array('Agecitacalendariodia')));
			$TalCitaCalendar=$this->Agecitacalendario->findById($talcitacalendar_id);
			
			foreach ($this->request->data['Agecitacalendariodia'] as $item=>$valor){ //verificamos que no sobrepase los cupos permitidos en el servicio.
				if ($this->Appcast->getCantidadCitasPorPlantilla($valor)>$TalCitaCalendar['Agecitacalendario']['programadas']){
					$this->Session->setFlash(__('TALLER_NO_SE_PUEDE_GUARDAR_MAS_DE_CUPOS').$TalCitaCalendar['Agecitacalendario']['programadas']);
					return;
				}
			}
			
			if (!$lunesCastId || !$martesCastId || !$miercolesCastId || !$juevesCastId || !$viernesCastId  || !$sabadoCastId || !$domingoCastId || !$anio || !$talcitacalendar_id) {
				$this->Session->setFlash(__('GENERAL_CORRIJA_ERRORES'));
				return;
			}
			$primeraFecha = date('Y-m-d',mktime(0, 0, 0, $mes, $dia, $anio));
		
			$this->Agecitacalendariodia->begin();
			//cambiamos el estado a los calendarios anteriores
			if(!$this->Agecitacalendariodia->updateAll($talcitacalendar_id,$primeraFecha)){
				$this->Agecitacalendariodia->rollback();
				$this->Session->setFlash(__('GENERAL_ERROR_GRABACION'));
				return;
			}
			
			//generamos el Calendario de hoy para delante
			if (!$this->Agecitacalendariodia->generarCalendarioDesdeFecha($anio,$mes,$dia, $talcitacalendar_id, $lunesCastId,$martesCastId,$miercolesCastId,$juevesCastId,$viernesCastId, $sabadoCastId, $domingoCastId)){
				$this->Agecitacalendariodia->rollback();
				$this->Session->setFlash(__('GENERAL_ERROR_GRABACION'));
				return;
			}
			
			$this->Agecitacalendariodia->commit();
			$this->Session->setFlash(__('TALLER_CITACALENDARDAY_HORARIOS_GENERADOS').' '.$anio);
			$this->Session->write('actualizarPadre', true);
			//$this->redirect('comun/cerrar');
		}
	}
	
	/**
	 * NuevoRq: Ronald Tineo Santamaria
	 * correo: dinho.rhts@gmail.com
	 */
	function modificarCalendarioGeneradoFormNew($idCalendar=null) {
		$this->pageTitle = __('CITAS_TALLER_MODIFICAR_CALENDARIO_GENERADO');
		$this->layout = 'modulo_taller'.DS.'default_grid';
		
		$this->loadModel('Agecitacalendario');
		$this->loadModel('Appcast');
		$this->loadModel('Agedetallecita');
		
		$estadoActivo = 'AC';
		$citasReprogramar = array();
		// se establece la fecha y hora actual como defecto
		$this->request->data['Fecha']['desde'] =(empty($this->request->data['Fecha']['desde']))?date('d-m-Y', strtotime($this->Agecitacalendario->getFechaOneMoreDay($this->Agecitacalendario->fechaHoraActual()))):date('d-m-Y',strtotime($this->request->data['Fecha']['desde']));
		$this->request->data['Fecha']['hasta'] =(empty($this->request->data['Fecha']['hasta']))?date('d-m-Y', strtotime($this->Agecitacalendario->getFechaOneMoreDay($this->Agecitacalendario->fechaHoraActual()))):date('d-m-Y',strtotime($this->request->data['Fecha']['hasta']));
		// se obtienen los anios de los calendarios generados
		$aniosGenerados = $this->Agecitacalendario->obtenerAniosGeneradoPorCitaCalendar($idCalendar);
		if(!empty($aniosGenerados)) {
			// se obtienen las plantillas de los calendarios
			$condicion = array('Appcast.status'=>$estadoActivo);
			$casts = $this->Appcast->generateLista();
			if (!$casts) {
				$this->Session->setFlash(__('TALCITACALENDARDAYS_ETIQUETA_MODIFICAR_CALENDARIO_NO_EXISTEN_PLANTILLA'));
			}
		} else {
			$this->Session->setFlash(__('TALCITACALENDARDAYS_ETIQUETA_MODIFICAR_CALENDARIO_NO_EXISTEN_CALENDARIO'));
		}
		$this->set('casts', !empty($casts) ? $casts : array());
		$this->set('calendar', !empty($idCalendar) ? $idCalendar :null);
		$this->set('aniosGenerados', !empty($aniosGenerados) ? $aniosGenerados : array());
		$this->set('procesoTerminado',false);
		if(!empty($this->request->data['Agecitacalendario']) && !empty($this->request->data['Agecitacalendariodia'])){
			$talcitacalendar_id = $this->request->data['Agecitacalendario']['id'];
			//obteniendo las plantillas
			//se comento el diario cast id porque se va IMPLEMENTAR UNO DIARIO que comprenda de lunes a viernes,
			//$diarioCastId = $this->request->data['Agecitacalendariodia']['diario_cast_id'];
			$lunesCastId = $this->request->data['Agecitacalendariodia']['lunes_cast_id'];
			$martesCastId = $this->request->data['Agecitacalendariodia']['martes_cast_id'];
			$miercolesCastId = $this->request->data['Agecitacalendariodia']['miercoles_cast_id'];
			$juevesCastId = $this->request->data['Agecitacalendariodia']['jueves_cast_id'];
			$viernesCastId = $this->request->data['Agecitacalendariodia']['viernes_cast_id'];
			
			$sabadoCastId = $this->request->data['Agecitacalendariodia']['sabado_cast_id'];
			$domingoCastId = $this->request->data['Agecitacalendariodia']['domingo_cast_id'];
			$desde = $this->request->data['Fecha']['desde'];
			$hasta = $this->request->data['Fecha']['hasta'];
			
			if (!$lunesCastId || !$martesCastId || !$miercolesCastId || !$juevesCastId || !$viernesCastId  
				|| !$sabadoCastId || !$domingoCastId || !$desde || !$hasta || !$talcitacalendar_id) {
				$this->Session->setFlash(__('GENERAL_OBLIGATORIO_INGRESO_DATOS'));
				return;
			}
			
			$desde = date('Y-m-d',strtotime($desde));
			$hasta = date('Y-m-d',strtotime($hasta));
			
			$this->Agecitacalendario->unbindModel(array('belongsTo'=>array('Secproject'),'hasMany'=>array('Agecitacalendariodia')));
			$TalCitaCalendar=$this->Agecitacalendario->findById($talcitacalendar_id);
			
			foreach ($this->request->data['Agecitacalendariodia'] as $item=>$valor){ //verificamos que no sobrepase los cupos permitidos en el servicio.
				if ($this->Appcast->getCantidadCitasPorPlantilla($valor)>$TalCitaCalendar['Agecitacalendario']['programadas']){
					$this->Session->setFlash(__('TALLER_NO_SE_PUEDE_GUARDAR_MAS_DE_CUPOS').$TalCitaCalendar['Agecitacalendario']['programadas']);
					return;
				}
			}
			//Empezamos el proceso y boquear los registros tomados hasta cuminar con el proceso
			$this->Agecitacalendariodia->begin();
			//obtenemos en base a lo ingresado en el formulario
			//el array estructurado por dia y horas
			$agecitacalendariodias_new = $this->Agecitacalendariodia->getConstructedAgecitacalendariodiaInRange($desde,$hasta,$talcitacalendar_id,$lunesCastId,$martesCastId,$miercolesCastId,$juevesCastId,$viernesCastId, $sabadoCastId, $domingoCastId);
			//obtenemos el array que esta en la base de datos
			//con la misma estructura que el anterior array
			$agecitacalendariodias = $this->Agecitacalendariodia->getAgecitacalendariodiaInRange($desde,$hasta,$talcitacalendar_id);
			//el proceso principal que verifica si hay cambios en el horario
			//y se encarge de bloquear el dia en el cual haya cambios de horarios
			//y si es que no haya solo devuelve la diferencia de acuerdo al horario
			$result = $this->Agecitacalendariodia->procesoEditCalendario($agecitacalendariodias_new,$agecitacalendariodias);
			//pr('Calendario Nuevo');pr($agecitacalendariodias_new);
			//pr('Calendario Anriguo');pr($agecitacalendariodias);
			//pr('Resultado Proceso');pr($result);
			//todos los registros $agecitacalendariodias anteriores se tiene que cambiar de estado a 'DE'
			//pr($result);//exit();
			if(!$this->Agecitacalendariodia->updateAllNew($agecitacalendariodias,$result)){
				$this->Agecitacalendariodia->rollback();
				$this->Session->setFlash(__('GENERAL_ERROR_GRABACION'));
				$this->set('procesoTerminado',true);
				return;
			}
			
			//validamos el $result para el registro y si existe bloqueo registrar en otra tabla los bloqueos
			$result_process = $this->Agecitacalendariodia->generateCalendarAndRegisterBloqueos($result);
			if(!$result_process['Estado']){
				$this->Agecitacalendariodia->rollback();
				$this->Session->setFlash($result_process['Mensaje']);
				$this->set('procesoTerminado',true);
				return;
			}
			$citasReprogramar = $result_process['CitasReprogramar'];
			$this->set('procesoTerminado',true);
			$this->Session->setFlash($result_process['Mensaje']);
			$this->Agecitacalendariodia->commit();
		}
		$this->set('citasReprogramar',$citasReprogramar);
	}
	
	
	/**
	 * MIGRADO POR: VENTURA RUEDA JOSE ANTONIO
	 * correo: jose.antonio.ventura.rueda@hotmail.com
	 */
	function mostrarMes($anio, $mes, $citacalendarioId) {
		$this->loadModel('Agecitacalendario');
		$this->layout = 'modulo_taller'.DS.'default_grid';
		//pr('aca'); exit();
		$estadoGenerado = 'GE';
		
		//validando si el taller posee horarios
		$horarios = $this->Agecitacalendariodia->find('first',array(
			'conditions'=>array('Agecitacalendariodia.agecitacalendario_id' => $citacalendarioId),
			'recursive' => -1
		));
		if (empty($horarios)) {
			$this->Session->setFlash(__('TALLER_CITACALENDARDAY_NO_EXISTEN_CITACALENDARDAYS'));
			$this->redirect('/agecitacalendariodias/generarHorariosForm/'.$citacalendarioId);
			return;
		}

        $fechaActualDB = $this->Agecitacalendario->fechaHoraActual();
		$anioActual = date('Y', strtotime($fechaActualDB));
		$mesActual = date('n', strtotime($fechaActualDB));
		$diaActual = date('d', strtotime($fechaActualDB));
		
		$anio = $anio?$anio:$anioActual;
		$mes = $mes?$mes:$mesActual;
		
		$fechaActual = mktime(0, 0, 0, $mesActual, $diaActual, $anioActual);
        $anteriorMes = getDate(mktime(0, 0, 0, $mes-1, 1, $anio));
        $siguienteMes = getDate(mktime(0, 0, 0, $mes+1, 1, $anio));
        $anterior = array(
            'anio'=>$anteriorMes['year'],
            'mes'=>$anteriorMes['mon']
        );
        $actual = array(
            'anio'=>$anio,
            'mes'=>$mes
        );
        $siguiente = array(
            'anio'=>$siguienteMes['year'],
            'mes'=>$siguienteMes['mon']
        );

		$anteriorAppmonth = $this->Agecitacalendariodia->find('first',array(
			'conditions'=>array(
				'month(initDateTime)=month(now() - interval 1 month)',
				'Agecitacalendariodia.agecitacalendario_id' => $citacalendarioId,
				'Agecitacalendariodia.estado'=>'AC'

			),
			'recursive' => -1
		));
		if (empty($anteriorAppmonth)) $anterior = null; 

		$siguienteAppmonth = $this->Agecitacalendariodia->find('first',array(
			'conditions'=>array(
				'month(initDateTime)=month(now() + interval 1 month)',
				'Agecitacalendariodia.agecitacalendario_id' => $citacalendarioId,
				'Agecitacalendariodia.estado'=>'AC'
			),
			'recursive'=>-1
		));
		if (empty($siguienteAppmonth)) $siguiente = null;
		
		$calendarioMes = $this->Calendario->obtenerCalendarioMes($anio, $mes);
		
		// a) corregir feriados
		// b) desactivar fechas anteriores a la actual
		foreach ($calendarioMes['semanas'] as $numeroSemana=>$semana) {
			foreach ($semana as $numeroDia=>$dia) {
				if ($dia['tipo']!='vacio') {
					// a)
					$hayHorario = $this->hayHorario(
						$dia['fecha']['anio'],
						$dia['fecha']['mes'],
						$dia['fecha']['dia'],
						$citacalendarioId);
					if (!$hayHorario) {
						$calendarioMes['semanas'][$numeroSemana][$numeroDia]['tipo'] = 'feriado';
					} else if ($calendarioMes['semanas'][$numeroSemana][$numeroDia]['tipo'] == 'feriado') {
						$calendarioMes['semanas'][$numeroSemana][$numeroDia]['tipo'] = 'normal';
					}
					// b)
					$fecha = mktime(0, 0, 0, $dia['fecha']['mes'], $dia['fecha']['dia'], $dia['fecha']['anio']);
					if ($fecha<$fechaActual) {
						$calendarioMes['semanas'][$numeroSemana][$numeroDia]['tipo'] = 'inactivo';
					}
				}
			}
		}
		
		$tallerName = $this->Agecitacalendario->obtenerTaller($citacalendarioId);
		
        $this->set('anterior', $anterior);
        $this->set('siguiente', $siguiente);
        $this->set('calendarioMes', $calendarioMes);
        $this->set('talcitacalendar_id', $citacalendarioId);
        $this->set('tallerName', $tallerName);
    }
	
	/**
	 * MIGRADO POR: VENTURA RUEDA JOSE ANTONIO
	 * correo: jose.antonio.ventura.rueda@hotmail.com
	 */
	function mostrarDia($anio=null, $mes=null, $dia=null, $talcitacalendar_id=null) {
		$this->layout = 'modulo_taller'.DS.'default_grid';
		$this->loadModel('Agecitacalendario');
		
        $this->pageTitle = __('TALLER_CITACALENDARDAY_MOSTRAR_DIA');

		$this->set('talcitacalendar_id', $talcitacalendar_id);
		$this->set('anio', $anio);
		$this->set('mes', $mes);
		$this->set('dia', $dia);
		
		$listaMeses = $this->Datos->_getDato('meses'); //$this->datos('meses');
		$nombreMes = $listaMeses[$mes-1];
		
		$fecha = date('Y-m-d', mktime(0, 0, 0, $mes, $dia, $anio));
		
		$etiquetaFecha = $dia.' '.$nombreMes.' '.$anio;
		$this->set('etiquetaFecha', $etiquetaFecha);
		
		//condicion: seleccionar de la tabla la fecha que se esta eligiendo  
//        $id = $this->request->data['Agecitacalendariodia']['id'];
        $horarios = $this->Agecitacalendariodia->find('all',array(
			'conditions'=>array(
				'agecitacalendario_id'=>$talcitacalendar_id,
				'DATE(initDateTime)'=>$fecha,
				'Agecitacalendariodia.estado'=>'AC'
			),
			'fields'=>array('Agecitacalendariodia.initDateTime', 'Agecitacalendariodia.endDateTime', 'Agecitacalendariodia.programed', 'Agecitacalendariodia.available') 
		));
															
		$this->set('horarios', $horarios);
		
		$this->Appcast = new Appcast();
		$casts = $this->Appcast->find('list', array(
			'order'=>array('Appcast.description'=>'ASC'),
			'fields'=>array('Appcast.id', 'Appcast.description')
		));
		
		$casts = $casts?$casts:array();
		$this->set('casts', $casts);
		
		$tallerName = $this->Agecitacalendario->obtenerTaller($talcitacalendar_id);
		$this->set('tallerName', $tallerName);
	}
	
	 /**
	 * MIGRADO POR: VENTURA RUEDA JOSE ANTONIO
	 * correo: jose.antonio.ventura.rueda@hotmail.com
     * Permite ingresar un intervalo de horas
     Reglas: 
	 * 1. El intervalo de horas que se agregue no debe estar contenido en los intervalos ya existentes
     */  
    function agregar($anio=0, $mes=0, $dia=0, $talcitacalendar_id=0) {
    	$this->set('anio', $anio);
    	$this->set('mes', $mes);
    	$this->set('dia', $dia);
    	$this->set('talcitacalendar_id', $talcitacalendar_id);
		$this->layout = 'modulo_taller'.DS.'default_grid';
		
        $this->pageTitle = __('TALLER_CITACALENDARDAY_AGREGAR');
		
		if(empty($this->request->data['Agecitacalendariodia'])) {
			$this->cargarDatosAgregar();
			$this->request->data['Agecitacalendariodia']['agecitacalendario_id'] = $talcitacalendar_id;
			$this->request->data['Agecitacalendariodia']['anio'] = $anio;
			$this->request->data['Agecitacalendariodia']['mes'] = $mes;
			$this->request->data['Agecitacalendariodia']['dia'] = $dia;
		} else {
			$talcitacalendar_id = $this->request->data['Agecitacalendariodia']['agecitacalendario_id'];
			$anio = $this->request->data['Agecitacalendariodia']['anio'];
			$mes = $this->request->data['Agecitacalendariodia']['mes'];
			$dia = $this->request->data['Agecitacalendariodia']['dia'];
			$fecha = date('Y-m-d', mktime(0, 0, 0, $mes, $dia, $anio));
			
			$horarios = $this->Agecitacalendariodia->find('all',array(
				'conditions'=>array('agecitacalendario_id'=>$talcitacalendar_id,'DATE(initDateTime)'=>$fecha,'estado !='=>'EL'),
				'fields'=>array('Agecitacalendariodia.initDateTime', 'Agecitacalendariodia.endDateTime')
			));
			//si no se ingresaron minutos de inicio
			if(empty($this->request->data['Agecitacalendariodia']['initTimeMinute'])) { 
				$this->request->data['Agecitacalendariodia']['initTimeMinute']  = 0;
			}			
			//si no se ingresaron minutos de termino
			if(empty($this->request->data['Agecitacalendariodia']['endTimeMinute'])) { 
				$this->request->data['Agecitacalendariodia']['endTimeMinute']  = 0;
			}

			//validamos que la hora de termino sea mayor a la hora de inicio
			$initTimeNuevo = sprintf("%02d",$this->request->data['Agecitacalendariodia']['initTime']).":".
							sprintf("%02d",$this->request->data['Agecitacalendariodia']['initTimeMinute']);     
			$endTimeNuevo = sprintf("%02d",$this->request->data['Agecitacalendariodia']['endTime']).":".
							sprintf("%02d",$this->request->data['Agecitacalendariodia']['endTimeMinute']);
			
			if($endTimeNuevo <= $initTimeNuevo) {
				$this->Session->setFlash(__('CAST_TIME_END_TIME_MAYOR_INIT_TIME'));
				$this->cargarDatosAgregar();
				return;
			}
			
			//validamos que los tiempo ingresados no se encuentre dentro de otros intervalos ya existentes
			foreach($horarios as $horario) {
				$initTimeExistente = date('h:i', strtotime($horario['Agecitacalendariodia']['initDateTime']));
				$endTimeExistente = date('h:i', strtotime($horario['Agecitacalendariodia']['endDateTime']));

				// el nuevo intervalo no debe estar dentro de ningún intervalo
				if(($initTimeNuevo >= $initTimeExistente) && ($endTimeNuevo <= $endTimeExistente)) {
					$this->Session->setFlash(__('CAST_TIME_INTERVALO_EN_INTERVALO'));
					$this->cargarDatosAgregar();
					return;
				} 
				
				// ningún intervalo debe estar dentro del nuevo intervalo
				if(($initTimeNuevo <= $initTimeExistente) && ($endTimeNuevo >= $endTimeExistente)) {
					$this->Session->setFlash(__('CAST_TIME_INTERVALO_EN_INTERVALO'));
					$this->cargarDatosAgregar();
					return;
				} 
				
				//la hora de inicio del nuevo intervalo no se debe encontrar en ningún otro intervalo
				if(($initTimeNuevo >= $initTimeExistente) && ($initTimeNuevo <= $endTimeExistente)) {
					$this->Session->setFlash(__('CAST_TIME_INIT_TIME_YA_EXISTE_EN_INTERVALO'));
					$this->cargarDatosAgregar();
					return;
				}
				
				//la hora de termino del nuevo intervalo no se debe encontrar en ningún otro intervalo
				if(($endTimeNuevo >= $initTimeExistente) && ($endTimeNuevo <= $endTimeExistente)) {
					$this->Session->setFlash(__('CAST_TIME_END_TIME_YA_EXISTE_EN_INTERVALO'));
					$this->cargarDatosAgregar();
					return;
				}
			}
			
			$this->request->data['Agecitacalendariodia']['initDateTime'] = $fecha.' '.$initTimeNuevo;
			$this->request->data['Agecitacalendariodia']['endDateTime'] = $fecha.' '.$endTimeNuevo;
			$this->request->data['Agecitacalendariodia']['available'] = $this->request->data['Agecitacalendariodia']['programed'];
			
			if ($this->Agecitacalendariodia->save($this->request->data['Agecitacalendariodia'])) {
				$this->Session->setFlash(__('GENERAL_REGISTRO_AGREGADO'));
				$this->Session->write('actualizarPadre',true);
				$horarioAgregarUrl = "/agecitacalendariodias/agregar/".$anio.'/'.$mes.'/'.$dia.'/'.$talcitacalendar_id;
				$this->redirect($horarioAgregarUrl);
			} else {
				$this->cargarDatosAgregar();
				$this->Session->setFlash(__('GENERAL_CORRIJA_ERRORES'));
			}
			$this->cargarDatosAgregar();
		}
    }
	
	/**
	 * MIGRADO POR: VENTURA RUEDA JOSE ANTONIO
	 * correo: jose.antonio.ventura.rueda@hotmail.com
	 */
	function cargarDatosAgregar() {
		$horas = $this->Datos->_getDato('horas');
		$minutos = $this->Datos->_getDato('minutos');
		$this->set('horas', $horas);
		$this->set('minutos', $minutos);
	}
	
	/**
	 * MIGRADO POR: VENTURA RUEDA JOSE ANTONIO
	 * correo: jose.antonio.ventura.rueda@hotmail.com
	* Elimina el intervalo de horas programadas
    * Reglas: 
    * * 1. Para poder eliminar la cantidad de horas disponibles y programadas deben ser iguales
	 * @param string $id : id del intervalo de horas que se desea eliminar
     */
	function eliminar($id) {	
		$item = $this->Agecitacalendariodia->findById($id);
		$personId = $item['Agecitacalendariodia']['agecitacalendario_id'];
		$fecha = strtotime($item['Agecitacalendariodia']['initDateTime']);
		$anio = date('Y', $fecha);
		$mes = date('m', $fecha);
		$dia = date('d', $fecha);
		$talcitacalendar_id = $item['Agecitacalendariodia']['agecitacalendario_id'];
		
		if (!$id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}else{
			//Si existen datos asociados no se puede eliminar---Miguel Chiuyari
			$existAgedetallecita= $this->Agecitacalendariodia->Agedetallecita->find('count', array('conditions' => array('Agedetallecita.estado' => 'AC', 'Agedetallecita.agecitacalendariodia_id'=>$id)));
			if($existAgedetallecita)	
				{
				$this->Session->setFlash(__('GENERALES_REGISTRO_ASOCIADO',true));				
				}
				else{					
					if ($this->Agecitacalendariodia->delete($id)) {
						$this->Session->setFlash(__('GENERAL_REGISTRO_ELIMINADO'));
					} else {
						$this->Session->setFlash(__('GENERAL_ERROR_GRABACION'));
					}
				}
			}
			
		$url = '/agecitacalendariodias/mostrarDia/'.$anio.'/'.$mes.'/'.$dia.'/'.$talcitacalendar_id;
		$this->redirect($url);
	}
	
	/**
	 * MIGRADO POR: VENTURA RUEDA JOSE ANTONIO
	 * correo: jose.antonio.ventura.rueda@hotmail.com
	 */
	function generarHorariosForm($talcitacalendar_id=null) {
		$this->loadModel('Appcast');
		$this->loadModel('Agecitacalendario');
		
		$this->layout = 'modulo_taller'.DS.'default_grid';
		
		$casts=$this->Appcast->generateLista();
		if (!$casts) {
			$casts = array();
			$this->Session->setFlash(__('TALLER_CITACALENDARDAY_NO_EXISTEN_CALENDARIOS'));
		}
		$this->set('casts', $casts);
		$this->set('guardado',0);
		if (empty($this->request->data)) {
			$this->request->data['Agecitacalendario']['id'] = $talcitacalendar_id;
			$horario = $this->Agecitacalendariodia->find('first',array(
				'conditions'=>array('Agecitacalendariodia.agecitacalendario_id' => $talcitacalendar_id,
									'Agecitacalendariodia.estado'=>'AC'), 
				'order'=>array('Agecitacalendariodia.initDateTime'=>'DESC')
			));
			if ($horario) {
				$anio = 1 + date('Y',strtotime($horario['Agecitacalendariodia']['initDateTime']));
			} else {
				$anio = date('Y',strtotime($this->Agecitacalendariodia->fechaHoraActual()));
			}
			$this->request->data['Agecitacalendario']['anio'] = $anio;
			
			// lista de plantillas

		}
		else{
			$talcitacalendar_id = $this->request->data['Agecitacalendario']['id'];
			//obteniendo las plantillas
			//se comento el diario cast id porque se va IMPLEMENTAR UNO DIARIO que comprenda de lunes a viernes,
			//$diarioCastId = $this->request->data['Agecitacalendariodia']['diario_cast_id'];
			$lunesCastId = $this->request->data['Agecitacalendariodia']['lunes_cast_id'];
			$martesCastId = $this->request->data['Agecitacalendariodia']['martes_cast_id'];
			$miercolesCastId = $this->request->data['Agecitacalendariodia']['miercoles_cast_id'];
			$juevesCastId = $this->request->data['Agecitacalendariodia']['jueves_cast_id'];
			$viernesCastId = $this->request->data['Agecitacalendariodia']['viernes_cast_id'];
			
			$sabadoCastId = $this->request->data['Agecitacalendariodia']['sabado_cast_id'];
			$domingoCastId = $this->request->data['Agecitacalendariodia']['domingo_cast_id'];
			$anio = $this->request->data['Agecitacalendario']['anio'];
			
			$this->Agecitacalendario->unbindModel(array('belongsTo'=>array('Secproject'),'hasMany'=>array('Agecitacalendariodia')));
			$TalCitaCalendar=$this->Agecitacalendario->findById($talcitacalendar_id);
			foreach ($this->request->data['Agecitacalendariodia'] as $item=>$valor){ //verificamos que no sobrepase los cupos permitidos en el servicio.
				if ($this->Appcast->getCantidadCitasPorPlantilla($valor)>$TalCitaCalendar['Agecitacalendario']['programadas']){
					$this->Session->setFlash(__('TALLER_NO_SE_PUEDE_GUARDAR_MAS_DE_CUPOS').$TalCitaCalendar['Agecitacalendario']['programadas']);
					return;
				}
			}
			
			if (!$lunesCastId || !$martesCastId || !$miercolesCastId || !$juevesCastId || !$viernesCastId  || !$sabadoCastId || !$domingoCastId || !$anio || !$talcitacalendar_id) {
				$this->Session->setFlash(__('GENERAL_CORRIJA_ERRORES'));
				return;
			}
			
			$this->Agecitacalendariodia->begin();
			
			if (!$this->Agecitacalendariodia->generarCalendario($anio, $talcitacalendar_id, $lunesCastId,$martesCastId,$miercolesCastId,$juevesCastId,$viernesCastId, $sabadoCastId, $domingoCastId)){
				$this->Agecitacalendariodia->rollback();
				$this->Session->setFlash(__('GENERAL_ERROR_GRABACION'));
				return;
			}
			
			$this->Agecitacalendariodia->commit();
			$this->Session->setFlash(__('TALLER_CITACALENDARDAY_HORARIOS_GENERADOS').' '.$anio);
			$this->Session->write('actualizarPadre', true);
			$this->set('guardado',1);
		}
	}
}	