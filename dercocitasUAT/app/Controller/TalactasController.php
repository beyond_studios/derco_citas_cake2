<?php
class TalactasController extends AppController {
	public $name = 'Talactas';
  	public $helpers = array('Html', 'Xhtml', 'Form', 'Idioma');  // 'fpdf'
    
    var $listaFinalPDF;// usado al exportar en pdf
	
    function agregar($ot){
    	$this->layout = 'aventura';
   		$this->set('ot',$ot);
		
		//MODELOS UTILIZADOS	
		$this->loadModel('Talot');
		$this->Talot = new Talot;
				
   		$tiposActas = array();
   		$this->set('tiposActas',$tiposActas);
		
		$horas = $this->Datos->_getDato('horas');
		$this->set('horas',$horas);
		
		$minutos = array('00'=>'00',15=>15,30=>30,45=>45);			
		$this->set('minutos',$minutos);
		
		//MODIFICADO POR RTINEO
		$dtLog = $this->_getDtLg();
		$secperson_id  = $dtLog['Secperson']['id'];
		
		if(!empty($this->request->data)){		
			$talot = $this->Talot->findByNrocars($ot);
			$this->request->data['Talacta']['talot_id'] = $talot['Talot']['id'];
			$rpta = $this->Talacta->setGuardarActa($this->request->data,$secperson_id);
			if($rpta){				
				$this->Session->write('actualizarPadre', true);
				$this->redirect('/talactas/view/'.$ot);
			}else{
				 $this->Session->setFlash(__('GENERAL_ERROR_GRABACION'),empty($talot)?'flash_failure':'flash_success');
			}
		}
		
		//AVENTURA //RECUPERO LAS ACTAS
		$this->Talot->recursive = -1; 
		$talot = $this->Talot->findByNrocars($ot);
		$actas = '';
		if(!empty($talot))
			$actas = $this->Talacta->getTalactaByOT($talot['Talot']['id']);

		$this->set('actas',$actas);
		$this->set('talot',$talot);
   }
   
   function view($nro_cars){
    	$this->layout = 'aventura';
		
   		//MODELOS UTILIZADOS	
		$this->LoadModel('Talot');
		
   		//RECUPERO LAS ACTAS
		$this->Talot->recursive = -1; 
		$talot = $this->Talot->findByNrocars($nro_cars);
		$actas = '';
		if(!empty($talot))
			$actas = $this->Talacta->getTalactaByOT($talot['Talot']['id']);
		
		$this->set('actas',$actas);
		$this->set('talot',$talot);
   }
}