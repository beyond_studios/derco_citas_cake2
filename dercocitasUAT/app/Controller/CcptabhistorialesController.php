<?php
class CcptabhistorialesController extends AppController {
	public $name='Ccptabhistoriales';
	var $listaFinalPDF = array();
	
	
    public function beforeFilter() {
        parent::beforeFilter();
		//permite ver las funciones del controlador sin tener permisos
        //$this->Auth->allow();//descomentar esta linea para hacer pruebas 
		$this->loadmodel('Ccptabestado');
		$this->loadmodel('Ccpsolicitudservicio');
    }
	
	 public function indexAsistente($exportar=null,$parametros=null){
		$this->loadModel('SistemaPlanificadorCcp');
		$this->loadModel('Secproject');
		
		$estadoCcp="7,11,21,22";//lavado y ccasesor 10,11,21,22 //anteioerior 11,14,15
		$estadoEliminado = 'EL'; $estadoActivo = 'AC';
		$dtLg = $this->_getDtLg();
		$data = empty($exportar)?$this->request->query:$parametros;
		
		$this->set('data',$data);
		
		//RECUPERO EL ID DE LA SUCURSAR SEGUN CARS
		$project = $this->Secproject->findById($dtLg['Secproject']['id'], null,null,-1);
		$dtLg['Secproject']['carscod'] = $project['Secproject']['carscod'];		
		
		// RECUPERAMOS las CONDICIONES
		$cnd = $this->SistemaPlanificadorCcp->cndBsc_getTalotCarsAsistente($data, $dtLg);		
//		debug($cnd);
		
		$this->paginate = array(
			'SistemaPlanificadorCcp'=>array('limit' => 10,
				'page' => 1,
				'order' => array ('SistemaPlanificadorCcp.ot' => 'ASC'),
				'conditions' => $cnd
			)
		); 
		
		$otsCars = empty($exportar)?$this->paginate('SistemaPlanificadorCcp')
			:$this->SistemaPlanificadorCcp->find('all',array('conditions'=>$cnd, 'order'=>array('SistemaPlanificadorCcp.OT' => 'ASC')));

		$otsCars = $this->SistemaPlanificadorCcp->getTalotCarsAsistente($otsCars);
		$this->set('otsCars',$otsCars);

		$this->listaFinalPDF = isset($otsCars) && !empty($otsCars) ? $otsCars : array();
		
		//RECUPERAMOS LOS COMBOS PARA EL BUSCADOR
		$estadosOtCars = $this->Datos->_getDato('estadosOtCarsCcp');
		$estadosOtCarsTooltip = $this->Datos->_getDato('estadosOtCars');
		$estadosOtCarsSelect = empty($estadosOtCars)?array(''=>__('Seleccionar')):array_merge(array(''=>__('Seleccionar')),$estadosOtCars);
			 		

		//obtenemos los estados de las ots en ccp
		$estadosCcp=$this->Ccptabestado->getEstados();
		$estadosCcpSelect =(!empty($estadosCcp) && isset($estadosCcp))?array(''=>'Seleccionar')+$estadosCcp:array(''=>'Seleccionar');
		
		$this->set('estadosOtCarsTooltip',$estadosOtCarsTooltip);				
		$this->set('estadosCcpSelect', $estadosCcpSelect);
	}
	
	/**migracion de bandeja del responsable de deposito
 	*  autor: Miguel Chiuyari 
	*  MODIFICADO POR: VENTURA RUEDA, JOSE ANTONIO 
	*  FECHA: 2013-05-14 
 	*/	
	function indexResponsableDeposito($exportar=null,$parametros=null){
		$this->loadModel('SistemaPlanificadorCcp');
		$this->loadModel('Secproject');
		
		$estadoCcp="1,2,4,13,21,22,7";//lavado y ccasesor
		$estadoEliminado = 'EL'; $estadoActivo = 'AC';
		$dtLg = $this->_getDtLg();
		
		$data = empty($exportar)?$this->request->query
			:$parametros;
		
		$this->set('data',$data);

		//RECUPERO EL ID DE LA SUCURSAR SEGUN CARS
		$project = $this->Secproject->findById($dtLg['Secproject']['id'], null,null,-1);
		$dtLg['Secproject']['carscod'] = $project['Secproject']['carscod'];
		
		// RECUPERAMOS las CONDICIONES
		$cnd = $this->SistemaPlanificadorCcp->cndBsc_getTalotCarsResponsableDeposito($data, $dtLg);		
//		debug($cnd);
		$this->paginate = array(
			'SistemaPlanificadorCcp'=>array('limit' => 10,
				'page' => 1,
				'order' => array ('SistemaPlanificadorCcp.OT' => 'ASC'),
				'conditions' => $cnd
			)
		); 
		
		$otsCars = empty($exportar)?$this->paginate('SistemaPlanificadorCcp')
			:$this->SistemaPlanificadorCcp->find('all',array('conditions'=>$cnd, 'order'=>array('SistemaPlanificadorCcp.OT' => 'ASC')));
		$otsCars = $this->SistemaPlanificadorCcp->getTalotCarsResponsableDeposito($otsCars);
		$this->set('otsCars',$otsCars);		
		
		$this->listaFinalPDF = isset($otsCars) && !empty($otsCars) ? $otsCars : array();
		
		//obtenemos los estados de las ots de cars
		$estadosOtCars = $this->Datos->_getDato('estadosOtCarsCcp');
		$estadosOtCarsTooltip = $this->Datos->_getDato('estadosOtCars');
		//no debuelve lo q quiero
		$estadosOtCarsSelect = empty($estadosOtCars)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$estadosOtCars;
		$this->set('estadosOtCarsTooltip',$estadosOtCarsTooltip);	 		

		//obtenemos los estados de las ots en ccp
		$estadosCcp=$this->Ccptabestado->getEstados();
		$estadosCcpSelect =(!empty($estadosCcp) && isset($estadosCcp))?array(''=>'Seleccionar')+$estadosCcp:array(''=>'Seleccionar');
						
		$this->set('estadosCcpSelect', $estadosCcpSelect);
	}

	public function indexResponsableDepositoExportarExcel() {
		$this->layout = 'excel_db';
	
		$this->indexResponsableDeposito($exportar=1,$parametros=$this->request->query);
		header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=indexResponsableDeposito.xls");
		
		$reporte = $this->listaFinalPDF;
		$this->set('reporte',$reporte);
	}

	public function indexExportarExcel() {
		$this->layout = 'excel_db';
		
		$this->index($exportar=1,$parametros=$this->request->query);
		header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=index.xls");
		
		$reporte = $this->listaFinalPDF;
		$this->set('reporte',$reporte);
	}
	
	/*
 	*autor: Rodrigo Beteta 
	*migracion de bandeja de probador
 	*/	
	public function indexProbador($exportar=null,$parametros=null){
		$this->loadModel('SistemaPlanificadorCcp');
		$this->loadModel('Secproject');
		
		$estadoCcp="5,6,12";		//lavado y ccasesor 10,11,21,22 //anteioerior 11,14,15
		$estadoEliminado = 'EL'; $estadoActivo = 'AC';
		$dtLg = $this->_getDtLg();
		$data = empty($exportar)?$this->request->query:$parametros;
		
		$this->set('data',$data);
		
		//RECUPERO EL ID DE LA SUCURSAR SEGUN CARS
		$project = $this->Secproject->findById($dtLg['Secproject']['id'], null,null,-1);
		$dtLg['Secproject']['carscod'] = $project['Secproject']['carscod'];		
		
		// RECUPERAMOS las CONDICIONES
		$cnd = $this->SistemaPlanificadorCcp->cndBsc_getTalotCarsProbador($data, $dtLg);		
//		debug($cnd);
		$this->paginate = array(
			'SistemaPlanificadorCcp'=>array('limit' => 10,
				'page' => 1,
				'order' => array ('SistemaPlanificadorCcp.OT' => 'ASC'),
				'conditions' => $cnd
			)
		); 
		
		$otsCars = empty($exportar)?$this->paginate('SistemaPlanificadorCcp')
			:$this->SistemaPlanificadorCcp->find('all',array('conditions'=>$cnd, 'order'=>array('SistemaPlanificadorCcp.OT' => 'ASC')));

		$otsCars = $this->SistemaPlanificadorCcp->getTalotCarsProbador($otsCars);
		$this->set('otsCars',$otsCars);
		//pr($otsCars);
		$this->listaFinalPDF = isset($otsCars) && !empty($otsCars) ? $otsCars : array();
		
		//RECUPERAMOS LOS COMBOS PARA EL BUSCADOR
		$estadosOtCars = $this->Datos->_getDato('estadosOtCarsCcp');
		$estadosOtCarsTooltip = $this->Datos->_getDato('estadosOtCars');
		$estadosOtCarsSelect = empty($estadosOtCars)?array(''=>__('Seleccionar')):array_merge(array(''=>__('Seleccionar')),$estadosOtCars);
			 		

		//obtenemos los estados de las ots en ccp
		$estadosCcp=$this->Ccptabestado->getEstados();
		$estadosCcpSelect =(!empty($estadosCcp) && isset($estadosCcp))?array(''=>'Seleccionar')+$estadosCcp:array(''=>'Seleccionar');
		
		$this->set('estadosOtCarsTooltip',$estadosOtCarsTooltip);				
		$this->set('estadosCcpSelect', $estadosCcpSelect);
	}
	
	public function indexProbadorExportarExcel() {
		$this->layout = 'excel_db';
		
		$this->indexProbador($exportar=1,$parametros=$this->request->query);
		header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=indexProbadorExportarExcel.xls");
		
		$reporte = $this->listaFinalPDF;
		$this->set('reporte',$reporte);
	}
	
	 public function pasarEntregadofile($ot_numero){
	 	$this->layout = 'garantias';
		
        $this->loadModel('Ccptabhistorialdocumento');
		
		$estadoPasarA=array(1=>'Entregado File'); //estado a cambiar
		$estadoAnterior=array();
		$estadoCCPOT=$this->Ccptabhistorialdocumento->obtenerEstadoCcpPotNumeroOt($ot_numero);
		if(!empty($estadoCCPOT) && isset($estadoCCPOT)){
			$estadoAnterior[$estadoCCPOT[0]['Ccptabestadodocumento']['id']]=$estadoCCPOT[0]['Ccptabestadodocumento']['descripcion'];
		}else $estadoAnterior=array();
		$this->set('guardado',0);
		
		//logica de grabacion
		$secperson=$this->_getDtLg('personId');
		$secperson_id=$secperson['Secperson']['id'];
		
		if(!empty($this->request->data) && isset($this->request->data)){
			$estadoInicial=(!empty($this->request->data['Ccptabhistorialdocumento']['ccptabestado_anterior']) && isset($this->request->data['Ccptabhistorialdocumento']['ccptabestado_anterior']))?$this->request->data['Ccptabhistorialdocumento']['ccptabestado_anterior']:0;
			$estadoFinal=$this->request->data['Ccptabhistorialdocumento']['ccptabestadodocumento_id'];
			$comentario=$this->request->data['Ccptabhistorialdocumento']['comentario'];
			$ot_numero=$this->request->data['Ccptabhistorialdocumento']['ot_numero'];
			$this->Ccptabhistorialdocumento->begin();
			$respuesta=$this->Ccptabhistorialdocumento->cambiarEstado($estadoInicial,$estadoFinal,$ot_numero,$comentario,$secperson_id);
			if($respuesta['respuesta']){
				$this->Ccptabhistorialdocumento->commit();
				$this->set('guardado',1);
			}else{
				$this->Ccptabhistorialdocumento->rollback();
			}
			$this->Session->setFlash($respuesta['msg'],empty($respuesta['respuesta'])?'flash_failure':'flash_success');
		}
		//fin de logica
		
		$this->set('estadoAnterior',$estadoAnterior);
		$this->set('estadoPasarA',$estadoPasarA);
		$this->set('ot_numero',$ot_numero);

	}
	
	
    public function observarfile($ot_numero){
    	$this->layout = 'garantias';
		
		$this->loadModel('Ccptabhistorialdocumento');
		$this->loadModel('Talacta');
		$this->loadModel('Talot');
        
		$estadoPasarA=array(2=>'File Observado'); //estado a cambiar
		$estadoAnterior=array();
		$estadoCCPOT=$this->Ccptabhistorialdocumento->obtenerEstadoCcpPotNumeroOt($ot_numero);
		
		if(!empty($estadoCCPOT) && isset($estadoCCPOT)){
			$estadoAnterior[$estadoCCPOT[0]['Ccptabestadodocumento']['id']]=$estadoCCPOT[0]['Ccptabestadodocumento']['descripcion'];
		}else $estadoAnterior=array();
		$this->set('guardado',0);
		
		//logica de grabacion
		$secperson=$this->_getDtLg('personId');
		$secperson_id=$secperson['Secperson']['id'];
		
		if(!empty($this->request->data) && isset($this->request->data)){
			$estadoInicial=(!empty($this->request->data['Ccptabhistorialdocumento']['ccptabestado_anterior']) && isset($this->request->data['Ccptabhistorialdocumento']['ccptabestado_anterior']))?$this->request->data['Ccptabhistorialdocumento']['ccptabestado_anterior']:0;
			$estadoFinal=$this->request->data['Ccptabhistorialdocumento']['ccptabestadodocumento_id'];
			$ot_numero=$this->request->data['Ccptabhistorialdocumento']['ot_numero'];
			$comentario=$this->request->data['Ccptabhistorialdocumento']['comentario'];// es el primer comentario
			$comentarioActa = $this->request->data['Ccptabhistorialdocumento']['comentario'];
			//recorremos el array para armar los siguientes comentarios
			$historialComentarios = !empty($this->request->data['Ccptabhistorialcomentario']) && isset($this->request->data['Ccptabhistorialcomentario'])?$this->request->data['Ccptabhistorialcomentario']:array();
			if(!empty($historialComentarios) && isset($historialComentarios)){
				foreach($this->request->data['Ccptabhistorialcomentario'] as $id => $item){
					$comentario .= '<br>'.$item['comentario'];
				}
			}

			$this->Ccptabhistorialdocumento->begin();
			$respuesta1= array(0=>true,1=>'');	
			$respuesta=$this->Ccptabhistorialdocumento->cambiarEstado($estadoInicial,$estadoFinal,$ot_numero,$comentario,$secperson_id);
			if($respuesta['respuesta']){
				//guardamos en talacta
				$respuesta1=$this->Talacta->guardarActaMultiple($historialComentarios,$comentarioActa,$ot_numero,$secperson_id);
				if($respuesta1[0]){
					$this->Ccptabhistorialdocumento->commit();
					$this->set('guardado',1);
					$this->Session->write('actualizarPadre',true);
					$this->redirect('/talactas/view/'.$ot_numero);					
				}else $this->Ccptabhistorialdocumento->rollback();
			}else{
				$this->Ccptabhistorialdocumento->rollback();
			}
			$respuestaUnica = $respuesta['msg'].(!empty($respuesta1[1])?' y '.$respuesta1[1]:'');
			$this->Session->setFlash($respuestaUnica,empty($respuesta['respuesta'])?'flash_failure':'flash_success');
		}
		//fin de logica
		
		$this->set('estadoAnterior',$estadoAnterior);
		$this->set('estadoPasarA',$estadoPasarA);
		$this->set('ot_numero',$ot_numero);

		//RECUPERO LAS ACTAS
		$this->Talot->recursive = -1; 
		$talot = $this->Talot->findByNrocars($ot_numero);
		$actas = '';
		if(!empty($talot))
			$actas = $this->Talacta->getTalactaByOT($talot['Talot']['id']);

		$this->set('actas',$actas);
	}

    public function indexAsistenteExportarExcel() {
		$this->layout = 'excel_db';
	
		$this->indexAsistente($exportar=1,$parametros=$this->request->query);
		header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=indexAsistente.xls");
		
		$reporte = $this->listaFinalPDF;
		$this->set('reporte',$reporte);
	}
	
	public function reversar($ot_numero){
		$this->layout='contenido';
		$estadoPasarA=array(1=>'Pendiente'); //estado a cambiar
		$estadoAnterior=array();
		$estadoCCPOT=$this->Ccptabhistorial->obtenerEstadoCcpPotNumeroOt($ot_numero);
		if(!empty($estadoCCPOT) && isset($estadoCCPOT)){
			$estadoAnterior[$estadoCCPOT[0]['Ccptabestado']['id']]=$estadoCCPOT[0]['Ccptabestado']['descripcion'];
		}else $estadoAnterior=array(1=>'Pendiente');
		$this->set('guardado',0);
		//validar estado anterior
		if($this->Ccptabhistorial->validarEstadoAnterior($ot_numero,13)){
			$estadoPasarA=array(13=>'Ampliacion');	
		}		
		//logica de grabacion
		//$secperson_id=$this->login('personId');
		$secperson=$this->_getDtLg('personId');
		$secperson_id=$secperson['Secperson']['id'];
		
		if(!empty($this->request->data) && isset($this->request->data)){
			$estadoInicial=$this->request->data['Ccptabhistorial']['ccptabestado_anterior'];
			$estadoFinal=$this->request->data['Ccptabhistorial']['ccptabestado_id'];
			$comentario=$this->request->data['Ccptabhistorial']['comentario'];
			//$ot_numero=$this->request->data['Ccptabhistorial']['ot_numero'];
			$id_solicitud = $this->Ccptabhistorial->getSolitudServicioId($ot_numero);

			$this->Ccptabhistorial->begin();			
			$respuesta=$this->Ccptabhistorial->cambiarEstado($estadoInicial,$estadoFinal,$ot_numero,$comentario,$secperson_id,$id_solicitud);
			if($respuesta['respuesta']){
				$this->Ccptabhistorial->commit();
				$this->set('guardado',1);
			}else{
				$this->Ccptabhistorial->rollback();
			}
			$this->Session->setFlash(__($respuesta['msg'], true),'flash_success');
		}
		//fin de logica
				
		$this->set('estadoAnterior',$estadoAnterior);
		$this->set('estadoPasarA',$estadoPasarA);
		$this->set('ot_numero',$ot_numero);

	}

	public function enviarDeposito($ot_numero){
		$this->layout='contenido';
		$estadoPasarA=array(2=>'En Deposito'); //estado a cambiar
		$estadoAnterior=array();
		$estadoCCPOT=$this->Ccptabhistorial->obtenerEstadoCcpPotNumeroOt($ot_numero);
		if(!empty($estadoCCPOT) && isset($estadoCCPOT)){
			$estadoAnterior[$estadoCCPOT[0]['Ccptabestado']['id']]=$estadoCCPOT[0]['Ccptabestado']['descripcion'];
		}else $estadoAnterior=array(1=>'Pendiente');
		$this->set('guardado',0);
		
		//logica de grabacion
		//$secperson_id=$this->login('personId');
		$secperson=$this->_getDtLg('personId');
		$secperson_id=$secperson['Secperson']['id'];
		if(!empty($this->request->data) && isset($this->request->data)){
			$estadoInicial=$this->request->data['Ccptabhistorial']['ccptabestado_anterior'];
			$estadoFinal=$this->request->data['Ccptabhistorial']['ccptabestado_id'];
			$comentario=$this->request->data['Ccptabhistorial']['comentario'];
			//$ot_numero=$this->request->data['Ccptabhistorial']['ot_numero'];
			$id_solicitud = $this->Ccptabhistorial->getSolitudServicioId($ot_numero);
			$this->Ccptabhistorial->begin();			
			$respuesta=$this->Ccptabhistorial->cambiarEstado($estadoInicial,$estadoFinal,$ot_numero,$comentario,$secperson_id,$id_solicitud);
			if($respuesta['respuesta']){
				$this->Ccptabhistorial->commit();
				$this->set('guardado',1);
			}else{
				$this->Ccptabhistorial->rollback();
			}
			$this->Session->setFlash(__($respuesta['msg'], true),'flash_success');
		}
		//fin de logica
		
		$this->set('estadoAnterior',$estadoAnterior);
		$this->set('estadoPasarA',$estadoPasarA);
		$this->set('ot_numero',$ot_numero);
	}

	function iniciarAbp($ot_numero){
		$this->layout='contenido';
		$estadoPasarA=array(5=>'Inicio ABP'); //estado a cambiar
		$estadoAnterior=array();
		$estadoCCPOT=$this->Ccptabhistorial->obtenerEstadoCcpPotNumeroOt($ot_numero);
		if(!empty($estadoCCPOT) && isset($estadoCCPOT)){
			$estadoAnterior[$estadoCCPOT[0]['Ccptabestado']['id']]=$estadoCCPOT[0]['Ccptabestado']['descripcion'];
		}else $estadoAnterior=array(1=>'Pendiente');
		$this->set('guardado',0);
		
		//logica de grabacion
		//$secperson_id=$this->login('personId');
		$secperson=$this->_getDtLg('personId');
		$secperson_id=$secperson['Secperson']['id'];
		if(!empty($this->request->data) && isset($this->request->data)){
			$estadoInicial=$this->request->data['Ccptabhistorial']['ccptabestado_anterior'];
			$estadoFinal=$this->request->data['Ccptabhistorial']['ccptabestado_id'];
			$comentario=$this->request->data['Ccptabhistorial']['comentario'];
			//$ot_numero=$this->request->data['Ccptabhistorial']['ot_numero'];
			$id_solicitud = $this->Ccptabhistorial->getSolitudServicioId($ot_numero);
			$this->Ccptabhistorial->begin();			
			$respuesta=$this->Ccptabhistorial->cambiarEstado($estadoInicial,$estadoFinal,$ot_numero,$comentario,$secperson_id,$id_solicitud);
			if($respuesta['respuesta']){
				$this->Ccptabhistorial->commit();
				$this->set('guardado',1);
			}else{
				$this->Ccptabhistorial->rollback();
			}
			$this->Session->setFlash(__($respuesta['msg'], true),'flash_success');
		}
		//fin de logica
		
		$this->set('estadoAnterior',$estadoAnterior);
		$this->set('estadoPasarA',$estadoPasarA);
		$this->set('ot_numero',$ot_numero);

	}

	public function reprogramar($ot_numero){
		$this->layout='contenido';
		$estadoPasarA=array(12=>'Reprogramado'); //estado a cambiar
		$estadoAnterior=array();
		$estadoCCPOT=$this->Ccptabhistorial->obtenerEstadoCcpPotNumeroOt($ot_numero);
		if(!empty($estadoCCPOT) && isset($estadoCCPOT)){
			$estadoAnterior[$estadoCCPOT[0]['Ccptabestado']['id']]=$estadoCCPOT[0]['Ccptabestado']['descripcion'];
		}else $estadoAnterior=array(1=>'Pendiente');
		$this->set('guardado',0);
		
		//logica de grabacion
		//$secperson_id=$this->login('personId');
		$secperson=$this->_getDtLg('personId');
		$secperson_id=$secperson['Secperson']['id'];
		if(!empty($this->request->data) && isset($this->request->data)){
			$estadoInicial=$this->request->data['Ccptabhistorial']['ccptabestado_anterior'];
			$estadoFinal=$this->request->data['Ccptabhistorial']['ccptabestado_id'];
			$comentario=$this->request->data['Ccptabhistorial']['comentario'];
			//$ot_numero=$this->request->data['Ccptabhistorial']['ot_numero'];
			$id_solicitud = $this->Ccptabhistorial->getSolitudServicioId($ot_numero);
			$this->Ccptabhistorial->begin();			
			$respuesta=$this->Ccptabhistorial->cambiarEstado($estadoInicial,$estadoFinal,$ot_numero,$comentario,$secperson_id,$id_solicitud);
			if($respuesta['respuesta']){
				$this->Ccptabhistorial->commit();
				$this->set('guardado',1);
			}else{
				$this->Ccptabhistorial->rollback();
			}
			$this->Session->setFlash(__($respuesta['msg'], true),'flash_success');
		}
		//fin de logica
		
		$this->set('estadoAnterior',$estadoAnterior);
		$this->set('estadoPasarA',$estadoPasarA);
		$this->set('ot_numero',$ot_numero);

	}

	public function pendienteEntrega($ot_numero){
		$this->layout='contenido';
		$estadoPasarA=array(21=>'Pendiente de Entrega'); //estado a cambiar
		$estadoAnterior=array();
		$estadoCCPOT=$this->Ccptabhistorial->obtenerEstadoCcpPotNumeroOt($ot_numero);
		if(!empty($estadoCCPOT) && isset($estadoCCPOT)){
			$estadoAnterior[$estadoCCPOT[0]['Ccptabestado']['id']]=$estadoCCPOT[0]['Ccptabestado']['descripcion'];
		}else $estadoAnterior=array(1=>'Pendiente');
		$this->set('guardado',0);
		
		//logica de grabacion
		//$secperson_id=$this->login('personId');
		$secperson=$this->_getDtLg('personId');
		$secperson_id=$secperson['Secperson']['id'];
		if(!empty($this->request->data) && isset($this->request->data)){
			$estadoInicial=$this->request->data['Ccptabhistorial']['ccptabestado_anterior'];
			$estadoFinal=$this->request->data['Ccptabhistorial']['ccptabestado_id'];
			$comentario=$this->request->data['Ccptabhistorial']['comentario'];
			//$ot_numero=$this->request->data['Ccptabhistorial']['ot_numero'];
			$id_solicitud = $this->Ccptabhistorial->getSolitudServicioId($ot_numero);
			$this->Ccptabhistorial->begin();			
			$respuesta=$this->Ccptabhistorial->cambiarEstado($estadoInicial,$estadoFinal,$ot_numero,$comentario,$secperson_id,$id_solicitud);
			if($respuesta['respuesta']){
				$this->Ccptabhistorial->commit();
				$this->set('guardado',1);
			}else{
				$this->Ccptabhistorial->rollback();
			}
			$this->Session->setFlash(__($respuesta['msg'], true),'flash_success');
		}
		//fin de logica
		
		$this->set('estadoAnterior',$estadoAnterior);
		$this->set('estadoPasarA',$estadoPasarA);
		$this->set('ot_numero',$ot_numero);		
	}

	public function salidaUnidad($ot_numero){
		$this->layout='contenido';
		$estadoPasarA=array(22=>'Salida de Unidad'); //estado a cambiar
		$estadoAnterior=array();
		$estadoCCPOT=$this->Ccptabhistorial->obtenerEstadoCcpPotNumeroOt($ot_numero);
		if(!empty($estadoCCPOT) && isset($estadoCCPOT)){
			$estadoAnterior[$estadoCCPOT[0]['Ccptabestado']['id']]=$estadoCCPOT[0]['Ccptabestado']['descripcion'];
		}else $estadoAnterior=array(1=>'Pendiente');
		$this->set('guardado',0);
		
		//logica de grabacion
		//$secperson_id=$this->login('personId');
		$secperson=$this->_getDtLg('personId');
		$secperson_id=$secperson['Secperson']['id'];
		if(!empty($this->request->data) && isset($this->request->data)){
			$estadoInicial=$this->request->data['Ccptabhistorial']['ccptabestado_anterior'];
			$estadoFinal=$this->request->data['Ccptabhistorial']['ccptabestado_id'];
			$comentario=$this->request->data['Ccptabhistorial']['comentario'];
			//$ot_numero=$this->request->data['Ccptabhistorial']['ot_numero'];
			$id_solicitud = $this->Ccptabhistorial->getSolitudServicioId($ot_numero);
			$this->Ccptabhistorial->begin();			
			$respuesta=$this->Ccptabhistorial->cambiarEstado($estadoInicial,$estadoFinal,$ot_numero,$comentario,$secperson_id,$id_solicitud);
			if($respuesta['respuesta']){
				$this->Ccptabhistorial->commit();
				$this->set('guardado',1);
			}else{
				$this->Ccptabhistorial->rollback();
			}
			$this->Session->setFlash(__($respuesta['msg'], true),'flash_success');
		}
		//fin de logica
		
		$this->set('estadoAnterior',$estadoAnterior);
		$this->set('estadoPasarA',$estadoPasarA);
		$this->set('ot_numero',$ot_numero);		
	}
	
	/**
	 * AUTOR: VENTURA RUEDA, JOSE ANTNIO
	 * FECHA: 2013-03-20
	 * @return 
	 */
	function getIndexExel(){
		//BORRAMOS LAS VARIABLES DE SESSION DEL BUSCADOR PRIMER INGRESO
		@$this->setInitSessionConditions();
		
		$estadoCcp="1,2,4,13,21,22,7";//lavado y ccasesor
		$conditions=array();
				
		if(!empty($this->params['named']['placa']) || !empty($this->params['named']['nrocars'])  || !empty($this->params['named']['chasis'])  || !empty($this->params['named']['estadootccp_id']))
		{
			//RECUPERAMOS LAS CONDICIONES DE BUSQUEDA ALMACENADOS EN SESSION
			$bscCnd = $this->getSessionConditions();
			$this->request->data['buscador']['estadootccp_id'] = $this->params['named']['estadootccp_id'];
			$this->request->data['buscador']['placa'] = $this->params['named']['placa'];
			$this->request->data['buscador']['nrocars'] = $this->params['named']['nrocars'];
			$this->request->data['buscador']['chasis'] = $this->params['named']['chasis'];
		}				
		
		//buscamos por el numero OT
		if(!empty($this->request->data['buscador']['nrocars'])){	
			$valorDeBusqueda = isset($this->request->data['buscador']['nrocars'])?trim($this->request->data['buscador']['nrocars']):null;
			$conditions = !empty($valorDeBusqueda)?
							array('Ccptabhistorial.ot_numero LIKE'=>'%'.trim($this->request->data['buscador']['nrocars']).'%'):
							array();	
		}	
		
		//realizamos la busqueda por estado
		if(!empty($this->request->data['buscador']['estadootccp_id'])){			
			//buscador por el numero OT
			$valorDeBusqueda = isset($this->request->data['buscador']['estadootccp_id'])?trim($this->request->data['buscador']['estadootccp_id']):null;
			$conditions = !empty($valorDeBusqueda)?
							array('Ccptabestado.id '=>'%'.trim($this->request->data['buscador']['estadootccp_id']).'%'):
							array();	
		}	
				
		//realizamos la busqueda por la placa			
		if(!empty($this->request->data['buscador']['placa'])){	
			$valorDeBusqueda = isset($this->request->data['buscador']['placa'])?trim($this->request->data['buscador']['placa']):null;
			$condicion = !empty($valorDeBusqueda)?
							array('Talot.placa LIKE'=>'%'.trim($this->request->data['buscador']['placa']).'%'):
							array();	
			$talots = $this->Ccptabhistorial->Ccpsolicitudservicio->Talot->find('all', array(
				'conditions' => $condicion, 
				'fields' => 'Talot.id'
			));
			foreach($talots as $key => $item) {
				$ids_talots[$key] = $item['Talot']['id'];
			}
			$ids_talots = implode(',', $ids_talots);
			$conditions += array('Ccpsolicitudservicio.talot_id IN ('.$ids_talots.')');							
		}

		//realizamos la busqueda por chasis
		if(!empty($this->request->data['buscador']['chasis'])){							
			//buscador por el numero OT
			$valorDeBusqueda = isset($this->request->data['buscador']['chasis'])?trim($this->request->data['buscador']['chasis']):null;
			$condicion = !empty($valorDeBusqueda)?
							array('Talot.chassis LIKE'=>'%'.trim($this->request->data['buscador']['chasis']).'%'):
							array();	
			$talots = $this->Ccptabhistorial->Ccpsolicitudservicio->Talot->find('all', array(
				'conditions' => $condicion, 
				'fields' => 'Talot.id'
			));
			foreach($talots as $key => $item) {
				$ids_talots[$key] = $item['Talot']['id'];
			}
			$ids_talots = implode(',', $ids_talots);
			$conditions += array('Ccpsolicitudservicio.talot_id IN ('.$ids_talots.')');							
		}
		
		//GUARDAMOS LAS CONDICIONES EN SESSION
		@$this->setSessionConditions($this->request->data);		
		
		$ccptabhistoriales=$this->Ccptabhistorial->find('all', array(
								'order' => array ('Ccptabhistorial.id' => 'asc'),
								'conditions' => array($conditions, 'Ccpsolicitudservicio.talot_id !='=>'NULL')
								));		
		
		// se obtiene las solicitudes de los registros obtenidos
		foreach($ccptabhistoriales as $key => $row) {	
			$this->Ccptabhistorial->Ccpsolicitudservicio->Talot->recursive = -1;	
			$talots = $this->Ccptabhistorial->Ccpsolicitudservicio->Talot->find('first', array(
				'conditions' => array('Talot.id' => $row['Ccpsolicitudservicio']['talot_id'])
			));
			if(!empty($talots))
				$ccptabhistoriales[$key]['Talot'] = $talots['Talot'];
		} 
		
		//obtenemos los estados de las ots en ccp
		$estadosCcp=$this->Ccptabestado->getEstados();
		$estadosCcpSelect =(!empty($estadosCcp) && isset($estadosCcp))?array(''=>'Seleccionar')+$estadosCcp:array(''=>'Seleccionar');
				
		$this->set('ccptabhistoriales', $ccptabhistoriales);		
		$this->set('estadosCcpSelect', $estadosCcpSelect);
		$this->set('dt', $this->request->data);
		
		//IMPRESION EN EXEL
		$this->layout = 'modulo_taller'.DS.'excel';
		header('Content-Type: text/html');
		header("Content-Disposition: attachment; filename=Historial_de_Citas.xls");
		
		set_time_limit(1200);
		ini_set('memory_limit', '512M');
		$fecha = date('d/m/Y');
        $this->set('fecha', $fecha);
	}

	public function indexLavador($exportar=null,$parametros=null){
		$data = $this->request->query;
		
		$this->pageTitle = __('CCP_BANDEJA_LAVADOR');
		
		$estadoEliminado = 'EL'; $estadoActivo = 'AC';
		$dtLg = $this->_getDtLg();
		$this->set('dtLog', $dtLg);
		
		//MODELOS CARS UTILIZADO
		$this->loadModel('Ccptiposervicio');
		$this->loadModel('Ccpsolicitudservicioestado');
		$this->loadModel('Ccpsolicitudservicio');
		$this->loadModel('Talot');
		$this->loadModel('Ccptabestado');
		$this->loadModel('Ccplavadoseguimientosestado');
		$this->loadModel('Secproject');
		
		//RECUPERO EL ID DE LA SUCURSAR SEGUN CARS
		$project = $this->Secproject->findById($dtLg['Secproject']['id'], null,null,-1);
		$dtLg['carscod'] = $project['Secproject']['carscod'];
		
		//RECUPERAMOS LOS COMBOS PARA EL BUSCADOR
   		$ccptiposervicios = $this->Ccptiposervicio->find('list');
		$ccpsolicitudservicioestados = $this->Ccpsolicitudservicioestado->find('list');
   		$asesoresCars = $this->Ccptabhistorial->getAsesoresCars($project['Secproject']['carscod']);
		$asesoresCars = empty($asesoresCars)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$asesoresCars;
		$estadoPorPresupuestos = array(''=>__('Seleccionar'),'1'=>'Sin Presupuesto','2'=>'Con Presupuesto','3'=>'Presupuesto listo','4'=>'Presupuesto aprobado');
		$this->set(compact('estadoPorPresupuestos','asesoresCars','ccptiposervicios','ccpsolicitudservicioestados','estadoOtCars'));
		
		if(!empty($exportar) && isset($exportar)){
			$data = $parametros;
		}else $data = $this->request->query;
		
		if(!isset($data['carsestado_id'])
			and !isset($data['chasis']) and !isset($data['nrocars']) 
			and !isset($data['estadootccp_id']) and !isset($data['placa'])){
			$data['carsestado_id'] = 1;
		}
		$this->set('data',$data);
		
//		// RECUPERAMOS DATOS DEL ORDEN, LIMITE, Y PAGINA POR EL PAGINADOR	
		$this->loadModel('Viewccplavadoseguimiento');
		$cnd = $this->Viewccplavadoseguimiento->getConditionsBsc($data, $dtLg);
//		debug($cnd);
		$this->paginate = array(
			'Viewccplavadoseguimiento'=>array(
				'limit' => 15,
				'page' => 1,
				'order' => array ('Viewccplavadoseguimiento.OT' => 'ASC'),
				'conditions' => $cnd
			)
		); 

		$otsCars = empty($exportar)?$this->paginate('Viewccplavadoseguimiento')
			:$this->Viewccplavadoseguimiento->find('all',array('conditions'=>$cnd, 'order'=>array('Viewccplavadoseguimiento.OT' => 'ASC')));
			
		$otsCars = $this->Viewccplavadoseguimiento->getTalotCarsLavador($otsCars);
		$this->set('otsCars',$otsCars);
		$this->listaFinalPDF = isset($otsCars) && !empty($otsCars) ? $otsCars : array();
		
		//obtenemos los estados de las ots de cars
		$estadosOtCars = $this->Datos->_getDato('estadosOtCarsCcp');
		$estadosOtCarsTooltip = $this->Datos->_getDato('estadosOtCars');
		foreach($estadosOtCars as $key => $value) $estadosOtCars_tmp[$key+1] = $value; 
		$estadosOtCarsSelect = empty($estadosOtCars)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$estadosOtCars_tmp;
		
		//obtenemos los estados de las ots en ccp
		$estadosCcp=$this->Ccptabestado->getEstados();
		$estadosCcpSelect =(!empty($estadosCcp) && isset($estadosCcp))?array(''=>__('Seleccionar'))+$estadosCcp:array(''=>__('Seleccionar'));
		
		//AVENTURA: RECUPERAMOS LOS ESTADOS DEL SEGUIMIENTO
		$seguimientoestados = $this->Ccplavadoseguimientosestado->find('list', array(
			'conditions'=>array("Ccplavadoseguimientosestado.estado <> 'DE'"), 
			'order'=>array('Ccplavadoseguimientosestado.id'=>'asc')
		));
		
		$seguimientoestados = empty($seguimientoestados)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar')) + $seguimientoestados;
		$this->set('seguimientoestados',$seguimientoestados);
		
		$this->set('estadosOtCarsSelect',$estadosOtCarsSelect);
		$this->set('estadosOtCars',$estadosOtCars);
		$this->set('estadosCcpSelect',$estadosCcpSelect);
		$this->set('estadosOtCarsTooltip',$estadosOtCarsTooltip);
	}
	
	function indexAsesor($exportar=null,$parametros=null){
		$this->pageTitle = __('CCP_BANDEJA_ASESOR_SERVICIO');
		$estadoCcp="7,22";//lavado y ccasesor
		$boolExportar=false;
		$data = empty($exportar)?$this->request->query
			:$parametros;
		
		if(!isset($data['fechaIni']) and !isset($data['fechaFin']) and !isset($data['asesand_id']) and !isset($data['ccptiposervicio_id'])
			and !isset($data['fechaIniOt']) and !isset($data['fechaFinOt']) and !isset($data['carsestado_id'])
			and !isset($data['ccpsolicitudservicioestado_id']) and !isset($data['preestado_id']) and !isset($data['nrocars']) 
			and !isset($data['estadootccp_id']) and !isset($data['placa'])){
			$data['carsestado_id'] = '';
			$data['estadootccp_id'] = '';
		}
		$this->set('data',$data);
		
		$estadoEliminado = 'EL'; $estadoActivo = 'AC';
		$dtLg = $this->_getDtLg();
		
		//MODELOS CARS UTILIZADO
		$this->loadModel('Ccptiposervicio');
		$this->loadModel('Ccpsolicitudservicioestado');
		$this->loadModel('Ccpsolicitudservicio');
		$this->loadModel('Talot');
		$this->loadModel('Ccptabestado');
		$this->loadModel('Secproject');
		
		//RECUPERO EL ID DE LA SUCURSAR SEGUN CARS
		$project = $this->Secproject->findById($dtLg['Secproject']['id'], null,null,-1);
		$dtLg['Secproject']['carscod'] = $project['Secproject']['carscod'];
		
		// RECUPERAMOS DATOS DEL ORDEN, LIMITE, Y PAGINA POR EL PAGINADOR
		$this->loadModel('SistemaPlanificadorCcp');
		$cnd = $this->SistemaPlanificadorCcp->cndBsc_getTalotCarsNew($data, $dtLg);		
//		debug($cnd);
		$this->paginate = array(
			'SistemaPlanificadorCcp'=>array(
				'limit' => 15,
				'page' => 1,
				'order' => array ('SistemaPlanificadorCcp.FECHA' => 'DESC'),
				'conditions' => $cnd
			)
		); 
		
		$otsCars = empty($exportar)?$this->paginate('SistemaPlanificadorCcp')
			:$this->SistemaPlanificadorCcp->find('all',array('conditions'=>$cnd, 'order'=>array('SistemaPlanificadorCcp.FECHA' => 'DESC')));
		$otsCars = $this->SistemaPlanificadorCcp->getTalotCars($otsCars);
		$this->set('otsCars',$otsCars);
		
		$this->listaFinalPDF = isset($otsCars) && !empty($otsCars) ? $otsCars : array();
		
		//RECUPERAMOS LOS COMBOS PARA EL BUSCADOR
   		$ccptiposervicios = $this->Ccptiposervicio->find('list');
   		$ccptiposervicios = empty($ccptiposervicios)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$ccptiposervicios;
		$ccpsolicitudservicioestados = $this->Ccpsolicitudservicioestado->find('list');
   		$ccpsolicitudservicioestados = empty($ccpsolicitudservicioestados)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$ccpsolicitudservicioestados;
		$asesoresCars = $this->Ccptabhistorial->getAsesoresCars($project['Secproject']['carscod']);
		$asesoresCars = empty($asesoresCars)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$asesoresCars;
		$estadoPorPresupuestos = array(''=>__('Seleccionar'),'1'=>'Sin Presupuesto','2'=>'Con Presupuesto','3'=>'Presupuesto listo','4'=>'Presupuesto aprobado');
		$this->set(compact('estadoPorPresupuestos','asesoresCars','ccptiposervicios','ccpsolicitudservicioestados','estadoOtCars'));
		
		//obtenemos los estados de las ots de cars
		$estadosOtCars = $this->Datos->_getDato('estadosOtCarsCcp');
		$estadosOtCarsTooltip = $this->Datos->_getDato('estadosOtCars'); 
		$estadosOtCarsSelect = empty($estadosOtCars)?array(''=>__('Seleccionar')):array_merge(array(''=>__('Seleccionar')),$estadosOtCars);
		
		//obtenemos los estados de las ots en ccp
		$estadosCcp=$this->Ccptabestado->getEstados();
		$estadosCcpSelect =(!empty($estadosCcp) && isset($estadosCcp))?array(''=>__('Seleccionar'))+$estadosCcp:array(''=>__('Seleccionar'));
		
		$this->set('estadosOtCarsSelect',$estadosOtCarsSelect);
		$this->set('estadosOtCars',$estadosOtCars);
		$this->set('estadosCcpSelect',$estadosCcpSelect);
		$this->set('estadosOtCarsTooltip',$estadosOtCarsTooltip);
	}
	
	public function indexLavadorExportarExcel() {
		$this->layout = 'excel_db';
		
		$this->indexLavador($exportar=1,$parametros=$this->request->query);
		header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=Lavador.xls");
		
		$reporte = $this->listaFinalPDF;
		$this->set('reporte',$reporte);
	}
	
	function indexAsesorExportarExcel(){
		$this->layout = 'excel_db';
		
		$this->indexAsesor($exportar=1,$parametros=$this->request->query);
		header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=Asesor.xls");
		
		$reporte = $this->listaFinalPDF;
		$this->set('reporte',$reporte);		
	}
	
	/**
	 * 
	 * @param object $ot_numero
	 * @return 
	 */
	function pasarLavado($ot_numero){
		$this->set('guardado',0);
		$this->layout = 'aventura';
		
		// MODELOS A UTILIZAR
		$this->loadModel('Talot');
		$this->loadModel('Ccplavadoseguimientomotivo');
		$this->loadModel('Ccplavadoseguimiento');
		
		$segMotivos = $this->Ccplavadoseguimientomotivo->find('list', array(
			'conditions'=>array("Ccplavadoseguimientomotivo.estado <> 'DE'"),
			'order'=>array('Ccplavadoseguimientomotivo.id'=>'asc'),
			'fields'=>array('id','descripcion')
		));   
		
		$segMotivos =(!empty($segMotivos) && isset($segMotivos))?array(''=>__('Seleccionar'))+$segMotivos:array(''=>__('Seleccionar'));
		$this->set('segMotivos',$segMotivos);
		
		//logica de grabacion
		if(!empty($this->request->data) && isset($this->request->data)){
			$this->Ccptabhistorial->begin();			
			$rpt=$this->Ccplavadoseguimiento->setLavar($this->request->data,$this->_getDtLg());
			if($rpt['0']){
				$this->Ccptabhistorial->commit();
				$this->set('guardado',1);
			}else{
				$this->Ccptabhistorial->rollback();
			}
			$this->Session->setFlash($rpt['1']);
		}
		//fin de logica
		$this->set('ot_numero',$ot_numero);
	}
	
	/**
	 * 
	 * @param object $ot_numero
	 * @return 
	 */
	function pasarJefe($seg_id){
		$this->layout = 'aventura';
		$this->set('guardado',0);
		$this->set('seg_id',$seg_id);
		
		// MODELOS A UTILIZAR
		$this->loadModel('Ccplavadoseguimiento');
		$seguimiento = $this->Ccplavadoseguimiento->findById($seg_id);
		
		//logica de grabacion
		if(!empty($this->request->data) && isset($this->request->data)){
			$this->Ccptabhistorial->begin();			
			$rpt=$this->Ccplavadoseguimiento->setJefe($this->request->data,$this->_getDtLg());
			if($rpt['0']){
				$this->Ccptabhistorial->commit();
				$this->set('guardado',1);
			}else{
				$this->Ccptabhistorial->rollback();
			}
			$this->Session->setFlash($rpt['1']);
		}
		//fin de logica
		$this->set('ot_numero',$seguimiento['Ccplavadoseguimiento']['talot_nrocars']);
	}
	
	/**
	 * 
	 * @param object $ot_numero
	 * @return 
	 */
	function pasarCoordinador($seg_id){
		$this->layout = 'aventura';
		$this->set('guardado',0);
		$this->set('seg_id',$seg_id);
		
		// MODELOS A UTILIZAR
		$this->loadModel('Ccplavadoseguimiento');
		$seguimiento = $this->Ccplavadoseguimiento->findById($seg_id);
		
		//logica de grabacion
		if(!empty($this->request->data) && isset($this->request->data)){
			$this->Ccptabhistorial->begin();			
			$rpt=$this->Ccplavadoseguimiento->setCoordinador($this->request->data,$this->_getDtLg());
			if($rpt['0']){
				$this->Ccptabhistorial->commit();
				$this->set('guardado',1);
			}else{
				$this->Ccptabhistorial->rollback();
			}
			$this->Session->setFlash($rpt['1']);
		}
		//fin de logica
		$this->set('ot_numero',$seguimiento['Ccplavadoseguimiento']['talot_nrocars']);
	}
	
	function pasarEntregado($ot_numero){
		$this->layout = 'aventura';
		$estadoPasarA=array(11=>'Entregado'); //estado a cambiar
		$estadoAnterior=array();
		$estadoCCPOT=$this->Ccptabhistorial->obtenerEstadoCcpPotNumeroOt($ot_numero);
		if(!empty($estadoCCPOT) && isset($estadoCCPOT)){
			$estadoAnterior[$estadoCCPOT[0]['Ccptabestado']['id']]=$estadoCCPOT[0]['Ccptabestado']['descripcion'];
		}else $estadoAnterior=array(1=>'Pendiente');
		$this->set('guardado',0);
		
		//logica de grabacion
		$dtLg = $this->_getDtLg();
		$secperson_id= $dtLg['Secperson']['id'];
		if(!empty($this->request->data) && isset($this->request->data)){
			$estadoInicial=$this->request->data['Ccptabhistorial']['ccptabestado_anterior'];
			$estadoFinal=$this->request->data['Ccptabhistorial']['ccptabestado_id'];
			$comentario=$this->request->data['Ccptabhistorial']['comentario'];
			$ot_numero=$this->request->data['Ccptabhistorial']['ot_numero'];
			$id_solicitud = $this->Ccptabhistorial->getSolitudServicioId($ot_numero);
			$this->Ccptabhistorial->begin();			
			$respuesta=$this->Ccptabhistorial->cambiarEstado($estadoInicial,$estadoFinal,$ot_numero,$comentario,$secperson_id,$id_solicitud);
			if($respuesta['respuesta']){
				$this->Ccptabhistorial->commit();
				$this->set('guardado',1);
			}else{
				$this->Ccptabhistorial->rollback();
			}
			$this->Session->setFlash($respuesta['msg']);
		}
		//fin de logica
		
		$this->set('estadoAnterior',$estadoAnterior);
		$this->set('estadoPasarA',$estadoPasarA);
		$this->set('ot_numero',$ot_numero);

	}
	
	function indexGerente($exportar=null,$parametros=null){
		$this->pageTitle = __('CCP_BANDEJA_ASESOR_SERVICIO');
		$estadoCcp="1,2,3,4,5,6,7,9,10,11,12,13,14,15,21,22";//lavado y ccasesor
		$boolExportar=false;
		$data = empty($exportar)?$this->request->query
			:$parametros;
		
		$this->set('data',$data);
		
		$estadoEliminado = 'EL'; $estadoActivo = 'AC';
		$dtLg = $this->_getDtLg();
		
		//MODELOS CARS UTILIZADO
		$this->loadModel('Ccptiposervicio');
		$this->loadModel('Ccpsolicitudservicioestado');
		$this->loadModel('Ccpsolicitudservicio');
		$this->loadModel('Talot');
		$this->loadModel('Ccptabestado');
		$this->loadModel('Secproject');
		
		//RECUPERO EL ID DE LA SUCURSAR SEGUN CARS
		$project = $this->Secproject->findById($dtLg['Secproject']['id'], null,null,-1);
		$dtLg['Secproject']['carscod'] = $project['Secproject']['carscod'];
		
		// RECUPERAMOS DATOS DEL ORDEN, LIMITE, Y PAGINA POR EL PAGINADOR
		$this->loadModel('SistemaPlanificadorCcp');
		$cnd = $this->SistemaPlanificadorCcp->cndBsc_getTalotCarsGerente($data, $dtLg);		
//		debug($cnd);
		$this->paginate = array(
			'SistemaPlanificadorCcp'=>array(
				'limit' => 15,
				'page' => 1,
				'order' => array ('SistemaPlanificadorCcp.FECHA' => 'DESC'),
				'conditions' => $cnd
			)
		); 
		
		$otsCars = empty($exportar)?$this->paginate('SistemaPlanificadorCcp')
			:$this->SistemaPlanificadorCcp->find('all',array('conditions'=>$cnd, 'order'=>array('SistemaPlanificadorCcp.FECHA' => 'DESC')));
		$otsCars = $this->SistemaPlanificadorCcp->getTalotCars($otsCars);
		$this->set('otsCars',$otsCars);
		
		$this->listaFinalPDF = isset($otsCars) && !empty($otsCars) ? $otsCars : array();
		
		//RECUPERAMOS LOS COMBOS PARA EL BUSCADOR
   		$ccptiposervicios = $this->Ccptiposervicio->find('list');
   		$ccptiposervicios = empty($ccptiposervicios)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$ccptiposervicios;
		$ccpsolicitudservicioestados = $this->Ccpsolicitudservicioestado->find('list');
   		$ccpsolicitudservicioestados = empty($ccpsolicitudservicioestados)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$ccpsolicitudservicioestados;
		$asesoresCars = $this->Ccptabhistorial->getAsesoresCars($project['Secproject']['carscod']);
		$asesoresCars = empty($asesoresCars)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$asesoresCars;
		$estadoPorPresupuestos = array(''=>__('Seleccionar'),'1'=>'Sin Presupuesto','2'=>'Con Presupuesto','3'=>'Presupuesto listo','4'=>'Presupuesto aprobado');
		$this->set(compact('estadoPorPresupuestos','asesoresCars','ccptiposervicios','ccpsolicitudservicioestados','estadoOtCars'));
		
		//obtenemos los estados de las ots de cars
		$estadosOtCars = $this->Datos->_getDato('estadosOtCarsCcp');
		$estadosOtCarsTooltip = $this->Datos->_getDato('estadosOtCars'); 
		$estadosOtCarsSelect = empty($estadosOtCars)?array(''=>__('Seleccionar')):array_merge(array(''=>__('Seleccionar')),$estadosOtCars);
		
		//obtenemos los estados de las ots en ccp
		$estadosCcp=$this->Ccptabestado->getEstados();
		$estadosCcpSelect =(!empty($estadosCcp) && isset($estadosCcp))?array(''=>__('Seleccionar'))+$estadosCcp:array(''=>__('Seleccionar'));
		
		$this->set('estadosOtCarsSelect',$estadosOtCarsSelect);
		$this->set('estadosOtCars',$estadosOtCars);
		$this->set('estadosCcpSelect',$estadosCcpSelect);
		$this->set('estadosOtCarsTooltip',$estadosOtCarsTooltip);
	}
	
	function indexGerenteExportarExcel(){
		$this->layout = 'excel_db';
		
		$this->indexGerente($exportar=1,$parametros=$this->request->query);
		header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=BandejaGerente.xls");
		
		$reporte = $this->listaFinalPDF;
		$this->set('reporte',$reporte);		
	}
	
	/**
	 * MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-05-16
	 * @return 
	 */
	public function finalizarAbp($ot_numero){
		$this->layout = 'garantias';
		
		$estadoPasarA=array(6=>'Fin ABP'); //estado a cambiar
		$estadoAnterior=array();
		$estadoCCPOT=$this->Ccptabhistorial->obtenerEstadoCcpPotNumeroOt($ot_numero);
		if(!empty($estadoCCPOT) && isset($estadoCCPOT)){
			$estadoAnterior[$estadoCCPOT[0]['Ccptabestado']['id']]=$estadoCCPOT[0]['Ccptabestado']['descripcion'];
		}else $estadoAnterior=array(1=>'Pendiente');
		$this->set('guardado',0);
		
		//logica de grabacion
		$secperson_id=$this->_getDtLg();
		$secperson_id = $secperson_id['Secperson']['id'];
		
		if(!empty($this->request->data) && isset($this->request->data)){
			$estadoInicial=$this->request->data['Ccptabhistorial']['ccptabestado_anterior'];
			$estadoFinal=$this->request->data['Ccptabhistorial']['ccptabestado_id'];
			$comentario=$this->request->data['Ccptabhistorial']['comentario'];
			$ot_numero=$this->request->data['Ccptabhistorial']['ot_numero'];
			$id_solicitud = $this->Ccptabhistorial->getSolitudServicioId($ot_numero);
			$this->Ccptabhistorial->begin();			
			$respuesta=$this->Ccptabhistorial->cambiarEstado($estadoInicial,$estadoFinal,$ot_numero,$comentario,$secperson_id,$id_solicitud);
			if($respuesta['respuesta']){
				$this->Ccptabhistorial->commit();
				$this->set('guardado',1);
			}else{
				$this->Ccptabhistorial->rollback();
			}
			$this->Session->setFlash($respuesta['msg'],empty($respuesta['respuesta'])?'flash_failure':'flash_success');
		}
		
		$this->set('estadoAnterior',$estadoAnterior);
		$this->set('estadoPasarA',$estadoPasarA);
		$this->set('ot_numero',$ot_numero);
	}
	
	public function pasarControlcalidad($ot_numero,$solicitud_id=null){
		$this->layout = 'garantias';
		
		//verificar si tiene un ampliacion para agregar el estado ampliacion
		$this->loadModel('Ccpsolicitudservicio');
		
		$this->Ccpsolicitudservicio->recursive=-1;
		$condition=array('Ccpsolicitudservicio.ot_numero'=>$ot_numero,
						'Ccpsolicitudservicio.ccpsolicitudservicioestado_id'=>2);
		$solicitudServicio= $this->Ccpsolicitudservicio->find('all',array(
			'conditions'=>$condition,
			'order'=>array('Ccpsolicitudservicio.id'=>'desc')
		));
		$comboAmpliacion=array();
		if(!empty($solicitudServicio) && isset($solicitudServicio)){
			if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']['comentario_ampliacion']) && isset($solicitudServicio[0]['Ccpsolicitudservicio']['comentario_ampliacion'])){
				$comboAmpliacion=array(13=>'Ampliación');
			}
		}
		//fin de combo ampliacion 
		
		$estadoPasarA=array(7=>'Control de Calidad Aprobada',12=>'Reprogramado (CC Rechazado)') + $comboAmpliacion; //estado a cambiar
		$estadoAnterior=array();
		$estadoCCPOT=$this->Ccptabhistorial->obtenerEstadoCcpPotNumeroOt($ot_numero);
		if(!empty($estadoCCPOT) && isset($estadoCCPOT)){
			$estadoAnterior[$estadoCCPOT[0]['Ccptabestado']['id']]=$estadoCCPOT[0]['Ccptabestado']['descripcion'];
		}else $estadoAnterior=array(1=>'Pendiente');
		$this->set('guardado',0);
		
		//logica de grabacion
		$secperson_id=$this->_getDtLg();
		$secperson_id = $secperson_id['Secperson']['id'];
		
		if(!empty($this->request->data) && isset($this->request->data)){
			$estadoInicial=$this->request->data['Ccptabhistorial']['ccptabestado_anterior'];
			$estadoFinal=$this->request->data['Ccptabhistorial']['ccptabestado_id'];
			$comentario=$this->request->data['Ccptabhistorial']['comentario'];
			$ot_numero=$this->request->data['Ccptabhistorial']['ot_numero'];
			$id_solicitud = $this->Ccptabhistorial->getSolitudServicioId($ot_numero);
			$this->Ccptabhistorial->begin();			
			$respuesta=$this->Ccptabhistorial->cambiarEstado($estadoInicial,$estadoFinal,$ot_numero,$comentario,$secperson_id,$id_solicitud);
			if($respuesta['respuesta']){
				$this->Ccptabhistorial->commit();
				$this->set('guardado',1);
			}else{
				$this->Ccptabhistorial->rollback();
			}
			$this->Session->setFlash($respuesta['msg'],empty($respuesta['respuesta'])?'flash_failure':'flash_success');
		}
		//fin de logica
		
		$this->set('estadoAnterior',$estadoAnterior);
		$this->set('estadoPasarA',$estadoPasarA);
		$this->set('ot_numero',$ot_numero);
		$this->set('solicitud',$solicitud_id);
	}
	
	/**MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-05-17
	 * @return 
	 */
	public function indexDocumentos(){
		$this->layout='aventura';
		
		$this->loadModel('Viewccptabhistorialdocumento');
		$this->loadModel('Ccptabhistorialdocumento');
		$this->loadModel('Secproject');
		
		$this->pageTitle = __('CCP_ESTADO_DOCUMENTO');
		$estadoCcp="9,10,22";		//lavado y ccasesor
		$boolExportar=false;
		$estadoEliminado = 'EL'; 
		$estadoActivo = 'AC';
		
		$var_login = $this->_getDtLg();
		$project = $this->Secproject->findById($var_login['Secproject']['id'], null,null,-1);
		
		$data = empty($exportar)?$this->request->query
			:$parametros;

		$this->set('data',$data);
		
		$params = array();
		//RECUPERAMOS LOS COMBOS PARA EL BUSCADOR
		$asesoresCars = $this->Ccptabhistorialdocumento->getAsesorescars($project['Secproject']['carscod']);
		$asesoresCars = empty($asesoresCars)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$asesoresCars;
		$estadoPorDocumento = array(''=>__('Seleccionar'),'1'=>'Entregado File','2'=>'File Observado');
		$this->set(compact('estadoPorDocumento','asesoresCars'));
		
		// RECUPERAMOS DATOS DEL ORDEN, LIMITE, Y PAGINA POR EL PAGINADOR
		$cnd = $this->Ccptabhistorialdocumento->getCndBsc_indexDocumentos($data);
		
		$this->paginate = array(
	        'Viewccptabhistorialdocumento' => array(
	            'limit' => 10,
	            'order' => array('Viewccptabhistorialdocumento' => 'ot_numero'),
				'conditions'=>$cnd
	        )
	    );
		$otsCars = $this->paginate('Viewccptabhistorialdocumento');
		
		// REALIZAMOS LA BUSQUEDA DE LOS DATOS EN EL MODELO
		$this->set('otsCars',$otsCars);
	}
	
	/**DESARROLLADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-05-17
	 * @return 
	 */
	function indexLavadorReporte($exportar=null,$parametros=null){
		//$this->layout = 'aventura';
		$this->pageTitle = __('CCP_BANDEJA_LAVADOR');
		
		set_time_limit(0);
		ini_set('memory_limit', '1024M');
		$estadoEliminado = 'EL'; $estadoActivo = 'AC';

		$data = empty($exportar)?$this->request->query
			:$parametros;
		$this->set('data',$data);
		
		$var_login = $this->_getDtLg();
		$this->set('dtLog', $var_login);
		
		//MODELOS CARS UTILIZADO
		$this->loadModel('Ccptiposervicio');
		$this->loadModel('Ccpsolicitudservicioestado');
		$this->loadModel('Ccpsolicitudservicio');
		$this->loadModel('Talot');
		$this->loadModel('Ccptabestado');
		$this->loadModel('Ccptabhistorial');
		$this->loadModel('Viewccplavadoseguimiento');
		$this->loadModel('Ccplavadoseguimientosestado');
		$this->loadModel('Secproject');
		
		//RECUPERO EL ID DE LA SUCURSAR SEGUN CARS
		$this->Secproject = new Secproject;
		$project = $this->Secproject->findById($var_login['Secproject']['id'], null,null,-1);
		$params['params']['carscod'] = $var_login['carscod'] = $project['Secproject']['carscod'];
		
		$cnd = $this->Viewccplavadoseguimiento->getConditionsBsc($data, $var_login, true);
		
		$this->paginate = array(
			'Viewccplavadoseguimiento'=>array(
				'limit' => 15,
				'page' => 1,
				'order' => array ('Viewccplavadoseguimiento.OT' => 'ASC'),
				'conditions' => $cnd
			)
		); 

		$otsCars = empty($exportar)?$this->paginate('Viewccplavadoseguimiento')
			:$this->Viewccplavadoseguimiento->find('all',array('conditions'=>$cnd, 'order'=>array('Viewccplavadoseguimiento.OT' => 'ASC')));
			
		$otsCars = $this->Viewccplavadoseguimiento->getTalotCarsLavador($otsCars);
		$this->set('otsCars',$otsCars);
		$this->listaFinalPDF = isset($otsCars) && !empty($otsCars) ? $otsCars : array();

		//RECUPERAMOS LOS COMBOS PARA EL BUSCADOR
   		$ccptiposervicios = $this->Ccptiposervicio->find('list');
   		$ccptiposervicios = empty($ccptiposervicios)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$ccptiposervicios;
		$ccpsolicitudservicioestados = $this->Ccpsolicitudservicioestado->find('list');
   		$ccpsolicitudservicioestados = empty($ccpsolicitudservicioestados)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$ccpsolicitudservicioestados;
		$asesoresCars = $this->Ccptabhistorial->getAsesoresCars($project['Secproject']['carscod']);
		$asesoresCars = empty($asesoresCars)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$asesoresCars;
		$estadoPorPresupuestos = array(''=>__('Seleccionar'),'1'=>'Sin Presupuesto','2'=>'Con Presupuesto','3'=>'Presupuesto listo','4'=>'Presupuesto aprobado');
		$this->set(compact('estadoPorPresupuestos','asesoresCars','ccptiposervicios','ccpsolicitudservicioestados','estadoOtCars'));
		
		
		//obtenemos los estados de las ots de cars
		$estadosOtCars = $this->Datos->_getDato('estadosOtCarsCcp');
		$estadosOtCarsTooltip = $this->Datos->_getDato('estadosOtCars'); 
		$estadosOtCarsSelect = empty($estadosOtCars)?array(''=>__('Seleccionar')):array_merge(array(''=>__('Seleccionar')),$estadosOtCars);
		
		//obtenemos los estados de las ots en ccp
		$estadosCcp=$this->Ccptabestado->getEstados();
		$estadosCcpSelect =(!empty($estadosCcp) && isset($estadosCcp))?array(''=>__('Seleccionar'))+$estadosCcp:array(''=>__('Seleccionar'));
		
		//AVENTURA: RECUPERAMOS LOS ESTADOS DEL SEGUIMIENTO
		$this->Ccplavadoseguimientosestado = new Ccplavadoseguimientosestado;
		$seguimientoestados = $this->Ccplavadoseguimientosestado->find('list', array(
			'conditions'=>array("Ccplavadoseguimientosestado.estado <> 'DE'"),
			'order'=>array('Ccplavadoseguimientosestado.id'=>'asc')
		));  
		
		$seguimientoestados = empty($seguimientoestados)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar')) + $seguimientoestados;
		
		$this->set('estadosOtCarsSelect',$estadosOtCarsSelect);
		$this->set('estadosOtCars',$estadosOtCars);
		$this->set('estadosCcpSelect',$estadosCcpSelect);
		$this->set('estadosOtCarsTooltip',$estadosOtCarsTooltip);
		$this->set('seguimientoestados',$seguimientoestados);
	}
	
	/**MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-05-17
	 * @return 
	 */
	public function indexLavadorReporteExel() {
		$this->layout = 'excel_db';
		
		$this->indexLavadorReporte($exportar=1,$parametros=$this->request->query);
		header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=index.xls");
		
		$reporte = $this->listaFinalPDF;
		$this->set('reporte',$reporte);
	}

	/**MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-05-17
	 * @return 
	 */
	public function reporte($exportar=0, $parametros = array()) {
		$this->loadModel('Ccptabestado');
		$this->loadModel('SistemaPlanificadorCcp');
		
		$reportes = array();
		$data = empty($exportar)?$this->request->query
			:$parametros;
		$this->set('data',$data);

		$var_login = $this->_getDtLg();
		$this->set('dtLog', $var_login);
		
		$cnd=$this->SistemaPlanificadorCcp->getReporte_cndBsc($data, $var_login);
	
		$reportes = $this->SistemaPlanificadorCcp->find('all',array(
			'conditions'=>$cnd,
			'limit'=>'1'
		));
		
		$reportes=$this->SistemaPlanificadorCcp->getReporte_complete($reportes);
		
		//LLENAMOS LOS DATO LLENADOS POR DEFECTO
		$this->listaFinalPDF = isset($reportes) && !empty($reportes) ? $reportes : array();
		
		//OBTENEMOS LOS ESTADOS CCP
		$estadosCcp=$this->Ccptabestado->getEstados();
		$estadosCcpSelect =(!empty($estadosCcp) && isset($estadosCcp))?array(''=>__('Seleccionar'))+$estadosCcp:array(''=>__('Seleccionar'));
		$this->set('estadosCcp',$estadosCcp);		
		$this->set('reportes',$reportes);
		$this->set(compact('nrocars','placa','proceso_id','estadoccp_id'));
	}
	
	/**MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-05-17
	 * @return 
	 */
	function reporteExcel(){
		$this->layout = 'excel_db';
		
		$this->reporte($exportar=1,$parametros=$this->request->query);
		header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=ReporteSeguimiento.xls");
		
		$reporte = $this->listaFinalPDF;
		$this->set('reporte',$reporte);	
	}
}