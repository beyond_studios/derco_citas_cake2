<?php
class AyudasController extends AppController {
	public $name = 'Ayudas';
    public $helpers = array('Html', 'Xhtml', 'Form', 'Idioma');
	var $components = array('RequestHandler', 'Pdf');
	
	public function index() {
		$this->layout = "default_02";
		$this->paginate = array(
			'limit' => 10, 
			'page' => 1,
			'order' => array ('Ayuda.id' => 'desc'),
			'conditions'=>array('Ayuda.status'=>'AC')
		);
		
		$this->set('ayudas',$this->paginate('Ayuda'));	
	}
	
	public function add() {
		$this->pageTitle = __('DOCUMENTOS PDF DE AYUDA AL CLIENTE');
		$this->layout='modulo_taller'.DS.'default_grid';
		
		if(!empty($this->request->data)){
			$dt = $this->request->data;
			
			/* GUARDAMOS LOS DATOS ENVIADOS POR EL FORMULARIO */
			$this->Ayuda->begin();
			$dt['Ayuda']['fileName'] = $this->Pdf->subirPdf($dt['Ayuda']['img'], 'pdf/');
			
			if((isset($dt['Ayuda']['img'])) && ($dt['Ayuda']['img'] !='') && !empty($dt['Ayuda']['fileName'])){
				//guardamos el archivo
				$dt['Ayuda']['extension'] = $dt['Ayuda']['img']['type'];
				
				if(!$this->Ayuda->save($dt['Ayuda'])){
					$this->Ayuda->rollback();
					$this->Session->setFlash("El archivo no se pudo guardar, intente nuevamente", 'flash_failure');
				}else{
					$this->Session->write('actualizarPadre', true);
					$this->Ayuda->commit();
					$this->Session->setFlash(__('Archivo guardado'), 'flash_success');
				}
			}else{
				$this->Session->setFlash("VERIFIQUE EL PDF ENVIADO", 'flash_failure');
			}
		}
	}
	
	function delete($ayudaId = null){
		$this->layout = 'modulo_taller'.DS.'default_grid';
		
		if (!$ayudaId) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}else{
			$ayuda['Ayuda']['id'] = $ayudaId;
			$ayuda['Ayuda']['status'] = 'EL';
			
			$this->Ayuda->begin();
			if($this->Ayuda->save($ayuda)){
				$this->Ayuda->commit();
				$this->Session->write('actualizarPadre', true);
				$this->Session->setFlash("SERVICIO ELIMINADO", 'flash_failure');
			}else{
				$this->Ayuda->rollback();
				$this->Session->setFlash("NO SE PUDO ELIMINAR EL SERVICIO", 'flash_failure');
			}
		}
		
		$this->redirect(array('action'=>'index'));
	}
	
	public function view($id){
		$this->layout = 'modulo_taller'.DS.'web_cliente';
		$this->set('bodyClass','backRegister');

		$this->set('id',$id);
		$ayuda = $this->Ayuda->find('first', array(
			'order'=>array('id'=>'desc'),
			'conditions'=>array('id'=>$id,'status'=>'AC'),
			'recursive'=>-1
		));
		
		$this->set('ayuda' ,$ayuda);
	}
}