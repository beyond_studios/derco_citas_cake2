<?php
App::uses('AppController', 'Controller');
/**
 * Asesorservicios Controller
 *
 * @property Asesorservicio $Asesorservicio
 */
class AsesorserviciosController extends AppController {

	public $name = 'Asesorservicios';
	public $components = array('RequestHandler');
	public $helpers = array('Js');
	
	public function beforeFilter() {
      	
		$this -> loadModel('Ageconfiguration');	
		 parent::beforeFilter();
		//$this -> loadModel('Secassign');	
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Asesorservicio->recursive = 0;
		$elementos = array('Secperson.username'=>__('usuario', TRUE),
						   'Secperson.appaterno'=>__('apellidoPaterno', TRUE),
						   'Secperson.apmaterno'=>__('apellidoMaterno', TRUE),
						   'Secperson.firstname'=>__('nombre', TRUE),
						   'Secproject.name'=>__('sucursal', TRUE)					   
						   );
		$this->set('elementos',$elementos);	
		
		// se obtienen los valores del buscador
		if(!empty($this->params['named']['valor']) || !empty($this->params['named']['desactivo']))
		{
			$this->request->data['Buscar']['buscador'] = $this->params['named']['buscador'];
			$this->request->data['Buscar']['valor'] = $this->params['named']['valor'];
			$this->request->data['Buscar']['desactivo'] = $this->params['named']['desactivo'];
		}
		// se establecen las condiciones de busqueda
		$valorDeBusqueda = isset($this->request->data['Buscar']['valor'])?trim($this->request->data['Buscar']['valor']):null;
		$conditions = !empty($valorDeBusqueda)?
						array($this->request->data['Buscar']['buscador'].' LIKE'=>'%'.trim($this->request->data['Buscar']['valor']).'%'):
						array();		
		
		$conditionsActivos = (!empty($this->request->data['Buscar']['desactivo']) == 1) ?
								array('Asesorservicio.status'=>'DE') :
								array('Asesorservicio.status'=>'AC');
		
		$conditions = $conditions + $conditionsActivos;
					
		$this->paginate = array(
			'conditions' => $conditions, 
			'fields' => array(
				'Asesorservicio.id', 
				'Asesorservicio.status', 
				'Asesorservicio.codigo_sap', 
				'Secperson.id', 
				'Secperson.firstname', 
				'Secperson.appaterno', 
				'Secperson.apmaterno', 
				'Secperson.username', 				
				'Secproject.id', 
				'Secproject.secorganization_id', 
				'Secproject.code', 
				'Secproject.name', 
				), 
			'recursive' => 0, 
			'order' => array('Asesorservicio.id' => 'desc'), 
			'limit' => 10, 
			'page' => 1
								);
	
		$asesorservicios=$this->paginate();	

		// se obtiene la organizacion de los registros obtenidos
		foreach($asesorservicios as $key => $row) {
			$this->Asesorservicio->Secproject->Secorganization->recursive = -1;			
			$secorganization = $this->Asesorservicio->Secproject->Secorganization->find('first', array(
				'conditions' => array('Secorganization.id' => $row['Secproject']['secorganization_id']), 
				'fields' => array(
					'Secorganization.id', 
					'Secorganization.code', 
					'Secorganization.name'
				)
			));
			$asesorservicios[$key]['Secorganization'] = $secorganization['Secorganization'];
		}
		foreach ($asesorservicios as $key => $value) {
				$asesorservicios[$key]['Secperson']['apellidoNombre'] = $value['Secperson']['appaterno'].' '.$value['Secperson']['apmaterno'].', '.$value['Secperson']['firstname'];			
		}
	$this->set('asesorservicios', $asesorservicios);
}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	function view($id = null) {
		$this->layout = 'contenido';
		if (!$id) {
			$this->Session->setFlash(__('GENERAL_REGISTRO_NO_VALIDO', true),'flash_failure');
			$this->redirect(array('action'=>'index'));
		}
		$asesorservicio = $this->Asesorservicio->read(null, $id);
		
		$this->Asesorservicio->Secproject->Secorganization->recursive = -1;			
		$secorganization = $this->Asesorservicio->Secproject->Secorganization->find('first',
								array('conditions' => array('Secorganization.id' => $asesorservicio['Secproject']['secorganization_id'])));

		$asesorservicio['Secorganization'] = $secorganization['Secorganization'];		
		$this->set('asesorservicio',$asesorservicio);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'contenido';
		if (!empty($this->request->data)) {
			if(empty($this->request->data['Asesorservicio']['secperson_id']) || empty($this->request->data['Asesorservicio']['secproject_id'])){
				$this->Session->setFlash(__('GENERAL_ERROR_GRABACION'),'flash_failure');
			}else{
				if($this->Asesorservicio->unicSecproject_Secrol($this->request->data)){
					if ($this->Asesorservicio->ValidarCodigo($this->request->data)) {
						$this->Asesorservicio->create();
						if ($this->Asesorservicio->save($this->request->data)) {
							$this->Session->setFlash(__('GENERAL_REGISTRO_AGREGADO'),'flash_success');
							$this->Session->write('actualizarPadre', true);	
							$this->redirect(array('action'=>'view',$this->Asesorservicio->getInsertID()));
						} else {
							$this->Session->setFlash(__('GENERAL_REGISTRO_AGREGADO'),'flash_failure');
						}
					} else {															
							$this->Session->setFlash(__('CODIGOSAPUNICO'),'flash_failure');													
					}
				}else{
					$this->Session->setFlash(__('REGISTRODUPLICADOAGREGAR'),'flash_failure');
				}				
			}
		}
		$secorganizations = array();
		$this->Ageconfiguration->recursive = 0;	
		$secorganization = $this->Ageconfiguration->find('all',array('conditions'=>array('Ageconfiguration.status'=>'AC'),
																 'fields'=>array('Secorganization.name','Secorganization.id')));
		
		foreach($secorganization as $keys=>$value)
		{
			$secorganizations[$value['Secorganization']['id']]=$value['Secorganization']['name'];			
			break;
		}
		
		$this->Asesorservicio->Secproject->recursive = 0;
		$secproject= array();
		$secproject = $this->Asesorservicio->Secproject->find('all',array('conditions'=>array('Secproject.status'=>'AC','Secproject.secorganization_id'=>$value['Secorganization']['id'])));
		foreach($secproject as $keys=>$value)
		{
			$secprojects[$value['Secproject']['id']]=$value['Secproject']['name'];
		}
		//debug($secprojects);
		$this->Ageconfiguration->Secrole->recursive = -1;
		$secroles = $this->Ageconfiguration->find('list',array('fields' => array('secrole_id','secrole_id'),'conditions' => array('status' => 'AC')));

		//obtener todos id de las personas  con la condicion de rol respectivo de ageconfiguraciones
		//$this->Asesorservicio->Secperson->Secassign = -1;
		
		$idpersonas = $this->Asesorservicio->Secperson->Secassign->find('list', array('fields' => array('secperson_id','secperson_id') ,
																  'conditions' => array('status' => 'AC','secrole_id'=>$secroles)));

		$this->Asesorservicio->Secperson->recursive = -1;
		$personas = $this->Asesorservicio->Secperson->find('all', array('fields' => array('id','appaterno','apmaterno','firstname') ,
																  'conditions' => array('status' => 'AC','id'=>$idpersonas),
																  'order' => 'appaterno'));
		$secpeople = array();
		//Forma en un array a las $personas para el combo														  
		foreach($personas as $persona)
		{
			$secpeople[$persona['Secperson']['id']] = $persona['Secperson']['appaterno'].
											' '.$persona['Secperson']['apmaterno'].
											', '.$persona['Secperson']['firstname'];
		}
		
		//se llena los motivos servicios
		$agemotivoservicios = $this->Asesorservicio->Agemotivoservicio->find('list',array('fields'=>array('id','description'),
																							'conditions'=>array('status'=>'AC')));
		$this->set(compact('secorganizations','secpeople','secprojects','agemotivoservicios'));

	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
           
		$this->layout = 'contenido';
		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__('GENERAL_ERROR_GRABACION', true),'flash_failure');
			$this->redirect(array('action'=>'index'));
		}
		if (!empty($this->request->data)) {
			if($this->Asesorservicio->unicSecproject_Secrol($this->request->data)){
				if ($this->Asesorservicio->ValidarCodigo($this->request->data)) {
					if ($this->Asesorservicio->save($this->request->data)) {				
						$this->Session->setFlash(__('GENERAL_REGISTRO_MODIFICADO', true),'flash_success');
						$this->Session->write('actualizarPadre', true);	
						$this->redirect(array('action'=>'view',$id));
					} else {
						$this->Session->setFlash(__('GENERAL_ERROR_GRABACION', true),'flash_failure');
					}
				} else {
					$this->Session->setFlash(__('CODIGOSAPUNICO'),'flash_failure');	
				}
			}else{
				$this->Session->setFlash(__('REGISTRODUPLICADOAGREGAR'),'flash_failure');
			}
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Asesorservicio->read(null, $id);
			$secorganization = $this->Asesorservicio->Secproject->Secorganization->find('first',
								array('conditions' => array('Secorganization.id' => $this->request->data['Secproject']['secorganization_id'])));
			$this->request->data['Asesorservicio']['secorganization_id'] = $secorganization['Secorganization'];  
		}
		
		$this->Ageconfiguration->recursive = 0;	
		$secorganization = $this->Ageconfiguration->find('all',array('conditions'=>array('Ageconfiguration.status'=>'AC'),
																 'fields'=>array('Secorganization.name','Secorganization.id')));
		foreach($secorganization as $keys=>$value)
		{
			$secorganizations[$value['Secorganization']['id']]=$value['Secorganization']['name'];
			break;
		}
		$this->Asesorservicio->Secproject->recursive = 0;
		$secproject = $this->Asesorservicio->Secproject->find('all',array('conditions'=>array('Secproject.status'=>'AC','Secproject.secorganization_id'=>$value['Secorganization']['id'])));
		foreach($secproject as $keys=>$value)
		{
			$secprojects[$value['Secproject']['id']]=$value['Secproject']['name'];
		}
		//debug($secprojects);
		$this->Ageconfiguration->Secrole->recursive = -1;
		$secroles = $this->Ageconfiguration->find('list',array('fields' => array('secrole_id','secrole_id'),'conditions' => array('status' => 'AC')));

		
		//obtener todos id de las personas  con la condicion de rol respectivo de ageconfiguraciones
		//$this->Asesorservicio->Secperson->Secassign = -1;
		$idpersonas = $this->Asesorservicio->Secperson->Secassign->find('list', array('fields' => array('secperson_id','secperson_id') ,
																  'conditions' => array('status' => 'AC','secrole_id'=>$secroles)));

		$this->Asesorservicio->Secperson->recursive = -1;
		$personas = $this->Asesorservicio->Secperson->find('all', array('fields' => array('id','appaterno','apmaterno','firstname') ,
																  'conditions' => array('status' => 'AC','id'=>$idpersonas),
																  'order' => 'appaterno'));

		//Forma en un array a las $personas para el combo														  
		foreach($personas as $persona)
		{
			$secpeople[$persona['Secperson']['id']] = $persona['Secperson']['appaterno'].
											' '.$persona['Secperson']['apmaterno'].
											', '.$persona['Secperson']['firstname'];
		}
		
		//se llena los motivos servicios
		$agemotivoservicios = $this->Asesorservicio->Agemotivoservicio->find('list',array('fields'=>array('id','description'),
																							'conditions'=>array('status'=>'AC')));
		$this->set(compact('secorganizations','secpeople','secprojects','agemotivoservicios'));

	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	function delete($id = null) {
		$estadoEliminado = 'EL';
		$estadoActivo = 'AC';
		if (!$id){
					$this->Session->setFlash(__('GENERAL_REGISTRO_NO_VALIDO', true),'flash_failure');
		}else{
			pr($this->Asesorservicio->Agedetallecita->find());
				$existAgedetallecita = 	$this->Asesorservicio->Agedetallecita->find('count',array('conditions' => array('Asesorservicio.status' =>'AC',
																										'Agedetallecita.asesor_id'=>$id)));
				
				if($existAgedetallecita)
				{
					$this->Session->setFlash(__('GENERALES_REGISTRO_ASOCIADO',true),'flash_failure');
				}else{
					$this->request->data['Asesorservicio']['id']= $id;
					$this->request->data['Asesorservicio']['status']= $estadoEliminado;
					if ($this->Asesorservicio->save($this->request->data['Asesorservicio'])) {
					$this->Session->setFlash(__('GENERAL_REGISTRO_ELIMINADO', true),'flash_success');
					}else{
						$this->Session->setFlash(__('GENERAL_REGISTRO_NO_VALIDO',true),'flash_failure');
					}
				}

			}
		$this->redirect(array( 'action'=>'index'));
	}
	
		function listprojects()
	{
		$this->layout = "ajax";
		Configure::write('debug', '1');
		
		$secprojects = array();
		$projects = $this->Asesorservicio->Secproject->find('all',array(	'fields' => array('Secproject.id','Secproject.name'),
															'conditions' => array( 'Secproject.secorganization_id' => $this->request->data['Asesorservicio']['secorganization_id'])));

		foreach($projects as $value)
			$secprojects[$value['Secproject']['id']] = $value['Secproject']['name'];
		//$secprojects =  $this->Secassign->Secproject->find('list',array('conditions' => array('Secproject.secorganization_id' => $this->request->data['Secorganization']['id'])));
		
		$this->set('secprojects',$secprojects);
	}
	function listrolpersonas()
	{
		
		$this->layout = "ajax";
		Configure::write('debug', '1');
		//obtener ids de roles de  ageconfiguraciones 
		$idsroles = $this->Ageconfiguration->find('list',array('fields' => array('secrole_id','secrole_id'),
																		'conditions' => array('status' => 'AC','secorganization_id'=>$this->request->data['Asesorservicio']['secorganization_id'])));
		//Obtener  ids de las personas  asigano con los roles
		$idspersonas = $this->Asesorservicio->Secperson->Secassign->find('list', array('fields' => array('secperson_id','secperson_id') ,
																  'conditions' => array('status' => 'AC','secrole_id'=>$idsroles)));
																  
		$personas = $this->Asesorservicio->Secperson->find('all', array('fields' => array('id','appaterno','apmaterno','firstname') ,
																  'conditions' => array('status' => 'AC','id'=>$idspersonas),
																  'order' => 'appaterno'));
		//Forma en un array a las $personas para el combo														  
		foreach($personas as $persona)
		{
			$secpeople[$persona['Secperson']['id']] = $persona['Secperson']['appaterno'].
											' '.$persona['Secperson']['apmaterno'].
											', '.$persona['Secperson']['firstname'];
		}			
		$this->set('secpeople',$secpeople);
	}
}
