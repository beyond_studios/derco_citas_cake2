<?php
App::import('Vendor', 'nusoap/web_services_controller');
App::uses('Talot', 'Model');
class ServicioPlanificadorTallerSoapController extends WebServicesController {
	public $name = 'ServicioPlanificadorTallerSoap';
	public $uses = null;
	var $api = array(
		'ZDBM_CREA_OT_TALLER_PERU' => array (
			'doc' 		=> 'Return mensaje about the ot', 
			'input' 	=> array('Sociedad'					=> 'xsd:string',
						        'FechaOT' 					=> 'xsd:string',
								'HoraOT' 					=> 'xsd:string',
								'FechaCita' 				=> 'xsd:string',
								'HoraCita' 					=> 'xsd:string',
								'NroOT' 					=> 'xsd:string',
								'DetalleSolicitud' 			=> 'xsd:string',
								'TipoCliente' 				=> 'xsd:string',
								'NombreCliente' 			=> 'xsd:string',
								'IDClienteSAP' 				=> 'xsd:string',
								'TipoDocFiscal' 			=> 'xsd:string',
								'NroDocFiscal' 				=> 'xsd:string',
								'CodAsesor' 				=> 'xsd:string',
								'NumCono' 					=> 'xsd:string',
								'FechaPromEnt' 				=> 'xsd:string',
								'HoraPromEnt' 				=> 'xsd:string',
								'CentroCod' 				=> 'xsd:string',
								'Placa' 					=> 'xsd:string',
								'IDUnidad' 					=> 'xsd:string',
								'NroPresupuesto' 			=> 'xsd:string',
								'FechaIniPresupuesto' 		=> 'xsd:string', 
								'FechaFinPresupuesto' 		=> 'xsd:string', 
								'ClasePedido'			 	=> 'xsd:string',
								'MotivoPedido' 				=> 'xsd:string',
								'Chasis' 					=> 'xsd:string',
								'EstadoOT' 					=> 'xsd:string',
								'EstadoUnidad' 				=> 'xsd:string',
								'UbicacionUnidad' 			=> 'xsd:string',
								'Marca' 					=> 'xsd:string',
								'Modelo' 					=> 'xsd:string',
								'Color' 					=> 'xsd:string',
								'Concesionario'			 	=> 'xsd:string',
								'FechaTrasladoDeposito' 	=> 'xsd:string',
								'FechaRecepcionPDI' 		=> 'xsd:string',
								'UtilizacionVehiculo' 		=> 'xsd:string',
								'CodSAPCiaAseguradora' 		=> 'xsd:string'),
			'output' 	=> array('ESTADO' 	=> 'xsd:string', 'MENSAJE' 	=> 'xsd:string' )
		)
	);
	

	/**
	 * CREA la ot en Apoyo
	 * @return mensaje OK NOOK
	 * @author Ronald Tineo Santamaria
	 * @version 0.1 2013-02-14 11:35
	 */
	function ZDBM_CREA_OT_TALLER_PERU($p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,
									$p11,$p12,$p13,$p14,$p15,$p16,$p17,$p18,$p19,$p20,
									$p21,$p22,$p23,$p24,$p25,$p26,$p27,$p28,$p29,$p30,
									$p31,$p32,$p33,$p34,$p35,$p36) {
		$response = array('ESTADO'=>'NOOK','MENSAJE'=>'','HOST'=>'');
		try{
			$this->Talot = new Talot;
			$params = $this->convertParamsToArray($p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,
									$p11,$p12,$p13,$p14,$p15,$p16,$p17,$p18,$p19,$p20,
									$p21,$p22,$p23,$p24,$p25,$p26,$p27,$p28,$p29,$p30,
									$p31,$p32,$p33,$p34,$p35,$p36);
			$response = $this->Talot->ZDBM_CREA_OT_TALLER_PERU($params);
		}catch(Exception $e){
			$response = array('ESTADO'=>'NOOK','MENSAJE'=>$e->getMessage());
		}
		//registramos el log en el servidor
		$registerLog = Configure::read('logWsCreaOT');
		if($registerLog){
			try{
				$filename = $_SERVER['DOCUMENT_ROOT'].DS.'logs'.DS.'webservice'.DS.date('Ymd').'.txt';
				$date = date('Y-m-d H:i:s');
				$fp = fopen($filename,"a");
				fwrite($fp, "$date\tOT:$p6\tEstado:".$response['ESTADO']."\tHost: ".$response['HOST']."\t"."Mensaje:".$response['MENSAJE'].PHP_EOL);
				fclose($fp);
			}catch(Exception $ex){}
		}

		return $response;
	}
	
	/*CONVIERTE LOS DATOS EN PARAMETROS UNITARIOS*/
	function convertParamsToArray($p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,
									$p11,$p12,$p13,$p14,$p15,$p16,$p17,$p18,$p19,$p20,
									$p21,$p22,$p23,$p24,$p25,$p26,$p27,$p28,$p29,$p30,
									$p31,$p32,$p33,$p34,$p35,$p36){
		$array = array('Sociedad'=>$p1,'FechaOT'=>$p2,'HoraOT'=>$p3,'FechaCita'=>$p4,'HoraCita'=>$p5,
						'NroOT'=>$p6,'DetalleSolicitud'=>$p7,'TipoCliente'=>$p8,'NombreCliente'=>$p9,
						'IDClienteSAP'=>$p10,'TipoDocFiscal'=>$p11,'NroDocFiscal'=>$p12,'CodAsesor'=>$p13,
						'NumCono'=>$p14,'FechaPromEnt'=>$p15,'HoraPromEnt'=>$p16,'CentroCod'=>$p17,
						'Placa'=>$p18,'IDUnidad'=>$p19,'NroPresupuesto'=>$p20,'FechaIniPresupuesto'=>$p21, 
						'FechaFinPresupuesto'=>$p22,'ClasePedido'=>$p23,'MotivoPedido'=>$p24,'Chasis'=>$p25,
						'EstadoOT'=>$p26,'EstadoUnidad'=>$p27,'UbicacionUnidad'=>$p28,'Marca'=>$p29,
						'Modelo'=>$p30,'Color'=>$p31,'Concesionario'=>$p32,'FechaTrasladoDeposito'=>$p33,
						'FechaRecepcionPDI'=>$p34,'UtilizacionVehiculo'=>$p35,'CodSAPCiaAseguradora'=>$p36);
		return $array;
	}
}