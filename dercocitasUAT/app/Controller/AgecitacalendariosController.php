<?php
App::uses('AppController', 'Controller');
class AgecitacalendariosController extends AppController {

	public $name = 'Agecitacalendarios';
	public $helpers = array('Html', 'Form');
	
    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow();
    }
	
	public function index(){
		
		$elementos = array('Secorganization.name'=>__('organizacion', TRUE),
						   'Secproject.name'=>__('Sucursal', TRUE),
						   'Agemotivoservicio.description'=>__('Motivo de Servixio', TRUE),
						   'Agegrupo.portal'=>__('Grupos', TRUE),
						   'Agecitacalendario.maximo_citaprogramado'=>__('Nro de citas programadas', TRUE)
						   );
		$this->set('elementos',$elementos);		
		
		if(!empty($this->params['named']['valor']) || !empty($this->params['named']['desactivo']))
		{
			$this->request->data['Buscar']['buscador'] = $this->params['named']['buscador'];
			$this->request->data['Buscar']['valor'] = $this->params['named']['valor'];
			$this->request->data['Buscar']['desactivo'] = $this->params['named']['desactivo'];
		}
		
		$valorDeBusqueda = isset($this->request->data['Buscar']['valor'])?trim($this->request->data['Buscar']['valor']):null;
		$conditions = !empty($valorDeBusqueda)?
						array($this->request->data['Buscar']['buscador'].' LIKE'=>'%'.trim($this->request->data['Buscar']['valor']).'%'):
						array();		
		
		$conditionsActivos = (!empty($this->request->data['Buscar']['desactivo']) == 1) ?//segun la documentacion no requeire que esten activos
								array('Agecitacalendario.status'=>'DE') :
								array('Agecitacalendario.status'=>'AC');
		
		$conditions = $conditions + $conditionsActivos;	
		
		$this->paginate = array('limit' => 10,
								'page' => 1,
								'order' => array ('Agegrupo.description' => 'asc'),
								'conditions' => $conditions
								);
		
		$agecitacalendarios=$this->paginate('Agecitacalendario');
		
		// se obtiene la organizacion de los registros obtenidos
		foreach($agecitacalendarios as $key => $row) {	
			$this->Agecitacalendario->Secproject->Secorganization->recursive = -1;	
			$organizations = $this->Agecitacalendario->Secproject->Secorganization->find('first', array(
				'conditions' => array('Secorganization.status' =>'AC','Secorganization.id' => $row['Secproject']['secorganization_id'])
			));
			$agecitacalendarios[$key]['Secorganization'] = $organizations['Secorganization'];
		}
		
		// se obtiene el calendario de los registros obtenidos
		foreach($agecitacalendarios as $key => $row) {	
			$this->Agecitacalendario->Agecitacalendariodia->recursive = -1;	
			$agecitacalendariodias = $this->Agecitacalendario->Agecitacalendariodia->getAnio($row['Agecitacalendario']['id']);
			if(!empty($agecitacalendariodias))
				$agecitacalendarios[$key]['Agecitacalendariodia'] = $agecitacalendariodias['0']['0'];
		}
		$this->set('agecitacalendarios',$agecitacalendarios);		
		$fechaActualDB = $this->Agecitacalendario->Agecitacalendariodia->getFecha();
		$fechaActualDB=$fechaActualDB['0']['0']['fecha'];
		$anioActual = date('Y', strtotime($fechaActualDB));
		$mesActual = date('n', strtotime($fechaActualDB));
		$this->set('anioActual',$anioActual);
		$this->set('mesActual',$mesActual);
		
		$años = $this->Agecitacalendario->fechaHoraActual();
		list($anio,$mes,$dia) = explode('-', substr($años,0,10));
		$this->set('anio',$anio);
		$this->set('mes',$mes);
	}

	
	

	/**
     * Elimina una marca.
     * Reglas: 
     * 1. Una plantilla puede ser eliminada solo si no tiene cast_times activos o desactivos
	 * autor:CHIUYARI VERAMENDI, MIGUEL ANGEL
	 * @param string $id : id de la plantilla que se desea eliminar
     */
	public function delete($id=null) {
		$estadoEliminado = 'EL';
		$estadoActivo = 'AC';
		if (!$id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}else{
			//Si existen registros asociados no se peuden eliminar---Miguel Chiuyari
			$existAgecitacalendario = 	$this->Agecitacalendario->Agecitacalendariodia->find('count', array('conditions' => array('Agecitacalendariodia.estado' => 'AC', 'Agecitacalendariodia.agecitacalendario_id'=>$id)));
			if($existAgecitacalendario)
				{
				$this->Session->setFlash(__('GENERALES_REGISTRO_ASOCIADO',true),'flash_failure');				
				}
				else{					
					$this->request->data['Agecitacalendario']['id'] = $id;
					$this->request->data['Agecitacalendario']['status'] = $estadoEliminado;
					if ($this->Agecitacalendario->save($this->request->data['Agecitacalendario'])) {
						$this->Session->setFlash(__('GENERAL_REGISTRO_ELIMINADO', true),'flash_success');	
					} else {
						$this->Session->setFlash(__('GENERAL_REGISTRO_ACTIVADO', true),'flash_failure');
					}
				}
			}
			$this->redirect(array('action'=>'index'));
	}
	
	
	/** ############################################################################
	 * ###################### ACCIONES MODIFICADAS #################################
	 * ########## AUTOR: VENTURA RUEDA, JOSE ANTONIO ###############################
	 * */
	
	/** ENVIA A LA VISTA LOS DATOS BASICOS UTILIZADAS PARA LAS ACCIONES DEL CONTROLADOR
	 *  AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * @return 
	 */    
	function cargarDatos() {
		$this->loadModel('Secproject');
		$this->loadModel('Agemotivoservicio');
		$this->loadModel('Agegrupo');
		
		$this->set('secprojects',$this->Secproject->obtenerListaTalleres());
		$this->set('agemotivoservicios',$this->Agemotivoservicio->obtenerListaServicios(array('Agemotivoservicio.status'=>'AC')));
		$this->set('agegrupos',$this->Agegrupo->obtenerListaGrupos(array('Agegrupo.status'=>'AC')));	
	}
	
	/**Generación de calendarios x Sucursal - Motivo de Servicio
	 * AUTOR: VENTURA RUEDA, JOSE ANTONIO 
	 * @return 
	 */    
	function add($citacalendarioId = 0){
		$this->set('citacalendarioId', $citacalendarioId);
		
		// configuraciones de la pagina
		$this->pageTitle = __('TALLER_CITACALENDAR_AGREGAR', true);
		$this->layout = 'modulo_taller'.DS.'default_grid';
		
		if ($this->request->is('post')) {
			$this->Agecitacalendario->begin();
			$rpt = $this->Agecitacalendario->setCitaCalendario($this->request->data, $this->_getDtLg());
			
			if($rpt[0]){
				$this->Agecitacalendario->commit();
				$this->Session->write('actualizarPadre',true);	
				$this->redirect(array('action'=>'view',$rpt['id']));
			}else{
				$this->Agecitacalendario->rollback();
			}
			
			$this->Session->setFlash(__($rpt['1'], true));
		}
		
		$this->cargarDatos();
	}
	
	/**
     * Mostrar Marca.
	 * @param string $id : id del calendario que se desea mostrar
     */    
	public function view($id=true) {
		$this->pageTitle = __('TALLER_CITACALENDAR_MOSTRAR', true);
		$this->layout = 'modulo_taller'.DS.'default_grid';
		
		$this->loadModel('Secorganization');
		
		// obteniendo los datos del calnedario
        $this->Agecitacalendario->recursive = 1;
		$calendar = $this->Agecitacalendario->read(null, $id);
		
		// obteniendo la empresa a la que pertenece la sucursal
		$organizationName = $this->Secorganization->obtenerName($calendar['Secproject']['secorganization_id']);

		$calendar['Agecitacalendario']['status'] = $this->Agecitacalendario->stdBasic[$calendar['Agecitacalendario']['status']];
		$this->data = $calendar;
		$this->set('organizationName', $organizationName);		
		$this->set('calendar', $calendar);	
	}
	
	/**
     * Esta función permite modificar un calendario.
     */	
	 function edit($citacalendarioId = 0){
		$this->set('citacalendarioId', $citacalendarioId);
		
		// configuraciones de la pagina
		$this->pageTitle = __('TALLER_CITACALENDAR_MODIFICAR', true);
		$this->layout = 'modulo_taller'.DS.'default_grid';
		
		if ($this->request->is('post')) {
			$this->Agecitacalendario->begin();
			$rpt = $this->Agecitacalendario->setCitaCalendario($this->request->data, $this->_getDtLg());
	
			if($rpt[0]){
				$this->Agecitacalendario->commit();
				$this->Session->write('actualizarPadre',true);	
				$this->redirect(array('action'=>'view',$rpt['id']));
			}else{
				$this->Agecitacalendario->rollback();
			}
			
			$this->Session->setFlash(__($rpt['1'], true));
		}
		
		if($this->request->is('get')){
			$this->Agecitacalendario->recursive = 1;
			$this->request->data = $this->Agecitacalendario->read(null, $citacalendarioId);
		}
		
		$this->cargarDatos();
	    $this->set('status', $this->Agecitacalendario->stdBasic);
	}	 
		
	function actualizarNumeroCitas(){
		$this->layout = 'modulo_taller'.DS.'default_grid';
		$this->loadModel('Secproject');
		$this->loadModel('Agemotivoservicio');
		$this->loadModel('Agecitacalendariodia');
		$this->loadModel('Agegrupo');
		$projects=$this->Secproject->find('list',array('conditions'=>array('Secproject.status'=>'AC')));
		$grupos=$this->Agegrupo->find('list',array('conditions'=>array('Agegrupo.status'=>'AC')));
		$servicios=$this->Agemotivoservicio->find('list',array('conditions'=>array('Agemotivoservicio.status'=>'AC')));

		$this->set('projects',$projects);
		$this->set('grupos',$grupos);
		$this->set('servicios',$servicios);
		
		/*if(!empty($secprojectId) && !empty($servicioId) && !empty($talgrupoId) && !empty($fechaEntrada) && !empty($fechaSalida)) {
			$this->request->data['Secproject']['id']=$secprojectId;
			$this->request->data['Agecitacalendario']['fechaDesde']=$this->Agecitacalendario->configurarFechaDMY($fechaEntrada);
			$this->request->data['Agecitacalendario']['fechaHasta']=$this->Agecitacalendario->configurarFechaDMY($fechaSalida);
			$this->obtenerServicio($this->request->data['Secproject']['id']);
			$this->request->data['Agecitacalendario']['id']=$servicioId;
			$this->request->data['Agegrupo']['id']=$talgrupoId;
			$secproject=$secprojectId;
			$agecitacalendarioId=$servicioId;
			$fechaIN = $fechaEntrada;
		}*/		
		debug($this->request->data);
		if(!empty($this->request->data)){
			$this->obtenerGrupo($this->request->data['Buscador']['secproject_id']);
			$this->obtenerServicio($this->request->data['Buscador']['agegrupo_id'],$this->request->data['Buscador']['secproject_id']);
			$secproject=$this->request->data['Buscador']['secproject_id'];
			$fechaIN=$this->Agecitacalendario->configurarFechaYMD($this->request->data['Buscador']['fechaInicial']);
			$fechaOUT=$this->Agecitacalendario->configurarFechaYMD($this->request->data['Buscador']['fechaFinal']);
			$secprojectId=$this->request->data['Buscador']['secproject_id'];
			$agecitacalendarioId=(!empty($this->request->data['Buscador']['agemotivoservicio_id'])&& isset($this->request->data['Buscador']['agemotivoservicio_id']))?$this->request->data['Buscador']['agemotivoservicio_id']:'';
			$agegrupoId=(!empty($this->request->data['Buscador']['agegrupo_id'])&& isset($this->request->data['Buscador']['agegrupo_id']))?$this->request->data['Buscador']['agegrupo_id']:'';
		}else{
			$agecitacalendarioId=0;
			$fechaIN=$this->Agecitacalendario->configurarFechaDMY($this->Agecitacalendario->fechaHoraActual());
			$fechaOUT=$this->Agecitacalendario->configurarFechaDMY($this->Agecitacalendario->fechaHoraActual());
			$this->request->data['Buscador']['fechaInicial']=$fechaIN;
			$this->request->data['Buscador']['fechaFinal']=$fechaOUT;
			$secproject=0;
			$agegrupoId=0;
		}
		$condicion=array('agecitacalendario_id'=>$agecitacalendarioId,'initDateTime'=>'>='.$fechaIN,
						'endDateTime'=>'<='.$fechaOUT,'estado'=>'AC');
		$agecitacalendariodias = $this->Agecitacalendariodia->find('all',array('conditions'=>array($condicion),'order'=>'initDateTime ASC'));
		
		debug($agecitacalendariodias);
		$this->set('agecitacalendariodias',$agecitacalendariodias);
		$this->set('fechaIN',$fechaIN);
		$this->set('fechaOUT',$fechaOUT);
		$this->set('agecitacalendarioId',$agecitacalendarioId);
		$this->set('secproject',$secproject);
		$this->set('agegrupoId',$agegrupoId);
	}

	function obtenerServicio($idGrupo=null,$idSucursal=null){
		$servicios=$this->Agecitacalendario->obtenerTodosServicioPorGrupo($idGrupo,$idSucursal);
		$this->set('servicios',$servicios);
	}
	
	function obtenerGrupo($idSucursal=null){
		$grupos=$this->Agecitacalendario->obtenerTodosGruposPorSucursal($idSucursal);
		//$servicios=array(''=>'Seleccione');
		$this->set('grupos',$grupos);
		//$this->set('servicios',$servicios);
	}	
}	
?>