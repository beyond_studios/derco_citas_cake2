<?php
class AppcastsController extends AppController {

	public $name = 'Appcasts';
	public $helpers = array('Html', 'Form');
	
    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow();
    }

     /*
       Lista las plantillas (casts).
       Reglas: 
       1. Solo se muestran aquellas plantillas cuyo estado sea diferente de 'EL' (eliminado)
     */    
	function index()
        {
           //$this->pageTitle = $this->titulo('CAST_LISTADO');
           //$this->layout='inicio';
		          
		$estadoEliminado = 'EL';
		//pr($this->etiqueta('BUSCADOR_BUSCAR_POR'));
		$elementos = array('Appcast.description'=>__('Descripcion',true));
        
		$this->set('elementos',$elementos);	

        if(!empty($this->params['named']['valor']) || !empty($this->params['named']['desactivo']))
		{
			$this->request->data['Buscar']['buscador'] = $this->params['named']['buscador'];
			$this->request->data['Buscar']['valor'] = $this->params['named']['valor'];
			$this->request->data['Buscar']['desactivo'] = $this->params['named']['desactivo'];
		}
		
		$valorDeBusqueda = isset($this->request->data['Buscar']['valor'])?trim($this->request->data['Buscar']['valor']):null;
		$conditions = !empty($valorDeBusqueda)?
						array($this->request->data['Buscar']['buscador'].' LIKE'=>'%'.trim($this->request->data['Buscar']['valor']).'%'):
						array();
							
		
		$conditionsActivos = (!empty($this->request->data['Buscar']['desactivo']) == 1) ?
								array('Appcast.status'=>'DE') :
								array('Appcast.status'=>'AC');
		$conditions = $conditions + $conditionsActivos;
		$this->paginate = array('limit' => 10,
								'page' => 1,
								'order' => array ('Appcast.description' => 'asc'),
								'conditions' => $conditions
								);

		$casts = $this->paginate('Appcast');       
		$this->set('casts', $casts);

	}

	function indexplantilla()
        {
           //$this->pageTitle = $this->titulo('CAST_LISTADO');
		$this->layout='contenido';
		$estadoEliminado = 'EL';
		//pr($this->etiqueta('BUSCADOR_BUSCAR_POR'));
		$elementos = array('Appcast.description'=>__('Descripcion',true));
        
		$this->set('elementos',$elementos);	

        if(!empty($this->params['named']['valor']) || !empty($this->params['named']['desactivo']))
		{
			$this->request->data['Buscar']['buscador'] = $this->params['named']['buscador'];
			$this->request->data['Buscar']['valor'] = $this->params['named']['valor'];
			$this->request->data['Buscar']['desactivo'] = $this->params['named']['desactivo'];
		}
		
		$valorDeBusqueda = isset($this->request->data['Buscar']['valor'])?trim($this->request->data['Buscar']['valor']):null;
		$conditions = !empty($valorDeBusqueda)?
						array($this->request->data['Buscar']['buscador'].' LIKE'=>'%'.trim($this->request->data['Buscar']['valor']).'%'):
						array();
							
		
		$conditionsActivos = (!empty($this->request->data['Buscar']['desactivo']) == 1) ?
								array('Appcast.status'=>'DE') :
								array('Appcast.status'=>'AC');
		$conditions = $conditions + $conditionsActivos;
		$this->paginate = array('limit' => 10,
								'page' => 1,
								'order' => array ('Appcast.description' => 'asc'),
								'conditions' => $conditions
								);

		$casts = $this->paginate('Appcast');       
		$this->set('casts', $casts);

	}

	/**
     * Permite ingresar una nueva plantilla.
     */    
	function add() 
	{
        //$this->pageTitle = $this->titulo('CAST_AGREGAR');
		$this->layout = 'contenido';
		if (!empty($this->request->data)) 
		{
			$this->Appcast->create();
			if ($this->Appcast->save($this->request->data))
            {
				$this->Session->setFlash(__('plantillaGuardado', true),'flash_success');
				$this->Session->write('actualizarPadre', true);	
			} 
            else 
            {
				$this->Session->setFlash(__('plantillaNoGuardado', true),'flash_failure');
			}
		}
		//pr($this->data['Appcast']);
	}

	/**
     * Esta función permite modificar los datos de una plantilla.
	 Reglas: 
	 * 1. Una plantilla puede ser desactivado solo si no tiene cast_time activos
	 * @param string $id :  id del cast que se desea modificar
     */	
	 function edit($id=true) 
        {
	 	$this->layout = 'contenido';
		if (!$id && empty($this->request->data)) {
			$this->Session->setflash(__('rolNoValido', true),'flash_failure');
			$this->redirect(array('action'=>'index'));
		}
      //$this->pageTitle = $this->titulo('CAST_MODIFICAR');
        $estadoActivo = 'AC';
		$estadoDesactivo = 'DE';

		
		if (empty($this->request->data['Appcast'])) {
				$this->request->data = $this->Appcast->read(null, $id);
		} else {
            $id = $this->request->data['Appcast']['id'];
			$nuevoEstado = $this->request->data['Appcast']['status'];
            
        // si el estado es desactivo
		if ($nuevoEstado == $estadoDesactivo) {
			//verificamos que no tenga cast_time activos
			$this->Appcast->recursive=2;
			$castTimes = $this->Appcast->Appcasttime->find('all',array('conditions'=>array('Appcasttime.appcast_id' => $id,
														'Appcast.status' => $estadoActivo)));
			//pr($castTimes);exit();
            if(!empty($castTimes)) {
				$this->Session->setFlash(__('noesposibleEditar', true),'flash_failure');
				return;        
			}   
        }

		if ($this->Appcast->save($this->request->data['Appcast'])) {			
				$this->Session->setFlash(__('GENERAL_REGISTRO_MODIFICADO', true),'flash_success');
				$this->Session->write('actualizarPadre', true);
				//$this->Session->write($this->redirect(array('action'=>'view',$this->request->data['Appcast']['id'])));	
				
		} else {
				$this->Session->setFlash(__('plantillaNoGuardado', true),'flash_failure');
        }
	}
}

	/**
     * Elimina una plantilla.
     * Reglas: 
     * 1. Una plantilla puede ser eliminada solo si no tiene cast_times activos o desactivos
	 * @param string $id : id de la plantilla que se desea eliminar
     */
	function delete($id=null) {
		$estadoEliminado = 'DE';
		$estadoActivo = 'AC';
		if (!$id) {
			$this->Session->setFlash(__('plantillaNoValido', true),'flash_failure');
		}else{
			//verificamos si tiene horario asignado
			$castTimes = $this->Appcast->Appcasttime->find('all',array('conditions'=>array('Appcasttime.appcast_id'=>$id,
																'Appcast.status'=>$estadoActivo)));
			if(!empty($castTimes)) {
				$this->Session->setFlash(__('noesposibleEditar', true),'flash_failure');
			}else{
				$this->request->data['Appcast']['id'] = $id;
				$this->request->data['Appcast']['status'] = $estadoEliminado;
				if ($this->Appcast->save($this->request->data['Appcast'])) {
					$this->Session->setFlash(__('plantillaDesactivado', true),'flash_success');	
				} else {
					$this->Session->setFlash(__('plantillaNoDesactivado', true),'flash_failure');
				}
			}
			$this->redirect(array('action'=>'index'));
		}
	}

	
	/**
     * Mostrar calendario.
	 * @param string $id : id del calendario que se desea mostrar
     */    
	function view($id=true) 
	{
		$this->layout = 'contenido';
		if (!$id) 
		{
			$this->Session->setFlash(__('plantillaGuardado',true),'flash_failure');
			$this->redirect(array('action'=>'index'));
		}
		$this->set('appcast', $this->Appcast->read(null, $id));
	}
    
}
?>
