<?php
App::import('Vendor', 'datos');
class WebservicessapsController extends AppController {
    public $name = 'Webservicessaps';
	public $components = array('Soapsap');
	public $defaultInterface = "";
	public $defaultInterfaceNew = "";
    // public $proxy = array('host'=>'10.31.1.19','port'=>3128);
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->defaultInterface = Configure::read('InterfaceSap');
		$this->defaultInterfaceNew = Configure::read('InterfaceSapNew');
    }
	
	public function getUrlConsultaClientePlaca(){
		$datos = new Datos();
		$interfaces = $datos->getDatos('interfacesSap');
		$url = $interfaces[$this->defaultInterface]['consultaCliente'];
		if(!empty($url) && isset($url)){
			return $url;
		}else{
			return false;
		}
	}
	
	public function getProxy(){
		if(!empty($this->proxy) && isset($this->proxy)){
			return $this->proxy;
		}else{
			array();
		} 
	}

	public function getConsultaClientePlaca(){
	/*
	$data = array(
	    'Estado' => 1,
	    'CLIENTE' => Array
	        (
	            'IdCliente' => '0000015282',
	           	'Nombres' => 'Erick Jose Manuel Caceres Gonzales',
	            'Apellidos' =>'',
	            'Direccion' => 'Puerto de palos',
	            'Distrito' => 'Lima',
	            'Ciudad' => 'Surco',
	            'Telefono' => '989231950',
	            'Correo' => 'ecaceresg@gmail.com',
	            'TipoCliente' => '10',
	            'TipoDoc' => 'DN',
	            'NumeroDoc' => '43434049',
	        ),
	
	    'VEHICULO' => Array
	        (
	            'CodUnidad' => 'asdasdsaf',
	            'Placa' =>'dw1233',
	            'Marca' => 'Suzuki',
	            'Modelo' => 'dsfdsf',
	        ),
        );
	return $data;*/
	
	/** ONLY TEST - 01 **/
	 $this->params['documento'] = 'DN';
	 $this->params['numeroDocumento'] = '43195177';
	 $this->params['placa'] = 'QW1234';
//	 debug($this->params); // die;
	/** ONLY TEST - 01 END **/
	
		$tipoDocumento = $this->params['documento'];
		$numeroDocumento = $this->params['numeroDocumento'];
		$placa = $this->params['placa'];
		$data = array();
		if($tipoDocumento && $numeroDocumento && $placa){
			$url = $this->getUrlConsultaClientePlaca();
			$soapAction = "http://sap.com/xi/WebService/soap1.1";
			$body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:derco-com-pe:piperiferico:dbm:ConsultaClientePlaca">
					   <soapenv:Header/>
					   <soapenv:Body>
					      <urn:mt_ConsultaCliente_req>
					         <!--Optional:-->
					         <TipoDoc>'.$tipoDocumento.'</TipoDoc>
					         <!--Optional:-->
					         <NumeroDoc>'.$numeroDocumento.'</NumeroDoc>
					         <!--Optional:-->
					         <Placa>'.$placa.'</Placa>
					      </urn:mt_ConsultaCliente_req>
					   </soapenv:Body>
					</soapenv:Envelope>';
			
//			var_dump($url);
//			var_dump($soapAction);
//			var_dump($body);
					
			$data = $this->Soapsap->client($url,$soapAction,$body);
			
//			var_dump($data); die;
			if(!empty($data['faultcode']) && isset($data['faultcode'])){
				$data['Servidor'] = $this->defaultInterface;
			}else{
				$data = $this->utf8_encode_array($data);
			}
		}
		
//		var_dump($data); 
//		var_dump("hola mundo");
//		die;
		return $data;
	}
	
	public function getUrlConsultaClientePlacaNew(){
		$datos = new Datos();
		$interfaces = $datos->getDatos('interfacesSapNew');
		$url = $interfaces[$this->defaultInterfaceNew]['consultaCliente'];
		if(!empty($url) && isset($url)){
			return $url;
		}else{
			return false;
		}
	}
	
	public function getUrlConsultaVehiculoPlacaNew(){
		$datos = new Datos();
		$interfaces = $datos->getDatos('interfacesSapNew');
		$url = $interfaces[$this->defaultInterfaceNew]['consultaVehiculo'];
		if(!empty($url) && isset($url)){
			return $url;
		}else{
			return false;
		}
	}
	
	//2017-001
	public function getConsultaClientePlacaNew(){
		$tipoDocumento = $this->params['documento'];
		$numeroDocumento = $this->params['numeroDocumento'];
		$placa = $this->params['placa'];
		$dataCliente = array();
		$dataVehiculo = array();
		$dataReturn = array();
		$dataReturn['Estado']='2';
		if($tipoDocumento && $numeroDocumento && $placa){
			$url = $this->getUrlConsultaClientePlacaNew();
			$soapAction = "http://sap.com/xi/WebService/soap1.1";
			$body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:derco-com-pe:piperiferico:dbm:CitaWeb:Clientes">
					   <soapenv:Header/>
					   <soapenv:Body>
					      <urn:ClientesSend>
					         <IdCliente></IdCliente>
					         <Nombres></Nombres>
					         <Apellidos></Apellidos>
					         <Direccion></Direccion>
					         <Distrito></Distrito>
					         <Ciudad></Ciudad>
					         <Telefono></Telefono>
					         <Correo></Correo>
					         <TipoCliente></TipoCliente>
					         <TipoDoc>'.$tipoDocumento.'</TipoDoc>
					         <NumDoc>'.$numeroDocumento.'</NumDoc>
					         <Placa>'.$placa.'</Placa>
					         <Marca></Marca>
					         <TipoDocVeh></TipoDocVeh>
					         <NumDocVeh></NumDocVeh>
					         <Recepcionista></Recepcionista>
					         <Motivo_Ped></Motivo_Ped>
					         <Paquete></Paquete>
					      </urn:ClientesSend>
					   </soapenv:Body>
					</soapenv:Envelope>';
			$credenciales = $this->getUsuarioPasswordNew('Clientes');
			$this->log('getUsuarioPasswordNew: '.$credenciales,'debug');
			if(!empty($credenciales['usuario']) && isset($credenciales['usuario'])){
				$this->log('empezo el envio de datos por web service: ','debug');
				$usuario = $credenciales['usuario'];
				$password = $credenciales['password'];
				$dataCliente = $this->Soapsap->clientNew($url,$soapAction,$body,$usuario,$password);
				if(!empty($dataCliente['faultcode']) && isset($dataCliente['faultcode'])){
					$dataCliente['Servidor'] = $this->defaultInterfaceNew;
					$dataCliente['Estado'] = 'NOOK';
				}else{
					$dataCliente = $this->utf8_encode_array($dataCliente);
					$idCliente = $dataCliente['CLIENTE']['IDCLIENTE'];
					//llamamos al servicio web de vehiculos
					if(!empty($idCliente) && isset($idCliente)){
						$dataReturn['Estado']='1';
						$body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:derco-com-pe:piperiferico:dbm:CitaWeb:Vehiculos">
							   <soapenv:Header/>
							   <soapenv:Body>
							      <urn:VehiculosSend>
							         <IDCliente>'.$idCliente.'</IDCliente>
							         <Placa>'.$placa.'</Placa>
							      </urn:VehiculosSend>
							   </soapenv:Body>
							</soapenv:Envelope>';
						$credenciales = $this->getUsuarioPasswordNew('Vehiculos');
						$url = $this->getUrlConsultaVehiculoPlacaNew();
						if(!empty($credenciales['usuario']) && isset($credenciales['usuario'])){
							$usuario = $credenciales['usuario'];
							$password = $credenciales['password'];
							$dataVehiculo = $this->Soapsap->clientNew($url,$soapAction,$body,$usuario,$password);
							if(!empty($dataVehiculo['faultcode']) && isset($dataVehiculo['faultcode'])){
								$dataVehiculo['Servidor'] = $this->defaultInterfaceNew;
								$dataVehiculo['Estado'] = 'NOOK';
							}else{
								$dataVehiculo = $this->utf8_encode_array($dataVehiculo);
								$idVehiculo = $dataVehiculo['Cod_Unidad'];
								if(!empty($idVehiculo) && isset($idVehiculo)){
									$dataReturn['Estado']='0';
								}
							}
						}else{
							$dataVehiculo['faultcode'] = 'Error de usuario y contrasenia';
							$dataVehiculo['Servidor'] = $this->defaultInterfaceNew;
							$dataVehiculo['Estado'] = 'NOOK';
						}
					}
				}
			}else{
				$dataCliente['faultcode'] = 'Error de usuario y contrasenia';
				$dataCliente['Servidor'] = $this->defaultInterfaceNew;
				$dataCliente['Estado'] = 'NOOK';
			}				

		}
		$dataReturn['CLIENTE'] = $this->getClienteLastFormat($dataCliente);
		$dataReturn['VEHICULO'] = $this->getVehiculoLastFormat($dataVehiculo);
		$this->log('Datos a retornar: '. Debugger::dump($dataReturn),'debug');
		return $dataReturn;
	}
	
	public function getClienteLastFormat($dataClienteWebservice){
		$return = array();
		$return['IdCliente'] = $dataClienteWebservice['CLIENTE']['IDCLIENTE'];
		$return['Nombres'] = $dataClienteWebservice['CLIENTE']['NOMBRES'];
		$return['Apellidos'] = $dataClienteWebservice['CLIENTE']['NOMBRES'];
		$return['Direccion'] = $dataClienteWebservice['CLIENTE']['DIRECCION'];
		$return['Distrito'] = $dataClienteWebservice['CLIENTE']['DISTRITO'];
		$return['Ciudad'] = $dataClienteWebservice['CLIENTE']['CIUDAD'];
		$return['Telefono'] = $dataClienteWebservice['CLIENTE']['TELEFONO'];
		$return['Correo'] = $dataClienteWebservice['CLIENTE']['CORREO'];
		$return['TipoCliente'] = $dataClienteWebservice['CLIENTE']['TIPOCLIENTE'];
		$return['TipoDoc'] = $dataClienteWebservice['CLIENTE']['TIPODOC'];
		$return['NumeroDoc'] = $dataClienteWebservice['CLIENTE']['NUMDOC'];
		return $return;
	}
	
	public function getVehiculoLastFormat($dataVehiculoWebservice){
		$return = array();
		$return['CodUnidad'] = $dataVehiculoWebservice['Cod_Unidad'];
		$return['Placa'] = $dataVehiculoWebservice['Placa'];
		$return['Marca'] = $dataVehiculoWebservice['Marca'];
		$return['Modelo'] = $dataVehiculoWebservice['Modelo'];
		return $return;
	}
	
	public function utf8_encode_array($array){
		$array_local = $array;
		if(is_array($array)){
			foreach($array as $id=>$item){
				if(is_array($item)){
					$array_local[$id] = $this->utf8_encode_array($item);
				}else{
					$array_local[$id] = utf8_encode($item);
				}
			}
		}else{
			$array_local = utf8_encode($array);
		}
		return $array_local;
	}
	
	public function getUrlAgendamientoCitas(){
		$datos = new Datos();
		$interfaces = $datos->getDatos('interfacesSap');
		$url = $interfaces[$this->defaultInterface]['agendamientoCitas'];
		if(!empty($url) && isset($url)){
			return $url;
		}else{
			return false;
		}
	}
	
	public function getAgendamientoCitas(){
		//pr($this->params); exit();
		app::import('Model', 'Secproject');	$this->Secproject = new Secproject();
		app::import('Model', 'Cliente');	$this->Cliente = new Cliente();
		if($this->params['Cliente']['cliente_estado']!='PE'){
			$datosCliente = $this->params['Cliente'];
		}else{
			$datosCliente = $this->Cliente->getClienteDummy();
			$datosCliente = $datosCliente['Cliente'];
		}
		$tipoDocumento = $datosCliente['documento_tipo'];
		$numeroDocumento = $datosCliente['documento_numero'];
		$idCliente = $datosCliente['codigo_sap'];
		$nombres = $datosCliente['nombres'];
		$apellidos = "";
		if(!empty($datosCliente['apellidoPaterno'])){
			$apellidos = $datosCliente['apellidoPaterno'];
		}
		if(!empty($datosCliente['apellidoMaterno'])){
			if(!empty($apellidos)) $apellidos = $apellidos.' '.$datosCliente['apellidoMaterno'];
			else $apellidos = $datosCliente['apellidoMaterno'];
		}
		$direccion = $datosCliente['direccion'];
		$distrito = $datosCliente['distrito'];
		$ciudad = $datosCliente['ciudad'];
		$telefono = $datosCliente['telefono'];
		$email = $datosCliente['email'];
		$clienteTipo = $datosCliente['cliente_tipo'];
		
		$textoServicio = $this->params['Agetiposervicio']['description'];
		$requerimiento = 'Cita';
		if(!empty($this->params['Agedetallecita']['otrosServicios'])){
			$requerimiento = $this->params['Agedetallecita']['otrosServicios'];
		}
		$paquete = 'Reparación';
		if(!empty($this->params['Agetipomantenimiento']['id']) && isset($this->params['Agetipomantenimiento']['id'])){
			$paquete = $this->params['Agetipomantenimiento']['description'];
		}

		$placa = $this->params['Agedetallecita']['placa'];
		$marca = $this->params['Agedetallecita']['marca'];
		
		$motivoPedido =$this->params['Agetiposervicio']['codigo_sap']; 
		$recepcionista = $this->params['Asesorservicio']['codigo_sap'];
		
		//obtenemos la empresa y sucursal
		$empresaSucursal = $this->Secproject->findById($this->params['Asesorservicio']['secproject_id']);
		$empresa = $empresaSucursal['Secorganization']['code'];
		$sucursal = $empresaSucursal['Secproject']['code'];

		$fechaCita = $this->getFechaHoraSeparados($this->params['Agecitacalendariodia']['initDateTime']);
		$fecha = $fechaCita['fecha'];
		$hora = $fechaCita['hora'];
		
		$data = array();
		if($tipoDocumento && $numeroDocumento && $placa){
			$url = $this->getUrlAgendamientoCitas();
			$soapAction = "http://sap.com/xi/WebService/soap1.1";
			$body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:derco-com-pe:piperiferico:dbm:CitaWeb">
					   <soapenv:Header/>
					   <soapenv:Body>
					      <urn:mt_CitaWeb_req>
					         <BUSCAR>
					            <TipoDoc>'.$tipoDocumento.'</TipoDoc>
					            <NumeroDoc>'.$numeroDocumento.'</NumeroDoc>
					            <Placa>'.$placa.'</Placa>
					            <Marca>'.$marca.'</Marca>
					         </BUSCAR>
					         <CLIENTE>
					            <IdCliente>'.$idCliente.'</IdCliente>
					            <Nombres>'.$nombres.'</Nombres>
					            <Apellidos>'.$apellidos.'</Apellidos>
					            <Direccion>'.$direccion.'</Direccion>
					            <Distrito>'.$distrito.'</Distrito>
					            <Ciudad>'.$ciudad.'</Ciudad>
					            <Telefono>'.$telefono.'</Telefono>
					            <Correo>'.$email.'</Correo>
					            <TipoCliente>'.$clienteTipo.'</TipoCliente>
					            <TipoDoc>'.$tipoDocumento.'</TipoDoc>
					            <NumDoc>'.$numeroDocumento.'</NumDoc>
					         </CLIENTE>
					         <VEHICULO>
					            <Paquete>'.$paquete.'</Paquete>
					            <TipoOT>2000</TipoOT>
					            <MotivoPed>'.$motivoPedido.'</MotivoPed>
					            <TextoServicio>'.$textoServicio.'</TextoServicio>
					            <Requerimiento>'.$requerimiento.'</Requerimiento>
					            <Fecha>'.$fecha.'</Fecha>
					            <Hora>'.$hora.'</Hora>
					            <Taller>'.$sucursal.'</Taller>
					            <Sociedad>'.$empresa.'</Sociedad>
					            <Recepcionista>'.$recepcionista.'</Recepcionista>
					            <Usuario>CALUSER</Usuario>
					         </VEHICULO>
					      </urn:mt_CitaWeb_req>
					   </soapenv:Body>
					</soapenv:Envelope>';
		
			$data = $this->Soapsap->client($url,$soapAction,$body);
			if(!empty($data['faultcode']) && isset($data['faultcode'])){
				$data['Servidor'] = $this->defaultInterface;
			}else{
				$data = $this->utf8_encode_array($data);
			}
			$data['Tramaxml'] = $body;
		}
		return $data;
	}
	
	public function getUrlAgendamientoCitasNew(){
		$datos = new Datos();
		$interfaces = $datos->getDatos('interfacesSapNew');
		$url = $interfaces[$this->defaultInterfaceNew]['agendamientoCitas'];
		if(!empty($url) && isset($url)){
			return $url;
		}else{
			return false;
		}
	}
	
	public function getUsuarioPasswordNew($interface){
		$datos = new Datos();
		$credenciales = array();
		$credenciales = $datos->getDatos('usuariosSapNew');
		$this->log('$credenciales: '.$credenciales,'debug');
		$usuario = $credenciales['usuario'.$interface];
		$password = $credenciales['password'.$interface];
		$credencialesReturn['usuario']=$usuario;
		$credencialesReturn['password']=$password;
		$this->log('$usuario: '.$usuario,'debug');
		$this->log('$usuario: '.$password,'debug');
		if(!empty($usuario) && isset($usuario) && !empty($password) && isset($password)){
			return $credencialesReturn;
		}else{
			return false;
		}
	}
	
	public function getAgendamientoCitasNew(){
		$this->log('Entro en el metodo getAgendamientoCitasNew','debug');
		//pr($this->params); exit();
		app::import('Model', 'Secproject');	$this->Secproject = new Secproject();
		app::import('Model', 'Cliente');	$this->Cliente = new Cliente();
		$timeout = 60;
		if($this->params['Cliente']['cliente_estado']!='PE'){
			$datosCliente = $this->params['Cliente'];
		}else{
			$datosCliente = $this->Cliente->getClienteDummy();
			$datosCliente = $datosCliente['Cliente'];
		}
		$tipoDocumento = $datosCliente['documento_tipo'];
		$numeroDocumento = $datosCliente['documento_numero'];
		$idCliente = $datosCliente['codigo_sap'];
		$nombres = $datosCliente['nombres'];
		$apellidos = "";
		if(!empty($datosCliente['apellidoPaterno'])){
			$apellidos = $datosCliente['apellidoPaterno'];
		}
		if(!empty($datosCliente['apellidoMaterno'])){
			if(!empty($apellidos)) $apellidos = $apellidos.' '.$datosCliente['apellidoMaterno'];
			else $apellidos = $datosCliente['apellidoMaterno'];
		}
		$direccion = $datosCliente['direccion'];
		$distrito = $datosCliente['distrito'];
		$ciudad = $datosCliente['ciudad'];
		$telefono = $datosCliente['telefono'];
		$email = $datosCliente['email'];
		$clienteTipo = $datosCliente['cliente_tipo'];
	
		$textoServicio = $this->params['Agetiposervicio']['description'];
		$requerimiento = 'Cita';
		if(!empty($this->params['Agedetallecita']['otrosServicios'])){
			$requerimiento = $this->params['Agedetallecita']['otrosServicios'];
		}
		$paquete = 'Reparación';
		if(!empty($this->params['Agetipomantenimiento']['id']) && isset($this->params['Agetipomantenimiento']['id'])){
			$paquete = $this->params['Agetipomantenimiento']['description'];
		}
	
		$placa = $this->params['Agedetallecita']['placa'];
		$marca = $this->params['Agedetallecita']['marca'];
	
		$motivoPedido =$this->params['Agetiposervicio']['codigo_sap'];
		$recepcionista = $this->params['Asesorservicio']['codigo_sap'];
	
		//obtenemos la empresa y sucursal
		$empresaSucursal = $this->Secproject->findById($this->params['Asesorservicio']['secproject_id']);
		$empresa = $empresaSucursal['Secorganization']['code'];
		$sucursal = $empresaSucursal['Secproject']['code'];
	
		$fechaCita = $this->getFechaHoraSeparados($this->params['Agecitacalendariodia']['initDateTime']);
		$fecha = $fechaCita['fecha'];
		$hora = $fechaCita['hora'];
	
		$data = array();
		if($tipoDocumento && $numeroDocumento && $placa){
			$url = $this->getUrlAgendamientoCitasNew();
			$this->log('getUrlAgendamientoCitasNew: '.$url,'debug');
			$soapAction = "http://sap.com/xi/WebService/soap1.1";
			$body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:derco-com-pe:piperiferico:dbm:CitaWeb:CreaOrdenTrabajo">
					   <soapenv:Header/>
					   <soapenv:Body>
					      <urn:OrdenTrabajoSend>
					         <IDCliente>'.$idCliente.'</IDCliente>
					         <Placa>'.$placa.'</Placa>
					         <Marca>'.$marca.'</Marca>
					         <Paquete>'.$paquete.'</Paquete>
					         <TipoOT>2000</TipoOT>
					         <MotivoPed>'.$motivoPedido.'</MotivoPed>
					         <TextoServicio>'.$textoServicio.'</TextoServicio>
					         <Requerimiento>'.$requerimiento.'</Requerimiento>
					         <Fecha>'.$fecha.'</Fecha>
					         <Hora>'.$hora.'</Hora>
					         <Taller>'.$sucursal.'</Taller>
					         <Sociedad>'.$empresa.'</Sociedad>
					         <Recepcionista>'.$recepcionista.'</Recepcionista>
					         <Usuario>CALUSER</Usuario>
					      </urn:OrdenTrabajoSend>
					   </soapenv:Body>
					</soapenv:Envelope>';
			$credenciales = $this->getUsuarioPasswordNew('Citas');
			$this->log('getUsuarioPasswordNew: '.$credenciales,'debug');
			if(!empty($credenciales['usuario']) && isset($credenciales['usuario'])){
				$this->log('empezo el envio de datos por web service: ','debug');
				$usuario = $credenciales['usuario'];
				$password = $credenciales['password'];
				$data = $this->Soapsap->clientNew($url,$soapAction,$body,$usuario,$password,$timeout);
				if(!empty($data['faultcode']) && isset($data['faultcode'])){
					$data['Servidor'] = $this->defaultInterface;
				}else{
					$data = $this->utf8_encode_array($data);
				}
				$data['Tramaxml'] = $body;
			}else{
				$data['faultcode'] = 'No se ingreso el usuario y contrasenia';
				$data['Servidor'] = $this->defaultInterfaceNew;
				$data['Estado'] = 'NOOK';
			}
		}
		return $data;
	}
	
	public function getUrlPlanificadorTaller(){
		$url = 'http://10.110.20.16:93/ServicioPlanificadorTaller.asmx?WSDL';
		if(!empty($url) && isset($url)){
			return $url;
		}else{
			return false;
		}
	}
	
	public function getPlanificadorTaller(){
		$tipoDocumento = $this->params['documento'];
		$numeroDocumento = $this->params['numeroDocumento'];
		$placa = $this->params['placa'];
		$data = array();
		//if($tipoDocumento && $numeroDocumento && $placa){
			$url = $this->getUrlPlanificadorTaller();
			$soapAction = "http://dercoperu.net/webservices/ZDBM_CREA_OT_TALLER_PERU";
			$body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://dercoperu.net/webservices">
					   <soapenv:Header/>
					   <soapenv:Body>
					   </soapenv:Body>
					</soapenv:Envelope>';
			$data = $this->Soapsap->client($url,$soapAction,$body);
		//}
		return $data;
	}
	
	function getFechaHoraSeparados($fecha){
		$fechaPhp = date('Y-m-d', strtotime($fecha));
		$horaPhp = date('H:i:s', strtotime($fecha));
		return array('fecha'=>$fechaPhp,'hora' =>$horaPhp);
	}
}?>